﻿using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;

[ExecuteInEditMode]
public class QuickBuild : EditorWindow
{

    [MenuItem("File/Save All")]
    public static void QuickSave()
    {
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.SaveAssets();
    }

    [MenuItem("File/Save And Exit")]
    public static void SaveExit()
    {
        EditorSceneManager.SaveOpenScenes();
        AssetDatabase.SaveAssets();
        EditorApplication.Exit(0);
    }

    [MenuItem("File/Quick Build Win64")]
    static void PerformWinBuild()
    {
        // Closes a running build if there is one
        if (System.Diagnostics.Process.GetProcessesByName("Win64" + GetProjectName()) != null)
        {
            foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("Win64" + GetProjectName()))
            {
                p.Kill();
            }
        }
        if (!EditorApplication.isCompiling)
        {
            Debug.Log("Building windows 64");

            // Change player settings for quick testing
            PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.Disabled;
            PlayerSettings.defaultIsFullScreen = false;
            PlayerSettings.defaultIsNativeResolution = false;
            PlayerSettings.resizableWindow = false;
            PlayerSettings.d3d9FullscreenMode = D3D9FullscreenMode.FullscreenWindow;
            PlayerSettings.d3d11FullscreenMode = D3D11FullscreenMode.FullscreenWindow;
            PlayerSettings.visibleInBackground = true;
            PlayerSettings.defaultScreenHeight = 432;
            PlayerSettings.defaultScreenWidth = 768;


            // Save scene and project
            EditorSceneManager.SaveOpenScenes();
            AssetDatabase.SaveAssets();

            // Make a build to the build folder in the project
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.StandaloneWindows64);
            BuildPipeline.BuildPlayer(GetScenePaths(), @"Build/Win64" + GetProjectName() + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.AutoRunPlayer);

            // Change player setting to default
            PlayerSettings.displayResolutionDialog = ResolutionDialogSetting.Enabled;
            PlayerSettings.defaultIsFullScreen = true;
            PlayerSettings.defaultIsNativeResolution = true;
            PlayerSettings.resizableWindow = false;
            PlayerSettings.d3d9FullscreenMode = D3D9FullscreenMode.FullscreenWindow;
            PlayerSettings.d3d11FullscreenMode = D3D11FullscreenMode.FullscreenWindow;
            PlayerSettings.visibleInBackground = true;
            PlayerSettings.captureSingleScreen = true;
        }
        else
        {
            Debug.Log("Compiling... try building again");

        }
    }

    [MenuItem("File/Quick Build Android")]
    static void PerformAndroidBuild()
    {
        if (!EditorApplication.isCompiling)
        {
            Debug.Log("Building Android APK");

            // Save scene and project
            EditorSceneManager.SaveOpenScenes();
            AssetDatabase.SaveAssets();

            // Find path of project to make a build folder in
            string path = Application.dataPath;
            int index = path.Length - 6;
            path = path.Remove(index);

            // Make a build to the build folder in the project
            if (!Directory.Exists(path + "Build"))
                Directory.CreateDirectory(path + "/Build");

            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
            BuildPipeline.BuildPlayer(GetScenePaths(), @"Build/" + GetProjectName() + ".apk", BuildTarget.Android, BuildOptions.None);
        }
        else
            Debug.Log("Compiling... try building again");
    }

    [MenuItem("File/Quick Build Webplayer")]
    static void PerformWebBuild()
    {
        if (!EditorApplication.isCompiling)
        {
            Debug.Log("Building WebPlayer");

            EditorSceneManager.SaveOpenScenes();
            AssetDatabase.SaveAssets();
            EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.WebPlayer);
            BuildPipeline.BuildPlayer(GetScenePaths(), @"Build/Web" + GetProjectName(), BuildTarget.WebPlayer, BuildOptions.None);
            EditorUtility.RevealInFinder("Build/Web" + GetProjectName());

            //			for(int i = 0; i < 1; i++)
            //				System.IO.File.Delete(System.IO.Directory.GetFiles (@"Build/Web" + GetProjectName (), ".pdb")[i]);

        }
        else
            Debug.Log("Compiling... try building again");
    }

    static string GetProjectName()
    {
        string[] s = Application.dataPath.Split('/');
        return s[s.Length - 2];
    }

    static string[] GetScenePaths()
    {
        string[] scenes = new string[EditorBuildSettings.scenes.Length];

        for (int i = 0; i < scenes.Length; i++)
        {
            scenes[i] = EditorBuildSettings.scenes[i].path;
        }

        return scenes;
    }

    // Settings window
    //public static QuickBuild window;
    //int resWidth = 768;
    //int resHeight = 432;

    //public static bool IsOpen
    //{
    //    get { return window != null; }
    //}

    //[MenuItem("Window/Set Quick Windows Build Res")]
    //public static void SetDefaultResolution()
    //{
    //    window = (QuickBuild)GetWindow(typeof(QuickBuild));
    //    window.Show();
    //}

    //void OnGUI()
    //{
    //    if (!EditorApplication.isPlaying)
    //    {
    //        GUILayout.Label("Resolution", EditorStyles.boldLabel);

    //        string width = GUILayout.TextField("Width; ");
    //        GUILayout.Space(10);
    //        string height = GUILayout.TextField("Height; ");

    //        if (width != "")
    //            resWidth = int.Parse(width);
    //        if (height != "")
    //            resHeight = int.Parse(height);
    //    }
    //}
}
