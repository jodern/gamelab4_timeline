﻿using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class CardScaling : EditorWindow
{
    public static CardScaling window;
    public static bool IsOpen
    {
        get { return window != null; }
    }

    bool defaultSet = false;

    float TimelineScale;
    float PlayerScale;
    float OtherPlayerScale;
    float NeutralScale;

    float DefTimelineScale;
    float DefPlayerScale;
    float DefOtherPlayerScale;
    float DefNeutralScale;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Scale Tool")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        window = (CardScaling)GetWindow(typeof(CardScaling));
        window.Show();
    }

    void OnEnable()
    {
        defaultSet = false;
        SetDefault();
    }

    void OnGUI()
    {
        if(!EditorApplication.isPlaying)
        {
            GUILayout.Label("Scale settings", EditorStyles.boldLabel);

            if (defaultSet)
            {
                SetTimelineScale();
                GUILayout.Space(10);
                SetPlayerScale();
                GUILayout.Space(10);
                SetOtherPlayerScale();
                GUILayout.Space(10);
                SetNeutralScale();
                GUILayout.Space(20);
            }

            if (GUILayout.Button("Reset scales to default"))
            {
                Reset();
            }
        }
    }

    void SetDefault()
    {
        if (!defaultSet)
        {
            // Timeline scale
            TimelineController timelineController;

            if (FindObjectOfType<TimelineController>())
            {
                timelineController = FindObjectOfType<TimelineController>();
                DefTimelineScale = timelineController.CardsScale.x;
                TimelineScale = DefTimelineScale;
            }

            // Neutral deck scale
            GameController gameController;

            if (FindObjectOfType<GameController>())
            {
                gameController = FindObjectOfType<GameController>();
                DefNeutralScale = gameController.neutralDeckScale.x;
                NeutralScale = DefNeutralScale;
            }

            // Other players
            PlayerController[] otherPlayers;

            if (FindObjectsOfType<PlayerController>().Length != 0)
            {
                otherPlayers = FindObjectsOfType<PlayerController>();

                foreach (PlayerController p in otherPlayers)
                {
                    if (p.name != "Player0")
                    {
                        DefOtherPlayerScale = p.originalCardScale.x;
                        OtherPlayerScale = DefOtherPlayerScale;
                    }
                }
            }

            // Local player
            PlayerController player;

            try
            {
                player = GameObject.Find("Player0").GetComponent<PlayerController>();
                DefPlayerScale = player.originalCardScale.x;
                PlayerScale = DefPlayerScale;
            }
            catch
            {

            }
            Debug.Log("Default set");
            defaultSet = true;
        }
    }

    void SetTimelineScale()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Timeline scale");
        EditorGUILayout.TextField("", TimelineScale.ToString(), GUILayout.MaxWidth(200));
        TimelineScale = EditorGUILayout.Slider(TimelineScale, -3, 3, GUILayout.MaxWidth(240));
        GUILayout.EndHorizontal();

        TimelineController controller;

        if (FindObjectOfType<TimelineController>())
        {
            controller = FindObjectOfType<TimelineController>();
            controller.CardsScale = Vector3.one * TimelineScale;
        }
    }

    void SetPlayerScale()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Player scale");
        EditorGUILayout.TextField("", PlayerScale.ToString(), GUILayout.MaxWidth(200));
        PlayerScale = EditorGUILayout.Slider(PlayerScale, -3, 3, GUILayout.MaxWidth(240));
        GUILayout.EndHorizontal();

        PlayerController controller;

        try
        {
            controller = GameObject.Find("Player0").GetComponent<PlayerController>();
            controller.originalCardScale = Vector3.one * PlayerScale;
        }
        catch
        {

        }
    }

    void SetOtherPlayerScale()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Other Player scale");
        EditorGUILayout.TextField("", OtherPlayerScale.ToString(), GUILayout.MaxWidth(200));
        OtherPlayerScale = EditorGUILayout.Slider(OtherPlayerScale, -3, 3, GUILayout.MaxWidth(240));
        GUILayout.EndHorizontal();

        PlayerController[] controllers;

        if (FindObjectsOfType<PlayerController>().Length != 0)
        {
            controllers = FindObjectsOfType<PlayerController>();

            foreach (PlayerController p in controllers)
            {
                if (p.name != "Player0")
                    p.originalCardScale = Vector3.one * OtherPlayerScale;
            }
        }
    }

    void SetNeutralScale()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Neutral deck scale");
        EditorGUILayout.TextField("", NeutralScale.ToString(), GUILayout.MaxWidth(200));
        NeutralScale = EditorGUILayout.Slider(NeutralScale, -3, 3, GUILayout.MaxWidth(240));
        GUILayout.EndHorizontal();

        GameController controller;

        if (FindObjectOfType<GameController>())
        {
            controller = FindObjectOfType<GameController>();
            controller.neutralDeckScale = Vector3.one * NeutralScale;
        }
    }

    void Reset()
    {
        if (defaultSet)
        {
            Debug.Log("Reset");
            // Timeline scale
            TimelineScale = DefTimelineScale;

            // Neutral deck reset
            NeutralScale = DefNeutralScale;

            // Other players
            OtherPlayerScale = DefOtherPlayerScale;

            // Local player
            PlayerScale = DefPlayerScale;

        }
    }
}