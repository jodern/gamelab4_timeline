﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class WarningPopup : MonoBehaviour {

	static GameObject warning;
	static Canvas canvas;

	public static void Display(string warningMsg, Action action, GameObject prefab = null)
	{
		if (prefab != null)
		{
			warning = Instantiate (prefab) as GameObject;
		}
		
		if(warning == null)
		{
			if (prefab == null) 
			{
				warning = Instantiate (Resources.Load ("Prefabs/Warning")) as GameObject;
			}

			warning.transform.SetParent (FindObjectOfType<Canvas> ().transform, false);
			if (canvas == null) 
			{
				Canvas[] c = FindObjectsOfType<Canvas> ();
				for (int i = 0; i < c.Length; i++) 
				{
					if (c [i].renderMode == RenderMode.ScreenSpaceOverlay || c [i].renderMode == RenderMode.ScreenSpaceCamera) 
					{
						canvas = c [i];
						break;
					}
				}
				if (canvas == null)
					return;
				else 
				{
					warning.transform.SetParent (canvas.transform, false);
				}
			}
			if(canvas.renderMode == RenderMode.ScreenSpaceOverlay)
				warning.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (Screen.width, Screen.height) * 0.5f;
			else
				warning.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		}
		else
			warning.SetActive(true);
		
		warning.transform.FindChild ("WarningText").GetComponent<Text> ().text = warningMsg;
		Button btn = warning.transform.FindChild("Continue").GetComponent<Button>();
		btn.onClick.RemoveAllListeners();
		btn.onClick.AddListener(() => {action(); warning.SetActive(false);});
	}


	public void CancelgameObject()
	{
		gameObject.SetActive (false);
	}


	void Update()
	{
		if (gameObject.activeInHierarchy)
		{
			if (Input.GetMouseButtonDown (0))
			{
				RectTransform rect = GetComponent<RectTransform> ();
				if (!RectTransformUtility.RectangleContainsScreenPoint (rect, Input.mousePosition, Camera.main))
				{
					gameObject.SetActive (false);
				}
			}
		}
	}
}
