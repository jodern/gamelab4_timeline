﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using SimpleJSON;

public class DeckHolder : MonoBehaviour {

	public static DeckHolder instance;
    public List<Deck> storedDecks = new List<Deck>();
	public List<Deck> defaultDecks = new List<Deck>();
	public Deck selectedDeck = null;

	public bool isLoading = false;

	public static string savePath;

	public const string secretKey = "95e839097a566471c70fe357e5a101d2";

	// Use this for initialization
	void Awake () 
    {
		savePath = Application.persistentDataPath + @"/SaveData/";
		if (FindObjectsOfType<DeckHolder>().Length > 1)
		{
			Destroy(gameObject);
			Debug.Log("More than one DeckHolder in scene!");
			return;
		}
			
        instance = this;
        DontDestroyOnLoad(gameObject);
		LoadDecks();
		CheckLoadDefaultDeck ();
		selectedDeck = defaultDecks[0];
		if(!queueRunning)
			StartCoroutine(QueueManager());
	}


	void OnLevelWasLoaded()
	{
		if(!queueRunning)
			StartCoroutine(QueueManager());
	}


	//Load default deck if it needs to be loaded
	public void CheckLoadDefaultDeck()
	{
		if (storedDecks.Count != 0)
		{
			return;
		}
		Object[] o = Resources.LoadAll("DefaultDecks");

		foreach (TextAsset tx in o) 
		{
			Deck deck = new Deck ();
			System.Xml.XmlDocument dx = new System.Xml.XmlDocument ();
			try
			{
				dx.LoadXml(tx.text); 
			}
			catch (System.Exception e)
			{
				Debug.LogError ("Loading default deck failed: " + e);
			}
			deck.LoadDataFromFile(dx);
			defaultDecks.Add(deck);
		}
	}



//	IEnumerator PostDefaultDecks()
//	{
//		yield return new WaitForSeconds(3);
//		foreach (Deck deck in defaultDecks)
//		{
//			WWWForm form = new WWWForm();
//			form.AddField("name", deck.name);
//			form.AddField("owner", deck.owner);
//
//			string deckPost = "www.mediastudent.no/spo2014/gamelab03/deck_post.php";
//
//			WWW www = new WWW(deckPost, form);
//
//			yield return www;
//
//			if (!string.IsNullOrEmpty(www.error))
//			{
//				Debug.LogWarning(www.error);
//				yield break;
//			}
//
//			int id = int.Parse(www.text);
//
//			foreach (CardData data in deck.cards)
//			{
//				form = new WWWForm();
//				form.AddField("deckID", id.ToString());
//				form.AddField("name", data.title);
//				form.AddField("url", data.imageURL);
//				form.AddField("offsetX", data.imageOffset.x.ToString());
//				form.AddField("offsetY", data.imageOffset.y.ToString());
//				form.AddField("zoom", data.zoom.ToString());
//				form.AddField("date", data.year.ToString("00") + "-" + ((int)data.month+1).ToString("00") + "-" + data.day.ToString("00"));
//
//				string cardPost =  "www.mediastudent.no/spo2014/gamelab03/card_post.php";
//
//				www = new WWW(cardPost, form);
//
//				yield return www;
//
//				if (!string.IsNullOrEmpty(www.error))
//				{
//					Debug.LogWarning(www.error);
//					yield break;
//				}
//			}
//		}
//	}



	public void SaveDecks()
	{
		#if UNITY_WEBPLAYER || Unity_WEBGL
			SaveDecksToPlayerPref();
		#else
		Debug.Log("Saving deck");
		try
		{
			foreach (Deck deck in DeckHolder.instance.storedDecks) 
			{
				deck.SaveDataToFile();
			}
		}
		catch (System.Exception e)
		{
			Debug.Log("Failed to Save deck to file: " + e);
			//Fall back to player prefs if savin g fails
			SaveDecksToPlayerPref();
		}
		try
		{
			//Delete decks that have been deleted
			DirectoryInfo d = new DirectoryInfo(savePath);
			FileInfo[] files = d.GetFiles("*.xml");
			foreach (FileInfo file in files)
			{
				if(!storedDecks.Exists(s => "Deck-" + s.ID == file.Name))
					file.Delete(); 
			}
		}
		catch (System.Exception e)
		{
			Debug.LogError("Error saving deck: " + e);
			Debug.Log("Fallback to playerprefs");
			SaveDecksToPlayerPref();
		}
		#endif

		CheckLoadDefaultDeck ();
	}


	public void LoadDecks()
	{
		if (Application.platform == RuntimePlatform.WebGLPlayer || Application.isWebPlayer)
		{
			LoadDecksFromPlayerPrefs();
		}
		else 
		{	
			//LoadXmlDecks();
			WWWForm form = new WWWForm();
			form.AddField ("user", Profile.playerName ?? "Somebody");
			form.AddField("key", secretKey);
			LoadFromDB("deck_getAll.php", AddToStoredDecks, form);
		}
	}


	void AddToStoredDecks(Deck[] decks)
	{
		if(decks != null)
			storedDecks.AddRange(decks);
	}


    //Save cards on exiting the game
    void OnApplicationQuit()
    {
		//SaveDecks();
    }
       

    //Saves all decks with all cards to playerprefs
    void SaveDecksToPlayerPref()
    {
		PlayerPrefs.DeleteAll ();
        int totalCards = 0;
        for (int i = 0; i < storedDecks.Count; i++)
        {
            PlayerPrefs.SetString("Deck" + i, storedDecks[i].name);
			PlayerPrefs.SetString("Deck" + i + "Owner", storedDecks[i].owner);
			PlayerPrefs.SetString ("Deck" + i + "ID", storedDecks [i].ID.ToString());
            for (int o = 0; o < storedDecks[i].cards.Count; o++)
            {
				PlayerPrefs.SetInt ("Deck" + i + "Card" + o + "ID", storedDecks [i].cards [o].ID);
                PlayerPrefs.SetString("Deck" + i + "Card" + o, storedDecks[i].cards[o].title);
                PlayerPrefs.SetString("Deck" + i + "Card" + o + "URL", storedDecks[i].cards[o].imageURL);
                PlayerPrefs.SetFloat("Deck" + i + "Card" + o + "ImageOffsetX", storedDecks[i].cards[o].imageOffset.x);
                PlayerPrefs.SetFloat("Deck" + i + "Card" + o + "ImageOffsetY", storedDecks[i].cards[o].imageOffset.y);
                PlayerPrefs.SetFloat("Deck" + i + "Card" + o + "Zoom", storedDecks[i].cards[o].zoom);
                PlayerPrefs.SetString("Deck" + i + "Card" + o + "Description", storedDecks[i].cards[o].description);
                PlayerPrefs.SetInt("Deck" + i + "Card" + o + "Year", storedDecks[i].cards[o].year);
				PlayerPrefs.SetInt("Deck" + i + "Card" + o + "Month", (int)storedDecks[i].cards[o].month);
                PlayerPrefs.SetInt("Deck" + i + "Card" + o + "Day", storedDecks[i].cards[o].day);
                totalCards++;
            }
        }
		PlayerPrefs.SetInt("TotalCards", totalCards);
		PlayerPrefs.Save();
        Debug.Log("Done saving " + totalCards + " cards");
    }
		


    //Loads all decks with all cards from playerprefs
    void LoadDecksFromPlayerPrefs()
    {
        int totalCards = 0;
        int i = 0;
        //Get all stored decks
        while (PlayerPrefs.HasKey("Deck" + i))
        {
            int o = 0;
            //Get name of deck and recreate it
			storedDecks.Add(new Deck(PlayerPrefs.GetString("Deck" + i), PlayerPrefs.GetString("Deck" + i + "Owner"), int.Parse(PlayerPrefs.GetString("Deck" + i + "ID"))));
            //Get all stored cards in deck
            while (PlayerPrefs.HasKey("Deck" + i + "Card" + o))
            {
				//Get ID
				storedDecks[i].cards.Add(new CardData(PlayerPrefs.GetInt("Deck" + i + "Card" + o + "ID")));
                storedDecks[i].cards[o].title = PlayerPrefs.GetString("Deck" + i + "Card" + o);
                storedDecks[i].cards[o].imageURL = PlayerPrefs.GetString("Deck" + i + "Card" + o + "URL");
                storedDecks[i].cards[o].imageOffset.x = PlayerPrefs.GetFloat("Deck" + i + "Card" + o + "ImageOffsetX");
                storedDecks[i].cards[o].imageOffset.y = PlayerPrefs.GetFloat("Deck" + i + "Card" + o + "ImageOffsetY");
                storedDecks[i].cards[o].zoom = PlayerPrefs.GetFloat("Deck" + i + "Card" + o + "Zoom");
                storedDecks[i].cards[o].description = PlayerPrefs.GetString("Deck" + i + "Card" + o + "Description");
                storedDecks[i].cards[o].year = PlayerPrefs.GetInt("Deck" + i + "Card" + o + "Year");
				storedDecks[i].cards[o].month = (CardData.Months)PlayerPrefs.GetInt("Deck" + i + "Card" + o + "Month");
                storedDecks[i].cards[o].day = (byte)PlayerPrefs.GetInt("Deck" + i + "Card" + o + "Day");
                totalCards++;
                o++;
				if (o > 1000)
					break;
            }
			//Sort cards in deck by date
			storedDecks[i].SortCards();
            i++;
			if (i > 1000)
				break;
        }
        Debug.Log("Done loading " + totalCards + " cards");
    }


    void LoadXmlDecks()
    {
        if (!Directory.Exists(savePath))
			Directory.CreateDirectory(savePath);

        storedDecks.Clear();
		string[] files = Directory.GetFiles(savePath, "*.xml");
		if (files == null || files.Length == 0)
		{
			LoadDecksFromPlayerPrefs();
			return;
		}
		foreach (string file in files)
        {
            Deck deck = new Deck();
            deck.LoadDataFromFile(file);
            storedDecks.Add(deck);
        }
		selectedDeck = storedDecks[storedDecks.Count - 1];
    }


	public void LoadFromDB(string getMethod, System.Action<Deck[]> callback, WWWForm form = null)
	{
		StartCoroutine (DBLoad (getMethod, callback, form));
	}

	IEnumerator DBLoad(string getMethod, System.Action<Deck[]> callback, WWWForm form)
	{
		string url = "https://cpanel48.proisp.no/~mediaopc/spo2014/gamelab03/" + getMethod;

		WWW www;
		if (form == null)
			www = new WWW (url);
		else
			www = new WWW (url, form);

		isLoading = true;

		yield return www;
	
		isLoading = false;

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogError("Error loading deck from DB: " + www.error);
			Debug.LogError (url);
		}
		else
		{
			//Debug.Log("from " + url + ": " + www.text);

			JSONArray data = JSON.Parse(www.text) as JSONArray;

			if (data["ERROR"] != null)
			{
				Debug.LogWarning("ERROR: " + data["ERROR"]);
			}
			else
			{
				Deck[] decks = new Deck[data.Count];
				int cardsSoFar = 0;
				for (int i = 0; i < data.Count; i++)
				{
					Deck deck = new Deck(data[i]["name"], data[i]["owner"], data[i]["id"].AsInt);
					if (data [i] ["views"] != null)
						deck.views = data [i] ["views"].AsInt;
					for (int j = 0; j < data[i]["cards"].Count; j++)
					{
						cardsSoFar++;
						string fullDate = data[i]["cards"][j]["date"];
						string[] date = fullDate.Split('-');
						CardData newCard = new CardData(
							                data[i]["cards"][j]["title"],
							                data[i]["cards"][j]["url"],
							                new Vector2(data[i]["cards"][j]["offsetX"].AsFloat, data[i]["cards"][j]["offsetY"].AsFloat),
							                data[i]["cards"][j]["zoom"].AsFloat,
							                "",
							                int.Parse(date[0]),
							                (CardData.Months)(int.Parse(date[1]) - 1),
							                byte.Parse(date[2]),
											data[i]["cards"][j]["id"].AsInt);
						deck.cards.Add(newCard);
					}
					decks[i] = deck;
				}
				callback(decks);
				yield break;
			}
		}
		callback(null);
	}


	public void LogPlayedDeckWrapper(int deckID, string user)
	{
		StartCoroutine (LogPlayedDeck (deckID, user));
	}
		
	IEnumerator LogPlayedDeck(int deckID, string user)
	{
		string url = "https://cpanel48.proisp.no/~mediaopc/spo2014/gamelab03/queryStats_post.php";

		WWWForm form = new WWWForm ();
		form.AddField ("id", deckID);
		form.AddField ("user", user);
		form.AddField("key", DeckHolder.secretKey);

		WWW www = new WWW (url, form);

		yield return www;

		if (!string.IsNullOrEmpty (www.error)) 
		{
			Debug.LogWarning ("Failed posting stats: " + www.error);
		} 
	}


	public struct AssignData
	{
		public Transform t;
		public CardData data;
		public Card c;
		public AssignData(Transform t, CardData data, Card c)
		{
			this.t = t;
			this.data = data;
			this.c = c;
		}
	}
	Queue<AssignData> cardAssignPool = new Queue<AssignData>();
	bool queueRunning = false;


	public void AssignCard(AssignData d)
	{
		cardAssignPool.Enqueue(d);
	}


	IEnumerator QueueManager()
	{
		queueRunning = true;
		while (true)
		{
			yield return new WaitWhile(() => cardAssignPool.Count == 0);

			while (cardAssignPool.Count > 0)
			{
				AssignData d = cardAssignPool.Dequeue();
				d.c.AssignCard(d.data, d.t);
				yield return new WaitForSeconds(0.1f);
			}
		}
		queueRunning = false;
	}


    [ContextMenu("Clear decks!")] void DeleteAllDecks()
    {
        storedDecks.Clear();
        PlayerPrefs.DeleteAll();
    }
}
