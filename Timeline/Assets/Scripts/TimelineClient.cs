﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using ExitGames.Client.Photon.LoadBalancing;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class TimelineClient : LoadBalancingClient {

	public delegate void GamelistAction(Dictionary<string, RoomInfo> data);
	public event GamelistAction OnUpdateGameList;

	public PhotonManager networkManager;

    public override void OnEvent(EventData photonEvent)
    {
        base.OnEvent(photonEvent);
		Debug.Log("Recieved Data: " + photonEvent.Code + "!");
//		for (byte i = 0; i < byte.MaxValue; i++) 
//		{
//			if (photonEvent.Parameters.ContainsKey (i))
//				Debug.Log ("Event " + photonEvent.Code + " contains key: " + i);
//		}
        switch (photonEvent.Code)
        {
			case EventCode.Join:
			Debug.Log ("A player Joined");

			int id = (int) photonEvent.Parameters [ParameterCode.ActorNr];
			networkManager.Join(id);
			if (CurrentRoom.PlayerCount == CurrentRoom.MaxPlayers)
			{
				CurrentRoom.IsOpen = false;
				CurrentRoom.IsVisible = false;
				networkManager.GameFull();
			}
            break;

			//Data is game list for lobby
			case EventCode.GameListUpdate:
			case EventCode.GameList:
				Dictionary<string, RoomInfo> list = photonEvent.Parameters[ParameterCode.GameList] as Dictionary<string, RoomInfo>;
				OnUpdateGameList(list);
				break;
				
			//Data is chat message
			case 1: 
				Hashtable msg = photonEvent.Parameters[ParameterCode.Data] as Hashtable;
				int senderId = (int) photonEvent.Parameters[ParameterCode.ActorNr];
				Debug.Log("Received message from: " + senderId);
				msg["Sender"] = senderId;
				
				networkManager.RecieveMessage(msg);
	            break;

			//Data is deck data
			case 2 : 
				Hashtable deck = photonEvent.Parameters[ParameterCode.Data] as Hashtable;
				networkManager.RecieveDeck(deck);
	            break;

			//End turn event
			case 10: 
				Hashtable endturn = photonEvent.Parameters [ParameterCode.Data] as Hashtable;
				endturn ["Sender"] = photonEvent.Parameters [ParameterCode.ActorNr];
				networkManager.RecieveEndTurn(endturn);
				break;

			//Recieving distributed cards
			case 12 :
				object obj = photonEvent.Parameters[ParameterCode.Data];
				networkManager.ReceiveGameReady(obj);
				break;

			case 11 :
				Hashtable table = photonEvent.Parameters[ParameterCode.Data] as Hashtable;
				networkManager.RecieveDistributedCards(table);
				break;

			case 17:
				int winnerID = (int) photonEvent.Parameters[ParameterCode.ActorNr];
				networkManager.ReceiveGameOver(winnerID);
				break;

			case 132:
				int recID = (int)photonEvent.Parameters [ParameterCode.ActorNr];
				networkManager.RecieveLoadedGame (recID);
				break;
			case 21:
				int ID = (int)photonEvent.Parameters [ParameterCode.ActorNr];
				bool readyState = (bool)photonEvent.Parameters [ParameterCode.Data];
				networkManager.ReceieveReady (readyState, ID);
				break;

			case 89:
				string levelName = (string)photonEvent.Parameters [ParameterCode.Data];
				networkManager.RecieveNewLevel (levelName);
				break;
				
			//Player left room
			case EventCode.Leave:
				Debug.Log("A Player left!");
				//networkManager.PhotonDisconnect();
				//networkManager.OnClickLoadLevel("Main Menu");
				int plyId = (int)photonEvent.Parameters[ParameterCode.ActorNr];
				networkManager.Leave(plyId);
				if (SceneManager.GetActiveScene().name == "MP_Lobby")
				{
					if (CurrentRoom.PlayerCount < CurrentRoom.MaxPlayers)
					{
						networkManager.OnGameNotReady.Invoke();
						CurrentRoom.IsOpen = true;
						CurrentRoom.IsVisible = true;
					}
				}
	            break;
        }
    }


	void OnOperationResponse (OperationResponse operationResponse)
	{
		switch (operationResponse.OperationCode) 
		{
			case OperationCode.JoinLobby:
				Debug.Log("User Joined lobby: " + operationResponse.ReturnCode);
				break;

			case OperationCode.Leave:
				Debug.Log("User left: " + operationResponse.ReturnCode);
				break;
			case OperationCode.JoinGame:
				if (operationResponse.ReturnCode != 0)
				{
					Debug.Log(operationResponse.DebugMessage);
					networkManager.OnClickLoadLevel("Main Menu");
				}
				break;
		}
	}


	public override void OnStatusChanged(StatusCode statusCode)
	{
		base.OnStatusChanged(statusCode);
		switch (statusCode) 
		{
			//Return to main menu if disconnected involuntarily
			case StatusCode.EncryptionFailedToEstablish:
			case StatusCode.DisconnectByServerUserLimit:
			case StatusCode.TimeoutDisconnect:
//				if (SceneManager.GetActiveScene().name != "Main Menu")
//					Scene
				break;
		}
		//Debug.Log("Status changed: " + statusCode);
	}


	public override void DebugReturn(DebugLevel level, string message)
	{
		base.DebugReturn(level, message);
		//Debug.Log(message);
	}
}
