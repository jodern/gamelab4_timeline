﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class DeckBrowser : MonoBehaviour {

	public GameObject deckListingPrefab;
	[SerializeField] Transform listingParent;
	[SerializeField] Slider playerCount;
	[SerializeField] Dropdown popularSort;
	[SerializeField] InputField searchField;
	[SerializeField] GameObject searchFieldClearbutton;
	[SerializeField] Toggle nameSortToggle;
	[SerializeField] DragTabs drag;
	[Space(10)]
	[SerializeField] string[] sortModes;

	string sortMode;
	int dayLimit = -1;

	public static Deck[] currentDecks;

	public ToggleGroup group;
	[SerializeField] ToggleGroup barGroup;

	List<Transform> instances = new List<Transform>();

	int oldPopValue = 0;


	bool isSearching;

	ReportDeck reportDeck;

	// Use this for initialization
	void Awake () 
	{
		reportDeck = FindObjectOfType<ReportDeck>();
		sortModes = new string[] 
		{
			"nameAsc",
			"nameDesc",
			"dateAsc",
			"dateDesc",
			"countDesc",
			"countAsc",
			"ownerAsc",
			"ownerDesc",
			"viewsDesc",
			"viewsAsc"
		};
		sortMode = "dateDesc";
	}


	public void ChangeSortMode(int i)
	{
		if (sortModes [i] != sortMode || oldPopValue != popularSort.value)
			sortMode = sortModes [i];
		else
			sortMode = sortModes [i + 1];

		if (sortMode == "viewsDesc" || sortMode == "viewsAsc")
		{
			switch (popularSort.value)
			{
				case 0:
					dayLimit = -1;
					break;
				case 1:
					dayLimit = 30;
					break;
				case 2:
					dayLimit = 7;
					break;
			}
			oldPopValue = popularSort.value;
		}
		else
		{
			dayLimit = -1;
		}

		RefreshListing ();
	}


	public void ToggleBrowser()
	{
		gameObject.SetActive(!gameObject.activeSelf);
	}


	void OnEnable()
	{
		if(!DeckHolder.instance.isLoading)
			RefreshListing ();
	}


	public void RefreshListing()
	{
		RefreshAllDecks();
	}


	void RefreshYourDecks()
	{
		WWWForm form = new WWWForm();

		form.AddField("user", Profile.playerName ?? "Somebody");
		form.AddField("owner", Profile.playerName ?? "Somebody");
		form.AddField("limit", 1000);
		form.AddField("days", dayLimit.ToString());
		form.AddField("min", (playerCount.value * 10).ToString());
		form.AddField("orderBy", "nameDesc");
		//form.AddField("age", Profile.GetAgeYear());
		form.AddField("key", DeckHolder.secretKey);
		 
		DeckHolder.instance.LoadFromDB ("deck_getAll.php", PopulateBrowser, form);
	}


	void RefreshAllDecks()
	{
		WWWForm form = new WWWForm();

		form.AddField("user", Profile.playerName ?? "Somebody");
		form.AddField("days", dayLimit.ToString());
		form.AddField("min", (playerCount.value * 10).ToString());
		form.AddField("orderBy", sortMode);
		if (searchField.text != "")
			form.AddField("search", searchField.text);
		//form.AddField("age", Profile.GetAgeYear());
		form.AddField("limit", 1000);
		form.AddField("key", DeckHolder.secretKey);

		DeckHolder.instance.LoadFromDB ("deck_getAll.php", PopulateBrowser, form);
	}


	public void RefreshNameSearch(string name)
	{
		isSearching = name != "";
		searchFieldClearbutton.SetActive(isSearching);
		if (!isSearching)
		{
			RefreshListing();
			return;
		}
		nameSortToggle.isOn = true;
		sortMode = "nameDesc";
		
		WWWForm form = new WWWForm();
		form.AddField("user", Profile.playerName ?? "Somebody");
		form.AddField("limit", "1000");
		form.AddField("search", name);
		form.AddField("orderBy", "nameDesc");
		form.AddField("min", (playerCount.value * 10).ToString());
		//form.AddField("age", Profile.GetAgeYear());
		form.AddField ("key", DeckHolder.secretKey);
		DeckHolder.instance.LoadFromDB ("deck_getAll.php", PopulateBrowser, form);
	}


	public void EndSearch()
	{
		Debug.Log("ended search");
	}
		


	public void PopulateBrowser(Deck[] d)
	{
		if (!gameObject.activeInHierarchy)
			return;
		currentDecks = d;

		StopCoroutine("BuildDeckBrowser");


		if (d == null)
		{
			for (int i = 0; i < instances.Count; i++)
			{
				instances[i].gameObject.SetActive(false);
			}
			return;
		}

		StartCoroutine("BuildDeckBrowser");
	}


	IEnumerator BuildDeckBrowser()
	{
		drag.gameTabList = new DragTabs.DragTab[currentDecks.Length][];
		//DeckHolder.instance.selectedDeck = d[0];
		int deckCount = currentDecks.Length;

		if (instances.Count > deckCount)
		{
			for (int i = deckCount; i < instances.Count; i++)
			{
				instances[i].gameObject.SetActive(false);
			}
		}

		for (int i = 0; i < deckCount; i++)
		{
			BuildTab (i);
			yield return null;
		}
	}


	void BuildTab(int i)
	{
		Debug.Log (currentDecks [i].name);
		Transform inst;
		if (i < instances.Count)
		{
			instances[i].gameObject.SetActive(true);
			inst = instances[i];
		}
		else
		{
			Debug.Log ("Prefab: " + deckListingPrefab);
			GameObject tmp = Instantiate (deckListingPrefab) as GameObject;
			inst = tmp.transform;
			instances.Add(inst);
		}
		inst.SetParent(listingParent, false);
		Transform scale = inst.FindChild("ScaleBar");
		RectTransform name = scale.FindChild("Name").GetComponent<RectTransform>();
		name.GetComponentInChildren<Text>().text = currentDecks[i].name;
		RectTransform numCards = scale.FindChild("NumCards").GetComponent<RectTransform>();
		numCards.GetComponentInChildren<Text>().text = currentDecks[i].cards.Count.ToString("00");
		RectTransform user = scale.FindChild("User").GetComponent<RectTransform>();
		user.GetComponentInChildren<Text>().text = currentDecks[i].owner;
		RectTransform views = scale.FindChild("Views").GetComponent<RectTransform>();

		Debug.Log (inst.name + "," + name.name + ", " + numCards.name + ", " + user.name + ", " + views.name);

		name.anchorMax = drag.dragTabs[0].l.anchorMax;
		numCards.anchorMin = drag.dragTabs[0].r.anchorMin;
		numCards.anchorMax = drag.dragTabs[1].l.anchorMax;
		user.anchorMin = drag.dragTabs[1].r.anchorMin;
		user.anchorMax = drag.dragTabs[2].l.anchorMax;
		views.anchorMin = drag.dragTabs[2].r.anchorMin;

		name.anchoredPosition = Vector2.zero;
		numCards.anchoredPosition = Vector2.zero;
		user.anchoredPosition = Vector2.zero;
		views.anchoredPosition = Vector2.zero;

		drag.gameTabList[i] = new DragTabs.DragTab[3];

		drag.gameTabList[i][0].l = name;
		drag.gameTabList[i][0].r = numCards;
		drag.gameTabList[i][1].l = numCards;
		drag.gameTabList[i][1].r = user;
		drag.gameTabList[i][2].l = user;
		drag.gameTabList[i][2].r = views;

		views.GetComponentInChildren<Text> ().text = "Views: " + currentDecks [i].views;
		int index = i;
		inst.GetComponentInChildren<Button>().onClick.AddListener(() => { reportDeck.OpenReportWindow(); reportDeck.SetReportDeckName(index);});

		Toggle toggle = scale.GetComponent<Toggle>();
		//Turn on first in list
		//toggle.isOn = i == 0;
		toggle.group = group;
		toggle.onValueChanged.AddListener((isOn) => SelectDeck(index, isOn));
	}


	public void SelectDeck(int index, bool isOn)
	{
		if (!isOn)
			return;
		DeckHolder.instance.selectedDeck = currentDecks[index];
		Debug.Log ("Selected deck: " + DeckHolder.instance.selectedDeck + " : Index: " + index);
	}
}
