﻿using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Deck {

	public string name = "";
	public int ID = -1;
	public string creationDate;
	public string owner = "Somebody";
	public int views = 0;

	public List<CardData> cards = new List<CardData>();

	public Deck(string name, string owner, int ID)
    {
		this.name = name;
		this.owner = owner;
		this.ID = ID;
    }

    public Deck(){}

	//Sort cards by date
	public void SortCards()
	{
		cards.Sort(delegate(CardData a, CardData b)
		{
			int c = a.year.CompareTo(b.year);
			if (c == 0)
			{
				int aMonth = (int) a.month;
				int bMonth = (int) b.month;
				c = aMonth.CompareTo(bMonth);
				if (c == 0)
				{
					c = a.day.CompareTo(b.day);
				}
			}
			return c;
		});
	}


    public void SaveDataToFile()
    {
		string path = DeckHolder.savePath;
        //Check if save directory exist and create it if it doesn't
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
		
	   	using (FileStream fs = new FileStream(path, FileMode.Create))
	    using (StreamWriter sw = new StreamWriter(fs))
	    //Create a new writer that saves in SavedDecks
	    using (XmlTextWriter writer = new XmlTextWriter(sw))
	    {
	        writer.Formatting = Formatting.Indented;
	        writer.Indentation = 4;
	        writer.WriteStartDocument();
	        //Deck node
	        writer.WriteStartElement("Deck");
	        //Name and ID under deck node
	        writer.WriteElementString("Name", name);
			writer.WriteElementString("ID", ID.ToString());
	        //Cards node that contains all cards
	        writer.WriteStartElement("Cards");
	        foreach (CardData data in cards)
	        {
	            //Card node that contain all cardData for each card
	            writer.WriteStartElement("Card");
	            //Write card cata
				writer.WriteElementString("ID", data.ID.ToString());
	            writer.WriteElementString("Title", data.title);
	            writer.WriteElementString("url", data.imageURL);
	            writer.WriteElementString("OffsetX", data.imageOffset.x.ToString());
	            writer.WriteElementString("OffsetY", data.imageOffset.y.ToString());
	            writer.WriteElementString("Zoom", data.zoom.ToString());
	            writer.WriteElementString("Description", data.description);
	            writer.WriteElementString("Year", data.year.ToString());
	            writer.WriteElementString("Month", data.month.ToString());
	            writer.WriteElementString("Day", data.day.ToString());
	            //End of card
	            writer.WriteEndElement();
	        }
	        //End of cards
	        writer.WriteEndElement();
	        //End of deck
	        writer.WriteEndElement();
	        //End of document
	        writer.WriteEndDocument();
    	}
    }


    public void LoadDataFromFile(string filePath)
    {
        XmlDocument dx = new XmlDocument();
        dx.Load(filePath);

		LoadXmlData(dx);
    }


	public void LoadDataFromFile(XmlDocument doc)
	{
		LoadXmlData(doc);
	}

	void LoadXmlData(XmlDocument doc)
	{
		//Get the deck node in document
		XmlNode deckNode = doc.SelectSingleNode("/Deck");
		name = deckNode.ChildNodes[0].InnerText;
		ID = int.Parse(deckNode.ChildNodes[1].InnerText);
		//Get the card that contains card data in third node
		foreach (XmlNode node in deckNode.ChildNodes[2])
		{
			CardData data = new CardData(-1);
			//Parse all data
			int loadedID = -1;
			int.TryParse(node.ChildNodes[0].InnerText, out loadedID);
				data.ID = loadedID;
			data.title = node.ChildNodes[1].InnerText;
			data.imageURL = node.ChildNodes[2].InnerText;
			data.imageOffset.x = float.Parse(node.ChildNodes[3].InnerText);
			data.imageOffset.y = float.Parse(node.ChildNodes[4].InnerText);
			data.zoom = float.Parse(node.ChildNodes[5].InnerText);
			data.description = node.ChildNodes[6].InnerText;
			data.year = int.Parse(node.ChildNodes[7].InnerText);
			data.month = (CardData.Months)System.Enum.Parse(typeof(CardData.Months), node.ChildNodes[8].InnerText);
			data.day = byte.Parse(node.ChildNodes[9].InnerText);
			//Add parsed data
			cards.Add(data);
		}
	}
}
