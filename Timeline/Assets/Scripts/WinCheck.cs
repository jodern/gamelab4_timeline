﻿using UnityEngine;

public class WinCheck : MonoBehaviour
{
    GameController gameCtrl;
    SuddenDeath suddenDeath;
    PhotonManager photonManager;

    // Use this for initialization
    void Start()
    {
        // hello testing
        gameCtrl = FindObjectOfType<GameController>();
        suddenDeath = FindObjectOfType<SuddenDeath>();
        photonManager = FindObjectOfType<PhotonManager>();
    }

    public bool CheckWinner(int index)
    {
        bool win = false;
        Debug.Log("Checking winner");
        PlayerController player = gameCtrl.players[index];

        if (player.hasLost)
            return false;

        if (GameController.soloPlay && player.Cards.Count == 0)
        {
            return true;
        }
        else if (GameController.soloPlay && player.Cards.Count > 0)
            return false;

        if (player.Cards.Count == 0 &&
            !SuddenDeath.suddenDeath)
        {
            //win = true;
            win = false;

            TurnController turnController = FindObjectOfType<TurnController>();

            int j = -1;
            // more tests
            Debug.Log("card count = " + player.Cards.Count);
            if(player.Cards.Count == 0)
            {
                for (int i = 0; i < gameCtrl.players.Count; i++)
                {
                    if (player != gameCtrl.players[i] &&
                        gameCtrl.players[i].Cards.Count >= 1)
                    {
                        if (j == -1)
                            j = 0;
                        j++;
                        Debug.Log(j);
                    } else if(gameCtrl.players[i].Cards.Count == 0 &&
                        player != gameCtrl.players[i])
                    {
                        j = 0;
                    }
                }
                if (j == gameCtrl.players.Count - 1 && j != 0)
                    win = true;
                else if (j == -1)
                    win = true;
            }
        }
        else if (player.Cards.Count == 0 &&
          SuddenDeath.suddenDeath &&
          !suddenDeath.AllWonOrLost() &&
          !gameCtrl.players[index].Losing &&
          !gameCtrl.players[index].hasLost && !suddenDeath.givingNewCards/* &&
          suddenDeath.CheckAllPlayerCards() >= 2*/)
        {
            Debug.Log("You win");
            win = true;
        }


        int lostPlayers = -1;

        for (int i = 0; i < gameCtrl.players.Count; i++)
        {
            if (gameCtrl.players[i].hasLost)
            {
                if (lostPlayers == -1)
                    lostPlayers = 0;
                lostPlayers++;
            }
        }

        if (lostPlayers == gameCtrl.players.Count - 1)
            win = true;

        Debug.Log("win check returned " + win.ToString());
        return win;
    }
}
