﻿using UnityEngine;
using System.Collections;


public class ClickingBools : MonoBehaviour {

	private bool click1 = true;
	private bool click2 = false;
	private bool click3 = false;
	private bool click4 = false;
	private bool click5 = false;

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)){
				Debug.Log("Click");
				Animator anim = hit.transform.GetComponent<Animator> ();
				if (hit.collider.tag == "LunchBox") {
					if (click1 == true) {
						anim.SetTrigger ("Open");
						click2 = true;
						click1 = false;
					}
					else if (click2 == true) {
						anim.SetTrigger ("Eat1");
						click2 = false;
						click3 = true;
					}
					else if (click3 == true) {
						anim.SetTrigger ("Eat2");
						click3 = false;
						click4 = true;
					}
					else if (click4 == true) {
						anim.SetTrigger ("Eat3");
						click4 = false;
						click5 = true;
					}
					else if (click5 == true) {
						anim.SetTrigger ("Close");
						click5 = false;
						click1 = true;
					}


//					anim.SetBool ("OpenLunchbox", !anim.GetBool("OpenLunchbox"));
				}
			}
		}
	}
}
