﻿using UnityEngine;
using System.Collections;

public class BirdAnims : MonoBehaviour {

	Animator animator;
	private bool Grounded = true;
	public string[] param;
	public string[] paramFly;
	private float Rnd;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
			StartCoroutine ("FlyIn");
			Rnd = Random.Range(5,20);
		}		
	
	// Update is called once per frame
	void Update () {
		if (animator.GetCurrentAnimatorStateInfo (0).IsName ("StartFly")) {
			Grounded = false;
		}
		if (animator.GetCurrentAnimatorStateInfo (0).IsName ("Land") || animator.GetCurrentAnimatorStateInfo (0).IsName ("Land2")) {
			Grounded = true;
		}
	}
	

	IEnumerator PlayRandom(){
		while (true) {
			if (Grounded == true) 
			{
				yield return new WaitWhile (() => !animator.GetCurrentAnimatorStateInfo (0).IsName ("Idle"));
				animator.SetTrigger (param [Random.Range (0, param.Length - 1)]);
				yield return new WaitForSeconds (Rnd);
			}
			if (Grounded == false){
				yield return new WaitWhile (() => !animator.GetCurrentAnimatorStateInfo (0).IsName ("Fly"));
				animator.SetTrigger (paramFly [Random.Range (0, paramFly.Length - 1)]);
				yield return new WaitForSeconds (Rnd);
			}
		}
	}


	IEnumerator FlyIn(){
		yield return new WaitForSeconds (5);
		animator.SetTrigger ("Land");
		yield return new WaitForSeconds (Rnd);
		StartCoroutine ("PlayRandom");
	}
}