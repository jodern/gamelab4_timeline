﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SquirrelAnims : MonoBehaviour {

	Animator animator;
	public AudioSource soundSquirrel;
	public AudioClip squirrelThrowSound;
	public float tossWait = 5f;
	private bool OneTime = true;

	// Use this for initialization
	void Start () {
			animator = GetComponent<Animator>();
		if (LoadGame.sceneToLoad == ("Main Menu")) {
			animator.SetBool("StartWalk", false);
			animator.SetBool("TossCards", false);
			if (OneTime == true) {
				StartCoroutine (ThrowCards ());
			}
		} else {
			animator.SetBool("TossCards", false);
			animator.SetBool("StartWalk", true);
		}
	}

	IEnumerator ThrowCards(){
		yield return new WaitForSeconds (tossWait);
		animator.SetBool("TossCards", true);
		soundSquirrel.PlayOneShot (squirrelThrowSound);
		yield return new WaitForSeconds (1);
		animator.SetBool("TossCards", false);
		OneTime = false;
	}
}
