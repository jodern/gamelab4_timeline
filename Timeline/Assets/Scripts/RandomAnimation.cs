﻿using UnityEngine;
using System.Collections;

public class RandomAnimation : MonoBehaviour
{
    public Animator anim;
    public AudioSource soundSquirrel;
    public AudioClip[] squirrelAnoyed;
    public AudioClip squirrelChill;
    public AudioClip squirrelHello;
    public bool randomIDLE = false;
    [HideInInspector]
    public int Rnd;
    private float timer;

    [System.Serializable]
    public struct Animations
    {
        public AnimationClip[] clips;
    }

    public Animations[] anims;

    void Start()
    {
        if (anim == null)
            anim = GetComponent<Animator>();

        Rnd = Random.Range(10, 15);

        if (randomIDLE)
            RandomAnimLoop(0);
    }

    void Update()
    {
        timer += Time.deltaTime;
        ClickPlay();
    }

    public void RandomAnimLoop(int index)
    {
        GetComponent<BoxCollider>().enabled = true;
        StartCoroutine(RandomLoop(index));
    }

    IEnumerator RandomLoop(int index)
    {
        while (true)
        {
            yield return new WaitWhile(() => !CheckAnimStateName("Idle"));
            PlayRandomAnim(index);
            yield return new WaitForSeconds(Rnd);
            Rnd = Random.Range(10, 20);
        }
    }

    // Player a random animation clip from the clips contained in the anims array
    public void PlayRandomAnim(int index)
    {
        if (index > anims.Length - 1)
        {
            return;
        }

        Animations curAnim = anims[index];
        int random = Random.Range(0, curAnim.clips.Length);

        AnimationClip clip = curAnim.clips[random];
        //		anim.Play (clip.name);
        if (clip == null)
            return;

        anim.CrossFade(clip.name, 0.2f);

        if (clip.name == "Lean_tail")
        {
            soundSquirrel.PlayOneShot(squirrelChill);
        }
        if (clip.name == "Wave")
        {
            soundSquirrel.PlayOneShot(squirrelHello);
        }
    }

    public void PlayAnimByName(string name)
    {
        //		anim.Play (name);
        anim.CrossFade(name, 0.2f);
    }

    public bool CheckAnimStateName(string name)
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName(name);
    }

    public void SetBool(string boolToSet)
    {
        anim.SetBool(boolToSet, true);
    }

    private void ClickPlay()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (timer >= 3)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == this.gameObject)
                    {
                        PlayRandomAnim(2);
                        soundSquirrel.PlayOneShot(squirrelAnoyed[Random.Range(0, squirrelAnoyed.Length)]);
                        timer = 0;
                    }
                }
            }
        }
    }
}

