﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using ExitGames.Client.Photon.LoadBalancing;
using System.Linq;

public class GameController : MonoBehaviour
{
    // Ready check
    //public bool[] readyPlayers;
    public static float startDelayTime = 2f;

    // General info
    public static int amountOfPlayers = 2;
    public GameObject cardFab;
    public Transform neutralDeckPos;
    public Vector3 neutralDeckScale = new Vector3(1, 1, 1);
    public Transform[] playerStart;
    public int editorCardsPerPlayer = 2;
    public int cardsPerPlayer = 5;
    public static int amountCardsPerPlayer = 5;
    public int amountCards = 10;
    public Vector3 otherScale = new Vector3(0.8f, 0.8f, 0.8f);
    public Vector3 cardRotations = Vector3.zero;
    public float mouseZoom = 200f;
    public int deckToUse = 0;
    public Deck selectedDeck;
    public static Dictionary<int, string> idAndName = new Dictionary<int, string>();
    private WinCheck winChecker;

    // Card lists
    public static bool soloPlay = true;
    public static bool suddenDeath = false;
    public bool useLocalDeck = false;
    public List<CardData> neutralDeck;
    public List<CardData> sortedDeck;
    public GameObject topCard;

    // Turn variables
    private TurnController turnController;
    public PlayerController localPlayer;
    public List<PlayerController> players = new List<PlayerController>();
    private TimelineController timelineController;
    public List<int> playerIds = new List<int>(4);
    private PhotonManager photonManager;

    public List<GameObject> otherCards = new List<GameObject>(10);
    public List<CardData> allPlayerCards = new List<CardData>(10);
    [HideInInspector]
    public static GameObject Selected;
    public int cardIndex;
    int listPos;

    public static bool gameRunning = false;
    public bool sceneLoaded = true;
    int loadedPlayers = 0;
    public AudioSource SoundDrawCard;
    public AudioClip DrawCardSound;
	List<int> readyPlayers = new List<int> ();

    //Disable report button if solo play
    public GameObject reportButton;

    void Awake()
    {
#if UNITY_STANDALONE
        if (Screen.height == 432 && Screen.width == 768)
            cardsPerPlayer = editorCardsPerPlayer;
#endif
#if UNITY_EDITOR
        cardsPerPlayer = editorCardsPerPlayer;
#endif
        sceneLoaded = true;
        if (!soloPlay)
        {
            SuddenDeath.roundCount = 0;
            turnController = FindObjectOfType<TurnController>();
            photonManager = FindObjectOfType<PhotonManager>();
            timelineController = FindObjectOfType<TimelineController>();
            winChecker = FindObjectOfType<WinCheck>();
        }
        else
            FindObjectOfType<PhotonManager>().PhotonDisconnect();
    }

    void Start()
    {
        if (soloPlay)
            StartCoroutine(StartDelay());
        else
        {
            //RaiseEventOptions options = new RaiseEventOptions();
            //options.Receivers = ReceiverGroup.All;
            //photonManager.SendNetMessage(sceneLoaded, options, 90);
            //StartCoroutine(SendReady(6f));
			//StartCoroutine(WaitLevelLoad());
        }
    }


	void OnLevelWasLoaded(int lvl)
	{
		if (lvl == 3) 
		{
			//Ready checks
			//photonManager.SendGameReady ();
			//photonManager.LoadedGame ();
		}
	}


	IEnumerator WaitLevelLoad()
	{
		yield return new WaitUntil (() => UnityEngine.SceneManagement.SceneManager.GetActiveScene ().isLoaded);

		//Ready checks
		photonManager.SendGameReady ();
		photonManager.LoadedGame ();
	}


	public void ReceiveReady(int player)
	{
		
		if (!readyPlayers.Contains (player) && UnityEngine.SceneManagement.SceneManager.GetActiveScene().isLoaded) 
		{
			readyPlayers.Add (player);
			Debug.Log("recieved ready. Count: " + readyPlayers.Count + "/" + amountOfPlayers + ", From: " + player);

			if (readyPlayers.Count == amountOfPlayers)
				StartGame ();
		}
	}

    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(startDelayTime);
        SoloSetup();
        SoloDistribution();
    }

    IEnumerator SendReady(float time)
    {
        yield return new WaitForSeconds(time);
        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;
        photonManager.SendNetMessage(null, options, 91);
    }

    public void ReadyCheck(object obj, byte code)
    {
        if (code != 91)
            return;

        if (loadedPlayers < amountOfPlayers)
            loadedPlayers++;

        Debug.Log("recieved ready");

        if (loadedPlayers != amountOfPlayers)
            return;

        StartGame();
    }

    public void StartGame()
    {
		if (!gameRunning) 
		{
			Debug.Log ("Game starting..");
			Debug.Log (FindObjectOfType<MaintainConnection> ().client.CurrentRoom.Players.Count);
			timelineController.SetStartIndex ();
			NetworkDistribution ();
			turnController.SetTime ();
			gameRunning = true;
		} 
		else
			Debug.LogWarning ("Game already running!");
    }

    void SoloSetup()
    {
        gameRunning = true;
        amountCardsPerPlayer = cardsPerPlayer;
        turnController = FindObjectOfType<TurnController>();
        winChecker = FindObjectOfType<WinCheck>();
        playerStart = new Transform[amountOfPlayers];
        localPlayer = GameObject.Find("Player0").GetComponent<PlayerController>();
        turnController.p = localPlayer;
        localPlayer.yourTurn = true;
        localPlayer.playerInfo.SetInfo(localPlayer.ID, Profile.playerName);

        reportButton.SetActive(false);

        GameObject[] Players = new GameObject[FindObjectsOfType<PlayerController>().Length];
        for (int i = 0; i < amountOfPlayers; i++)
        {
            Players[i] = GameObject.Find("Player" + i.ToString());
            players.Add(Players[i].GetComponent<PlayerController>());
            playerStart[i] = Players[i].transform;


            if (players[i] != localPlayer)
                players[i].playerInfo.SetInfo(players[i].ID, "Ai " + i.ToString());

        }
        for (int i = amountOfPlayers; i < Players.Length; i++)
        {
            Players[i] = GameObject.Find("Player" + i.ToString());
            Players[i].SetActive(false);
        }

        if (localPlayer.yourTurn)
        {
            turnController.StartThinkSound();
            turnController.StartTicking();
        }
    }

    void NetworkSetup()
    {
        amountCardsPerPlayer = cardsPerPlayer;
        neutralDeckScale = timelineController.CardsScale;
        playerStart = new Transform[photonManager.PlayerCount()];

        localPlayer = GameObject.Find("Player0").GetComponent<PlayerController>();
        turnController.p = localPlayer;
        localPlayer.playerInfo.SetInfo(localPlayer.ID, Profile.playerName);

        int p = 1;
        GameObject[] Players = new GameObject[FindObjectsOfType<PlayerController>().Length];
        for (int i = 0; i < photonManager.PlayerCount(); i++)
        {
            if (photonManager.client.CurrentRoom.Players.Keys.ElementAt(i) == photonManager.client.LocalPlayer.ID)
            {
                players.Add(localPlayer);
                localPlayer.ID = photonManager.client.LocalPlayer.ID;
            }
            else
            {
                Players[p] = GameObject.Find("Player" + p.ToString());
                players.Add(Players[p].GetComponent<PlayerController>());
                p++;
                players[i].ID = photonManager.client.CurrentRoom.Players.Keys.ElementAt(i);
                players[i].playerInfo.SetInfo(players[i].ID, idAndName[players[i].ID]);
            }
        }
        for (int i = photonManager.PlayerCount(); i < Players.Length; i++)
        {
            Players[i] = GameObject.Find("Player" + i.ToString());
            Players[i].SetActive(false);
        }

        for (int i = 0; i < photonManager.PlayerCount(); i++)
        {
            playerStart[i] = players[i].transform;
        }

        foreach (PlayerController player in players)
        {
            player.isHuman = true;
        }

        if (turnController.HasTurn())
        {
            turnController.StartThinkSound();
            turnController.StartTicking();
        }
    }

    public void DataDistributed(object obj, byte code)
    {
        if (code != 2)
            return;

        gameRunning = true;
        Debug.Log("Data is distributed");
    }

    void SoloDistribution()
    {
        if (FindObjectOfType<DeckHolder>() == null)
        {
			GameObject holder = new GameObject("Deck holder");
            holder.AddComponent<DeckHolder>();
        }

        //copying cardData from DeckHolder
        // Use this int as index when it works (DeckHolder.instance.selectedDeck)
        List<CardData> Data = new List<CardData>();

        // testing
        if (DeckHolder.instance.selectedDeck != null && !useLocalDeck)
            Data = DeckHolder.instance.selectedDeck.cards;
        else
            Data = DeckHolder.instance.defaultDecks[0].cards;

        for (int i = 0; i < Data.Count; i++)
        {
            CardData someData = null;
            neutralDeck.Add(someData);
            neutralDeck[i] = new CardData(Data[i].title, Data[i].imageURL, Data[i].imageOffset, Data[i].zoom,
                Data[i].description, Data[i].year, Data[i].month, Data[i].day, Data[i].ID);
        }

        //shuffles deck for this game
        neutralDeck = shuffledList(neutralDeck);

        Vector3 cardPos = Vector3.zero;
        Vector3 otherCardPos = Vector3.zero;

        for (int i = 0; i < players.Count; i++)
        {
            // Distribute cards to local player
            if (players[i].isLocal())
            {
                for (int j = 0; j < amountCardsPerPlayer; j++)
                {
                    cardPos = new Vector3((j - amountCardsPerPlayer / 2) * playerStart[0].GetComponent<PlayerController>().spacing, 0);
                    GameObject newCard = Instantiate(cardFab, cardPos, Quaternion.identity) as GameObject;
                    newCard.name = newCard.name + " " + j.ToString();
                    newCard.transform.SetParent(GameObject.Find("Player" + i).transform);
                    newCard.transform.localRotation = Quaternion.Euler(cardRotations);
                    newCard.GetComponentInParent<Card>().targetPos = cardPos;
                    newCard.GetComponentInParent<Card>().targetscale = localPlayer.originalCardScale;

                    localPlayer.Cards.Add(newCard);
                    localPlayer.Cards[j].GetComponentInParent<Card>().Assign(neutralDeck[j], newCard.transform.GetChild(0));

                    // turning date elements off after assigning is completed
                    Transform k = newCard.transform.GetChild(0);
                    k.transform.Find("Date").gameObject.SetActive(false);
                    k.transform.Find("Year").gameObject.SetActive(false);
                    k.transform.Find("BigYear").gameObject.SetActive(false);
                    k.transform.Find("Month").gameObject.SetActive(false);
                    k.transform.Find("Day").gameObject.SetActive(false);

                    AddListener(localPlayer.Cards[j].GetComponentInParent<EventTrigger>(), 0);
                }
                neutralDeck.RemoveRange(0, amountCardsPerPlayer);
            }
            else
            {
                // Distribute cards to AI
                for (int j = 0; j < amountCardsPerPlayer; j++)
                {
                    otherCardPos = new Vector3((j - amountCardsPerPlayer / 2) * playerStart[i].GetComponent<PlayerController>().spacing, 0);

                    GameObject newCard = Instantiate(cardFab, cardPos, Quaternion.identity) as GameObject;

                    newCard.name = newCard.name + " " + j.ToString();
                    newCard.transform.SetParent(GameObject.Find("Player" + i).transform);
                    newCard.transform.localRotation = Quaternion.Euler(cardRotations);

                    AddHoverListeners(newCard.GetComponentInParent<EventTrigger>(), 0);
                    //newCard.GetComponent<EventTrigger> ().triggers.RemoveRange (0, 3);

                    newCard.GetComponentInParent<Card>().targetPos = otherCardPos;
                    newCard.transform.position = otherCardPos;
                    newCard.GetComponentInParent<Card>().targetscale = players[i].originalCardScale;
                    newCard.GetComponentInParent<Card>().Assign(neutralDeck[j], newCard.transform.GetChild(0));

                    // turning date elements off after assigning is completed
                    Transform k = newCard.transform.GetChild(0);
                    k.transform.Find("Date").gameObject.SetActive(false);
                    k.transform.Find("Year").gameObject.SetActive(false);
                    k.transform.Find("BigYear").gameObject.SetActive(false);
                    k.transform.Find("Month").gameObject.SetActive(false);
                    k.transform.Find("Day").gameObject.SetActive(false);

                    players[i].Cards.Add(newCard);
                }
                neutralDeck.RemoveRange(0, amountCardsPerPlayer);
            }
        }
        // Turn card animations on
        if (localPlayer.yourTurn)
        {
            foreach (GameObject a in localPlayer.Cards)
            {
                a.GetComponent<Animator>().SetBool("Selected", true);
            }
        }
        UpdateTopDeckCard();
    }

    //adds random information to each card from the deck using the first 5 cards
    public void NetworkDistribution()
    {
        NetworkSetup();
        Debug.Log("Network Distribution ran");

        FindObjectOfType<TimelineController>().SetStartIndex();

        if (FindObjectOfType<DeckHolder>() == null)
        {
            GameObject holder = new GameObject();
            holder.AddComponent<DeckHolder>();
        }

        for (int i = 0; i < photonManager.PlayerCount(); i++)
        {
            playerIds.Add(photonManager.client.CurrentRoom.Players.ElementAt(i).Value.ID);
            players[i].ID = playerIds[i];
        }
        
        if (photonManager.IsMasterClient())
        {
            //copying cardData from DeckHolder
            // Use this int as index when it works (DeckHolder.instance.selectedDeck)
            List<CardData> Data = new List<CardData>();

            if (DeckHolder.instance.selectedDeck != null && !useLocalDeck)
            {
                Debug.Log(DeckHolder.instance.selectedDeck.cards.Count);
                Data = DeckHolder.instance.selectedDeck.cards;

                int id = DeckHolder.instance.selectedDeck.ID;
                RaiseEventOptions options = new RaiseEventOptions();
                options.Receivers = ReceiverGroup.Others;
                photonManager.SendNetMessage(id, options, 174);

                DeckHolder.instance.LogPlayedDeckWrapper(id, Profile.playerName ?? "Somebody");
            }
            else
            {
                Data = DeckHolder.instance.storedDecks[deckToUse].cards;
            }

            for (int i = 0; i < Data.Count; i++)
            {
                CardData someData = null;
                neutralDeck.Add(someData);
                neutralDeck[i] = new CardData(Data[i].title, Data[i].imageURL, Data[i].imageOffset, Data[i].zoom,
                    Data[i].description, Data[i].year, Data[i].month, Data[i].day, Data[i].ID);
            }

            //shuffles deck for this game
            neutralDeck = shuffledList(neutralDeck);

            selectedDeck = new Deck();
            selectedDeck.name = DeckHolder.instance.selectedDeck.name;
            selectedDeck.ID = DeckHolder.instance.selectedDeck.ID;
            selectedDeck.owner = DeckHolder.instance.selectedDeck.owner;
            selectedDeck.cards = neutralDeck;
            photonManager.SendDeck(selectedDeck);

            timelineController.InitialCard();

            Dictionary<int, int[]> cardsToDistribute = new Dictionary<int, int[]>();
            int k = 0;

            foreach (KeyValuePair<int, Player> player in photonManager.client.CurrentRoom.Players)
            {
                int[] data = new int[cardsPerPlayer];
                for (int j = 0; j < cardsPerPlayer; j++)
                {
                    data[j] = k * cardsPerPlayer + j;
                }
                k++;
                cardsToDistribute.Add(photonManager.client.CurrentRoom.Players[k].ID, data);
            }
            photonManager.DistributePlayerCards(cardsToDistribute);
        }
    }

	public void RecieveDistributeData(Dictionary<int, int[]> Data)
	{
		StartCoroutine (DistributeData (Data));
	}

	IEnumerator DistributeData(Dictionary<int, int[]> Data)
    {
        Debug.Log("Recived dictionary of players and indexes " + Data[photonManager.client.LocalPlayer.ID].Length);

		yield return new WaitWhile(() => neutralDeck == null || neutralDeck.Count == 0);

        // Assign all cards for the player
        Vector3 cardPos = Vector3.zero;

        for (int j = 0; j < photonManager.PlayerCount(); j++)
        {
            for (int i = 0; i < amountCardsPerPlayer; i++)
            {
                cardPos = new Vector3(playerStart[j].localPosition.x - (i * players[j].GetComponent<PlayerController>().spacing), 0);
                GameObject newCard = Instantiate(cardFab, cardPos, Quaternion.identity) as GameObject;
                newCard.name = newCard.name + " " + i.ToString();
                newCard.transform.SetParent(playerStart[j].transform);
                newCard.transform.localRotation = Quaternion.Euler(cardRotations);
                newCard.GetComponentInParent<Card>().targetPos = cardPos;
                newCard.GetComponentInParent<Card>().targetscale = players[j].originalCardScale;

                players[j].Cards.Add(newCard);
                players[j].Cards[i].GetComponentInParent<Card>().Assign(neutralDeck[Data[playerIds[j]][i]], newCard.transform.GetChild(0));

                // turning date elements off after assigning is completed
                Transform k = newCard.transform.GetChild(0);
                k.transform.Find("Date").gameObject.SetActive(false);
                k.transform.Find("Year").gameObject.SetActive(false);
                k.transform.Find("BigYear").gameObject.SetActive(false);
                k.transform.Find("Month").gameObject.SetActive(false);
                k.transform.Find("Day").gameObject.SetActive(false);

                if (players[j].isLocal())
                {
                    AddListener(players[j].Cards[i].GetComponentInParent<EventTrigger>(), players.IndexOf(localPlayer));
                }
                else
                    AddHoverListeners(players[j].Cards[i].GetComponentInParent<EventTrigger>(), players.IndexOf(localPlayer));
            }
            players[j].newCardPositions();
        }
        neutralDeck.RemoveRange(0, cardsPerPlayer * photonManager.client.CurrentRoom.PlayerCount);

        UpdateTopDeckCard();
    }

    public void UpdateTopDeckCard()
    {
        // Instantiate top neutral card from the deck
        topCard = null;
        topCard = Instantiate(cardFab, neutralDeckPos.position, Quaternion.identity) as GameObject;
        topCard.name = "Top deck card";
        topCard.transform.SetParent(neutralDeckPos);
        topCard.transform.localRotation = Quaternion.Euler(cardRotations);
        topCard.GetComponentInParent<Card>().targetscale = neutralDeckScale;
        topCard.GetComponentInParent<Card>().Assign(neutralDeck[0], topCard.transform.GetChild(0));
        neutralDeck.RemoveAt(0);
        SoundDrawCard.PlayOneShot(DrawCardSound);

        // turning date elements off after assigning is completed
        if (topCard.GetComponentInParent<Card>().data.day == 0)
        {
            //card.transform.Find("Description").gameObject.SetActive(true);
            Transform k = topCard.transform.GetChild(0);
            k.transform.Find("Date").gameObject.SetActive(false);
            k.transform.Find("Year").gameObject.SetActive(false);
            k.transform.Find("BigYear").gameObject.SetActive(false);
            k.transform.Find("Month").gameObject.SetActive(false);
            k.transform.Find("Day").gameObject.SetActive(false);
            k.transform.Find("FrontTitle").gameObject.SetActive(true);
        }
        AddHoverListeners(topCard.GetComponent<EventTrigger>(), players.IndexOf(localPlayer));
        //topCard.GetComponent<EventTrigger> ().triggers.RemoveRange (0, 3);
    }

    public void GetUpdatedDeck(Deck deck)
    {
        Debug.Log("Got an updated deck");
        selectedDeck = deck;
        neutralDeck = deck.cards;
        timelineController.InitialCard();
    }

    //randomizes a list
    List<CardData> shuffledList(List<CardData> cardsToShuffle)
    {
        CardData card;
        for (int i = 0; i < cardsToShuffle.Count; i++)
        {
            card = cardsToShuffle[i];
            int randomIndex = Random.Range(0, cardsToShuffle.Count);
            cardsToShuffle[i] = cardsToShuffle[randomIndex];
            cardsToShuffle[randomIndex] = card;
        }
        return cardsToShuffle;
    }

    public PlayerController GetPlayer(int ID)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].ID == ID)
                return players[i];
        }
        return null;
    }

    public int GetPlayerIndex(PlayerController player)
    {
        int index = players.IndexOf(player);
        return index;
    }

    public bool WinCheckLocalPlayer()
    {
        int index = GetPlayerIndex(localPlayer);

        return winChecker.CheckWinner(index);
    }

    //Log played deck on db
    public void RecieveDeckID(object obj, byte code)
    {
        if (code == 174)
        {
            int deckID = (int)obj;
            DeckHolder.instance.LogPlayedDeckWrapper(deckID, Profile.playerName ?? "Somebody");
        }
    }


	public void KickOnDisconnect(int id)
	{
		Debug.Log("Player " + id + " kicked from game");
		PlayerController player = GetPlayer(id);
        player.YouLost();
		if (player.yourTurn)
			turnController.EndTurn(true);

        if (WinCheckLocalPlayer())
            localPlayer.Win();
	}


    public void AddListener(EventTrigger trigger, int player)
    {
        // Start animation
        EventTrigger.Entry clickoffCard = new EventTrigger.Entry();
        clickoffCard.eventID = EventTriggerType.PointerUp;
        clickoffCard.callback.AddListener((eventData) =>
        {
            players[player].stopSparkles(eventData);
        });
        trigger.triggers.Add(clickoffCard);

        // Stop animation
        EventTrigger.Entry clickCard = new EventTrigger.Entry();
        clickCard.eventID = EventTriggerType.PointerDown;
        clickCard.callback.AddListener((eventData) =>
        {
            players[player].playSparkles(eventData);
        });
        trigger.triggers.Add(clickCard);

        // Press mouse down
        EventTrigger.Entry PointerDown = new EventTrigger.Entry();
        PointerDown.eventID = EventTriggerType.PointerDown;
        PointerDown.callback.AddListener((eventData) =>
        {
            players[player].SetOffset();
        });
        trigger.triggers.Add(PointerDown);

        // Dragging
        EventTrigger.Entry Drag = new EventTrigger.Entry();
        Drag.eventID = EventTriggerType.Drag;
        Drag.callback.AddListener((eventData) =>
        {
            players[player].HoldCard(eventData);
        });
        trigger.triggers.Add(Drag);

        // mouse up
        EventTrigger.Entry Up = new EventTrigger.Entry();
        Up.eventID = EventTriggerType.PointerUp;
        Up.callback.AddListener((eventData) =>
        {
            players[player].PlaceCard();
        });
        trigger.triggers.Add(Up);

        // mouse enter
        EventTrigger.Entry Enter = new EventTrigger.Entry();
        Enter.eventID = EventTriggerType.PointerEnter;
        Enter.callback.AddListener((eventData) =>
        {
            players[player].newCardsScale(eventData);
        });
        trigger.triggers.Add(Enter);

        // Exit
        EventTrigger.Entry Exit = new EventTrigger.Entry();
        Exit.eventID = EventTriggerType.PointerExit;
        Exit.callback.AddListener((eventData) =>
        {
            players[player].originalScale(eventData);
        });
        trigger.triggers.Add(Exit);
    }

    public void AddHoverListeners(EventTrigger trigger, int player)
    {
        // mouse enter
        EventTrigger.Entry Enter = new EventTrigger.Entry();
        Enter.eventID = EventTriggerType.PointerEnter;
        Enter.callback.AddListener((eventData) =>
        {
            players[player].newCardsScale(eventData);
        });
        trigger.triggers.Add(Enter);

        // Exit
        EventTrigger.Entry Exit = new EventTrigger.Entry();
        Exit.eventID = EventTriggerType.PointerExit;
        Exit.callback.AddListener((eventData) =>
        {
            players[player].originalScale(eventData);
        });
        trigger.triggers.Add(Exit);
    }
}
