﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon.LoadBalancing;

public class GameSetup {

	public static GameSetup currentGame;

	public TimelineClient client;
	public string name = "SomeRoom";
	public byte playerCount = 2;
	public bool createGame = true;
	public bool cheat;
	public Deck selectedDeck;

	public GameSetup(string name, byte playerCount, bool createGame, Deck selectedDeck, TimelineClient client)
	{
		this.name = name;
		this.playerCount = playerCount;
		this.createGame = createGame;
		this.selectedDeck = selectedDeck;
		this.client = client;

		currentGame = this;
	}
}
