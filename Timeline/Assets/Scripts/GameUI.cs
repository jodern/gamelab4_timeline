﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class GameUI : MonoBehaviour
{
    public Text ageText;
    public Dictionary<AudioSource, float> sources;
    public Dictionary<Image, Sprite> buttonSprites = new Dictionary<Image, Sprite>();
    public static bool isMuted = false;
    public Image MuteButton;
    public GameObject loadLevelProgressBar;

    // Back button
    private Button backButton;
    public Button defBackButton;

    [Space(10)]
    public GameObject settings, audioSettings, playerSettings;

    void Start()
    {
        if (ageText != null)
            ageText.text = "Date of birth: " + Profile.playerAge;

        if (defBackButton != null)
            backButton = defBackButton;

        sources = new Dictionary<AudioSource, float>();
        AudioSource[] tempSource = FindObjectsOfType<AudioSource>();
        foreach (AudioSource source in tempSource)
        {
            sources.Add(source, source.volume);
        }
        tempSource = null;

        if (isMuted)
        {
            MuteButton.sprite = MuteButton.GetComponent<SpriteSwap>().spriteToUse;
            MuteSources();
        }
    }

    void Update()
    {
        // Press escape on standalone or back on android to open the menu
        if (Input.GetKeyDown(KeyCode.Escape))
            Back();

        if (settings != null && settings.activeInHierarchy)
        {
            if (Input.GetMouseButtonDown(0))
            {
                RectTransform rect = settings.GetComponent<RectTransform>();
                if (!RectTransformUtility.RectangleContainsScreenPoint(rect, Input.mousePosition, Camera.main))
                {
                    Button settingsBtn = GameObject.Find("Settings_Button").GetComponent<Button>();
                    RectTransform btnRect = settingsBtn.GetComponent<RectTransform>();
                    if (!RectTransformUtility.RectangleContainsScreenPoint(btnRect, Input.mousePosition, Camera.main))
                    {
                        if (settingsBtn)
                            settingsBtn.onClick.Invoke();
                    }
                }
            }
        }
    }

    public void Mute()
    {
        isMuted = !isMuted;
        MuteSources();
    }

    // Enable/Disable the menu object
    public void EnableObject(GameObject obj)
    {
        obj.SetActive(!obj.activeInHierarchy);
    }

    public void ButtonState(Button button)
    {
        button.interactable = !button.interactable;
    }

    // Swaps sprite to the new sprite unless it already uses the new one
    // in which case it swaps to the previous sprite
    public void SwapSprite(Sprite spriteToUse)
    {
        Image selected = null;
        selected = EventSystem.current.currentSelectedGameObject.GetComponent<Image>();

        if (!buttonSprites.ContainsKey(selected))
            buttonSprites.Add(selected, selected.sprite);

        if (selected.sprite != spriteToUse)
            selected.sprite = spriteToUse;
        else
            selected.sprite = buttonSprites[selected];
    }

    // Mute all audio sources in the scene
    void MuteSources()
    {
        foreach (var keys in sources.Keys)
        {
            if (keys.volume == 0)
                keys.volume = sources[keys];
            else
                keys.volume = 0;
        }
    }

    public void SetBackButton(Button button)
    {
        if (button == null)
            return;

        backButton = button;
    }

    public void Back()
    {
        if (backButton == null)
        {
            ApplicationQuit();
            return;
        }

        backButton.onClick.Invoke();
    }

    public void LoadLevel(string level)
    {
        // Disconnect and destroy the maintain connection object in the scene
        MaintainConnection con = FindObjectOfType<MaintainConnection>();
        if (con != null)
        {
            con.PhotonDisconnect();
        }

        AsyncOperation async;
        loadLevelProgressBar.SetActive(true);
        System.GC.Collect();
        LoadGame.sceneToLoad = level;
        async = SceneManager.LoadSceneAsync("Loading");

        if (async.progress >= 0.9f)
            Resources.UnloadUnusedAssets();
    }

    public void changeLevel(string level)
    {
        // Disconnect and destroy the maintain connection object in the scene
        MaintainConnection con = FindObjectOfType<MaintainConnection>();
        if (con != null)
        {
            con.PhotonDisconnect();
        }

        GameController.gameRunning = false;
        System.GC.Collect();
        LoadGame.sceneToLoad = level;
        AsyncOperation async = SceneManager.LoadSceneAsync("Loading");

        if(async.progress >= 0.9f)
            Resources.UnloadUnusedAssets();
    }

    public void ApplicationQuit()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer && !Application.isWebPlayer && !Application.isEditor)
        {
#if UNITY_STANDALONE
            //Application.Quit();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
#if UNITY_ANDROID
            Application.Quit();
#endif
        }
    }
}