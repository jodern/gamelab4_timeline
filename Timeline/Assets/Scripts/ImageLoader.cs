﻿using UnityEngine;
using System.Collections;

public class ImageLoader : MonoBehaviour {

	static Sprite defaultSprite;

	public Sprite result;


	public static IEnumerator LoadImage(string url)
	{
		WWW www = new WWW(url);

		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogWarning("Loading image failed: " + www.error);

			//Revert url to default if image load fails
			yield return Resources.Load("Sprites/Default") as Sprite;
		}
		//Get texture
		Texture2D tex = www.texture;
		tex.name = www.url;
		tex.wrapMode = TextureWrapMode.Repeat;

		Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		yield return sprite;
	}


	public static IEnumerator LoadImage(string url, Transform frontLoadingBar)
	{
		WWW www = new WWW(url);

		frontLoadingBar.gameObject.SetActive(true);
		while (!www.isDone) 
		{
			frontLoadingBar.Rotate(0, 0, 360 * Time.deltaTime);
			yield return null;
		}
		frontLoadingBar.gameObject.SetActive(false);

		if(!string.IsNullOrEmpty(www.error))
		{
			Debug.LogWarning("Loading image failed: " + www.error);

			//Revert url to default if image load fails
			yield return Resources.Load("Sprites/Default") as Sprite;
			yield break;
		}
		//Get texture
		Texture2D tex = www.texture;
		tex.name = www.url;
		tex.wrapMode = TextureWrapMode.Repeat;

		Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		yield return sprite;
	}


	public static IEnumerator LoadImage(string url, Transform frontLoadingBar, Transform backLoadingBar)
	{
		if (string.IsNullOrEmpty (url)) 
		{
			Debug.LogWarning ("Aborting image loading: Empty url");
			yield break;
		}

		WWW www = new WWW(url);

		frontLoadingBar.gameObject.SetActive(true);
		backLoadingBar.gameObject.SetActive(true);
		while (!www.isDone) 
		{
			frontLoadingBar.Rotate(0, 0, 360 * Time.deltaTime);
			backLoadingBar.Rotate(0, 0, -360 * Time.deltaTime);
			yield return null;
		}
		frontLoadingBar.gameObject.SetActive(false);
		backLoadingBar.gameObject.SetActive(false);

		if(!string.IsNullOrEmpty(www.error))
		{
			Debug.LogWarning("Loading image failed: " + www.error);

			//Revert url to default if image load fails
			yield return Resources.Load("Sprites/Default") as Sprite;
			yield break;
		}
		//Get texture
		Texture2D tex = www.texture;
		tex.name = www.url;
		tex.wrapMode = TextureWrapMode.Repeat;

		Sprite sprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		yield return sprite;
	}
}
