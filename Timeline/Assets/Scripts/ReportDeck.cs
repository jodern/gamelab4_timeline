﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ReportDeck : MonoBehaviour {

	[SerializeField] InputField reason;
	[SerializeField] Text deckToReport;
	[SerializeField] Button sendReportButton;
	[SerializeField] Button cancelButton;
	[SerializeField] GameObject reportWindow;

	GameController gameController;
	int id = -1;

	// Use this for initialization
	void Start () 
	{
		gameController = FindObjectOfType<GameController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void CheckMsgInput(string msg)
	{
		sendReportButton.interactable = msg.Length > 3;
	}


	public void OpenReportWindow()
	{
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "Main Menu")
		{
			if (gameController.selectedDeck == null)
				return;
			deckToReport.text = gameController.selectedDeck.name;
		}

		reportWindow.SetActive (true);
		sendReportButton.interactable = false;
	}


	public void SendReport()
	{
		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "Main Menu")
		{
			if (gameController.selectedDeck == null)
				return;
			id = gameController.selectedDeck.ID;
		}
		if (id == -1)
			return;
		
		cancelButton.interactable = false;
		StartCoroutine(PostReport());
	}


	IEnumerator PostReport()
	{
		string url = "https://cpanel48.proisp.no/~mediaopc/spo2014/gamelab03/report_post.php";

		WWWForm form = new WWWForm ();
		form.AddField ("user", Profile.playerName ?? "Somebody");
		form.AddField ("deckID", id);
		form.AddField ("msg", reason.text);
		form.AddField ("key", DeckHolder.secretKey);

		WWW www = new WWW (url, form);

		yield return www;

		if (!string.IsNullOrEmpty(www.error))
		{
			Debug.LogWarning("Error posting report: " + www.error);
		}
		else
		{
			if (www.text.Contains("ERROR"))
			{
				Debug.LogWarning("Error posting Report: " + www.text);
			}
			else
			{
				Debug.Log("Successfully posted report: " + www.text);
			}
		}
		cancelButton.interactable = true;
		CloseReportWindow();

	}


	public void CloseReportWindow()
	{
		reportWindow.SetActive (false);
		reason.text = "";
	}


	public void SetReportDeckName(int i)
	{
		deckToReport.text = DeckBrowser.currentDecks[i].name;
		id = DeckBrowser.currentDecks[i].ID;
	}
}
