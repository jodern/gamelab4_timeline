﻿using UnityEngine;
using UnityEngine.UI;

public class SpriteSwap : MonoBehaviour {
    public Sprite original, spriteToUse;
    private Image image;

    void Start()
    {
        image = GetComponent<Image>();
    }

	public void Swap()
    {
        if (image == null)
            return;

        if (image.sprite == original)
            image.sprite = spriteToUse;
        else
            image.sprite = original;
    }
}
