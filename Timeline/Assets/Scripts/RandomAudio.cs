﻿using UnityEngine;
using System.Collections;

public class RandomAudio : MonoBehaviour
{
	public AudioClip[] soundtrack;

	// Use this for initialization
	void Start ()
	{
		if (!GetComponent<AudioSource>().playOnAwake)
		{
            PlayRandomTrack();
        }
	}

	// Update is called once per frame
	void Update ()
	{
		if (!GetComponent<AudioSource>().isPlaying)
		{
            PlayRandomTrack();
        }
	}

    private void PlayRandomTrack()
    {
        GetComponent<AudioSource>().clip = soundtrack[Random.Range(0, soundtrack.Length)];
        GetComponent<AudioSource>().Play();
    }
}