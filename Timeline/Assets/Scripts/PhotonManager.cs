﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Client.Photon.LoadBalancing;
using Hashtable = ExitGames.Client.Photon.Hashtable;


[System.Serializable]
public class ObjectEvent : UnityEvent<object> { }
[System.Serializable]
public class IntEvent : UnityEvent<int> { }
[System.Serializable]
public class MessageEvent : UnityEvent<object, byte> { }
[System.Serializable]
public class DeckEvent : UnityEvent<Deck> { }
[System.Serializable]
public class MsgEvent : UnityEvent<Player, string> { }
[System.Serializable]
public class DistributionEvent : UnityEvent<Dictionary<int, int[]>> { }
[System.Serializable]
public class EndTurnEvent : UnityEvent<Player, int, int, bool> { }
[System.Serializable]
public class PlayerReadyEvent : UnityEvent<bool, int> { }
[System.Serializable]
public class StartGameEvent : UnityEvent<string> { }


public class PhotonManager : MonoBehaviour
{

    [SerializeField]
    Text inputText, outputText;
    [SerializeField]
    string appId;
    [SerializeField]
    Text networkState;
    public TimelineClient client;


    public IntEvent OnJoined;
    public IntEvent OnLeave;
    public IntEvent OnLoadedGame;
    public IntEvent OnReceieveReady;
    public MessageEvent OnRecieveMessage;
    public DeckEvent OnRecievedDeck;
    public DistributionEvent OnRecieveDistributedCards;
    public UnityEvent OnGameFull;
    public UnityEvent OnGameNotReady;
    public IntEvent OnGameOver;
    public PlayerReadyEvent onPlayerReady;
    public StartGameEvent onLoadLevel;

    GameController gameController;
    TurnController turnController;
    PlayerController player;
    SuddenDeath suddenDeathInstance;
    WinCheck winChecker;

    // Debuging
    GameObject _nState;
    bool bDebug = false;

    // Use this for initialization
    void Awake()
    {
        //Destroy photonmanager if there are more than one in the scene
        if (FindObjectsOfType<PhotonManager>().Length > 1)
        {
            Destroy(this);
            return;
        }

        if (!gameObject.activeInHierarchy || GameController.soloPlay)
            return;

        gameController = FindObjectOfType<GameController>();
        turnController = FindObjectOfType<TurnController>();
        suddenDeathInstance = FindObjectOfType<SuddenDeath>();
        winChecker = FindObjectOfType<WinCheck>();

        client = GameSetup.currentGame.client;
        client.networkManager = this;

        if (GameObject.Find("Player0"))
        {
            player = GameObject.Find("Player0").GetComponent<PlayerController>();
        }

        // Set nickname
        client.LocalPlayer.NickName = Profile.playerName;
    }


    void OnEnable()
    {
        if (networkState)
            Application.logMessageReceived += ReceiveDebug;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= ReceiveDebug;
    }


    void Update()
    {
        if (_nState != null && Input.GetKeyDown(KeyCode.O) && !bDebug)
        {
            _nState.GetComponent<Text>().enabled = true;
            bDebug = true;
        }
        else if (_nState != null && Input.GetKeyDown(KeyCode.O) && bDebug)
        {
            _nState.GetComponent<Text>().enabled = false;
            bDebug = false;
        }
    }

    void OnLevelWasLoaded()
    {
        gameController = FindObjectOfType<GameController>();
        turnController = FindObjectOfType<TurnController>();
    }



    IEnumerator CheckConnected()
    {
        yield return new WaitForSeconds(5);
        if (client.State != ClientState.Joined)
        {
            OnClickLoadLevel("Main Menu");
        }
    }



    IEnumerator WaitForClientReady()
    {
        yield return new WaitWhile(() => !client.IsConnectedAndReady);
        //ConnectToRoom();
    }



    /*void ConnectToRoom()
    {
        bool createSuccess;
        print("Client ready: " + client.IsConnectedAndReady);

        if (GameSetup.currentGame.createGame)
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = GameSetup.currentGame.playerCount;
            options.IsVisible = true;
            options.IsOpen = true;
			options.EmptyRoomTtl = 5000;
			options.CustomRoomProperties = new Hashtable();
			options.CustomRoomProperties["name"] = DeckHolder.instance.selectedDeck.name;
			options.CustomRoomProperties["id"] = DeckHolder.instance.selectedDeck.ID;
            createSuccess = client.OpCreateRoom(GameSetup.currentGame.name, options, TypedLobby.Default);
            print(createSuccess);
        }
        else
        {
            if (GameSetup.currentGame.name == "")
            {
                Hashtable expectedProperties = new Hashtable();
                createSuccess = client.OpJoinRandomRoom(expectedProperties, GameSetup.currentGame.playerCount);
                print(createSuccess);
            }
            else
            {
                RoomOptions options = new RoomOptions();
                options.MaxPlayers = GameSetup.currentGame.playerCount;
                options.IsVisible = true;
                options.IsOpen = true;
				options.EmptyRoomTtl = 5000;
				options.CustomRoomProperties = new Hashtable();
				options.CustomRoomProperties["name"] = DeckHolder.instance.selectedDeck.name;
				options.CustomRoomProperties["id"] = DeckHolder.instance.selectedDeck.ID;
                createSuccess = client.OpJoinOrCreateRoom(GameSetup.currentGame.name, options, TypedLobby.Default);
            }
        }
    }*/


    void ReceiveDebug(string message, string stack, LogType logType)
    {
        string s = networkState.text;
        if (networkState != null)
            s += message + stack + "\n";

        //Remove excess lines from top
        int numLines = s.Split('\n').Length;
        if (numLines > 35)
        {
            int newlineIndex = s.IndexOf("\n") + 1;
            s = s.Substring(newlineIndex, s.Length - newlineIndex);
        }

        networkState.text = s;
    }



    /// <summary>
    /// Determines whether this client is the master client.
    /// </summary>
    /// <returns><c>true</c> if this instance is master client; otherwise, <c>false</c>.</returns>
    public bool IsMasterClient()
    {
        if (!client.IsConnectedAndReady)
            return false;
        return client.LocalPlayer.IsMasterClient;
    }



    public bool IsLocalClient(int ID)
    {
        return client.LocalPlayer.ID == ID;
    }


    /// <summary> 
    /// Returns player count in room
    /// </summary>
    /// <returns>The count.</returns>
    public int PlayerCount()
    {
        if (!client.IsConnectedAndReady)
            return -1;

        return client.CurrentRoom.PlayerCount;
    }


    public void LoadedGame()
    {
        byte eventCode = 132;

        object evData = client.LocalPlayer.ID;

        RaiseEventOptions option = new RaiseEventOptions();
        option.Receivers = ReceiverGroup.Others;

        client.OpRaiseEvent(eventCode, evData, true, option);
    }


    public void RecieveLoadedGame(object obj)
    {
        SendGameReady();

        int sender = (int)obj;
        OnLoadedGame.Invoke(sender);
    }


    public void SendGameReady()
    {
        if (!UnityEngine.SceneManagement.SceneManager.GetSceneByName(LoadGame.sceneToLoad).isLoaded)
            return;

        byte eventCode = 12;

        object evData = client.LocalPlayer.ID;

        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;
        Debug.Log("Sending ready");
        client.OpRaiseEvent(eventCode, evData, true, options);
    }


    public void ReceiveGameReady(object obj)
    {
        int sender = (int)obj;
        OnReceieveReady.Invoke(sender);
    }



    public void SendGameOver()
    {
        byte eventCode = 17;
        client.OpRaiseEvent(eventCode, null, true, RaiseEventOptions.Default);
    }



    public void ReceiveGameOver(int WinnerID)
    {
        Debug.Log("Player " + WinnerID + " won the game!");
        OnGameOver.Invoke(WinnerID);
    }


    /// <summary>
    /// Sends a deck to other clients. Can only be called from master
    /// </summary>
    /// <param name="deck">Deck to send</param>
    public void SendDeck(Deck deck)
    {
        if (!client.LocalPlayer.IsMasterClient)
            return;

        byte eventCode = 2;
        Hashtable evdata = new Hashtable();
        evdata["DeckName"] = deck.name;
        evdata["DeckID"] = deck.ID;
        evdata["owner"] = deck.owner;
        evdata["numCards"] = deck.cards.Count;
        for (int i = 0; i < deck.cards.Count; i++)
        {
            evdata["Card" + i + "ID"] = deck.cards[i].ID;
            evdata["Card" + i + "Title"] = deck.cards[i].title;
            evdata["Card" + i + "url"] = deck.cards[i].imageURL ?? "";
            evdata["Card" + i + "OffsetX"] = deck.cards[i].imageOffset.x;
            evdata["Card" + i + "OffsetY"] = deck.cards[i].imageOffset.y;
            evdata["Card" + i + "Zoom"] = deck.cards[i].zoom;
            evdata["Card" + i + "Description"] = deck.cards[i].description;
            evdata["Card" + i + "Year"] = deck.cards[i].year;
            evdata["Card" + i + "Month"] = deck.cards[i].month;
            evdata["Card" + i + "Day"] = deck.cards[i].day;
        }
        client.OpRaiseEvent(eventCode, evdata, true, RaiseEventOptions.Default);
        Debug.Log("Sent deck: " + deck.name + " with " + deck.cards.Count + " cards.");
    }


    public void RecieveDeck(Hashtable table)
    {
        Deck deck = new Deck();
        deck.name = table["DeckName"].ToString();
        deck.owner = table["owner"].ToString();
        deck.ID = (int)table["DeckID"];
        int numCards = (int)table["numCards"];
        for (int i = 0; i < numCards; i++)
        {
            CardData data = new CardData(-1);
            data.ID = (int)table["Card" + i + "ID"];
            data.title = table["Card" + i + "Title"].ToString();
            data.imageURL = table["Card" + i + "url"].ToString();
            data.imageOffset.x = (float)table["Card" + i + "OffsetX"];
            data.imageOffset.y = (float)table["Card" + i + "OffsetY"];
            data.zoom = (float)table["Card" + i + "Zoom"];
            data.description = table["Card" + i + "Description"].ToString();
            data.year = (int)table["Card" + i + "Year"];
            data.month = (CardData.Months)table["Card" + i + "Month"];
            data.day = (byte)table["Card" + i + "Day"];
            deck.cards.Add(data);
        }
        OnRecievedDeck.Invoke(deck);
        Debug.Log("Recieved deck \"" + deck.name + "\", with " + numCards + " cards.");
    }

    /// <summary>
    /// Distributes cards to each respective player.
    /// </summary>
    /// <param name="playerCards">Dictionary taking player id as key and an array of indexes as value.</param>
    public void DistributePlayerCards(Dictionary<int, int[]> playerCards)
    {
        byte eventCode = 11;

        Hashtable evData = new Hashtable();
        foreach (KeyValuePair<int, Player> player in client.CurrentRoom.Players)
        {
            evData.Add(player.Value.ID, playerCards[player.Value.ID]);
        }
        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;

        client.OpRaiseEvent(eventCode, evData, true, options);
    }


    public void RecieveDistributedCards(Hashtable table)
    {
        Dictionary<int, int[]> distributedCards = new Dictionary<int, int[]>();
        foreach (KeyValuePair<int, Player> player in client.CurrentRoom.Players)
        {
            distributedCards.Add(player.Value.ID, table[player.Value.ID] as int[]);
        }
        OnRecieveDistributedCards.Invoke(distributedCards);
    }


    /// <summary>
    /// Sends the end turn.
    /// </summary>
    /// <param name="cardOut">The card that is removed from player hand</param>
    /// <param name="cardInTimeline">The card that player receives from stack. -1 if no card.</param>
    public void SendEndTurn(int cardOut, int cardInTimeline, bool wasSuccessful)
    {
        byte eventCode = 10;
        Hashtable evData = new Hashtable();
        //Card placed in timeline by player
        evData["Sent Card"] = cardOut;
        evData["cardInTimeline"] = cardInTimeline;
        //if there is a card in
        evData["cardSuccess"] = wasSuccessful;

        client.OpRaiseEvent(eventCode, evData, true, RaiseEventOptions.Default);
    }



    public void RecieveEndTurn(Hashtable endTurnTable)
    {
        int cardOut = (int)endTurnTable["Sent Card"];
        int cardInTimeline = (int)endTurnTable["cardInTimeline"];
        bool successfulTimelinePlace = (bool)endTurnTable["cardSuccess"];
        PlayerController senderCtrl = gameController.GetPlayer((int)endTurnTable["Sender"]);
        turnController.NextTurn(successfulTimelinePlace);

        if (cardInTimeline != -1 && cardOut != -1)
            senderCtrl.NetworkPlaceCard(cardInTimeline, cardOut, successfulTimelinePlace);
        else
        {
            turnController.StartTicking();
            turnController.StartThinkSound();
        }

        PlayerController p = gameController.localPlayer;

        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;

        if (p.yourTurn)
        {
            // Turn off card animations
            foreach (GameObject a in p.Cards)
            {
                a.GetComponent<Animator>().SetBool("Selected", true);
            }
        }

        bool setLost = true;
        Debug.Log("bSetLost = " + setLost);
        // Checks if sudden death should commence at the start of a round
        if (turnController.RoundFinished() && SuddenDeath.suddenDeath &&
            suddenDeathInstance.AllWonOrLost())
        {
            Debug.Log("Round over");
            SuddenDeath.roundCount++;

            suddenDeathInstance.CheckRoundCount();

            suddenDeathInstance.ResetPlayerLosing();

            // Give out new cards
            if (SuddenDeath.roundCount != 3)
                SendNetMessage(null, options, 98);
        }
        else if (SuddenDeath.suddenDeath && !suddenDeathInstance.AllWonOrLost() && turnController.RoundFinished())
        {
            Debug.Log("Not all won or lost, give new cards");
            suddenDeathInstance.SetLostPlayers();

            // Give out new cards
            if (SuddenDeath.roundCount != 3)
                SendNetMessage(null, options, 98);
        }
        else if (turnController.RoundFinished() &&
            !SuddenDeath.suddenDeath && suddenDeathInstance.AllWonOrLost())
        {
            Debug.Log("Sending sudden death");
            suddenDeathInstance.SendSuddenDeath();
            suddenDeathInstance.ResetPlayerLosing();
        }
        else if (turnController.RoundFinished() &&
            !SuddenDeath.suddenDeath && !suddenDeathInstance.AllWonOrLost())
        {
            if (gameController.WinCheckLocalPlayer())
                p.Win();

            Debug.Log("not sudden death, not all won or lost, roundfinished");

            SendNetMessage(null, options, 105);

            if (gameController.players.Count >= 3)
            {
                Debug.Log("checking for players with 0 cards");
                setLost = false;
                int l = 0;
                for (int i = 0; i < gameController.players.Count; i++)
                {
                    PlayerController player = gameController.players[i];

                    if (!player.hasLost && !player.Losing && player.Cards.Count == 0)
                        l++;
                }

                if (l >= 2)
                {
                    Debug.Log("Sending sudden death");
                    suddenDeathInstance.SendSuddenDeath();
                }
            }
            if (!p.hasWon)
                suddenDeathInstance.ResetPlayerLosing();
        }

        Debug.Log("bSetLost = " + setLost);

        if (turnController.RoundFinished() && SuddenDeath.suddenDeath && !suddenDeathInstance.AllWonOrLost() && setLost)
        {
            Debug.Log("Start setting who lost");

            suddenDeathInstance.SetLostPlayers();

            //for (int i = 0; i < gameController.players.Count; i++)
            //{
            //    PlayerController l = gameController.players[i];
            //    if (l.Losing)
            //        l.YouLost(l.ID);
            //}
            Debug.Log("Win checking from end of turn");

            if (gameController.WinCheckLocalPlayer())
                p.Win();
        }
        //else if(turnController.RoundFinished() && !SuddenDeath.suddenDeath)
        //{
        //    int index = gameController.GetPlayerIndex(p);

        //    if (winChecker.CheckWinner(index))
        //        p.Win();
        //}
        Debug.Log("Received end turn from Player " + senderCtrl.ID);
    }


    public void SendNewLevel(string levelName)
    {
        client.OpRaiseEvent(89, levelName, true, RaiseEventOptions.Default);
    }

    public void RecieveNewLevel(string levelName)
    {
        FindObjectOfType<LobbyManager>().LoadLevelTimer(levelName);
    }


    /// <summary>
    /// Send custom data in object form
    /// </summary>
    /// <param name="evData">object data to send.</param>
    /// <param name="options">Send options.</param>
    /// </param name>"Event code">Code of the event.</param>
    public void SendNetMessage(object data, RaiseEventOptions options, byte eventCode)
    {
        byte eCode = 1;
        Hashtable evData = new Hashtable();
        evData["code"] = eventCode;
        evData["data"] = data;
        client.OpRaiseEvent(eCode, evData, true, options);
        Debug.Log("Sent " + data);
    }


    public void Join(int id)
    {
        if (OnJoined != null)
            OnJoined.Invoke(id);
    }


    public void Leave(int id)
    {
        if (OnLeave != null)
        {
            OnLeave.Invoke(id);
        }
    }


    public void SendReady(bool readyState)
    {
        byte eventCode = 21;
        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;
        client.OpRaiseEvent(eventCode, readyState, true, options);
    }


    public void ReceieveReady(bool readyState, int sender)
    {
        if (onPlayerReady != null)
            onPlayerReady.Invoke(readyState, sender);
    }


    public void RecieveJoinMessage(object msg)
    {
        outputText.text = msg.ToString();
    }



    public void RecieveMessage(object msg)
    {
        Hashtable table = msg as Hashtable;
        object message = table["data"];
        byte code = (byte)table["code"];
        int sender = (int)table["Sender"];
        OnRecieveMessage.Invoke(message, code);
    }


    public void GameFull()
    {
        Debug.Log("Game is full");
        OnGameFull.Invoke();
    }



    public void OnClickLoadLevel(string level)
    {
        //Photonnenonnect();
        UnityEngine.SceneManagement.SceneManager.LoadScene(level);
        //Destroy (gameObject);
    }


    public void PhotonDisconnect()
    {
        if (client != null && client.loadBalancingPeer != null)
        {
            client.Disconnect();
            client.loadBalancingPeer.StopThread();
        }
        client = null;
    }



    void OnDestroy()
    {
        //PhotonDisconnect();
    }



    void OnApplicationQuit()
    {
        PhotonDisconnect();
    }
}
