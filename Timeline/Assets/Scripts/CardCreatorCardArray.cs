﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CardCreatorCardArray : MonoBehaviour {

    public GameObject cardPrefab;
    static Transform spawn;
    CardCreator creator;
    //Card pool
    List<Card> cardPool = new List<Card>();
	public List<Card> cards = new List<Card>();
    public float cardOffset = 0;
    Vector3 oldmousePos = Vector3.zero;
    bool isMoving = false;
    int numCardsVisible;
    float clampVal;
    public float cardDist = 1.15f;
    float timestamp = 0;
    Transform selectedTransform;
	public float cardScale = 0.5f;
	float waitTimer = 0;
	bool wait = false;

	public static bool isOnTimeline = false;

	// Use this for initialization
	void Awake () 
    {
        creator = GetComponent<CardCreator>();
		spawn = GameObject.Find("CardScalePivot").transform;
		waitTimer = 0;
		wait = false;
		isOnTimeline = false;
		numCardsVisible = (int)(FindObjectOfType<CanvasScaler>().referenceResolution.x / cardDist);
        print("Can fit " + numCardsVisible + " cards on screen.");
	}
	
	// Update is called once per frame
	void Update () 
    {
        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].transform.localScale = Vector3.Lerp(cards[i].transform.localScale, cards[i].targetscale, Time.deltaTime * 10f);
            cards[i]._targetPos = Vector3.Lerp(cards[i]._targetPos, cards[i].targetPos, Time.deltaTime * 10f);
			cards[i].transform.localPosition = spawn.position + new Vector3((i - creator.currentDeck.cards.Count * 0.5f + 0.5f) * cardDist + cardOffset, 0) + cards[i]._targetPos;
        }
		if (wait)
		{
			waitTimer -= Time.deltaTime;
			if (waitTimer <= 0)
			{
				wait = false;
				//Rest targetpos of all cards
				for (int i = 0; i < cards.Count; i++)
				{
					cards [i].targetscale = new Vector3(cardScale, cardScale, cardScale);
					cards[i].targetPos.x = 0;
				}
			}
		}
	}
		

	//Called when player moves mouse over card
	public void OnPointerEnterCard(BaseEventData data)
	{
		//Stop exit if it is still running
		wait = false;

		//Get the gameobject the mouse entered
		PointerEventData pointData = data as PointerEventData;
		GameObject card = pointData.pointerEnter.GetComponentInParent<Card>().gameObject;

		int hitIndex = cards.FindIndex(a => a.gameObject == card);

		cards[hitIndex].targetscale = new Vector3(cardScale * 1.5f, cardScale * 1.5f, cardScale * 1.5f);
		cards[hitIndex].targetPos.x = 0;

		//All cards before hit index
		for (int i = 0; i < hitIndex; i++)
		{
			cards[i].targetPos.x = -cardDist * 0.25f;
			cards [i].targetscale = new Vector3(cardScale, cardScale, cardScale);

		}
		//All cards after hit index 
		for (int i = hitIndex + 1; i < cards.Count; i++)
		{
			cards[i].targetPos.x = cardDist * 0.25f;
			cards [i].targetscale = new Vector3(cardScale, cardScale, cardScale);
		}
	}


	public void OnPointerExitCard(BaseEventData data)
	{
		wait = true;
		waitTimer = 0.25f;
	}


	IEnumerator WaitExitCard()
	{
		yield return new WaitForSeconds (0.25f);
	}


	public void OnPointerBeginDrag()
	{
		oldmousePos = Input.mousePosition;
	}


	public void OnPointerDrag()
	{
		if(Application.isMobilePlatform)
			cardOffset += Input.GetTouch(0).deltaPosition.x;
		else
			cardOffset += (Input.mousePosition - oldmousePos).x;
		cardOffset = Mathf.Clamp(cardOffset, -clampVal, clampVal);

		oldmousePos = Input.mousePosition;
	}


	public void OnPointerEnterTimeline()
	{
		isOnTimeline = true;
	}

	public void OnPointerExitTimeline()
	{
		isOnTimeline = false;
	}


    public void CreateArray()
    {
        //disable all cards if no deck is selected
        if (creator.currentDeck == null)
        {
			cards.Clear ();
            for (int i = 0; i < cardPool.Count; i++)
                cardPool[i].gameObject.SetActive(false);
            return;
        }

        
		cards.Clear ();
        //Disable cards in pool if there are too many
        if (cardPool.Count >= creator.currentDeck.cards.Count)
        {
            for (int i = cardPool.Count - 1; i >= creator.currentDeck.cards.Count; i--)
            {
                cardPool[i].gameObject.SetActive(false);
            }
        }
        //Create new card if the pool is too small
        for (int i = cardPool.Count; i < creator.currentDeck.cards.Count; i++)
        {
            GameObject card = Instantiate(cardPrefab, spawn.position, Quaternion.identity) as GameObject;
			card.transform.localScale = new Vector3(cardScale, cardScale, cardScale);
			card.transform.SetParent(spawn, false);
            Card cardCtrl = card.GetComponentInChildren<Card>();
			EventTrigger trigger = card.GetComponent<EventTrigger> ();
			AddListeners (trigger);
            cardPool.Add(cardCtrl);
        }
        for (int i = 0; i < creator.currentDeck.cards.Count; i++)
        {
            //Only assigns and moves active cards
            cardPool[i].transform.localPosition = spawn.position + new Vector3((i - creator.currentDeck.cards.Count * 0.5f) * cardDist + cardOffset, 0);
            cardPool[i].gameObject.SetActive(true);
			cards.Add(cardPool[i]);
			if(cardPool[i].data == null || cardPool[i].data.ID != creator.currentDeck.cards[i].ID)
			{
				cardPool[i].Assign(creator.currentDeck.cards[i], cardPool[i].transform.GetChild(0));
				cardPool[i].targetscale = new Vector3(cardScale, cardScale, cardScale);
				//Get the delete button and make it call the delete method when clicked
				Button deleteButton = cardPool[i].GetComponentInChildren<UnityEngine.UI.Button>();
				deleteButton.onClick.RemoveAllListeners();
				int _i = i;
				deleteButton.onClick.AddListener(() => OnClickDeleteCard(_i));
			}
        }
        //Find the clamp value
		if (creator.currentDeck.cards.Count < numCardsVisible)
			clampVal = (numCardsVisible - creator.currentDeck.cards.Count) * cardDist * 0.5f;
		else
			clampVal = (creator.currentDeck.cards.Count - numCardsVisible) * cardDist * 0.5f;
        cardOffset = Mathf.Clamp(cardOffset, -clampVal, clampVal);
    }


    //Called when user clicks delete button on card
    void OnClickDeleteCard(int index)
    {
        Debug.Log("Deleting card " + index);
        //Reset all inputs if you are currently editing card the be deleted
        if(creator.currentDeck.cards[index].ID == creator.currentCard.data.ID)
            creator.ResetInput();
        //Remove card
        creator.currentDeck.cards.RemoveAt(index);
        //Update array to match
        CreateArray();
        //Rebuild deck picker to match number of cards
        creator.BuildDeckPicker();
        //Reset offset if all cards were deleted
        if (creator.currentDeck.cards.Count == 0)
            cardOffset = 0;
    }


	//Add listeners for eventtrigger on cards
	void AddListeners(EventTrigger trigger)
	{
		//Add event to be called when mouse over card
		EventTrigger.Entry entry = new EventTrigger.Entry ();
		entry.eventID = EventTriggerType.PointerEnter;
		entry.callback.AddListener ((eventData) => OnPointerEnterCard (eventData));
		trigger.triggers.Add (entry);

		//Add event to be called when mouse exit card
		EventTrigger.Entry exit = new EventTrigger.Entry ();
		exit.eventID = EventTriggerType.PointerExit;
		exit.callback.AddListener ((eventData) => OnPointerExitCard (eventData));
		trigger.triggers.Add (exit);

		//Add listener to be called when player starts drag
		EventTrigger.Entry beginDrag = new EventTrigger.Entry ();
		beginDrag.eventID = EventTriggerType.BeginDrag;
		beginDrag.callback.AddListener ((eventData) => OnPointerBeginDrag ());
		trigger.triggers.Add (beginDrag);

		//Add listener to be called when user drags timeline
		EventTrigger.Entry drag = new EventTrigger.Entry ();
		drag.eventID = EventTriggerType.Drag;
		drag.callback.AddListener ((eventData) => OnPointerDrag ());
		trigger.triggers.Add (drag);

		//Addlistener for when user clicks card 
		EventTrigger.Entry click = new EventTrigger.Entry ();
		click.eventID = EventTriggerType.PointerClick;
		click.callback.AddListener ((eventData) => creator.OnClickEditCard (eventData));
		trigger.triggers.Add (click);
	}
}
