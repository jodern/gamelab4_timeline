﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using ExitGames.Client.Photon.LoadBalancing;

public class TimelineController : MonoBehaviour {

    //cards and positions
    public List<GameObject> cards;
	public Vector3 CardsScale = new Vector3(1, 1, 1);
    [HideInInspector]public Transform cardParent;

	//card and placement info
	GameController gameController;
	TurnController turnController;
	PhotonManager photonManager;
	[HideInInspector]public GameObject selected;
    public Transform startPos;
    public int cardIndex = 0;
	private int numCardsVisible;
	public float spacing = 0;
    int startData;

	// Screen space
	float screenSpace;
	float scaleSpaceRelation;

    void Start () {        
        gameController = FindObjectOfType<GameController> ();
		turnController = FindObjectOfType<TurnController> ();
        cardParent = GameObject.Find("Timeline Cards").transform;       

		if (GameController.soloPlay) {
            StartCoroutine(StartDelay());
		} else {
			photonManager = FindObjectOfType<PhotonManager> ();
			Debug.Log ("network timeline");
		}

		// Finds the avaliable screen space for cards to fit into with a modifier to screen width
		// so cards don't go too far to the side of the screen
		screenSpace = Vector3.Distance(Camera.main.ScreenToWorldPoint (new Vector2(0, Screen.height)),
			Camera.main.ScreenToWorldPoint (new Vector2(Screen.width, Screen.height)));
		//Debug.Log ("Screen space = " + (int)screenSpace);

		scaleSpaceRelation = CardsScale.x / spacing;
    }
    
    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(GameController.startDelayTime);

        if (CardsScale == Vector3.zero)
            CardsScale = gameController.cardFab.transform.localScale;

        startData = Random.Range(2, 5);
        InitialCard();
        Debug.Log("solo timeline");

    }

    public void SetStartIndex()
    {
		if (photonManager.IsMasterClient ()) {
			startData = Random.Range (2, 5);
			RaiseEventOptions evOption = new RaiseEventOptions ();
			evOption.Receivers = ReceiverGroup.Others;
			photonManager.SendNetMessage (startData, evOption, 1);
			Debug.Log ("sent index =  " + startData.ToString ());
		}
    }

	public void GetStartIndex(object index, byte code)
    {
		if (code == 1) {
			startData = (int)index;
			Debug.Log ("neutral card index = " + startData.ToString ());
		}
    }

    public void InitialCard()
	{
        numCardsVisible = (int)(GameObject.Find("Cards").GetComponent<RectTransform>().sizeDelta.x / spacing);
        
		//spawning a startcard
        GameObject startCard = Instantiate(gameController.cardFab, startPos.localPosition, Quaternion.identity) as GameObject;

        startCard.GetComponentInParent<Card>().Assign(gameController.neutralDeck[gameController.neutralDeck.Count - startData], startCard.transform.GetChild(0));

        gameController.neutralDeck.RemoveAt(gameController.neutralDeck.Count - startData);

		startCard.name = "Start Card";
        startCard.transform.SetParent(GameObject.Find("Timeline Cards").transform);
        startCard.GetComponentInParent<Card>().targetPos = startPos.localPosition;

        startCard.transform.localRotation = Quaternion.Euler(gameController.cardRotations);
        startCard.GetComponentInParent<Card>().originalScale = CardsScale;
        startCard.GetComponentInParent<Card>().targetscale = CardsScale;

        if(startCard.GetComponentInParent<Card>().data.day == 0)
        {
            //card.transform.Find("Description").gameObject.SetActive(true);
            Transform k = startCard.transform.GetChild(0);
            k.transform.Find("Date").gameObject.SetActive(false);
            k.transform.Find("Year").gameObject.SetActive(false);
            k.transform.Find("BigYear").gameObject.SetActive(true);
            k.transform.Find("Month").gameObject.SetActive(false);
            k.transform.Find("Day").gameObject.SetActive(false);
            k.transform.Find("FrontTitle").gameObject.SetActive(false);
        }

        AddListener(startCard.GetComponent<EventTrigger>());
        cards.Add(startCard);
    }

	public void newCardPositions(bool correctPlaced)
	{
        if (selected == null) {
			// Calculates new spacing between cards
			numCardsVisible = (int)((screenSpace * 0.8f) / spacing);

			Vector3 oldScaling = CardsScale;
			Vector3 newScaling = oldScaling;
			float oldSpacing = spacing;
			float newSpacing = oldSpacing;

			newSpacing = (screenSpace * 0.8f) / cards.Count;

			// Calculates new scale of cards
			if (newSpacing <= 0.8f)
				newScaling = Vector3.one * (scaleSpaceRelation * spacing);
		
			// Re-sizes and re-calculates the spacing between each card
			if (numCardsVisible < cards.Count + 1 && correctPlaced) {
				spacing = newSpacing;
				CardsScale = newScaling;

				//Debug.Log ("New spacing = " + newSpacing);
				//Debug.Log ("New scaling = " + newScaling);
			}
		}

		// Sets new positions
		for (int i = 0; i < cards.Count; i++) {
			cards [i].transform.SetParent (cardParent);
				cards [i].GetComponentInParent<Card> ().targetPos = new Vector3 ((i - cards.Count * 0.5f + 0.5f) * spacing, 0);
		}

		foreach (GameObject card in cards) {
			card.GetComponentInParent<Card>().originalScale = CardsScale;
			card.GetComponentInParent<Card>().targetscale = CardsScale;
		}

		// Set back elements of card to active and front elements inactive when placing card
		foreach(GameObject card in cards)
		{
			bool fullDate = card.GetComponentInParent<Card>().data.day != 0;

			//card.transform.Find("Description").gameObject.SetActive(true);
			Transform k = card.transform.GetChild(0);
			k.transform.Find("Date").gameObject.SetActive(fullDate);
			k.transform.Find("Year").gameObject.SetActive(fullDate);
			k.transform.Find("BigYear").gameObject.SetActive(!fullDate);
			k.transform.Find("Month").gameObject.SetActive(fullDate);
			k.transform.Find("Day").gameObject.SetActive(fullDate);
			k.transform.Find("FrontTitle").gameObject.SetActive(false);
		}
	}

	public void AddListener(EventTrigger trigger)
	{
		EventTrigger.Entry enter = new EventTrigger.Entry ();
		enter.eventID = EventTriggerType.PointerEnter;
		enter.callback.AddListener ((eventData) => {
			gameController.localPlayer.newCardsScale (eventData);
		});
		trigger.triggers.Add (enter);

		EventTrigger.Entry exit = new EventTrigger.Entry ();
		exit.eventID = EventTriggerType.PointerExit;
		exit.callback.AddListener ((eventData) => {
			gameController.localPlayer.originalScale(eventData);
		});
		trigger.triggers.Add (exit);
	}

	public bool lookForCard()
    {
        CardData cardA = null;
        CardData cardB = null;
        CardData cardC = null;
        int dateA = 0;
        int dateB = 0;
        cardIndex = -1;

		if (!gameController.players [turnController.currentPlayer].isHuman)
			selected = gameController.players [turnController.currentPlayer].randomPos;
		for (int i = 0; i < cards.Count; i++) {
			if (cards [i].transform.position.x < selected.transform.position.x)
				cardIndex = i;
			if (cards [i].transform.position.x > selected.transform.position.x) 
			{
				break;
			}
		}

		if (cardIndex != -1 && cardIndex != cards.Count - 1) {
			cardA = selected.GetComponentInParent<Card> ().data;
			cardB = cards [cardIndex + 1].GetComponentInParent<Card> ().data;
			cardC = cards [cardIndex].GetComponentInParent<Card> ().data;

			dateA = checkingDate (cardA, cardC);
			dateB = checkingDate (cardA, cardB);

			cardIndex += 1;
			if (dateA == 0 || dateB == 0) {
				return true;
			} else if (dateA == -1) {
				return false;
			} else if (dateB == 1) {
				return false;
			} else if (dateA == 1 && dateB == -1) {
				return true;
			} else
				return false;
		} else if (cardIndex == -1) {
			cardA = selected.GetComponentInParent<Card> ().data;
			cardB = cards [cardIndex + 1].GetComponentInParent<Card> ().data;
			dateA = checkingDate (cardA, cardB);

			if (dateA == 0) {
				cardIndex += 1;
				return true;
			} else if (dateA == -1) {
				cardIndex = 0;
				return true;
			} else {
				cardIndex = 0;
				return false;
			}
		} else if (cardIndex == cards.Count - 1) {
			cardA = selected.GetComponentInParent<Card> ().data;
			cardB = cards [cardIndex].GetComponentInParent<Card> ().data;
			dateA = checkingDate (cardA, cardB);
			cardIndex = cardIndex + 1;

			if (dateA == 0) {
				return true;
			} else if (dateA == 1) {
				return true;
			} else if (dateA == 0)
				return true;
			else
				return false;
		} else
			return false;
    }

	public int checkingDate(CardData a, CardData b)
	{
        int c = a.year.CompareTo(b.year);
        if (c == 0)
        {
            int aMonth = (int)a.month;
            int bMonth = (int)b.month;
            c = aMonth.CompareTo(bMonth);
            if (c == 0)
            {
                c = a.day.CompareTo(b.day);
            }
        }
        return c;
    }
}
