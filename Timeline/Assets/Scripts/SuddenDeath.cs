﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon.LoadBalancing;

public class SuddenDeath : MonoBehaviour
{
    private bool giveCardsOnce = true;
    public bool givingNewCards = false;
    public static int roundCount = 0;
    public static bool suddenDeath = false;
    private PhotonManager photonManager;
    private GameController gameController;
    private MaintainConnection maintainConnection;

    // Sudden death notice popup
    public GameObject popup;
    public AudioSource soundSuddenDeath;
    public AudioClip SuddenDeathSound;

    void Start()
    {
        photonManager = FindObjectOfType<PhotonManager>();
        gameController = FindObjectOfType<GameController>();

        if (popup.activeInHierarchy)
            popup.SetActive(false);
    }

    // Send sudden death to all players so the bool gets set true
    public void SendSuddenDeath()
    {
        suddenDeath = CheckSuddenDeath();

        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;

        if (suddenDeath && photonManager.client.LocalPlayer.IsMasterClient)
            photonManager.SendNetMessage(null, options, 93);
    }

    public void SetLostPlayers()
    {
        Debug.Log("Set losing players");
        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;

        foreach (PlayerController p in gameController.players)
        {
            if (p.Losing)
            {
                p.YouLost_NoMessage();
                photonManager.SendNetMessage(p.ID, options, 101);
            }
        }
    }

    public bool AllWonOrLost()
    {
        bool allLostOrWon = false;

        int j = 0;
        int l = 0;
        
        foreach (PlayerController p in gameController.players)
        {
            if (!p.hasLost && p.Losing)
                j++;
            else if (p.hasLost)
                l++;
        }

        int playerCount = photonManager.client.CurrentRoom.PlayerCount;

        if (j == playerCount - l || j == 0 && l != playerCount - 1)
            allLostOrWon = true;
        
        Debug.Log("All won or lost = " + allLostOrWon.ToString() + " J = " + j + " L = " + l);

        return allLostOrWon;
    }

    public bool CheckSuddenDeath()
    {
        bool SD = false;

        Debug.Log("Sudden death check");

        if (CheckAllPlayerCards() >= 2)
            SD = true;

        Debug.Log("Sudden death = " + SD.ToString());
        return SD;
    }

    // Recieve sudden death setting the bool true
    public void RecieveSuddenDeath(object obj, byte code)
    {
        if (code != 93)
            return;

        //foreach (PlayerController p in gameController.players) {
        //	if (p.Cards.Count >= 1)
        //          {
        //              //p.hasLost = true;
        //              if(p.Losing)
        //                  p.YouLost_NoMessage(p.ID);
        //          }
        //      }
        SetLostPlayers();

        suddenDeath = true;
        StartCoroutine(Popup());
        GiveOutNewCards(null, 98);
    }

    IEnumerator Popup()
    {
        popup.SetActive(true);
        soundSuddenDeath.PlayOneShot(SuddenDeathSound);
        yield return new WaitForSeconds(2.2f);
        popup.SetActive(false);
    }

    // Start coroutine to send new cards
    public void GiveOutNewCards(object obj, byte code)
    {
        if (code != 98 || !giveCardsOnce)
            return;

        givingNewCards = true;
        giveCardsOnce = false;
        StartCoroutine(NewCards());
    }

    // Send message to all players to get a new card
    IEnumerator NewCards()
    {
        Debug.Log("new cards");

        if (CheckAllPlayerCards() >= 2)
        {
            foreach (int id in photonManager.client.CurrentRoom.Players.Keys)
            {
                foreach (PlayerController p in gameController.players)
                {
                    Debug.Log("new card for " + p.name);                        

                    if (p.ID == id && p.Cards.Count == 0 && !p.hasLost && !p.hasWon)
                    {
                        p.StartCoroutine(p.spawnCard(0));
                        yield return new WaitForSeconds(1.5f);
                    }
                    else Debug.Log("skipped player");

                    if (p == gameController.players[gameController.players.Count - 1])
                        givingNewCards = false;
                }
            }
        }
        giveCardsOnce = true;
    }

    // Check how many players has no cards left
    public int CheckAllPlayerCards()
    {
        int j = 0;
        foreach (PlayerController player in gameController.players)
            if (player.Cards.Count == 0 && !player.hasLost)
                j++;

        return j;
    }

    public void CheckRoundCount()
    {
        if (roundCount >= 3)
        {
            gameController.localPlayer.Draw();
        }
    }

    public void ResetPlayerLosing()
    {
        foreach (PlayerController p in gameController.players)
            p.Losing = false;
    }
}
