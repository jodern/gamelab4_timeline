﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CardCreator : MonoBehaviour {

	public Card currentCard;
    public Deck currentDeck; 

    public Dropdown deckPicker;
    public InputField newDeckNameInput;
	public InputField titleInput;
	public InputField ImageURLInput;
	public InputField descriptionInput;
	public InputField yearInput, dayInput;
	public Dropdown monthInput;
	public Slider zoom;
	public Toggle yearOnly;

	public Sprite defaultSprite;
	public Image[] previewCardImages;

	public Vector2 offset;
    bool offsetImage = false;
    Vector3 oldMousePos = Vector3.zero;
    string url = "Default";

	public bool moveImage = false;
	                   
	public Text errorDisplay;
	public RectTransform imageMask;
	CardCreatorCardArray cardArray;
	bool cardEditorIsEnabled = false;
	[SerializeField]Transform loadingbar; 



	// Use this for initialization
	void Start () 
	{
		if (!FindObjectOfType<DeckHolder> ()) 
		{
			GameObject deckholder = new GameObject ("Deckholder");
			deckholder.AddComponent<DeckHolder> ();
		}

        //Have all input disabled until user clicks new card
		currentCard.gameObject.SetActive(false);
        SetActiveInput(false);
		cardEditorIsEnabled = false;
		cardArray = GetComponent<CardCreatorCardArray>();
		foreach (Image rend in previewCardImages)
        {
			rend.rectTransform.anchoredPosition = Vector2.zero;
			rend.rectTransform.sizeDelta = new Vector2(120, 120);
			rend.sprite = defaultSprite;
        }
        //Select the last added deck
		if (DeckHolder.instance.storedDecks.Count > 0) 
		{
			currentDeck = DeckHolder.instance.storedDecks [DeckHolder.instance.storedDecks.Count - 1];
			//Build card display array
			cardArray.CreateArray ();
		} 
		else
			newDeckNameInput.gameObject.SetActive (true);
        //Setup the deckpicker and set the starting deck to the bottom one (probably the most recent one)
        BuildDeckPicker();
		deckPicker.value = DeckHolder.instance.storedDecks.Count;
	}

	

	public void OnClickLoadLevel(string level)
	{
		DeckHolder.instance.SaveDecks();
		UnityEngine.SceneManagement.SceneManager.LoadScene (level);
	}



	public void OnClickEditCard(BaseEventData data)
	{
		PointerEventData pData = data as PointerEventData;
		
		Card selectedCard = pData.pointerPress.GetComponentInParent<Card> ();

		//Set editor values to those of selected card
		titleInput.text = selectedCard.data.title;
		if (selectedCard.data.imageURL == "Default")
			ImageURLInput.text = "";
		else
			ImageURLInput.text = selectedCard.data.imageURL;
		descriptionInput.text = selectedCard.data.description;
		yearInput.text = selectedCard.data.year.ToString();
		monthInput.value = (int)selectedCard.data.month;
		dayInput.text = selectedCard.data.day.ToString();

		float texW = selectedCard.transform.GetChild (0).FindChild ("FrontImage").GetComponent<Image> ().rectTransform.sizeDelta.x;
		zoom.maxValue = texW * 10;
		zoom.minValue = texW * 0.1f;
		zoom.value = selectedCard.data.zoom;
		offset = selectedCard.data.imageOffset;
		UpdatePreviewImageOffset ();

		//Assign selected card to preview card
		if (!currentCard.gameObject.activeInHierarchy)
			currentCard.gameObject.SetActive (true);
		currentCard.Assign(selectedCard.data, currentCard.transform.GetChild(0));

		//enable editor
		cardEditorIsEnabled = true;
		SetActiveInput(true);

		url = selectedCard.data.imageURL;
	}



	public void OnBeginDrag()
	{
		oldMousePos = Input.mousePosition;
	}



	public void OnPointerDragPreviewCard()
	{
		if (!cardEditorIsEnabled)
			return;
		
		Vector3 mousePos = Input.mousePosition;
        currentCard.transform.position += Camera.main.ScreenToWorldPoint (mousePos) - Camera.main.ScreenToWorldPoint (oldMousePos);
		oldMousePos = mousePos;
	}



	public void OnPointerDragEndPreviewCard()
	{
		if (CardCreatorCardArray.isOnTimeline)
		{
			DropCard();
		}
		else
		{
			currentCard.transform.localPosition = Vector3.zero;
		}
	}



	//Called when user drags preview card image
	public void OnPointerDragPreviewImage()
	{
		//Abort if card creator isn't enabled or is currently on the default image
        if (!cardEditorIsEnabled || url == "Default")
			return;

		//Get drag for mobile and pc
		if (Application.isMobilePlatform)
			offset += Input.GetTouch (0).deltaPosition;
		else 
		{
			Vector3 mousePos = Input.mousePosition;
			Vector2 deltaMousePos = mousePos - oldMousePos;
			oldMousePos = mousePos;
			offset += deltaMousePos;
		}
		//Clamp offset so image cannot be outside of previewimage
		Vector2 sImg = previewCardImages[0].rectTransform.sizeDelta;
		Vector2 sMask = imageMask.sizeDelta;
		float xClamp;
		if (sImg.x < sMask.x)
			xClamp = (sMask.x - sImg.x) * 0.5f;
		else
			xClamp = (sImg.x - sMask.x) * 0.5f;

		float yClamp;
		if (sImg.y < sMask.y)
			yClamp = (sMask.y - sImg.y) * 0.5f;
		else
			yClamp = (sImg.y - sMask.y) * 0.5f;
        
		offset = new Vector2 (Mathf.Clamp (offset.x, -xClamp, xClamp), Mathf.Clamp (offset.y, -yClamp, yClamp));

		UpdatePreviewImageOffset ();
	}



	void UpdatePreviewImageOffset()
	{
		for (int i = 0; i < previewCardImages.Length; i++)
		{
			previewCardImages[i].rectTransform.anchoredPosition = new Vector3(offset.x,offset.y, previewCardImages[i].rectTransform.localPosition.z);
		}
	}
        

	void DropCard()
	{
		if (SaveCardToDeck())
		{
			//Reset and clear current card if card was saved
			currentCard.transform.localPosition = Vector3.zero;
			currentCard.Assign(new CardData(-1), currentCard.transform.GetChild(0));
			cardEditorIsEnabled = false;
			//Sort current deck by date
			cardArray.CreateArray();
			ResetInput();
			currentCard.gameObject.SetActive (false);
			SetActiveInput (false);
			errorDisplay.text = "";
			moveImage = false;

			DeckHolder.instance.SaveDecks();
		}
		else
		{
			currentCard.transform.localPosition = Vector3.zero;
			moveImage = false;
		}
	}


	//Called when the zoom slider is called
    public void OnImageZoom(float value)
    {
		if (url != "Default")
		{
			for (int i = 0; i < previewCardImages.Length; i++)
			{
				if (previewCardImages [i].sprite != null) 
				{
					Vector2 aspectScale = GetAspectScale (previewCardImages [i].sprite);
					previewCardImages [i].rectTransform.sizeDelta = new Vector2 (value * aspectScale.x, value * aspectScale.y);
					previewCardImages [i].material.mainTextureOffset = new Vector2 (((value - 50) * -0.01f) * aspectScale.x, ((value - 50) * -0.01f) * aspectScale.y) + offset;
				}
			}
		}
    }


	public void OnYearOnlyToggle(bool toggle)
	{
		monthInput.interactable = !toggle;
		monthInput.value = 0;
		dayInput.interactable = !toggle;
		dayInput.text = "";
	}



	Vector2 GetAspectScale(Sprite sprite)
    {
		Vector2 aspectScale = new Vector2(sprite.bounds.size.x, sprite.bounds.size.y);
        if (aspectScale.x >= aspectScale.y)
            aspectScale /= aspectScale.x;
        else
            aspectScale /= aspectScale.y;
        return aspectScale;
    }



	//Called to update deck dropdown
    public void BuildDeckPicker()
    {
        //Add option to create new deck
        deckPicker.ClearOptions();
        System.Collections.Generic.List<string> options = new System.Collections.Generic.List<string>();
        //First option is always to add new decks
        options.Add("New Deck");
        //Add current decks
        for (int i = 0; i < DeckHolder.instance.storedDecks.Count; i++)
        {
            options.Add(DeckHolder.instance.storedDecks[i].name + " (" + DeckHolder.instance.storedDecks[i].cards.Count + ")");
        }
        //Add dropdown options
        deckPicker.AddOptions(options);
    }



	public void ClickOption(bool val)
	{
		if (!val)
			return;

        //Find all items indropdown
		Transform itemList = deckPicker.transform.FindChild ("Dropdown List").FindChild ("Viewport").FindChild("Content");
		int i = 0;
		foreach(Transform item in itemList) 
		{
            //Find all buttons on all items
			int _i = i - 2;
            //Disable delete button for "New Deck" button
			if (i == 1)
				item.FindChild ("Item Delete").gameObject.SetActive (false);
			else 
			{
                //Make all other buttons call Delete method when clicked
				Button button = item.FindChild ("Item Delete").GetComponent<Button> ();
				button.onClick.RemoveAllListeners ();
				button.onClick.AddListener (() => OnClickDeleteDeck (_i));
			}
			i++;
		}
	}



	public void OnClickDeleteDeck(int i)
	{
		print (i);
        bool deleteSelected = false;
		if (currentDeck == DeckHolder.instance.storedDecks [i]) 
		{
			DeckHolder.instance.storedDecks.RemoveAt (i);
            if (DeckHolder.instance.storedDecks.Count > 0)
            {
                deleteSelected = true;
                currentDeck = DeckHolder.instance.storedDecks[DeckHolder.instance.storedDecks.Count - 1];
            }
            else
            {
                currentDeck = null;
                newDeckNameInput.gameObject.SetActive(true);
            }
		}
		else
			DeckHolder.instance.storedDecks.RemoveAt (i);
        Destroy(deckPicker.transform.FindChild("Dropdown List").gameObject);
		cardArray.CreateArray ();
		BuildDeckPicker ();
		deckPicker.RefreshShownValue ();
        if(deleteSelected)
            deckPicker.value = DeckHolder.instance.storedDecks.Count;
	}
		


    //Reset all input fields for new card 
	public void OnClickNewCard()
	{
        SetActiveInput(true);
		cardEditorIsEnabled = true;
		ResetInput();
		currentCard.gameObject.SetActive (true);
	}



	public void ResetInput()
	{
		titleInput.text = "";
		ImageURLInput.text = "";
		descriptionInput.text = "";
		//Set default date to today
		yearInput.text = System.DateTime.Now.Year.ToString();
		monthInput.value = System.DateTime.Now.Month - 1;
		dayInput.text = System.DateTime.Now.Day.ToString("0");
		zoom.value = 120;
		offset = Vector2.zero;
		url = "Default";

		BuildDeckPicker();
		OnChangeDeck();

		//Reset the preview card
		CardData data = new CardData(-1);
		currentCard.Assign(data, currentCard.transform.GetChild(0));
	}



    //Called when user selects new deck in dropdown menu
    public void OnChangeDeck()
    {
		Debug.Log ("Changed deck");
        //First option is create new deck
        if (deckPicker.value == 0)
        {
            newDeckNameInput.gameObject.SetActive(true);
            //Deselect the current deck and wait for user to input a new name
            currentDeck = null;
			cardArray.CreateArray ();
        }
        else if(DeckHolder.instance.storedDecks.Count > 0)
        {
            newDeckNameInput.gameObject.SetActive(false);
            currentDeck = DeckHolder.instance.storedDecks[deckPicker.value - 1];
            cardArray.CreateArray();
            Debug.Log("Selected " + currentDeck.name + "!");
        }
    }



    //Called when user submits a new name after selecting new deck in dropdown menu
    public void OnSubmitDeckName(string name)
    {
        //Reset input field
        newDeckNameInput.text = "";
        //Create a new deck with given name
		Deck newDeck = new Deck(name, "Somebody", -1);
        //Add deck to used decks
        DeckHolder.instance.storedDecks.Add(newDeck);
        //Pick the deck so new cards are added to it
        currentDeck = newDeck;
        //Update array for newly created deck
        cardArray.CreateArray();
		//Update name of deck in deck picker
		BuildDeckPicker();
		deckPicker.value = DeckHolder.instance.storedDecks.Count;
        deckPicker.RefreshShownValue();
        newDeckNameInput.gameObject.SetActive(false);
        Debug.Log("Added new deck: " + name);
    }



    public void SetActiveInput(bool state)
    {
        titleInput.interactable = state;
        ImageURLInput.interactable = state;
        ImageURLInput.GetComponentInChildren<Button>().interactable = state;
        descriptionInput.interactable = state;
        yearInput.interactable = state;
        monthInput.interactable = state;
        dayInput.interactable = state;
        zoom.interactable = state;
		yearOnly.interactable = state;
    }


	public void OnMonthChange(int month)
	{
		previewCardImages[0].transform.parent.FindChild("Month").GetComponent<Text>().text = monthInput.options[month].text;
	}


	public void OnEditYear(string val)
	{
		print ("Editing year");
		int year;
		if (int.TryParse (val, out year)) 
		{
			if (year > 9999 || year < 1) 
			{
				dayInput.text = "1";
				dayInput.interactable = false;
			} 
			else 
			{
				dayInput.text = System.DateTime.Now.Day.ToString ();
				dayInput.interactable = true;
			}
		}
	}



	bool SaveCardToDeck()
	{
		CardData cardData = new CardData(-1);

        bool hasError = false;

		if (titleInput.text != "")
			cardData.title = titleInput.text;
		else 
		{
            DisplayFormatError("Title Cannot be empty!");
			hasError = true;
		}

        cardData.imageURL = url;
        //Default zoom ands offset if url is default
		if (url != "Default") {
			cardData.zoom = zoom.value;
			cardData.imageOffset = offset;
		} 
        
		cardData.description = descriptionInput.text;

        //Check is input is number
		int year;
        if (int.TryParse(yearInput.text, out year))
        {
            cardData.year = year;
        }
        //Display error and abort if not
        else
        {
            DisplayFormatError("Year must be a number!\n");
            hasError = true;
        }
			
		if (yearOnly)
			cardData.month = CardData.Months.January;
		else
			cardData.month = (CardData.Months)monthInput.value;

		byte day = 1;
		if (year > 0 && year <= 9999)
		{
			if (byte.TryParse(dayInput.text, out day))
			{
				if (day < 0)
				{
					DisplayFormatError("Day must be greater than 0!\n");
					dayInput.text = "0";
					hasError = true;
				}
            //Ignore check if something has already caused error
            	else if (!hasError)
				{
					if (day > System.DateTime.DaysInMonth(year, monthInput.value + 1))
					{
						DisplayFormatError("Day must be less than number of days in " + monthInput.options[monthInput.value].text + "!\n");
						dayInput.text = System.DateTime.DaysInMonth(year, monthInput.value + 1).ToString();
						hasError = true;
					}
				}
				else if (day > 31)
				{
					DisplayFormatError("Day must be less than number of days in month!\n");
					dayInput.text = System.DateTime.DaysInMonth(year, monthInput.value).ToString();
					hasError = true;
				}
			}
			else
			{
				DisplayFormatError("Day must be a number!\n");
				dayInput.text = "0";
				hasError = true;
			}
		}
		if (yearOnly)
			cardData.day = 0;
		else
			cardData.day = day;


        if (currentDeck == null)
        {
            DisplayFormatError("Pick a deck, dofus!");
            hasError = true;
        }

        //Abort save if there is error 
		if (hasError) 
		{
			print ("Save aborted with error!");
			return false;
		}


		cardEditorIsEnabled = false;

		//Check if current deck already contains a card with ID and overwrite if it does
		bool overwrite = false;
		for (int i = 0; i < currentDeck.cards.Count; i++)
		{
			if (currentDeck.cards[i].ID == currentCard.data.ID)
			{
				currentDeck.cards[i] = cardData;
				overwrite = true;
			}
		}

		//If no cards was overwritten, add instead
		if(!overwrite)
        	currentDeck.cards.Add(cardData);

		//Resort cards in deck according to date
		currentDeck.SortCards();
        //Rebuild deck picker to update number of cards
        BuildDeckPicker();
        Debug.Log("Saved card: " + cardData.title + " to deck: " + currentDeck.name);
		return true;
	}


	bool isLoadingImage = false;
    public void OnClickLoadImage()
    {
        if(!isLoadingImage)
            StartCoroutine(LoadImage());
    }


    IEnumerator LoadImage()
    {
        print("loading image");
        isLoadingImage = true;
        url = ImageURLInput.text;

		CoroutineWithData cd = new CoroutineWithData(this, ImageLoader.LoadImage(url,loadingbar));
		yield return cd.coroutine;
		Sprite sprite = cd.result as Sprite;

		if(sprite.name == "Default")
			url = "Default";

		//Store url in case user removes it
		url = ImageURLInput.text;
     
		Vector2 aspectScale = GetAspectScale (sprite);
        for (int i = 0; i < previewCardImages.Length; i++)
        {
			previewCardImages[i].sprite = sprite;
			previewCardImages [i].SetNativeSize ();
			previewCardImages [i].rectTransform.sizeDelta = new Vector2 (previewCardImages [i].rectTransform.sizeDelta.x * aspectScale.x, previewCardImages [i].rectTransform.sizeDelta.y * aspectScale.y);
        }
		zoom.value = previewCardImages [0].rectTransform.sizeDelta.x;
		zoom.minValue = previewCardImages [0].rectTransform.sizeDelta.x * 0.1f;
		zoom.maxValue = previewCardImages [0].rectTransform.sizeDelta.x * 10;

        isLoadingImage = false;
    }


	//Dev feature
	public void GenerateRandomCards(string val)
	{
		int numCards = int.Parse (val);
		numCards = Mathf.Clamp (numCards, 1, 100);
		for (int i = 0; i < numCards; i++) 
		{
			CardData data = new CardData (-1);
			data.title = i + "-" + "-1";
			data.year = i;
			data.month = (CardData.Months)Random.Range (0, 12);
			data.day = (byte) Random.Range (1, 29);
			currentDeck.cards.Add (data);
		}
		//Rebuild deck picker to display correct value
		BuildDeckPicker ();
		cardArray.CreateArray ();
	}


	void DisplayFormatError(string error)
	{
        Debug.Log(error);
        StopCoroutine("FadeText");
        errorDisplay.gameObject.SetActive(true);
        errorDisplay.text += error;
        StartCoroutine(FadeText());
	}


    IEnumerator FadeText()
    {
        yield return new WaitForSeconds(4);
        Color color = new Color(errorDisplay.color.r, errorDisplay.color.g, errorDisplay.color.g, 1);
        while (color.a > 0)
        {
            color.a -= Time.deltaTime * 0.5f;
            errorDisplay.color = color;
            yield return null;
        }
        color.a = 1;
        errorDisplay.color = color;
        errorDisplay.text = "";
        errorDisplay.gameObject.SetActive(false);
    }
}
