﻿using UnityEngine;
using System.Collections;
using System;

public class IDGenerator {

	//Generate unique ID with unique device id + time + random number
	public static string GenerateID()
	{

		int timestamp = Convert.ToInt32((DateTime.UtcNow - new DateTime (1970, 1, 1, 8, 0, 0)).TotalSeconds);

		string ID =
			SystemInfo.deviceUniqueIdentifier +
			"-" + timestamp +
			"-" + Time.time +
			"-" + UnityEngine.Random.Range(0, 1000000);

		return ID;
	}
}
