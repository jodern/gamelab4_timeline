﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ExitGames.Client.Photon.LoadBalancing;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class LobbyManager : MonoBehaviour
{
    public Button startGame;
    public GameObject readyToggle;
    private PhotonManager photonManager;
    private MaintainConnection maintainConnection;
    public GameObject infoObject;
    public Image loadLevelProgressBar;
    public Text RoomName;
    public Button startButton;
    AsyncOperation async;
    int deckID = 43;

    // Start timer
    private bool bStartCountDown = false;
    public float timeToStart = 5;
    private float countDown = 5;
    public Text timer;
    public string sceneToLoadName = "Board1";
    private IEnumerator startGameIE;

    // Player info, amount of ready players
    public int readyPlayers;
    public Dictionary<int, PlayerInfo> players = new Dictionary<int, PlayerInfo>();

    // Use this for initialization
    void Start()
    {
        countDown = timeToStart;
        timer.text = "";
        photonManager = FindObjectOfType<PhotonManager>();
        maintainConnection = FindObjectOfType<MaintainConnection>();
        loadLevelProgressBar.transform.parent.gameObject.SetActive(false);

        startButton.interactable = false;

        foreach (MaintainConnection k in FindObjectsOfType<MaintainConnection>())
        {
            if (k != maintainConnection)
                Destroy(k.gameObject);
        }

        if (!maintainConnection.client.IsConnected || !maintainConnection.client.LocalPlayer.IsMasterClient)
        {
            startGame.gameObject.SetActive(false);
            readyToggle.SetActive(false);
        }
        if (maintainConnection.client.LocalPlayer.IsMasterClient)
        {
            deckID = DeckHolder.instance.selectedDeck.ID;
        }
        if (DeckHolder.instance.selectedDeck != null)
            Debug.Log("Selected deck: " + DeckHolder.instance.selectedDeck.name + " : " + DeckHolder.instance.selectedDeck.ID);
    }

    void Update()
    {
        if (async != null)
            loadLevelProgressBar.fillAmount = Mathf.MoveTowards(loadLevelProgressBar.fillAmount, async.progress, 0.5f * Time.deltaTime);

        try
        {
            RoomName.text = maintainConnection.client.CurrentRoom.Name;

            if (!maintainConnection.client.IsConnectedAndReady)
                RoomName.text = "You're disconnected fella!";
        }
        catch
        {
            RoomName.text = "You're disconnected fella!";
        }
        
        // Count down timer to start the game
        if (bStartCountDown && countDown > 0)
        {
            countDown -= Time.deltaTime;
            timer.text = countDown.ToString("Starting in " + "0");
        }
    }


    public void SendDeckIDOnJoin(int p)
    {
        if (!maintainConnection.client.LocalPlayer.IsMasterClient)
            return;

        int id = DeckHolder.instance.selectedDeck.ID;
        photonManager.SendNetMessage(id, RaiseEventOptions.Default, 66);
    }


    public void ReceieveDeckID(object obj, byte code)
    {
        if (code != 66)
            return;

        deckID = (int)obj;
        Debug.Log("Received deck id = " + deckID);

        DeckHolder.instance.selectedDeck = FindDeckByID(deckID);
    }


    private Deck FindDeckByID(int deckId)
    {
        Deck deckToReturn = null;

        foreach (Deck d in DeckHolder.instance.storedDecks)
        {
            if (d.ID == deckId)
                deckToReturn = d;
        }

        return deckToReturn;
    }

    public void SendReady(bool ready)
    {
        // Send ready message to all players
        photonManager.SendReady(ready);

        if (ready)
            Debug.Log("You are ready");
        else
            Debug.Log("You are not ready");
    }

    public void RecieveReady(bool isReady, int id)
    {
        // Get ready message from a player
        if (isReady)
            readyPlayers++;
        else if (!isReady && readyPlayers > 0)
        {
            readyPlayers--;
            CancelStartGame();
        }

        players[id].SetReady(isReady);

        if (photonManager.PlayerCount() >= 2)
        {
            startButton.interactable = readyPlayers == photonManager.PlayerCount() ? startButton.interactable = true : startButton.interactable = false;
        }       
    }

    public void GetDBDeck(Deck[] d)
    {
        if (d != null)
            DeckHolder.instance.selectedDeck = d[0];
        else
            DeckHolder.instance.selectedDeck = DeckHolder.instance.defaultDecks[0];
    }

    public void GetNewPlayer(int id)
    {
        if (photonManager.IsMasterClient())
        {
            startGame.gameObject.SetActive(true);
        }
        else
        {
            startGame.gameObject.SetActive(false);
        }

        if (id == maintainConnection.client.LocalPlayer.ID)
        {
            Hashtable table = new Hashtable();
            table["name"] = Profile.playerName;
            table["id"] = id;
            RaiseEventOptions options = new RaiseEventOptions();
            options.Receivers = ReceiverGroup.All;

            photonManager.SendNetMessage(table, options, 106);
        }

        if (maintainConnection.client.LocalPlayer.IsMasterClient)
            SendNames();

        CancelStartGame();
        readyToggle.SetActive(true);
        int playerCount = maintainConnection.client.CurrentRoom.Players.Count();

        GameObject playerInfo = Instantiate(infoObject, Vector3.zero, Quaternion.identity) as GameObject;

        //string playerName = maintainConnection.client.CurrentRoom.Players[id].NickName;
        //playerName = "Player" + id;
        string playerName = Profile.playerName;
        Debug.Log(playerName + " joined the game");

        playerInfo.name = playerName;
        playerInfo.transform.SetParent(GameObject.Find("PlayerContainer").transform);
        PlayerInfo k = playerInfo.GetComponent<PlayerInfo>();
        k.SetInfo(id, playerName);

        players.Add(k.id, k);
        int Id = 0;
        for (int i = 0; i < playerCount; i++)
        {
            Id = maintainConnection.client.CurrentRoom.Players.ElementAt(i).Key;

            if (!players.ContainsKey(Id) && Id != maintainConnection.client.LocalPlayer.ID)
            {
                GameObject otherPlayerInfo = Instantiate(infoObject, Vector3.zero, Quaternion.identity) as GameObject;
                //otherPlayerInfo.name = lol;
                otherPlayerInfo.transform.SetParent(GameObject.Find("PlayerContainer").transform);
                PlayerInfo j = otherPlayerInfo.GetComponent<PlayerInfo>();
                j.SetInfo(Id, j.playerName);
                players.Add(j.id, j);
            }
        }

        if (readyPlayers == photonManager.PlayerCount() && maintainConnection.client.LocalPlayer.IsMasterClient)
        {
            startButton.interactable = true;
        }
    }

    void SendNames()
    {
        PlayerInfo[] info = FindObjectsOfType<PlayerInfo>();

        foreach (PlayerInfo k in info)
        {
            Hashtable table = new Hashtable();
            table["name"] = k.playerName;
            table["id"] = k.id;
            RaiseEventOptions options = new RaiseEventOptions();
            options.Receivers = ReceiverGroup.All;

            photonManager.SendNetMessage(table, options, 106);
        }
    }

    public void ReceiveName(object name, byte code)
    {
        if (code != 106)
            return;

        Hashtable table = name as Hashtable;
        players[(int)table["id"]].playerName = (string)table["name"];

        int Id = 0;
        int playerCount = maintainConnection.client.CurrentRoom.Players.Count();

        for (int i = 0; i < playerCount; i++)
        {
            Id = maintainConnection.client.CurrentRoom.Players.ElementAt(i).Key;

            if (Id == (int)table["id"])
            {
                players[Id].SetInfo(Id, (string)table["name"]);

                if (!GameController.idAndName.ContainsKey(Id))
                    GameController.idAndName.Add(Id, (string)table["name"]);
            }
        }
    }

    public void RemovePlayer(int id)
    {
        Player player = new Player(id.ToString(), id, false);
        players[id].gameObject.SetActive(false);
        maintainConnection.PhotonRemovePlayer(player);
        players.Remove(id);
        Debug.Log("Removed player with ID" + id);
        CancelStartGame();
        if (maintainConnection.client.LocalPlayer.IsMasterClient)
        {
            Debug.Log("deck id = " + deckID);

            WWWForm form = new WWWForm();
            form.AddField("id", deckID);
            form.AddField("key", DeckHolder.secretKey);

            //DeckHolder.instance.LoadFromDB("deck_getId.php", GetDBDeck, form);
        }
    }

    // Count down timer to starting the game
    public void StartGame(string levelName)
    {
        // Begin game if everyone is ready
        if (photonManager.PlayerCount() >= 2 && readyPlayers == photonManager.PlayerCount())
        {
            Debug.Log("Game is starting from lobby");
            startButton.interactable = false;
            photonManager.SendNewLevel(levelName);

            startGameIE = startCountDown(levelName);
            StartCoroutine(startGameIE);
        }
    }

    public void CancelStartGame()
    {
        if (startGameIE == null)
            return;

        Debug.Log("Stopped start game");
        StopCoroutine(startGameIE);
        bStartCountDown = false;
        countDown = timeToStart;
        timer.text = "";
    }

    public void loadLevel(string levelName)
    {
        players.Clear();

        if (levelName == "Main Menu" && DeckHolder.instance != null)
        {
            DeckHolder.instance.selectedDeck = null;
        }

        if (loadLevelProgressBar != null)
            loadLevelProgressBar.transform.parent.gameObject.SetActive(true);

        LoadGame.sceneToLoad = levelName;
        async = SceneManager.LoadSceneAsync("Loading");
    }

    public void LoadLevelTimer(string levelName)
    {
        startGameIE = startCountDown(levelName);
        StartCoroutine(startGameIE);
    }

    IEnumerator startCountDown(string levelName)
    {
        bStartCountDown = true;
        yield return new WaitForSeconds(countDown);

        loadLevel(levelName);
    }
}
