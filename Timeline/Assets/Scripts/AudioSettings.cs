﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSettings : MonoBehaviour
{
    public AudioMixer masterMixer;
    public Slider masterSlider, musicSlider, soundsSlider;

    void Start()
    {
        SetDefaultLevels();
    }

    private void SetDefaultLevels()
    {
        // Set saved volume levels
        float vol = PlayerPrefs.GetFloat("MasterVol");
        SetMasterVolume(vol);

        if (masterSlider != null)
            masterSlider.value = vol;

        vol = PlayerPrefs.GetFloat("MusicVol");
        SetMusicVolume(vol);

        if (musicSlider != null)
            musicSlider.value = vol;

        vol = PlayerPrefs.GetFloat("SoundsVol");
        SetSoundsVolume(vol);

        if (soundsSlider != null)
            soundsSlider.value = vol;
    }

    public void SetMasterVolume(float value)
    {
        PlayerPrefs.SetFloat("MasterVol", value);
        masterMixer.SetFloat("MasterVolume", value);
    }

    public void SetMusicVolume(float value)
    {
        PlayerPrefs.SetFloat("MusicVol", value);
        masterMixer.SetFloat("MusicVolume", value);
    }

    public void SetSoundsVolume(float value)
    {
        PlayerPrefs.SetFloat("SoundsVol", value);
        masterMixer.SetFloat("SoundsVolume", value);
    }
}

