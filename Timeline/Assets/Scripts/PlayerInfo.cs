﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour {
	public int id;
	public string playerName;
	public bool ready;

	public Text txtName;
	public Toggle tglReady;

	public void SetInfo(int Id, string Name)
	{
		id = Id;
		playerName = Name;
        name = playerName;
		txtName.text = playerName;
	}

	public void SetReady(bool isReady)
	{
		ready = isReady;
		tglReady.isOn = ready;
	}
}
