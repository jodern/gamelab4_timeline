/*
 * Copyright (C) 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using UnityEngine.SocialPlatforms;
#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

public class GoogleAuthenticator : MonoBehaviour
{
    private bool mWaitingForAuth = false;
    private string mStatusText = "Ready.";
    private const float FontSizeMult = 0.05f;

    public GameObject loginMenu;
    public GameObject profileMenu;
    public GameObject skipButton;
    [SerializeField]
    UnityEngine.UI.Text feedbackText;
    public static bool AutoLogin = false;
    public static bool authenticated = false;
    public bool bDebug = false;

    void Start()
    {
#if UNITY_ANDROID
        authenticated = false;

        // Select the Google Play Games platform as our social platform implementation
        PlayGamesPlatform.Activate();

        //playGamesClientConfiguration config = new playGamesClientConfiguration ().Builder.Build ();
        //PlayGamesPlateform.InitializeInstance (config);

        AutoLogin = PlayerPrefs.GetInt("Auto") == 1;

        if (AutoLogin)
            Authenticate();
#endif

#if UNITY_EDITOR || UNITY_STANDALONE
        if (profileMenu != null)
            profileMenu.SetActive(true);

        if (loginMenu.activeInHierarchy)
            loginMenu.SetActive(false);

        if (skipButton != null)
            skipButton.SetActive(false);
#endif
    }

    public void Authenticate()
    {
#if UNITY_ANDROID
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        if (mWaitingForAuth)
        {
            return;
        }

        if (!Social.localUser.authenticated)
        {
            // Authenticate
            mWaitingForAuth = true;
            Social.localUser.Authenticate((bool success) =>
            {
                mWaitingForAuth = false;

                if (success)
                {
                    authenticated = true;
                    //string token = PlayGamesPlatform.Instance.GetToken();
                    //Debug.Log(token);
                    PlayerPrefs.SetInt("Auto", 1);

                    profileMenu.SetActive(true);
                    loginMenu.SetActive(false);

                    Profile profile = FindObjectOfType<Profile>();
                    profile.getOnlineProfile();
                    profile.SetName();
                    profile.SetAge();

                    string id = PlayGamesPlatform.Instance.GetUserId();
                    PlayerPrefs.SetString("GoogleID", id);
                    Debug.Log("Logged in as " + id);
                    feedbackText.text = "Logged in as " + id;
                }
                else
                {
                    PlayerPrefs.SetInt("Auto", 1);
                    loginMenu.SetActive(true);
                    Debug.Log("Login Failed");
                    feedbackText.text = "Login Failed!";
                }
            });

            //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

            //PlayGamesPlatform.InitializeInstance(config);
            //// recommended for debugging:
            //PlayGamesPlatform.DebugLogEnabled = false;
        }


#endif
    }


    public void Skip()
    {
        profileMenu.SetActive(true);
        loginMenu.SetActive(false);
#if UNITY_ANDROID
        FindObjectOfType<Profile>().getOnlineProfile();
#endif
    }

    /*void OnGUI()
    {
        #if UNITY_ANDROID
        GUI.skin.button.fontSize = (int)(FontSizeMult * Screen.height);
        GUI.skin.label.fontSize = (int)(FontSizeMult * Screen.height);

        GUI.Label(new Rect(20, 20, Screen.width, Screen.height * 0.25f),
                  mStatusText);

        Rect buttonRect = new Rect(0.25f * Screen.width, 0.10f * Screen.height,
                          0.5f * Screen.width, 0.25f * Screen.height);
        Rect imageRect = new Rect(buttonRect.x + buttonRect.width / 4f,
                                  buttonRect.y + buttonRect.height * 1.1f,
                                  buttonRect.width / 2f, buttonRect.width / 2f);

        if (mWaitingForAuth)
        {
            return;
        }

        string buttonLabel;


        if (Social.localUser.authenticated)
        {
            buttonLabel = "Sign Out";
            if (Social.localUser.image != null)
            {
                GUI.DrawTexture(imageRect, Social.localUser.image,
                                ScaleMode.ScaleToFit);
            }
            else
            {
                GUI.Label(imageRect, "No image available");
            }

            mStatusText = "Ready";
        }
        else
        {
            buttonLabel = "Authenticate";
        }

        if (GUI.Button(buttonRect, buttonLabel))
        {
            if (!Social.localUser.authenticated)
            {
                // Authenticate
                mWaitingForAuth = true;
                mStatusText = "Authenticating...";
                Social.localUser.Authenticate((bool success) => {
                    mWaitingForAuth = false;
                    if (success)
                    {
                        mStatusText = "Welcome " + Social.localUser.userName;
                        string token = PlayGamesPlatform.Instance.GetToken();
                        Debug.Log(token);
                    }
                    else
                    {
                        mStatusText = "Authentication failed.";
                    }
                });
            }
            else
            {
                // Sign out!
                mStatusText = "Signing out.";
                ((PlayGamesPlatform)Social.Active).SignOut();
            }
        }
#endif
    }*/
}

