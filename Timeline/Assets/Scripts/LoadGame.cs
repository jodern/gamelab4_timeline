﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadGame : MonoBehaviour
{
    public static string sceneToLoad = "Main Menu";
    public GameObject cam;

    void Start()
    {
        Load(sceneToLoad);
    }

   public void Load(string level)
    {
        StartCoroutine(LoadLevel(level));
    }
	 
    IEnumerator LoadLevel(string level)
    {
        LoadSceneMode loadMode = LoadSceneMode.Additive;
		yield return SceneManager.LoadSceneAsync(level, loadMode);
        
		//yield return new WaitUntil(() => async.isDone);

		if (level == "Board1" && !GameController.soloPlay) 
		{
			PhotonManager photonManager = FindObjectOfType<PhotonManager> ();
			if (photonManager) 
			{
				photonManager.SendGameReady ();
				photonManager.LoadedGame ();
			}
		}

        cam.SetActive(false);
        SceneManager.UnloadScene(1);
    }
}
