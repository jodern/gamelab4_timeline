﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using ExitGames.Client.Photon.LoadBalancing;

public class TurnController : MonoBehaviour
{
    // UI
    public Text timer;
    public Image watch;
    public Sprite openWatch;
    public Sprite closedWatch;
    public Text turn;
    public static bool stop = false;

    public int currentPlayer;
    [HideInInspector]
    public PlayerController p;
    private GameController gameController;
    private TimelineController timelineController;
    private PhotonManager photonManager;
    private MaintainConnection maintainConnection;
    private RandomAnimation squirrel;
    public float EndTime = 60f;
    private float endTime = 0;

    private IEnumerator tickingCoroutine;
    private IEnumerator thinkingCoroutine;

    private bool gameStarted = false;

    public AudioSource soundSquirrel;
    public AudioClip squirrelThink;
    public AudioSource soundClock;
    public AudioClip clockTicking;
    public AudioClip OpenCloseWatch;
    
    void Start()
    {
        gameStarted = false;
        endTime = 0;
        timer.text = "";
        turn.text = "";

        gameController = FindObjectOfType<GameController>();
        timelineController = FindObjectOfType<TimelineController>();
        maintainConnection = FindObjectOfType<MaintainConnection>();

        if (GameController.soloPlay)
        {
            StartCoroutine(StartDelay());
        }
        else
        {
            photonManager = FindObjectOfType<PhotonManager>();
            try
            {
                squirrel = GameObject.Find("Player0").GetComponent<PlayerController>().squirrel.GetComponent<RandomAnimation>();
            }
            catch
            {
                Debug.Log("Local player squirrel is either null or th tmpPlayer is null");
            }

            //StartCoroutine(StartDelay());
        }
    }

    IEnumerator StartDelay()
    {
        yield return new WaitForSeconds(GameController.startDelayTime);
        SetTime();

        PlayerController tmpPlayer = GameObject.Find("Player0").GetComponent<PlayerController>();

        try
        {
            squirrel = tmpPlayer.squirrel.GetComponent<RandomAnimation>();
        }
        catch
        {
            Debug.Log("Local player squirrel is either null or th tmpPlayer is null");
        }

        // Ticking
        //if (HasTurn())
        //{
        //    StartCoroutine(ThinkSound());
        //    StartCoroutine(Ticking());
        //}
        //Debug.Log(tmpPlayer.name + " " + tmpPlayer.yourTurn);

        tmpPlayer = null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameStarted || stop)
            return;

            StartTurn();
    }

    public void SetTime()
    {
        gameStarted = true;
        stop = false;
        endTime = EndTime;

        if (photonManager != null)
            if (HasTurn())
                p.yourTurn = true;
    }

    public void StartTurn()
    {
        if (endTime > 0)
            endTime -= Time.deltaTime;
        else if (endTime <= 0)
        {
            endTime = 0;
            if (photonManager != null)
            {
                if (HasTurn())
                    EndTurn(true);
            }
            else
                EndTurn(false);
        }

        int displayNum = currentPlayer + 1;
        if (photonManager != null)
        {
            if (HasTurn())
            {
                //turn.text = "Your turn!";
                if (watch != null && openWatch != null)
                    watch.sprite = openWatch;
                if (timer != null)
                    timer.gameObject.SetActive(true);
            }
            else
            {
                //turn.text = "Player " + displayNum + " turn";
                if (watch != null && closedWatch != null)
                    watch.sprite = closedWatch;
                if (timer != null)
                    timer.gameObject.SetActive(false);
            }
        }
        else
        {
            if (currentPlayer == 0)
            {
                //turn.text = "Your turn!";
                if (watch != null && openWatch != null)
                    watch.sprite = openWatch;
                timer.gameObject.SetActive(true);
            }
            else
            {
                //turn.text = "Player " + displayNum + " turn";
                if(watch != null && closedWatch != null)
                    watch.sprite = closedWatch;
                if (timer != null)
                    timer.gameObject.SetActive(false);
            }
        }

        if(timer != null)
            timer.text = endTime.ToString("0");
    }

    //ends a turn and shuts player control on/off
    public void EndTurn(bool timeOut)
    {
        if (p.selected)
            p.PlaceOnPlayerDeck();

        // Ticking
        if (HasTurn())
        {
            EndThinkCoroutine();
            EndTicking();
        }

        p.yourTurn = false;

        UpdateCurrentPlayer();

        if (photonManager == null)
        {
            gameController.players[currentPlayer].yourTurn = true;
            gameController.players[currentPlayer].selected = null;
            StartTicking();
            StartThinkSound();
        }

        if (!gameController.players[currentPlayer].isHuman)
            gameController.players[currentPlayer].AIGetSelected();

        endTime = EndTime;

        timelineController.newCardPositions(false);
        for (int i = 0; i < timelineController.cards.Count; i++)
        {
            timelineController.cards[i].GetComponent<Card>().targetscale = timelineController.cards[i].GetComponent<Card>().originalScale;
        }

        if (HasTurn())
        {
            // Turn card animations on
            foreach (GameObject a in p.Cards)
            {
                a.GetComponent<Animator>().SetBool("Selected", true);
            }
        }
        else
        {
            // Turn off card animations
            foreach (GameObject a in p.Cards)
            {
                a.GetComponent<Animator>().SetBool("Selected", false);
            }
        }
        
        if (timeOut && photonManager != null)
        {
            photonManager.SendEndTurn(-1, -1, false);
            return;
        }

        if (photonManager != null)
            SendEndTurnData();
    }

    void SendEndTurnData()
    {
        p.yourTurn = false;
        int networkTimelineIndex = p.networkCardIndex;
        int networkSelectedIndex = p.networkSelectedIndex;
        bool correct = p.correct;
        Debug.Log("SelectedIndex = " + networkSelectedIndex + " TimelineIndex = " + networkTimelineIndex + " timeline card index = " + timelineController.cardIndex);

        photonManager.SendEndTurn(networkSelectedIndex, networkTimelineIndex, correct);
    }

    public void NextTurn(bool correct)
    {
        endTime = EndTime;
        UpdateCurrentPlayer();
        if (photonManager != null)
            if (HasTurn() && !correct)
                StartCoroutine(YourTurnDelay(5f));
            else if (HasTurn() && correct)
                p.yourTurn = true;
    }

    IEnumerator YourTurnDelay(float time)
    {
        yield return new WaitForSeconds(time);
        p.yourTurn = true;
    }

    public bool HasTurn()
    {
        bool updateOnce = true;
        try
        {
            if (maintainConnection != null)
            {
                if (currentPlayer <= maintainConnection.client.CurrentRoom.PlayerCount)
                    return maintainConnection.client.CurrentRoom.Players.Values.ElementAt(currentPlayer).ID == photonManager.client.LocalPlayer.ID;
                else
                    return false;
            }
            else
                return false;
        }
        catch
        {
            if(updateOnce)
            {
                Debug.Log("Oops, something appears to have gone wrong");
                UpdateCurrentPlayer();
                updateOnce = false;
            }
            return false;
        }
    }

    void UpdateCurrentPlayer()
    {
        currentPlayer = (currentPlayer == gameController.players.Count - 1) ? 0 : currentPlayer + 1;
        for (int i = currentPlayer; i < gameController.players.Count; i++)
        {
            if (gameController.players[currentPlayer].hasLost)
            {
                if (currentPlayer == gameController.players.Count - 1)
                    currentPlayer = 0;
                else
                    currentPlayer++;
            }
            else
                break;
        }
    }

    // Check if the round is completed
    public bool RoundFinished()
    {
        bool finished = false;
        PlayerController p = gameController.players.Find((k) => !k.hasLost);

        if (p == gameController.players[FindObjectOfType<TurnController>().currentPlayer])
            finished = true;

        return finished;
    }

    public void Stop()
    {
        endTime = EndTime;
        watch.gameObject.SetActive(false);
        EndTicking();
        EndThinkCoroutine();
        stop = true;
    }

    public void StartThinkSound()
    {
        thinkingCoroutine = ThinkSound();
        StartCoroutine(thinkingCoroutine);
    }

    public void StartTicking()
    {
        tickingCoroutine = Ticking();
        StartCoroutine(tickingCoroutine);
    }

    public IEnumerator ThinkSound()
    {
        // default time = 17
        yield return new WaitForSeconds(17);
        squirrel.PlayAnimByName("Thinking");
        soundSquirrel.PlayOneShot(squirrelThink);
    }

    public IEnumerator Ticking()
    {
        // default time = 20
        yield return new WaitForSeconds(20);
        soundClock.clip = clockTicking;
        soundClock.Play();
    }
    
    public void EndThinkCoroutine()
    {
        if (thinkingCoroutine != null)
            StopCoroutine(thinkingCoroutine);
    }

    public void EndTicking()
    {
        soundClock.clip = null;
        soundClock.Stop();
        soundClock.PlayOneShot(OpenCloseWatch);

        if(tickingCoroutine != null)
            StopCoroutine(tickingCoroutine);
    }
}
