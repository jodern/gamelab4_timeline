﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Card : MonoBehaviour {

	public Vector3 originalScale;
	public Vector3 targetPos;
    //Temp var for moving towards targetpos
    public Vector3 _targetPos;
	public Vector3 targetscale = new Vector3(0.8f, 0.8f, 0.8f);
	public CardData data;

	Sprite sprite;


	//animation
	[HideInInspector]public float startTime;
	private GameController gameController;
	[SerializeField] GameObject[] loadingCircle;


	void Start()
	{
		gameController = FindObjectOfType<GameController> ();
		startTime = Time.time;
		originalScale = targetscale;
	}

	void Update()
	{
		if (GameController.gameRunning && gameController != null) {
			if (transform.localPosition != targetPos && gameObject != gameController.localPlayer.selected) {
				transform.localPosition = Vector3.Lerp (transform.localPosition, targetPos, Time.deltaTime * 5f);
			}
			transform.localScale = Vector3.Lerp (transform.localScale, targetscale, Time.deltaTime * 5f);
		}
	}


	public void Assign(CardData data, Transform t = null)
	{
		if (t == null)
			t = transform;
		DeckHolder.AssignData d = new DeckHolder.AssignData(t, data, this);
		DeckHolder.instance.AssignCard(d);
	}


	public void AssignCard(CardData data, Transform root)
	{
        try
        {
            if (gameObject == null)
                Debug.Log(name + " is null");
        }
        catch
        {
            return;
        }


        this.data = data;
		if (root == null)
			root = transform;
		if (data.imageURL != "Default")
			StartCoroutine(LoadImage(data.imageURL, data.zoom, data.imageOffset, root));
		else
		{
			sprite = Resources.Load("Sprites/Default") as Sprite;
			AssignTex(sprite, root);
		}
		root.Find("FrontTitle").GetComponent<Text>().text = data.title;
		root.Find("BackTitle").GetComponent<Text>().text = data.title;
		root.Find("Description").GetComponent<Text>().text = data.description;
		root.Find("Year").GetComponent<Text>().text = data.year.ToString();
		root.Find("Month").GetComponent<Text>().text = data.month.ToString();
		root.Find("Day").GetComponent<Text>().text = data.day.ToString();
		Transform bigYear = root.Find("BigYear");
		if (bigYear != null)
		{
			bigYear.GetComponent<Text>().text = data.year.ToString();
		}
							
	}


	IEnumerator LoadImage(string url, float scale, Vector2 offset, Transform root)
    {
		CoroutineWithData cd;
		if(loadingCircle.Length > 1 && loadingCircle != null)
			cd = new CoroutineWithData(this, ImageLoader.LoadImage(url, loadingCircle[0].transform, loadingCircle[1].transform));
		else if(loadingCircle.Length > 0)
			cd = new CoroutineWithData(this, ImageLoader.LoadImage(url, loadingCircle[0].transform));
		else
			cd = new CoroutineWithData(this, ImageLoader.LoadImage(url));
		yield return cd.coroutine;

		sprite = cd.result as Sprite;

		if(sprite != null)
        	AssignTex(scale, offset, root);
    }


	void AssignTex(float scale, Vector2 offset, Transform root)
    {
		Image front = root.Find("FrontImage").GetComponent<Image>();
		Image back = root.Find("BackImage").GetComponent<Image>();
		front.sprite = sprite;
		back.sprite = sprite;

		Vector2 size =  new Vector2(sprite.bounds.size.x, sprite.bounds.size.y);

		Vector2 aspectScale = size;
        if (aspectScale.x >= aspectScale.y)
            aspectScale /= aspectScale.x;
        else
            aspectScale /= aspectScale.y;
		scale = CardCreatorZoomToUnity(scale, size);

		front.rectTransform.sizeDelta = new Vector2(scale * aspectScale.x, scale * aspectScale.y);
		back.rectTransform.sizeDelta = new Vector2(scale * aspectScale.x, scale * aspectScale.y);
		offset = CardCreatorOffsetToUnity(offset, size);
		front.rectTransform.anchoredPosition = offset;
		back.rectTransform.anchoredPosition = offset;
    }

	void AssignTex(Sprite sprite, Transform root)
    {
		Image front = root.Find("FrontImage").GetComponent<Image>();
		Image back = root.Find("BackImage").GetComponent<Image>();
		front.sprite = sprite;
		back.sprite = sprite;
		front.rectTransform.sizeDelta = new Vector2 (120, 120);
		back.rectTransform.sizeDelta = new Vector2 (120, 120); 
		front.rectTransform.anchoredPosition = Vector2.zero;
		back.rectTransform.anchoredPosition = Vector2.zero;
    }


	float CardCreatorZoomToUnity(float zoom, Vector2 size)
	{
		if (size.x >= size.y)
			return zoom * 1.2f;
		else
			return zoom * 2.3f;
	}


	Vector2 CardCreatorOffsetToUnity(Vector2 offset, Vector2 size)
	{
		Vector2 c = new Vector2(120, 230);
		return new Vector2(
			
			(offset.x) * c.x,
			-(offset.y) * c.y
		);
	}
}
