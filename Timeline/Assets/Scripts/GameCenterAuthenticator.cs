﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms.GameCenter;

public class GameCenterAuthenticator : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
        Social.localUser.Authenticate(success => {
            if (success)
                ReportAchievement();
            else
                Debug.Log("Failed to authenticate");
        });
    }
	
    void ReportAchievement()
    {
        Social.ReportProgress("Achievement01", 100, (result) => {
            Debug.Log(result ? "Reported achievement" : "Failed to report achievement");
        });
    }
}
