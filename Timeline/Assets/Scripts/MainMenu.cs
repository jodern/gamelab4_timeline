﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon.LoadBalancing;
using ExitGames.Client.Photon;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    Text searchByNameInputText;
    [SerializeField]
    Text maxPlayerDisplay;
    [SerializeField]
    Slider numPlayerSlider;
    [SerializeField]
    Toggle cheat;

    public Image loadLevelProgressBar;
    AsyncOperation async;
    [SerializeField]
    GameObject playGamePanel, createGamePanel, findGamePanel, creditsPanel, creditsButton;
    [SerializeField]
    Dropdown deckPicker;
    [SerializeField]
    Text playGameFeedback;
    [SerializeField]
    Button playGameButton, createGameButton, alsoCreateGameButton;
    [SerializeField]
    InputField createGameNameInput;

    [SerializeField]
    string appId;

    [SerializeField]
    Card[] menuCards;

    TimelineClient client;
    public GameObject listedRoomPrefab;
    [SerializeField]
    Text connectionStatus;
    [SerializeField]
    Transform roomContainer;
    [SerializeField]
    GameObject warning;
    List<GameObject> roomListPool = new List<GameObject>();
    List<GameObject> roomList = new List<GameObject>();
    string dotdotdot = "";

    bool connectInProgress;
    bool isShuttingDown;

    [SerializeField]
    Text debugLog;

    DeckBrowser deckBrowser;


    // Use this for initialization
    void Start()
    {
        // Destroy all maintain connection instances
        foreach (MaintainConnection k in FindObjectsOfType<MaintainConnection>())
        {
            Destroy(k.gameObject);
        }

        deckBrowser = FindObjectOfType<DeckBrowser>();

        // Destroy all DeckHolder instances
        //foreach(DeckHolder k in FindObjectsOfType<DeckHolder>())
        //{
        //Destroy(k.gameObject);
        //}

        // Clears player prefs
        // PlayerPrefs.DeleteAll();

        //Disconnect if player is connected still and then reconnect
        //Disconnect();

        sortMethods = new SortMethod[] { SortName, SortPlayers, SortDeck, SortCreator };

        Application.runInBackground = true;
        loadLevelProgressBar.transform.parent.gameObject.SetActive(false);
        cheat.isOn = PlayerController.bCheat;
        DeckHolder.instance.selectedDeck = null;
        //Check if default deck needs to be loaded
        BuildDeckPicker();

        SetupClient();

        StartCoroutine(TryReconnect());
        StartCoroutine(DotDotDot());
        StartCoroutine(GetMostPopularWeek());

        //Application.logMessageReceived += HandleLog;
    }


    void HandleLog(string logString, string stackTrace, LogType logType)
    {
        debugLog.text += logString + " " + stackTrace;
        if (debugLog.text.Length > 10000)
        {
            int index = debugLog.text.IndexOf(System.Environment.NewLine);
            debugLog.text = debugLog.text.Substring(index + System.Environment.NewLine.Length);
        }
    }


    void SetupClient()
    {
        client = new TimelineClient();
        client.AppId = appId;

        client.loadBalancingPeer.DebugOut = DebugLevel.INFO;
        client.loadBalancingPeer.TrafficStatsEnabled = true;

        client.OnUpdateGameList += RefreshRoomList;

        connectInProgress = client.ConnectToRegionMaster("eu");
    }


    IEnumerator TryReconnect()
    {
        yield return new WaitForSeconds(3);
        if (client.State == ClientState.ConnectingToNameServer || client.State == ClientState.Disconnected)
        {
            Disconnect();
            SetupClient();
            yield return TryReconnect();
        }
    }


    void Disconnect()
    {
        if (client != null && client.loadBalancingPeer != null)
        {
            client.Disconnect();
            client.loadBalancingPeer.StopThread();
        }
        client = null;
    }


    public void ApplicationQuit()
    {
        Disconnect();
        if (Application.platform != RuntimePlatform.WebGLPlayer && !Application.isWebPlayer && !Application.isEditor)
        {
#if UNITY_STANDALONE
            //Application.Quit();
            System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
#if UNITY_ANDROID
            Application.Quit();

#endif
        }
    }



    void OnApplicationQuit()
    {
        isShuttingDown = true;
        Disconnect();
    }


    void OnDestroy()
    {
        //Disconnect ();
    }


    void BuildDeckPicker()
    {
        //Add option to create new deck
        deckPicker.ClearOptions();
        System.Collections.Generic.List<string> options = new System.Collections.Generic.List<string>();

        //Add current decks
        for (int i = 0; i < DeckHolder.instance.storedDecks.Count; i++)
        {
            options.Add(DeckHolder.instance.storedDecks[i].name + " (" + DeckHolder.instance.storedDecks[i].cards.Count);
        }
        //Add dropdown options
        deckPicker.AddOptions(options);
        deckPicker.value = 0;
    }


    void RunExternalCardCreator()
    {
        string s = "http://mediastudent.no/spo2014/gamelab03/CardCreator.html";
        /*if (Application.isMobilePlatform)
            Application.ExternalEval("window.open('" + s + "','_blank')");
        else*/
        {
            Application.OpenURL(s);
            //System.Diagnostics.Process.Start(s);
        }
    }


    public void DisplayOpenCardCreatorWarning()
    {
        WarningPopup.Display("This will open an external window\nDo you want to continue?", RunExternalCardCreator);
    }


    // Update is called once per frame
    void Update()
    {
        if (client == null)
            return;

        client.Service();

        bool interactive = client.State == ClientState.JoinedLobby;
        if (alsoCreateGameButton != null && alsoCreateGameButton.gameObject.activeInHierarchy && createGameNameInput != null && deckBrowser != null)
            alsoCreateGameButton.interactable = interactive && createGameNameInput.text != "" && deckBrowser.group.AnyTogglesOn();
        //playGameButton.interactable = interactive;


        if (client != null)
        {
            if (client.State == ClientState.JoinedLobby)
                connectionStatus.text = "<color=green>Joined Master!</color>" + ", Players: " + client.PlayersOnMasterCount;
            else if (client.State == ClientState.ConnectingToMasterserver || client.State == ClientState.ConnectingToNameServer)
                connectionStatus.text = "<color=red>Connecting" + dotdotdot + "</color>, Players: " + client.PlayersOnMasterCount;
            else
                connectionStatus.text = "<color=red>" + client.State.ToString() + "</color>, Players: " + client.PlayersOnMasterCount;
        }
        if (async != null)
        {
            loadLevelProgressBar.fillAmount = Mathf.MoveTowards(loadLevelProgressBar.fillAmount, async.progress, 0.5f * Time.deltaTime);
        }
    }


    IEnumerator DotDotDot()
    {
        int count = 0;
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            dotdotdot = "";
            for (int i = 0; i < count; i++)
                dotdotdot += " . ";

            count = count == 3 ? 0 : count + 1;
        }
    }


    void LoadLevel(string level)
    {
        if (async != null)
            return;

        if (level != "MP_Lobby")
            Disconnect();

        loadLevelProgressBar.transform.parent.gameObject.SetActive(true);
        //async = SceneManager.LoadSceneAsync(level);
        System.GC.Collect();
        Resources.UnloadUnusedAssets();
        LoadGame.sceneToLoad = level;
        async = SceneManager.LoadSceneAsync("Loading");
    }


    public void OnTickUseAI(bool useAi)
    {
        GameController.soloPlay = useAi;
    }


    public void OnClikcPlayGame()
    {
        if (!client.IsConnectedAndReady)
        {
            GameController.soloPlay = true;
        }
        playGamePanel.SetActive(!playGamePanel.activeInHierarchy);
        findGamePanel.SetActive(false);
        createGamePanel.SetActive(false);
    }

    public void OnClickCredits()
    {
        creditsPanel.SetActive(!creditsPanel.activeInHierarchy);
    }

    public void OnClickBack()
    {
        playGamePanel.SetActive(false);
        creditsPanel.SetActive(false);
    }


    public void OnClickStartGame(string level)
    {
        GameController.soloPlay = true;
        PlayerController.bCheat = cheat.isOn;
        LoadLevel(level);
    }


    public void OnClickLoadLevel(string level)
    {
        LoadLevel(level);
    }


    public void OnClickFindGamePanel()
    {
        findGamePanel.SetActive(!findGamePanel.activeInHierarchy);

        if (findGamePanel.activeInHierarchy)
        {
            createGamePanel.SetActive(false);
            RefreshRoomList(null);
        }
    }

    public void OnInputChange(Button button)
    {
        button.interactable = createGameNameInput.text.Length > 0 ? true : false;
    }

    bool CanCreateGame()
    {
        if (DeckHolder.instance.storedDecks.Count == 0)
        {
            StartCoroutine(SetTextTemp(playGameFeedback, "(There are no decks)", 5));
            return false;
        }

        //Check if any deck has enough cards
        int minNumCards = 0;
        foreach (Deck deck in DeckHolder.instance.storedDecks)
        {
            if (deck.cards.Count > minNumCards)
                minNumCards = deck.cards.Count;
        }
        if (minNumCards < GameController.amountCardsPerPlayer)
        {
            StartCoroutine(SetTextTemp(playGameFeedback, "(No deck has more than " + GameController.amountCardsPerPlayer + " cards)", 5));
            return false;
        }

        if (DeckHolder.instance.storedDecks[deckPicker.value].cards.Count < GameController.amountCardsPerPlayer)
        {
            StartCoroutine(SetTextTemp(playGameFeedback, "(Selected deck has less than the minimum " + GameController.amountCardsPerPlayer + " cards)", 5));
            return false;
        }
        return true;
    }


    public void OnClickSearchForGame()
    {
        if (DeckHolder.instance.storedDecks.Count == 0)
            return;
        Deck deck = DeckHolder.instance.storedDecks[deckPicker.value];

        Hashtable expectedCustomRoomProperties = new Hashtable();
        if (!string.IsNullOrEmpty(searchByNameInputText.text))
        {
            GameSetup game = new GameSetup(searchByNameInputText.text, 2, false, deck, client);
            LoadLevel("Main menu");
        }
        else
        {
            GameSetup game = new GameSetup("", 2, false, deck, client);
            LoadLevel("MP_Lobby");
        }
    }


    public void OnclickCreateGamePanel()
    {
        DeckHolder.instance.selectedDeck = null;
        createGamePanel.SetActive(!createGamePanel.activeInHierarchy);

        if (!createGamePanel.activeInHierarchy)
            return;

        findGamePanel.SetActive(false);
    }


    public void OnClickCreateGame()
    {
        if (string.IsNullOrEmpty(createGameNameInput.text))
            return;

        //if (!CanCreateGame())
        //	return;

        GameController.soloPlay = false;
        PlayerController.bCheat = cheat.isOn;

        string roomName = createGameNameInput.text;

        Deck deck = DeckHolder.instance.selectedDeck;
        GameSetup game = new GameSetup(roomName, (byte)numPlayerSlider.value, true, deck, client);
        GameController.amountOfPlayers = (int)numPlayerSlider.value;
        LoadLevel("MP_Lobby");
    }


    public void MaxPlayerSliderChange(float i)
    {
        maxPlayerDisplay.text = i.ToString();
    }


    [SerializeField]
    DragTabs drag;
    List<KeyValuePair<string, RoomInfo>> gameList;

    delegate int SortMethod(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir);
    SortMethod[] sortMethods;
    int currentSort = 0;
    int sortDir = 1;


    public void ChangeRoomlistSort(int s)
    {
        if (currentSort == s)
            sortDir = -1;
        else
        {
            currentSort = s;
            sortDir = 1;
        }
        BuildRoomListing();
    }


    void BuildRoomListing()
    {
        if (!playGameButton.interactable)
            playGameButton.interactable = true;

        if (!findGamePanel)
            return;

        Debug.Log("Updating " + gameList.Count + " rooms!");

        gameList.Sort((a, b) => sortMethods[currentSort](a, b, sortDir));

        roomList.Clear();
        //Create rooms if they don't exist yet
        for (int i = roomListPool.Count; i < gameList.Count; i++)
        {
            GameObject room = Instantiate(listedRoomPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            room.transform.SetParent(roomContainer, false);
            roomListPool.Add(room);
        }
        //Disable cards that aren't used
        for (int i = gameList.Count; i < roomList.Count; i++)
        {
            roomList[i].SetActive(false);
            roomList.RemoveAt(i);
        }

        drag.gameTabList = new DragTabs.DragTab[gameList.Count][];

        //Enable and assign rooms
        for (int i = 0; i < gameList.Count; i++)
        {
            KeyValuePair<string, RoomInfo> pair = gameList[i];
            roomList.Add(roomListPool[i]);

            Transform scale = roomList[i].transform.FindChild("ScaleTab");
            RectTransform name = scale.FindChild("Name").GetComponent<RectTransform>();
            name.GetComponentInChildren<Text>().text = pair.Key;
            RectTransform plyCount = scale.FindChild("PlyCount").GetComponent<RectTransform>();
            plyCount.GetComponentInChildren<Text>().text = pair.Value.PlayerCount + "/" + pair.Value.MaxPlayers;
            RectTransform deck = scale.FindChild("Deck").GetComponent<RectTransform>();
            deck.GetComponentInChildren<Text>().text = (pair.Value.CustomProperties["name"] ?? "").ToString();
            RectTransform creator = scale.FindChild("Creator").GetComponent<RectTransform>();
            creator.GetComponentInChildren<Text>().text = (pair.Value.CustomProperties["creator"] ?? "").ToString();

            name.anchorMax = drag.dragTabs[0].l.anchorMax;
            plyCount.anchorMin = drag.dragTabs[0].r.anchorMin;
            plyCount.anchorMax = drag.dragTabs[1].l.anchorMax;
            deck.anchorMin = drag.dragTabs[1].r.anchorMin;
            deck.anchorMax = drag.dragTabs[2].l.anchorMax;
            creator.anchorMin = drag.dragTabs[2].r.anchorMin;

            name.anchoredPosition = Vector2.zero;
            plyCount.anchoredPosition = Vector2.zero;
            deck.anchoredPosition = Vector2.zero;
            creator.anchoredPosition = Vector2.zero;


            drag.gameTabList[i] = new DragTabs.DragTab[3];

            drag.gameTabList[i][0].l = name;
            drag.gameTabList[i][0].r = plyCount;
            drag.gameTabList[i][1].l = plyCount;
            drag.gameTabList[i][1].r = deck;
            drag.gameTabList[i][2].l = deck;
            drag.gameTabList[i][2].r = creator;

            Button tempButton = roomList[i].transform.FindChild("JoinButton").GetComponent<Button>();
            tempButton.interactable = pair.Value.PlayerCount != pair.Value.MaxPlayers;
            tempButton.onClick.AddListener(() => JoinRoom(pair.Key, tempButton));
        }
    }


    int SortName(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir)
    {
        return dir * a.Key.CompareTo(b.Key);
    }

    int SortDeck(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir)
    {
        return dir * a.Value.CustomProperties["name"].ToString().CompareTo(b.Value.CustomProperties["name"].ToString());
    }

    int SortPlayers(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir)
    {
        return dir * a.Value.PlayerCount.CompareTo(b.Value.PlayerCount);
    }

    int SortMaxPlayers(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir)
    {
        return dir * a.Value.MaxPlayers.CompareTo(b.Value.MaxPlayers);
    }

    int SortCreator(KeyValuePair<string, RoomInfo> a, KeyValuePair<string, RoomInfo> b, int dir)
    {
        return dir * a.Value.CustomProperties["creator"].ToString().CompareTo(a.Value.CustomProperties["creator"].ToString());
    }


    void RefreshRoomList(Dictionary<string, RoomInfo> list)
    {
        //If no list was sent, get list from client
        if (list == null)
            list = client.RoomInfoList;
        gameList = list.ToList();
        BuildRoomListing();
    }


    void JoinRoom(string room, Button button)
    {
        button.interactable = false;
        GameController.soloPlay = false;

        Deck deck = DeckHolder.instance.storedDecks[deckPicker.value];

        GameSetup game = new GameSetup(room, 2, false, deck, client);
        LoadLevel("MP_Lobby");
    }


    IEnumerator SetTextTemp(Text text, string message, float delay)
    {
        text.text = message;
        yield return new WaitForSeconds(delay);
        text.text = "";
    }


    IEnumerator GetMostPopularWeek()
    {
        yield return new WaitForSeconds(5);
        WWWForm form = new WWWForm();
        form.AddField("user", Profile.playerName ?? "Somebody");
        form.AddField("days", 7);
        form.AddField("limit", 1);
        form.AddField("min", menuCards.Length);
        form.AddField("age", Profile.GetAgeYear());
        form.AddField("orderBy", "viewsDesc");
        form.AddField("key", "95e839097a566471c70fe357e5a101d2");
        DeckHolder.instance.LoadFromDB("deck_getAll.php", PopulateMenuCards, form);
    }


    void PopulateMenuCards(Deck[] d)
    {
        if (d == null)
            Debug.LogWarning("No deck found!");
        else
            Debug.Log("Populating menu cards with \"" + (d[0].name ?? "null") + "\"");
        List<Deck> decks = new List<Deck>();
        if (d != null)
            decks = d.ToList();
        else
            decks.AddRange(DeckHolder.instance.defaultDecks);

        for (int i = 0; i < menuCards.Length; i++)
        {
            bool countNotZero = decks[0].cards.Count != 0;
            menuCards[i].gameObject.SetActive(countNotZero);

            if (countNotZero)
            {
                int randIndex = Random.Range(0, decks[0].cards.Count);
                CardData data = decks[0].cards[randIndex];
                decks[0].cards.RemoveAt(randIndex);
                Transform card = menuCards[i].transform.FindChild("Canvas").GetChild(0);
                menuCards[i].Assign(data, card);
                card.FindChild("Date").gameObject.SetActive(false);
                card.FindChild("Year").gameObject.SetActive(false);
                card.FindChild("Month").gameObject.SetActive(false);
                card.FindChild("Day").gameObject.SetActive(false);
                card.FindChild("BigYear").gameObject.SetActive(false);
            }
        }
    }
}
