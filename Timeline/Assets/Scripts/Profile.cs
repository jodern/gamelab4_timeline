﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
#if UNITY_ANDROID
using GooglePlayGames;
#endif
using SimpleJSON;

public class Profile : MonoBehaviour
{
    public static string playerName;
    public static string playerAge;
    public static string playerId;

    public InputField inputName;
    public InputField inputYear;
    public InputField inputMonth;
    public InputField inputDay;

    // Database stuff
    JSONNode data;

    void Awake()
    {
        GetOfflineProfile();
    }

    public void GetOfflineProfile()
    {

        if (PlayerPrefs.HasKey("Name"))
        {
            if (PlayerPrefs.GetString("Name") != "")
                playerName = PlayerPrefs.GetString("Name");

            if (playerName != null)
                inputName.text = playerName;
            else inputName.text = "Unnamed";
        }
        else inputName.text = "Unnamed";

        if (PlayerPrefs.HasKey("Year"))
        {
            if (PlayerPrefs.GetString("Year") != "")
                inputYear.text = PlayerPrefs.GetString("Year");
            else
                inputYear.text = DateTime.Now.Year.ToString("0000");
        }
        else
            inputYear.text = DateTime.Now.Year.ToString("0000");

        if (PlayerPrefs.HasKey("Month"))
        {
            if (PlayerPrefs.GetString("Month") != "")
                inputMonth.text = PlayerPrefs.GetString("Month");
            else
                inputMonth.text = DateTime.Now.Month.ToString("00");
        }
        else
            inputMonth.text = DateTime.Now.Month.ToString("00");

        if (PlayerPrefs.HasKey("Day"))
        {
            if (PlayerPrefs.GetString("Day") != "")
                inputDay.text = PlayerPrefs.GetString("Day");
            else
                inputDay.text = DateTime.Now.Day.ToString("00");
        }
        else
            inputDay.text = DateTime.Now.Day.ToString("00");

        FormatDate();
    }

    public void SetAge()
    {
        PlayerPrefs.SetString("Year", inputYear.text);
        PlayerPrefs.SetString("Month", inputMonth.text);
        PlayerPrefs.SetString("Day", inputDay.text);
        playerAge = CreateDate(inputYear.text, inputMonth.text, inputDay.text);

#if UNITY_ANDROID

        if (GoogleAuthenticator.authenticated)
        {
            setOnlineProfile();
        }
#endif
    }

    public void SetName()
    {
        PlayerPrefs.SetString("Name", playerName);
        playerName = inputName.text;

#if UNITY_ANDROID

        if (GoogleAuthenticator.authenticated)
        {
            setOnlineProfile();
        }
#endif
    }
#if UNITY_ANDROID

    public void getOnlineProfile()
    {
        StartCoroutine(GetOnlineProfile());
    }

    public void setOnlineProfile()
    {
        StartCoroutine(SetOnlineProfile());
    }

    IEnumerator GetOnlineProfile()
    {
        playerId = PlayGamesPlatform.Instance.localUser.id;

        string url = "https://cpanel48.proisp.no/~mediaopc/spo2014/gamelab03/account_get.php";
        WWWForm form = new WWWForm();
        form.AddField("id", playerId);

        WWW www;
        www = new WWW(url, form);

        data = JSON.Parse(www.text) as JSONNode;

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError("Error loading name and/or age from DB: " + www.error);
            Debug.LogError(url);
        }
        else
        {
            if (www.text.Contains("ERROR"))
            {
                Debug.LogWarning(www.text);
            }
            else
            {
                playerName = data["nickname"];
                GetDateOfBirth();
            }
        }
    }

    IEnumerator SetOnlineProfile()
    {
        string url = "https://cpanel48.proisp.no/~mediaopc/spo2014/gamelab03/account_post.php";
        WWWForm form = new WWWForm();

        form.AddField("id", playerId);
        form.AddField("nickname", playerName);
        form.AddField("age", CreateDate(inputYear.text, inputMonth.text, inputDay.text));
        form.AddField("key", DeckHolder.secretKey);

        WWW www;
        www = new WWW(url, form);

        data = JSON.Parse(www.text) as JSONNode;

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError("Error loading name and/or age from DB: " + www.error);
            Debug.LogError(url);
        }
        else
        {
            if (www.text.Contains("ERROR"))
            {
                Debug.LogWarning(www.text);
            }
        }
    }
#endif

    public void GetDateOfBirth()
    {
#if UNITY_ANDROID
        string[] dates = data["age"].ToString().Split('-');
        string year = dates[0];
        string month = dates[1];
        string date = dates[2];

        inputYear.text = year;
        inputMonth.text = month;
        inputDay.text = date;

        PlayerPrefs.SetString("Year", inputYear.text);
        PlayerPrefs.SetString("Month", inputMonth.text);
        PlayerPrefs.SetString("Day", inputDay.text);
        playerAge = CreateDate(inputYear.text, inputMonth.text, inputDay.text);

        //dates = "2016-01-01".Split('-');
        //Debug.Log(dates[0]);
#endif
    }

    public string CreateDate(string year, string month, string day)
    {
        return year + "-" + month + "-" + day;
    }


	public static int GetAgeYear()
	{
		if (string.IsNullOrEmpty(playerAge))
			return 0;
		Debug.Log(playerAge);
		string[] dateSplit = playerAge.Split('-');
		try
		{
			DateTime bDay = new DateTime(int.Parse(dateSplit[0]), int.Parse(dateSplit[1]), int.Parse(dateSplit[2]));
			DateTime now = DateTime.Today;
			int age = now.Year - bDay.Year;
			if (bDay > now.AddYears(-age))
				age--;
			return age;
		}
		catch(Exception e)
		{
			Debug.LogError(e + ": " + dateSplit[0] + ", " + dateSplit[1] + ", " + dateSplit[2]);
			return -1;
		}
	}


    public void FormatDate()
    {
        int month = 0;
        int year = 0;
        int day = 0;

        if (inputYear != null && inputYear.text != "")
        {
            year = int.Parse(inputYear.text);

            if (year >= DateTime.Now.Year)
                year = DateTime.Now.Year;
            else if (year <= 1900)
                year = 1900;

            inputYear.text = year.ToString();
        }
        else
            inputYear.text = DateTime.Now.Year.ToString();

        if (inputMonth != null && inputMonth.text != "")
        {
            month = int.Parse(inputMonth.text);

            if (month > 12)
                month = 12;
            else if (month < 1)
                month = 1;

            inputMonth.text = month.ToString("00");
        }
        else
            inputMonth.text = DateTime.Now.Month.ToString("00");

        if (inputDay != null && inputDay.text != "")
        {
            day = int.Parse(inputDay.text);
            int days = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            if (day >= days)
                day = days;
            else if (day < 1)
                day = 1;

            inputDay.text = day.ToString("00");
        }
        else
            inputDay.text = DateTime.Now.Day.ToString("00");
    }
}
