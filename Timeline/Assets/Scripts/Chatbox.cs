﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ExitGames.Client.Photon.LoadBalancing;

public class Chatbox : MonoBehaviour {
	PhotonManager photonManager;
	public Transform chatboxContainer;
	public GameObject textFab;
	public InputField txtToSend;
	public AudioSource source;
	string msg;
    public bool bTesting = false;
    TouchScreenKeyboard keyboard;

	// Use this for initialization
	void Start () {
		photonManager = FindObjectOfType<PhotonManager> ();
	}
	
    public void Send()
    {
        if (bTesting)
        {
            string message = txtToSend.text;
            int i = 0;
            foreach (char c in message)
            {
                if (c.ToString() == " ")
                    i++;
            }

            if (i == message.Length || message == "")
                return;

            message = Profile.playerName + ": " + message;

            TestMessage(message);
        }
        else
        {
            string message = txtToSend.text;
            int i = 0;
            foreach (char c in message)
            {
                if (c.ToString() == " ")
                    i++;
            }

            if (i == message.Length || message == "")
                return;

            message = Profile.playerName + ": " + message;

            if (source != null)
                source.Play();

            SendChatMessage(message);
        }
        txtToSend.text = "";
    }

	// Offline test message
    void TestMessage(string message)
    {
        GameObject txt = Instantiate(textFab, Vector3.zero, Quaternion.identity) as GameObject;
        txt.transform.SetParent(chatboxContainer);
        txt.transform.localScale = new Vector3(1, -1, 1);
        txt.transform.SetAsFirstSibling();
        System.DateTime timeStamp = System.DateTime.Now;
        txt.transform.GetChild(1).GetComponent<Text>().text = timeStamp.ToString("HH:mm");
        txt.transform.GetChild(0).GetComponent<Text>().text = (string)message;
    }

	// Send online message
	void SendChatMessage(string message)
	{
		StartCoroutine(SendDelayedMessage(message));
	}

	IEnumerator SendDelayedMessage(string message)
	{
		RaiseEventOptions options = new RaiseEventOptions ();
		options.Receivers = ReceiverGroup.All;

		photonManager.SendNetMessage (photonManager.client.LocalPlayer.ID, options, 96);
		yield return new WaitForSeconds (0.3f);
		photonManager.SendNetMessage (message, options, 92);
	}

	public void RecieveChatMessage(object message, byte code)
	{
		if (code != 92)
			return;
		
		// Spawn a text prefab and set new parent to message container and text to message
		GameObject txt = Instantiate (textFab, Vector3.zero, Quaternion.identity) as GameObject;
		txt.transform.SetParent (chatboxContainer);
		txt.transform.localScale = new Vector3 (1, -1, 1);
		txt.transform.SetAsFirstSibling ();
        System.DateTime timeStamp = System.DateTime.Now;
        txt.transform.GetChild(1).GetComponent<Text>().text = timeStamp.ToString("HH:mm");
        txt.transform.GetChild(0).GetComponent<Text> ().text = (string)message;
	}
}
