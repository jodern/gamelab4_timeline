﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ExitGames.Client.Photon.LoadBalancing;

public class PlayerController : MonoBehaviour
{
    // cheat
    Text cheat;
    public static bool bCheat = false;

    // Networked variables
    private int newCardIndex;
    public int ID;

    // UI
    public PlayerInfo playerInfo;
    public GameObject winMenu;
    public GameObject lostMenu;
    public GameObject drawMenu;
    public GameObject squirrel;

    //card movement
    RaycastHit hit;
    [HideInInspector]
    public GameObject selected;
    public bool yourTurn = false;
    public bool hasWon = false;
    public bool hasLost = false;
    public bool Losing = false;
    [HideInInspector]
    public bool addToList = false;

    // Offset and scale
    public float playerDeckLimit = 0f;
    public float scaleModifier = 1;
    private float actualScaleModifier = 1;
    public float zoomAmount = 0.11f;
    public Vector3 originalCardScale = Vector3.zero;
    public float offset;
    public float spacing = 0f;

    Vector3[] oldPositions;
    Vector3[] rOffset;
    Vector3[] lOffset;

    //player cards
    public int CardIndex;
    public int selectedIndex = -1;
    int cardToMoveTimeline;
    public List<GameObject> Cards;

    private bool onPlayerDeck = true;
    GameController gameController;
    TurnController turnController;
    TimelineController timelineController;
    PhotonManager photonManager;
    SuddenDeath suddenDeathInstance;
    WinCheck winCheckInstance;
    Vector3 mousePos;
    Vector3 oldMousePos;

    // AI variables
    [HideInInspector]
    public GameObject randomPos;
    public List<GameObject> AICards;
    public bool isHuman = true;

    // Network temp variables to send
    public int networkCardIndex;
    public int networkSelectedIndex;
    public bool correct = false;
    public bool networked = false;

    public AudioSource soundSquirrel;
    public AudioClip[] squirrelHappy;
    public AudioClip[] squirrelSad;

    public AudioSource SoundCard;
    public AudioSource EffectsCard;
    public AudioClip cardWrong;
    public AudioClip cardThud;
    public AudioClip cardSparkles;
    public AudioClip Winner;
    public AudioClip Loser;

    public Animator CardAnimator;
    private bool Selected;

    private bool BlueGlow;
    public float SoundDelay = 1f;

    void Start()
    {
        // cheat
        if (GameObject.Find("CheatText"))
        {
            cheat = GameObject.Find("CheatText").GetComponent<Text>();
            cheat.text = "";
        }

        gameController = FindObjectOfType<GameController>();
        if (originalCardScale == Vector3.zero)
            originalCardScale = gameController.cardFab.transform.localScale;
        timelineController = FindObjectOfType<TimelineController>();
        photonManager = FindObjectOfType<PhotonManager>();
        suddenDeathInstance = FindObjectOfType<SuddenDeath>();
        winCheckInstance = FindObjectOfType<WinCheck>();

        if (!isHuman)
        {
            Cards = AICards;
            randomPos = new GameObject();
            randomPos.name = name + " dummy card";
            randomPos.transform.SetParent(GameObject.Find("Cards").transform);
        }

        if (!GameController.soloPlay)
            yourTurn = false;

        actualScaleModifier = scaleModifier;
        turnController = FindObjectOfType<TurnController>();

        // testing
#if UNITY_EDITOR
        bCheat = true;
#endif

        if (Screen.height == 432 && Screen.width == 768)
            bCheat = true;

        CardAnimator = GetComponent<Animator>();
    }

    public void SetOffset()
    {
        oldPositions = new Vector3[timelineController.cards.Count];
        rOffset = new Vector3[timelineController.cards.Count];
        lOffset = new Vector3[timelineController.cards.Count];

        for (int j = 0; j < timelineController.cards.Count; j++)
        {
            oldPositions[j] = timelineController.cards[j].GetComponentInParent<Card>().targetPos;
            rOffset[j] = new Vector3(oldPositions[j].x + offset, oldPositions[j].y, oldPositions[j].z);
            lOffset[j] = new Vector3(oldPositions[j].x - offset, oldPositions[j].y, oldPositions[j].z);
        }
    }

    public void GetSelected(BaseEventData i)
    {
        if (yourTurn)
        {
            PointerEventData data;
            data = i as PointerEventData;

            if (data.pointerDrag != null)
            {
                if (selected == null)
                {
                    // Set selected card
                    GameObject tmp = selected = data.pointerDrag.GetComponentInParent<Card>().gameObject;

                    GameController.Selected = tmp;
                    timelineController.selected = tmp;

                    Card component = selected.GetComponentInParent<Card>();
                    actualScaleModifier = scaleModifier;
                    Vector3 upSize = new Vector3(actualScaleModifier, actualScaleModifier, actualScaleModifier);
                    Vector3 zoom = new Vector3(component.targetPos.x,
               component.targetPos.y, -zoomAmount);
                    component.targetscale = upSize;
                    component.targetPos = zoom;

                    selected = tmp;
                }

                if (Cards.Contains(selected))
                {
                    // Defaults all network variables
                    networkSelectedIndex = 0;
                    networkCardIndex = 0;
                    correct = false;

                    // Sets selectedIndex for use later
                    // and sets networkCardIndex to the same
                    selectedIndex = Cards.IndexOf(selected);
                    networkSelectedIndex = selectedIndex;
                    Cards.Remove(selected);
                }
            }
        }
    }

    public void HoldCard(BaseEventData data)
    {
        GetSelected(data);

        if (yourTurn && selected != null)
        {
            mousePos = Input.mousePosition;
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray MousePos = Camera.main.ScreenPointToRay(mousePos);
            float hitPoint = 0;
            plane.Raycast(MousePos, out hitPoint);
            Vector3 localPos = transform.InverseTransformPoint(MousePos.GetPoint(hitPoint));
            selected.transform.localPosition = new Vector3(localPos.x, localPos.y, selected.transform.localPosition.z);

            // Checks if player is placing card on player deck or on the timeline
            if (selected.transform.localPosition.y < playerDeckLimit)
                onPlayerDeck = true;
            else
                onPlayerDeck = false;

            if (onPlayerDeck)
            {
                timelineController.newCardPositions(true);

                // Revert sizes of the timeline cards on drag and when in player deck of cards
                foreach (GameObject card in timelineController.cards)
                {
                    if (card.GetComponentInParent<Card>().targetscale != originalCardScale)
                    {
                        card.GetComponent<Card>().targetscale = card.GetComponent<Card>().originalScale;
                        if (card == timelineController.cards[timelineController.cards.Count - 1].gameObject)
                        {
                            break;
                        }
                    }
                }

                for (int i = 0; i < Cards.Count; i++)
                {
                    if (Cards[i].transform.position.x < selected.transform.position.x)
                        CardIndex = i + 1;
                    if (Cards[i].transform.position.x > selected.transform.position.x)
                        break;
                }
                if (CardIndex == 1 && selected.transform.position.x < Cards[0].transform.position.x)
                    CardIndex = 0;
            }
            else
            {
                ReSizeRePosition();
            }
        }
    }

    public void PlaceOnPlayerDeck()
    {
        Cards.Insert(CardIndex, selected);
        selected.GetComponent<Card>().targetscale = originalCardScale;
        newCardPositions();
        CardIndex = 0;
        selected = null;
        timelineController.selected = null;
    }

    public void PlaceCard()
    {
        if (selected == null || !yourTurn)
            return;

        bool cardCorrect = false;
        Card selectedComponent = selected.GetComponentInParent<Card>();

        if ((gameController.localPlayer.yourTurn || turnController.HasTurn()) && EventSystem.current != null && selected != null)
        {
            if (onPlayerDeck)
            {
                PlaceOnPlayerDeck();
            }
            else if (!onPlayerDeck)
            {
                // Turn off card animations
                selected.GetComponent<Animator>().SetBool("Selected", false);
                foreach (GameObject a in Cards)
                {
                    a.GetComponent<Animator>().SetBool("Selected", false);
                }

                cardCorrect = timelineController.lookForCard();
                yourTurn = false;
                networkCardIndex = timelineController.cardIndex;
                Cards.Remove(selected);

                if (cardCorrect)
                    correct = true;

                timelineController.cards.Insert(timelineController.cardIndex, selected);
                selectedComponent.originalScale = timelineController.CardsScale;
                selectedComponent.targetscale = timelineController.CardsScale;
                selected.transform.SetParent(timelineController.cardParent);
                timelineController.selected = null;
                timelineController.newCardPositions(cardCorrect);

                if (!cardCorrect)
                {
                    Debug.Log("Removing incorrect card at " + timelineController.cardIndex);
                    timelineController.cards.RemoveAt(timelineController.cardIndex);
                }

                if (isLocal())
                {
                    EventTrigger trigger = selected.GetComponentInParent<EventTrigger>();
                    trigger.triggers.Clear();
                    int index = gameController.players.IndexOf(gameController.localPlayer);
                    gameController.AddHoverListeners(trigger, index);
                }

                if (cardCorrect)
                {
                    StartCoroutine(PlaceCardAnim(selected.GetComponent<Animator>()));
                    soundSquirrel.PlayOneShot(squirrelHappy[Random.Range(0, squirrelHappy.Length)]);
                    StartCoroutine(CardThudSound());
                    selected = null;
                    timelineController.selected = null;

                    //if (!SuddenDeath.suddenDeath)
                    Losing = false;
                }

                newCardPositions();

                oldPositions = new Vector3[timelineController.cards.Count];
                rOffset = new Vector3[timelineController.cards.Count];
                lOffset = new Vector3[timelineController.cards.Count];

                for (int j = 0; j < timelineController.cards.Count; j++)
                {
                    oldPositions[j] = timelineController.cards[j].GetComponent<Card>().targetPos;
                    rOffset[j] = new Vector3(oldPositions[j].x + offset, oldPositions[j].y, oldPositions[j].z);
                    lOffset[j] = new Vector3(oldPositions[j].x - offset, oldPositions[j].y, oldPositions[j].z);
                }
                if (cardCorrect)
                {
                    SquirrelPlayRandom(1);
                    if (turnController.RoundFinished() && SuddenDeath.suddenDeath)
                    {
                        suddenDeathInstance.SetLostPlayers();
                    }

                    int index = gameController.GetPlayerIndex(this);

                    if (GameController.soloPlay && winCheckInstance.CheckWinner(index))
                    {
                        Debug.Log("No no onononono");
                        Win();
                    }

                    turnController.EndTurn(false);
                }
                else if (!cardCorrect)
                {
                    //if (Cards.Count == 0 && !SuddenDeath.suddenDeath)
                    //    Losing = true;

                    yourTurn = false;
                    correct = false;
                    StartCoroutine(Incorrect(selected, true));
                    selected = null;
                    timelineController.selected = null;
                }
            }

            CardIndex = 0;
            selectedIndex = -1;
        }
        // Revert sizes
        for (int j = 0; j < timelineController.cards.Count; j++)
        {
            timelineController.cards[j].GetComponent<Card>().targetscale = timelineController.CardsScale;
        }
    }

    // Only for networked playerControllers that
    // aren't controlled by the local player
    public void NetworkPlaceCard(int cardIn, int cardOut, bool Correct)
    {
        if (isLocal())
            return;

        selected = Cards[cardOut];
        Cards.Remove(selected);
        timelineController.cards.Insert(cardIn, selected);
        timelineController.selected = null;
        timelineController.newCardPositions(Correct);

        if (!Correct)
            timelineController.cards.RemoveAt(cardIn);

        newCardPositions();

        if (Correct)
        {
            PlayerController p = gameController.localPlayer;
            p.StartCoroutine(p.PlaceCardAnim(selected.GetComponent<Animator>()));
            p.StartCoroutine(p.CardThudSound());
            selected = null;

            //if (!SuddenDeath.suddenDeath)
            Losing = false;

            turnController.StartTicking();
            turnController.StartThinkSound();
        }

        if (!Correct)
        {
            Debug.Log("network incorrect");
            StartCoroutine(Incorrect(selected, false));
        }
        // Revert sizes
        for (int j = 0; j < timelineController.cards.Count; j++)
        {
            timelineController.cards[j].GetComponent<Card>().targetscale = timelineController.CardsScale;
        }
        CardIndex = 0;
        selectedIndex = -1;
    }

    public void AIGetSelected()
    {
        if (!isHuman)
        {
            selected = null;
            timelineController.selected = null;
            int randomCard = Random.Range(0, Cards.Count - 1);
            selected = Cards[randomCard];
            Cards.Remove(selected);

            timelineController.selected = selected;

            int randomPlace = Random.Range(0, timelineController.cards.Count - 1);

            if (randomPos.GetComponent<Card>() == null)
                randomPos.AddComponent<Card>();

            CardData selectedData = selected.GetComponentInParent<Card>().data;
            CardData data = new CardData("", "", new Vector2(0, 0), 0, "", selectedData.year, selectedData.month, selectedData.day, -1);
            randomPos.GetComponent<Card>().data = data;

            randomPos.GetComponent<Card>().targetPos = new Vector3(timelineController.cards[randomPlace].GetComponentInParent<Card>().targetPos.x + spacing / 2,
                timelineController.cards[randomPlace].GetComponentInParent<Card>().targetPos.y);
            randomPos.transform.position = new Vector3(timelineController.cards[randomPlace].GetComponentInParent<Card>().targetPos.x + spacing / 2,
                timelineController.cards[randomPlace].GetComponentInParent<Card>().targetPos.y);

            selected.GetComponentInParent<Card>().targetscale = new Vector3
                (selected.GetComponentInParent<Card>().targetscale.x * actualScaleModifier,
                    selected.GetComponentInParent<Card>().targetscale.y * actualScaleModifier,
                    selected.GetComponentInParent<Card>().targetscale.z * actualScaleModifier);

            StartCoroutine(PlaceCardAI(3f, randomPlace));
        }
    }

    IEnumerator PlaceCardAI(float time, int index)
    {
        yield return new WaitForSeconds(time);
        if (!isHuman)
        {
            bool cardCorrect = timelineController.lookForCard();
            timelineController.cards.Insert(index + 1, selected);

            // Turn off card animations
            selected.GetComponent<Animator>().SetBool("Selected", false);
            foreach (GameObject a in Cards)
            {
                a.GetComponent<Animator>().SetBool("Selected", false);
            }

            selected.GetComponent<Card>().originalScale = timelineController.CardsScale;
            selected.GetComponent<Card>().targetscale = timelineController.CardsScale;
            selected.transform.SetParent(GameObject.Find("Cards").transform);
            timelineController.selected = null;
            timelineController.newCardPositions(cardCorrect);

            if (!cardCorrect)
                timelineController.cards.RemoveAt(index + 1);

            newCardPositions();
            if (cardCorrect)
            {
                StartCoroutine(PlaceCardAnim(selected.GetComponent<Animator>()));
                StartCoroutine(CardThudSound());
                selected = null;
                turnController.EndTurn(false);
                if (winCheckInstance.CheckWinner(turnController.currentPlayer))
                    gameController.localPlayer.YouLost_NoMessage();

                turnController.StartThinkSound();
                turnController.StartTicking();

            }
            if (!cardCorrect)
                yield return Incorrect(selected, true);
        }
    }

    IEnumerator Incorrect(GameObject Selected, bool endTurn)
    {
        if (isLocal() && isHuman)
        {
            Selected.GetComponent<Animator>().SetTrigger("Wrongplace");
            SquirrelPlayAnimOnce("ShakeHead");
            soundSquirrel.PlayOneShot(squirrelSad[Random.Range(0, squirrelSad.Length)]);
        }
        else
            Selected.GetComponent<Animator>().SetTrigger("Wrongplace");

        //if (!SuddenDeath.suddenDeath)
        Losing = true;

        gameController.localPlayer.EffectsCard.PlayOneShot(gameController.localPlayer.cardWrong);
        selected = null;
        timelineController.selected = null;

        yield return new WaitForSeconds(4);

        Selected.GetComponent<EventTrigger>().enabled = false;
        timelineController.newCardPositions(false);
        gameController.neutralDeck.Add(Selected.GetComponent<Card>().data);

        Selected.GetComponentInParent<Card>().targetPos = new Vector3(
            gameController.neutralDeckPos.localPosition.x,
            gameController.neutralDeckPos.localPosition.y,
            0.2f);

        Selected.GetComponentInParent<Card>().targetscale = originalCardScale;
        Selected.transform.SetParent(GameObject.Find("Cards").transform);
        Selected.GetComponentInParent<Card>().targetscale = gameController.neutralDeckScale;

        if (SuddenDeath.suddenDeath && !suddenDeathInstance.AllWonOrLost())
        {
            newCardPositions();
            RaiseEventOptions options = new RaiseEventOptions();
            options.Receivers = ReceiverGroup.All;

            photonManager.SendNetMessage(ID, options, 100);
        }
        //else if (!SuddenDeath.suddenDeath && isLocal())
        //{
        //    StartCoroutine(spawnCard(0.1f));
        //    RaiseEventOptions options = new RaiseEventOptions();
        //    options.Receivers = ReceiverGroup.All;
        //    photonManager.SendNetMessage(null, options, 105);
        //    Debug.Log("told you to win check");
        //}
        else if (!SuddenDeath.suddenDeath)
        {
            StartCoroutine(spawnCard(0.1f));
        }

        StartCoroutine(deleteSelected(4f, Selected));

        if (!isLocal() || !isHuman)
        {
            turnController.StartThinkSound();
            turnController.StartTicking();
        }

        if (endTurn)
            turnController.EndTurn(false);
    }

    // Spawn new card
    public IEnumerator spawnCard(float time)
    {
        newCardPositions();
        yield return new WaitForSeconds(time);

        GameObject newCard = gameController.topCard;
        newCard.transform.SetParent(transform);
        gameController.UpdateTopDeckCard();

        // turning date elements off after assigning is completed
        if (newCard.GetComponentInParent<Card>().data.day == 0)
        {
            //card.transform.Find("Description").gameObject.SetActive(true);
            Transform k = newCard.transform.GetChild(0);
            k.transform.Find("Date").gameObject.SetActive(false);
            k.transform.Find("Year").gameObject.SetActive(false);
            k.transform.Find("BigYear").gameObject.SetActive(false);
            k.transform.Find("Month").gameObject.SetActive(false);
            k.transform.Find("Day").gameObject.SetActive(false);
            k.transform.Find("FrontTitle").gameObject.SetActive(true);
        }

        Cards.Add(newCard);
        newCard.GetComponentInParent<Card>().targetscale = originalCardScale;
        newCard.GetComponentInParent<Card>().originalScale = GetComponent<PlayerController>().originalCardScale;
        newCard.name = "new card";
        newCardPositions();

        if (isLocal())
            gameController.AddListener(newCard.GetComponentInParent<EventTrigger>(), gameController.GetPlayerIndex(gameController.localPlayer));
        else
            gameController.AddHoverListeners(newCard.GetComponentInParent<EventTrigger>(), gameController.GetPlayerIndex(gameController.localPlayer));

        //newCard.GetComponentInParent<EventTrigger>().triggers.RemoveRange(0, 4);

        //Debug.Log("sudden death = " + SuddenDeath.suddenDeath);
        //Debug.Log("local = " + isLocal());

        // Win check when a networked playerController places a card incorrectly

        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;

        if (SuddenDeath.suddenDeath && !isLocal())
        {
            photonManager.SendNetMessage(null, options, 105);
        }
        else if (!SuddenDeath.suddenDeath && !isLocal())
        {
            Losing = true;
            //Debug.Log("round finished = " + turnController.RoundFinished());
            if (turnController.RoundFinished() && gameController.WinCheckLocalPlayer())
            {
                //Debug.Log("yoyoyoy");
                gameController.localPlayer.Win();

                // Send message that this player controller lost
                photonManager.SendNetMessage(ID, options, 101);
            }
        }
    }

    IEnumerator deleteSelected(float time, GameObject Selected)
    {
        yield return new WaitForSeconds(time);
        Selected.SetActive(false);
    }

    public void Win(bool sendLostPlayers = true)
    {
        if (hasWon || hasLost || SuddenDeath.roundCount == 3)
            return;

        StartCoroutine(WinOrDrawTimer(2.5f, "You won!"));
        Debug.Log("You won!");

        if (!SuddenDeath.suddenDeath && !GameController.soloPlay && sendLostPlayers)
            photonManager.SendGameOver();
        else if (SuddenDeath.suddenDeath && !GameController.soloPlay && sendLostPlayers)
            suddenDeathInstance.SetLostPlayers();

        yourTurn = false;
    }

    public void Draw()
    {
        StartCoroutine(WinOrDrawTimer(2.5f, "It's a draw!"));
        Debug.Log("It's a draw");
        turnController.Stop();

        if (!SuddenDeath.suddenDeath && !GameController.soloPlay)
            photonManager.SendGameOver();
        else if (SuddenDeath.suddenDeath && !GameController.soloPlay)
            suddenDeathInstance.SetLostPlayers();

        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;
        photonManager.SendNetMessage(null, options, 107);
    }

    public void ReceiveDraw(object obj, byte code)
    {
        if (code != 107 || Losing || hasLost)
            return;

        StartCoroutine(WinOrDrawTimer(2.5f, "It's a draw!"));
        Debug.Log("It's a draw");
        turnController.Stop();

        yourTurn = false;
    }

    IEnumerator WinOrDrawTimer(float time, string result)
    {
        yourTurn = false;
        yield return new WaitForSeconds(time);
        turnController.Stop();
        winMenu.SetActive(true);
        winMenu.transform.GetChild(0).GetComponentInChildren<Text>().text = result;
        SoundCard.PlayOneShot(Winner);
        yourTurn = false;
        GameController.gameRunning = false;
    }

    public void ReceiveLosingPlayer(object id, byte code)
    {
        if (code != 100)
            return;

        gameController.GetPlayer((int)id).Losing = true;
    }

    public void ReceiveLostPlayer(object id, byte code)
    {
        if (code != 101)
            return;

        Debug.Log("Received lost player");

        gameController.GetPlayer((int)id).YouLost_NoMessage();

        if (SuddenDeath.suddenDeath &&
            !suddenDeathInstance.AllWonOrLost())
        {

            if (gameController.WinCheckLocalPlayer())
            {
                gameController.localPlayer.Win(false);
            }
        }

        //RaiseEventOptions options = new RaiseEventOptions();
        //options.Receivers = ReceiverGroup.All;

        //photonManager.SendNetMessage(null, options, 105);
    }

    public void ReceiveWinCheck(object obj, byte code)
    {
        if (code != 105)
            return;

        Debug.Log("checking if you won");
        if (gameController.WinCheckLocalPlayer())
            Win();
    }

    public void YouLost_NoMessage()
    {
        if (hasLost || hasWon || SuddenDeath.roundCount == 3)
            return;

        hasLost = true;

        if (Cards.Count >= 1)
        {
            foreach (GameObject card in Cards)
            {
                if (card != null)
                {
                    gameController.neutralDeck.Add(card.GetComponentInParent<Card>().data);
                    card.SetActive(false);
                }
            }
        }

        if (!isLocal())
            return;

        Debug.Log(name + " has lost");

        StartCoroutine(LostTimer());

        yourTurn = false;
    }

    public void YouLost()
    {
        if (hasLost || hasWon || SuddenDeath.roundCount == 3)
            return;

        hasLost = true;

        if (Cards.Count >= 1)
        {
            foreach (GameObject card in Cards)
            {
                if (card != null)
                {
                    gameController.neutralDeck.Add(card.GetComponentInParent<Card>().data);
                    card.SetActive(false);
                }
            }
        }

        if (!isLocal())
            return;

        Debug.Log("You lost");

        StartCoroutine(LostTimer());

        yourTurn = false;

        if (SuddenDeath.suddenDeath)
        {
            RaiseEventOptions options = new RaiseEventOptions();
            options.Receivers = ReceiverGroup.All;
            photonManager.SendNetMessage(ID, options, 101);
        }
    }

    IEnumerator LostTimer()
    {
        yield return new WaitForSeconds(2.5f);
        GameController.gameRunning = false;
        turnController.Stop();
        lostMenu.transform.GetChild(1).gameObject.SetActive(false);
        lostMenu.SetActive(true);

        //if (!GameController.soloPlay && !SuddenDeath.suddenDeath && lostMenu != null)
        //{
        //    if(isLocal())
        //        lostMenu.transform.GetChild(0).GetComponent<Text>().text = "You lost!" + " Player " + playerId.ToString() + "\n won the game";
        //}
        //else if (GameController.soloPlay)
        //    lostMenu.transform.GetChild(0).GetComponent<Text>().text = "You lost!" + " Player " + turnController.currentPlayer + 1.ToString() + "\n won the game";
        //else 
        if (!GameController.soloPlay && SuddenDeath.suddenDeath)
        {
            //if(isLocal())
            //    lostMenu.transform.GetChild(0).GetComponent<Text>().text = "You lost!";

            SoundCard.PlayOneShot(Loser);

            yield return new WaitForSeconds(4f);

            //lostMenu.transform.GetChild(0).GetComponent<Text>().text = "";
            lostMenu.SetActive(false);
        }
        newCardPositions();
        timelineController.newCardPositions(true);
    }

    //re-organizes the player deck according to moved cards
    public void newCardPositions()
    {
        for (int i = 0; i < Cards.Count; i++)
        {
            Card card = null;
            if (Cards[i].GetComponentInParent<Card>() != null)
                card = Cards[i].GetComponentInParent<Card>();

            if(card != null)
            {
                Cards[i].GetComponentInParent<Card>().targetPos = new Vector3((i - Cards.Count * 0.5f + 0.5f) * spacing, 0);
            }
        }
    }

    public void newCardsScale(BaseEventData j)
    {
        if (selected == null)
        {
            actualScaleModifier = scaleModifier;
            Vector3 upSize = new Vector3(actualScaleModifier, actualScaleModifier, actualScaleModifier);
            GameObject cardToScale = null;
            PointerEventData data;
            data = j as PointerEventData;
            cardToScale = data.pointerEnter;
            Vector3 zoom = new Vector3(cardToScale.GetComponentInParent<Card>().targetPos.x,
                cardToScale.GetComponentInParent<Card>().targetPos.y, -zoomAmount);

            if (cardToScale != null)
            {
                cardToScale.GetComponentInParent<Card>().targetscale = upSize;
                cardToScale.GetComponentInParent<Card>().targetPos = zoom;
            }
        }
    }

    public void originalScale(BaseEventData j)
    {
        actualScaleModifier = 1f;
        GameObject cardToScale = null;
        PointerEventData data;
        data = j as PointerEventData;
        cardToScale = data.pointerEnter;
        Vector3 zoom = new Vector3(cardToScale.GetComponentInParent<Card>().targetPos.x,
            cardToScale.GetComponentInParent<Card>().targetPos.y, 0);

        if (selected == null && cardToScale != null)
        {
            cardToScale.GetComponentInParent<Card>().targetscale = cardToScale.GetComponentInParent<Card>().originalScale;
            cardToScale.GetComponentInParent<Card>().targetPos = zoom;
        }
    }

    void SquirrelPlayAnimOnce(string clipName)
    {
        if (!isHuman || !isLocal())
            return;

        RandomAnimation anim = squirrel.GetComponent<RandomAnimation>();

        if (anim != null)
            anim.PlayAnimByName(clipName);
    }

    void SquirrelPlayRandom(int index)
    {
        if (!isHuman || !isLocal())
            return;

        RandomAnimation anim = squirrel.GetComponent<RandomAnimation>();

        if (anim != null)
            anim.PlayRandomAnim(index);
    }

    void ReSizeRePosition()
    {
        Vector3 newScale = new Vector3(scaleModifier, scaleModifier, scaleModifier);

        GameObject leftCard = null;
        GameObject rightCard = null;

        Vector3 leftOffset = Vector3.zero;
        Vector3 rightOffset = Vector3.zero;

        foreach (GameObject card in timelineController.cards)
        {
            if (card.transform.position.x > selected.transform.position.x && !onPlayerDeck)
            {
                rightCard = card;
                break;
            }
        }
        foreach (GameObject card in timelineController.cards)
        {
            if (card.transform.position.x < selected.transform.position.x && !onPlayerDeck)
            {
                leftCard = card;
            }
        }
        for (int i = 0; i < timelineController.cards.Count; i++)
        {
            if (timelineController.cards[i] == rightCard)
            {
                rightOffset = rOffset[i];
            }
            else if (timelineController.cards[i] == leftCard)
            {
                leftOffset = lOffset[i];
            }
        }

        // upsize RIGHT card ONLY
        if (rightCard != null && leftCard == null)
        {
            rightCard.GetComponentInParent<Card>().targetscale = newScale;

            rightCard.GetComponentInParent<Card>().targetPos = rightOffset;

            for (int j = 0; j < timelineController.cards.Count; j++)
            {
                if (timelineController.cards[j] != rightCard)
                {
                    timelineController.cards[j].GetComponentInParent<Card>().targetscale = timelineController.CardsScale;
                }

                if (timelineController.cards[j].transform.position.x > rightCard.transform.position.x)
                    timelineController.cards[j].GetComponent<Card>().targetPos = rOffset[j];
            }
            // cheat
            if (bCheat && cheat != null)
                cheat.text = " Your: " + selected.GetComponent<Card>().data.year +
                    " R: " + rightCard.GetComponent<Card>().data.year;
        }
        // upsize LEFT card ONLY
        else if (leftCard != null && rightCard == null)
        {
            leftCard.GetComponentInParent<Card>().targetscale = newScale;
            leftCard.GetComponentInParent<Card>().targetPos = leftOffset;

            // Revert sizes
            for (int j = 0; j < timelineController.cards.Count; j++)
            {
                if (timelineController.cards[j] != leftCard && !onPlayerDeck)
                {
                    timelineController.cards[j].GetComponentInParent<Card>().targetscale = timelineController.CardsScale;
                }

                if (timelineController.cards[j].transform.position.x < leftCard.transform.position.x)
                    timelineController.cards[j].GetComponentInParent<Card>().targetPos = lOffset[j];
            }
            // cheat
            if (bCheat && cheat != null)
                cheat.text = " L: " +
                    leftCard.GetComponent<Card>().data.year + " Your: " + selected.GetComponent<Card>().data.year;
        }
        else if (leftCard != null && rightCard != null)
        {
            rightCard.GetComponentInParent<Card>().targetscale = newScale;
            leftCard.GetComponentInParent<Card>().targetscale = newScale;
            // cheat
            if (bCheat && cheat != null)
                cheat.text = "L: " + leftCard.GetComponent<Card>().data.year + " Your: " +
                    selected.GetComponent<Card>().data.year + " R: " + rightCard.GetComponent<Card>().data.year;
        }

        for (int i = 0; i < timelineController.cards.Count; i++)
        {

            if (timelineController.cards[i].transform.position.x < selected.transform.position.x)
            {
                timelineController.cards[i].GetComponentInParent<Card>().targetPos = lOffset[i];
            }
            else if (timelineController.cards[i].transform.position.x > selected.transform.position.x)
            {
                timelineController.cards[i].GetComponentInParent<Card>().targetPos = rOffset[i];
            }
        }

        // Revert sizes
        for (int j = 0; j < timelineController.cards.Count; j++)
        {
            if (timelineController.cards[j] != rightCard && timelineController.cards[j] != leftCard)
            {
                timelineController.cards[j].GetComponentInParent<Card>().targetscale = timelineController.CardsScale;
            }
        }
    }

    public bool isLocal()
    {
        return this == gameController.localPlayer;
    }

    public IEnumerator PlaceCardAnim(Animator anim)
    {
        yield return new WaitForSeconds(0);
        anim.SetTrigger("Placecard");
    }

    public void playSparkles(BaseEventData i)
    {
        if (!yourTurn)
            return;

        PointerEventData data;
        data = i as PointerEventData;
        CardAnimator = data.pointerEnter.gameObject.GetComponentInParent<Animator>();

        SoundCard.clip = cardSparkles;
        SoundCard.Play();
        CardAnimator.SetBool("BlueGlow", true);
    }

    public void stopSparkles(BaseEventData i)
    {
        if (!yourTurn)
            return;

        PointerEventData data;
        data = i as PointerEventData;

        if (selected != null)
            CardAnimator = selected.GetComponentInParent<Animator>();

        SoundCard.Stop();
        CardAnimator.SetBool("BlueGlow", false);
    }

    public IEnumerator CardThudSound()
    {
        yield return new WaitForSeconds(SoundDelay);
        EffectsCard.PlayOneShot(cardThud);
        StopCoroutine(CardThudSound());
    }
}
