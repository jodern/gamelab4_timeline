﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon.LoadBalancing;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

public class MaintainConnection : MonoBehaviour {
    public TimelineClient client;

    string appId = "84b8681-284b-4774-a5d5-0c8062752bae";

    // Use this for initialization
    void Awake () {
        Application.runInBackground = true;
		DontDestroyOnLoad(gameObject);
        
		ClientSetup ();

        //bool successRegionConnect = client.ConnectToRegionMaster("EU");
        //print("Connected to region: " + successRegionConnect);

        StartCoroutine(WaitForClientReady());

        StartCoroutine("CheckConnected");
	}
	
	// Update is called once per frame
	void Update () {
        client.Service();
	}

	void ClientSetup()
	{
		client = GameSetup.currentGame.client;
		//client.NickName = Random.Range(0, 10000).ToString("0+000");
        client.NickName = Profile.playerName;
        client.AppId = appId;
	}

    void ConnectToRoom()
    {
        bool createSuccess;
        print("Client ready: " + client.IsConnectedAndReady);

        if (GameSetup.currentGame.createGame)
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = GameSetup.currentGame.playerCount;
            options.IsVisible = true;
            options.IsOpen = true;
            options.EmptyRoomTtl = 0;
			options.PlayerTtl = 0;
			options.CustomRoomPropertiesForLobby = new string[] { "name", "creator" };
			options.CustomRoomProperties = new Hashtable() {{ "id",  DeckHolder.instance.selectedDeck.ID }};
            createSuccess = client.OpCreateRoom(GameSetup.currentGame.name, options, TypedLobby.Default);
			StartCoroutine(WaitRoomConnect());

            print(createSuccess);
        }
        else
        {
            if (GameSetup.currentGame.name == "")
            {
                Hashtable expectedProperties = new Hashtable();
                createSuccess = client.OpJoinRandomRoom(expectedProperties, GameSetup.currentGame.playerCount);
                print(createSuccess);
            }
            else
            {
                RoomOptions options = new RoomOptions();
                options.MaxPlayers = GameSetup.currentGame.playerCount;
                options.IsVisible = true;
                options.IsOpen = true;
				options.EmptyRoomTtl = 0;
				options.PlayerTtl = 0;
				options.CustomRoomPropertiesForLobby = new string[] { "name", "creator" };
				options.CustomRoomProperties = new Hashtable();
				if(DeckHolder.instance.selectedDeck != null)
					options.CustomRoomProperties = new Hashtable() {{ "id",  DeckHolder.instance.selectedDeck.ID }};
				createSuccess = client.OpJoinOrCreateRoom(GameSetup.currentGame.name, options, TypedLobby.Default);
				if (client.LocalPlayer.IsMasterClient)
					StartCoroutine(WaitRoomConnect());
            }
        }
    }


	IEnumerator WaitRoomConnect()
	{
		yield return new WaitForSeconds(1);
		Hashtable table = new Hashtable();
		table["name"] = DeckHolder.instance.selectedDeck.name;
		table["creator"] = Profile.playerName;
		table["id"] = DeckHolder.instance.selectedDeck.ID;
		client.CurrentRoom.SetCustomProperties(table);
	}

    IEnumerator WaitForClientReady()
    {
        yield return new WaitWhile(() => !client.IsConnectedAndReady);
        ConnectToRoom();
    }

    IEnumerator CheckConnected()
    {
        yield return new WaitForSeconds(5);
        if (client.State != ClientState.Joined)
        {
			PhotonRemovePlayer (client.LocalPlayer);
//			Destroy (gameObject);
//            OnClickLoadLevel("Main Menu");
        }
    }

    public void OnClickLoadLevel(string level)
    {
        SceneManager.LoadScene(level);
		PhotonDisconnect();
    }

	public void PhotonRemovePlayer(Player player)
	{
        if(client != null)
		    client.CurrentRoom.RemovePlayer(player);
	}

    public void PhotonDisconnect()
    {
        if (client != null && client.loadBalancingPeer != null)
        {
            client.Disconnect();
            client.loadBalancingPeer.StopThread();
			client = null;
            Destroy(gameObject);
        }
    }

	void OnApplicationQuit()
	{
		PhotonDisconnect();
	}
}
