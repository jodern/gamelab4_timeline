﻿using UnityEngine;
using System.Collections;

public class DragTabs : MonoBehaviour {

	[System.Serializable]
	public struct DragTab
	{
		public RectTransform l;
		public RectTransform r;
	}
	public DragTab[] dragTabs;
	public DragTab[][] gameTabList;

	[SerializeField] float scale = 0.8f;

	public void OnDragTab(int i)
	{

		float horizView = Input.mousePosition.x / Screen.width / scale;

		bool canDrag;			
		if (i == 0)
		{
			canDrag = (i == 0 && horizView > 0.02f && horizView + 0.02f < dragTabs[i + 1].l.anchorMax.x);
		}
		else if (i == dragTabs.Length - 1)
		{
			canDrag = (i == dragTabs.Length - 1 && horizView - 0.02f > dragTabs[i - 1].r.anchorMin.x && horizView < 0.93f );
		}
		else
		{
			canDrag = (horizView - 0.02f > dragTabs[i - 1].r.anchorMin.x && horizView + 0.02f < dragTabs[i + 1].l.anchorMax.x);
		}

		if (!canDrag)
			return;

		dragTabs[i].r.anchorMin = new Vector2(horizView, 0.5f);
		dragTabs[i].l.anchorMax = new Vector2(horizView, 0.5f);

		dragTabs[i].r.anchoredPosition = Vector2.zero;
		dragTabs[i].l.anchoredPosition = Vector2.zero;

		if (gameTabList == null)
			return;

		for(int l = 0; l < gameTabList.Length; l++)
		{

			if (gameTabList[l][i].r != null)
			{
				gameTabList[l][i].r.anchorMin = new Vector2(horizView, 0.5f);
				gameTabList[l][i].r.anchoredPosition = Vector2.zero;
			}

			if (gameTabList[l][i].l != null)
			{
				gameTabList[l][i].l.anchorMax = new Vector2(horizView, 0.5f);
				gameTabList[l][i].l.anchoredPosition = Vector2.zero;
			}
		}
	}
}
