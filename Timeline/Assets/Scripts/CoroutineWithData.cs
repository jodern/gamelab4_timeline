﻿using UnityEngine;
using System.Collections;

public class CoroutineWithData {

	public Coroutine coroutine;
	public object result;
	IEnumerator target;


	public CoroutineWithData(MonoBehaviour owner, IEnumerator target)
	{
		this.target = target;
		this.coroutine = owner.StartCoroutine(Run());
	}


	IEnumerator Run()
	{
		while (target.MoveNext())
		{
			result = target.Current;
			yield return result;
		}
	}
}
