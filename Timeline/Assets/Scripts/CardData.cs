﻿using UnityEngine;
using System.Collections; 
using ExitGames.Client.Photon;

[System.Serializable]
public class CardData {

	public string title = "Title";
	public string imageURL = "Default";
    public Vector2 imageOffset = Vector2.zero;
	public float zoom = 1;

	public string description = "";

	public int year = 1;
	public enum Months
	{
		January,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		November,
		December
	}
	public Months month = Months.January; 
    public byte day = 1;

	public int ID = -1;

	public CardData(string title, string imageURL, Vector2 imageOffset, float zoom, string description, int year, Months month, byte day, int ID)
    {
		this.ID = ID;
        this.title = title;
        this.imageURL = imageURL;
        this.imageOffset = imageOffset;
        this.zoom = zoom;
        this.description = description;
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public CardData(int ID) 
	{
		this.ID = ID;
        year = System.DateTime.Now.Year;
        month = (Months)System.DateTime.Now.Month - 1;
        day = (byte) System.DateTime.Now.Day;
	}
}
