<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

// Show PHP version information?
//phpinfo(); exit;

// Response as plain text (utf-8):
header( "Content-Type: text/plain; charset=utf-8" ); 

// Init:
$filename = "Timeline";

// Open database (SQlite3 object):
$db_handle = new SQLite3( $filename );

//$sql = "SELECT * FROM score WHERE name LIKE '%" . $name. "%'";
$decks = $db_handle->query( "SELECT * FROM decks" );

$array = array();
while ( $row = $decks->fetchArray( SQLITE3_ASSOC ) ) { // SQLITE3_ASSOC|SQLITE3_NUM|SQLITE3_BOTH(default)
	$cardArray = array();
	$cards = $db_handle->query( "SELECT * FROM cards WHERE deckID LIKE " . $row['id']);
	while( $cardRow = $cards->fetchArray( SQLITE3_ASSOC ))
	{
		$cardArray[] = [ 'id' => $cardRow['id'], 
						 'deckID' => $cardRow['deckID'], 
						 'title' => $cardRow['title'], 
						 'url' => $cardRow['url'], 
						 'offsetX' => $cardRow['offsetX'], 
						 'offsetY' => $cardRow['offsetY'], 
						 'zoom' => $cardRow['zoom'], 
						 'date' => $cardRow['date'] ];
	}
	$array[] = [ 'id' => $row['id'],'creationDate' => $row['creationDate'],'name' => $row['name'],'owner' => $row['owner'], 'cards' => $cardArray ];
}
if(count($array) == 0)
	echo "ERROR: No decks found!";
else
	echo json_encode($array);

?>