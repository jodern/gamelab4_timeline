<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

header( "Content-Type: text/plain; charset=utf-8" );

$filename = "Timeline";

$name = "";
$owner = "";

if ( isset($_REQUEST['name']) ) $name = $_REQUEST['name'];
if ( isset($_REQUEST['owner']) ) $owner = $_REQUEST['owner'];

$name = SQLite3::escapeString($name);
$owner = SQLite3::escapeString($owner);

if($name == "" || $owner == "")
	echo "ERROR: Invalid owner or name! " . $name . " " . $owner;
else
{
	$db_handle = new SQLite3( $filename );
	$ownerDecks = $db_handle->query( "SELECT * FROM decks WHERE owner LIKE '" . $owner . "'" );
	while($row = $ownerDecks->fetchArray( SQLITE3_ASSOC ))
	{
		if($row['name'] == $name)
		{
			echo "ERROR: owner already has a deck named " . $owner;	
			return;
		}
	}
	$sql = "INSERT INTO decks (name, owner) VALUES ('" . $name . "', '" . $owner . "')";
	$db_handle->query( $sql );
	$id = $db_handle->lastInsertRowID();
	
	echo $id;
}

?>