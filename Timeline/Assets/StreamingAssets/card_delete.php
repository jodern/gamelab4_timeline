<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

header( "Content-Type: text/plain; charset=utf-8" );

$filename = "Timeline";

$id = -1;

if ( isset($_REQUEST['id']) ) $id = $_REQUEST['id'];

if($id == -1)
	echo "ERROR: Invalid id! ";
else
{
	$db_handle = new SQLite3( $filename );
	$db_handle->query( "DELETE FROM cards WHERE id LIKE " . $id );
	
	echo "OK: Deleted card " . $id;
}

?>