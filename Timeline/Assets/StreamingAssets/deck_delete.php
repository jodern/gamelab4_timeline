<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

header( "Content-Type: text/plain; charset=utf-8" );

$filename = "Timeline";

$id = "";

if ( isset($_REQUEST['id']) ) $id = $_REQUEST['id'];

if($id == "")
	echo "ERROR: No ID specified";
else
{
	$db_handle = new SQLite3( $filename );
	$db_handle->query( "DELETE FROM decks WHERE id LIKE " . $id);
	$db_handle->query( "DELETE FROM cards WHERE deckID LIKE " . $id);

	echo "OK: Deleted deck " . $id;
}

?>