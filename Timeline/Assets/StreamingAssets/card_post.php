<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

header( "Content-Type: text/plain; charset=utf-8" );

$filename = "Timeline";

$deckID = -1;
$name = "";
$url = "";
$offsetX = 0;
$offsetY = 0;
$zoom = 120;
$date = "";
$id = -1;

if ( isset($_REQUEST['deckID']) ) $deckID = $_REQUEST['deckID'];
if ( isset($_REQUEST['name']) ) $name = $_REQUEST['name'];
if ( isset($_REQUEST['url']) ) $url = $_REQUEST['url'];
if ( isset($_REQUEST['offsetX']) ) $offsetX = $_REQUEST['offsetX'];
if ( isset($_REQUEST['offsetY']) ) $offsetY = $_REQUEST['offsetY'];
if ( isset($_REQUEST['zoom']) ) $zoom = $_REQUEST['zoom'];
if ( isset($_REQUEST['date']) ) $date = $_REQUEST['date'];
if ( isset($_REQUEST['id']) ) $id = $_REQUEST['id'];

$name = SQLite3::escapeString($name);
$url = SQLite3::escapeString($url);
$date = SQLite3::escapeString($date);

if($name == "" || $deckID == -1 || $date == null)
	echo "ERROR: Invalid card data! ";
else
{
	$db_handle = new SQLite3( $filename );
	
	if($id == -1)
	{
		$sql = "INSERT INTO cards (deckID, title, url, offsetX, offsetY, zoom, date) VALUES ('" . 
			$deckID . "','" . 
			$name . "','" . 
			$url . "'," . 
			$offsetX . "," . 
			$offsetY . "," . 
			$zoom . ",'" . 
			$date . "')";
		$db_handle->query( $sql );
		$id = $db_handle->lastInsertRowID();
	}
	else
	{
		$sql = "UPDATE cards SET " .
			"deckID = '" . $deckID .
			"',title = '" . $name .
			"',url = '" . $url .
			"',offsetX = " . $offsetX .
			",offsetY = "  .$offsetY .
			",zoom = " . $zoom .
			",date = '" . $date .
			"' WHERE id LIKE " . $id;
		$db_handle->query( $sql );
	}
	
	echo $id;
}

?>