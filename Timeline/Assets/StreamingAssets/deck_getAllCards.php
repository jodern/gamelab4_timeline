<?php

// SQLite Documentation: https://www.sqlite.org/docs.html
// PHP SQLite3 Manual: http://php.net/manual/en/book.sqlite3.php

// Display errors?
error_reporting( E_ALL );
ini_set( "display_errors", "1" );

// Response as plain text (utf-8):
header( "Content-Type: text/plain; charset=utf-8" ); 

// Init:
$filename = "Timeline";

$name = "";
$owner = "";

if ( isset($_REQUEST['name']) ) $name = $_REQUEST['name'];
if ( isset($_REQUEST['owner']) ) $owner = $_REQUEST['owner'];

$name = SQLite3::escapeString($name);
$owner = SQLite3::escapeString($owner);

if($name == "" || $owner == "")
{
	echo "ERROR: name or owner not defined!";
}
else
{
	$db_handle = new SQLite3( $filename );
	
	
	$deck = $db_handle->query( "SELECT * FROM decks WHERE name LIKE '" . $name . "' AND owner LIKE '" . $owner . "'" );
	
	$data = array();
	while($row = $deck->fetchArray( SQLITE3_ASSOC ) )
	{
		$cards = $db_handle->query( "SELECT * FROM cards WHERE deckID LIKE " . $row['id'] );
		while($cardRow = $cards-fetchArray( SQLITE3_ASSOC ))
		{
			$cardArray[] = [ 'id' => $cardRow['id'], 
							 'deckID' => $cardRow['deckID'], 
							 'title' => $cardRow['title'], 
							 'url' => $cardRow['url'], 
							 'offsetX' => $cardRow['offsetX'], 
							 'offsetY' => $cardRow['offsetY'], 
							 'zoom' => $cardRow['zoom'], 
							 'date' => $cardRow['date'] ];
		}
		$data = array( 'id' => $row['id'],'creationDate' => $row['creationDate'],'name' => $row['name'],'owner' => $row['owner'], 'cards' => $cardArray );
	}
	if(count($data) == 0)
		echo "ERROR: No deck found!";
	else
		echo json_encode($data);
}

?>