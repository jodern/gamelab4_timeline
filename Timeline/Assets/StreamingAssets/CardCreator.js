	"use strict";
	function Deck (name, owner)
	{
		this.name = name;
		this.owner = owner;
		this.id = -1;
		this.cards = [];
	}
	
	var Months = {January: 0, February: 1, March: 2, April: 3, May: 4, June: 5, July: 6, August:7, September:8, October: 9, Novermeber: 10, December: 11,};
	function Card(title, url, offsetX, offsetY, unityOffsetX, unityOffsetY, zoom, unityZoom, year, month, day, backgroundColor,id)
	{	
		this.title = title;
		this.url = url;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		this.unityOffsetX = unityOffsetX;
		this.unityOffsetY = unityOffsetY;
		this.zoom = zoom; 
		this.unityZoom = unityZoom;
		this.year = year;
		this.month = month;
		this.day = day;
		this.backgroundColor = backgroundColor;
		this.id = id;
	}
	
	
	var decks = [];
	var selectedDeck = 0;
	
	
	$(document).ready(function() 
	{
		$("img").on('load',checkDisplayImg());
		
		
		decks = [];
		selectedDeck = 0;
		$("#deleteDeck").removeAttr('disabled');
		$("#downloadButton").removeAttr('disabled');
		$("#colorPicker :input").removeAttr('disabled');
		$("#dateInput").prop('defaultValue', new Date().toDateInputValue());
		$("#yearInput").prop('defaultValue', new Date().getFullYear());
		$("#colorPicker :input").val("#FFFFFF");
		
		getAllDecks();
		
		checkEnableInput();
		buildPreviewTimeline();
		setDisplayDate();
		
		$("#deckNameInput").keydown(function(event) 
		{
			if(event.which === 13)
			{
				createDeck();
			}
		});
		
		$("#urlInput").keydown(function(event) 
		{
			if(event.which === 13)
			{
				loadImage();
			}
		});
	});
	
	
	function getAllDecks()
	{
		$.ajax({
			Type: "GET",
			url: "deck_getAll.php",
			data: "",
			dataType: "json",
			success: function(data){
				if(data.indexOf('ERROR') > - 1)
				{
					alert( "Error: " + data );
				}
				else
				{	
					console.log("Retrieved all decks");
					console.log(data);
					
					for(var i = 0; i < data.length; i++)
					{
						var newDeck = new Deck(data[i].name,data[i].owner);
						newDeck.id = data[i].id;
						var cards = new Array();
						for(var j = 0; j < data[i].cards.length; j++)
						{
							var d = data[i].cards[j];
							var date = d.date.split('-');
							var newDeckCard = new Card(d.title, d.url, 0, 0, d.offsetX, d.offsetY, d.zoom, d.zoom, parseInt(date[0]), parseInt(date[1]), parseInt(date[2]), "#FFFFFF", d.id);
							cards.push(newDeckCard);
						}
						newDeck.cards = cards;
						decks.push(newDeck);
					}
						buildDeckPicker();
						$("#deckPicker").prop('selectedIndex', decks.length - 1);
						setSelected();
						decks[selectedDeck].cards.sort(sortDeckByDate);
						checkEnableInput();
						buildPreviewTimeline();
						newCard();
				}
			},
			error: function (xhr, ajaxOptions, thrownError)
			{
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	
	
		
	function checkDisplayImg()
	{
		$(".displayImageContainer").each(function()
		{
			var refH = $(this).height();
			var refW = $(this).width();
			var refRatio = refW/refH;
		
			var imgH = $(this).children("img").height();
			var imgW = $(this).children("img").width();
		
			if ( (imgW/imgH) > refRatio ) 
			{ 
				$(this).addClass("portrait");
				$(this).removeClass("landscape");
			} 
			else 
			{
				$(this).addClass("landscape");
				$(this).removeClass("portrait");
			}
		});
	}
	
 	
	
	function checkEnableInput()
	{
		var state = decks.length > 0;
			
		if(state)
		{
			$("#cardInput :input").removeAttr("disabled");
			$("#colorPicker").css('display', 'inline');
			$("#deleteDeck").css('display','inline');
			if(decks[selectedDeck].cards.length > 0)
				$("#downloadButton").css('display','inline');
			else
				$("#downloadButton").css('display', 'none');
		}
		else
		{
			$("#cardInput :input").attr("disabled", true);
			$("#colorPicker").css('display', 'none');
			$("#deleteDeck").css('display','none');
			$("#downloadButton").css('display', 'none');
		}
	}
	
	
	function createDeck()
	{
		var name = $("#deckNameInput").val();
		if(name === null || name.length === 0)
		{
			console.log("Invalid name");
			return;
		}
		for(var i = 0; i < decks.length; i++)
		{
			if(decks[i].name === name)
			{
				console.log("A deck already exists with that name!");
				return;	
			}
		}
			
		var deck = new Deck(name, "Somebody");
		var varString = ""
		//Add new deck to decks
		decks.push(deck);
		postDeckData();
		//Update deckpicker with new deck
		buildDeckPicker();
		$("#deckPicker").prop('selectedIndex', decks.length - 1);
		setSelected();
		checkEnableInput();
		buildPreviewTimeline();
		newCard();
	}
	
	
	function postDeckData()
	{
		var name = $("#deckNameInput").val();
		$.ajax({
			Type: "POST",
			url: "deck_post.php",
			data: {
				name : name, 
				owner : "Somebody"
			},
			dataType: "text",
			success: function(data){
				if(data.indexOf('ERROR') > -1)
					alert( "Error: " + data );
				else
				{
					console.log("Saved deck with ID: " + data);
					decks[selectedDeck].id = data;
				}
			},
			error: function (xhr, ajaxOptions, thrownError)
			{
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	
	
	function buildDeckPicker()
	{
		var deckPicker = $("#deckPicker");
		//Don't show dropdown if there are no decks
				//Clear all options
		deckPicker.find('option').remove();
		if(decks.length === 0)
		{
			$("#deleteDeck").css('display', 'none');
			deckPicker.css('display', 'none');
			return;	
		}
		else
		{
			$("#deleteDeck").css('display', 'inline');
			deckPicker.css('display', 'inline');
		}
		var numDecks = decks.length;
		for(var i = 0; i < numDecks; i++)
		{
			deckPicker.append('<option value="' + i + '">' + decks[i].name + ' (' + decks[i].cards.length + ')</option>' );
		}
		deckPicker.prop('selectedIndex', selectedDeck);
	}
	
	
	function deleteDeck()
	{
		var index = $("#deckPicker").val();
		var id = decks[index].id;
		deleteDeckData(id);
		decks.splice(index, 1);
		buildDeckPicker();
		checkEnableInput();	
		buildPreviewTimeline();
		newCard();
	}
	
	
	function deleteDeckData(id)
	{
		$.ajax({
			Type: "POST",
			url: "deck_delete.php",
			data: {
				id : id, 
			},
			dataType: "text",
			success: function(data){
				if(data.indexOf('ERROR') > -1)
					alert( "Error: " + data );
				else
				{
					console.log("Deleted deck with ID: " + data);
				}
			},
			error: function (xhr, ajaxOptions, thrownError)
			{
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	
	
	function deleteCard(i)
	{
		var card = $("#previewTimelineContainer").children().eq(i);
		//Only clear input if you are deleting a card you are editing
		if(card.attr('id') === 'edit')
			newCard();
		var id = decks[selectedDeck].cards[i].id;
		deleteCardData(id);
		decks[selectedDeck].cards.splice(i, 1);
		buildPreviewTimeline();
		buildDeckPicker();
		checkEnableInput();
	}
	
	
	function deleteCardData(id)
	{
		$.ajax({
			Type: "POST",
			url: "card_delete.php",
			data: {
				id : id, 
			},
			dataType: "text",
			success: function(data){
				if(data.indexOf('ERROR') > -1)
					alert( "Error: " + data );
				else
				{
					console.log("Deleted card with ID: " + data);
				}
			},
			error: function (xhr, ajaxOptions, thrownError)
			{
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	
	
	//Copy timeline card data to display and input
	function selectTimelineCard(i)
	{
		var card = decks[selectedDeck].cards[i];
		if(card === null)
			return;
			
		//Clear edit and input fields
			
		var c = $("#previewTimelineContainer").children().get(i);
		if(c.id === 'edit')
		{
			c.removeAttribute('id');
			newCard();
			return;	
		}
		newCard();
		c.style.border = '4px solid red';
		c.id = 'edit';
			
		$("#titleInput").val(card.title);
		$("#urlInput").val(card.url);
		var yearOnly = card.day === 0;
		if(yearOnly)
		{
			$("#yearInput").val(card.year);
			$("#yearOnly").prop('checked', 'checked');
		}
		else
		{
			var day = ("0" + card.day).slice(-2);
			var month = ("0" + (new Date().getMonth() + 1)).slice(-2);
			var year = ("0" + (card.year)).slice(-2);
			var date = new Date(year, month, day);
			$("#dateInput").val(date);
			$("#yearOnly").removeAttr('checked');	
		}
		toggleYearOnly(yearOnly);
		var img = $("#displayImage");
		img.attr('src', card.url);
		img.css('top', card.offsetY * $("#displayImageContainer").height());
		img.css('left', card.offsetX * $("#displayImageContainer").width());
		$("#displayTitle").val(card.title);
		$("#zoomSlider").val(card.zoom);
		setZoom(card.zoom);
		$("#colorPicker :input").val(card.backgroundColor);
		changeBGColor(card.backgroundColor);
		clampAndSetImg(img, parseInt(img.css('top')), parseInt(img.css('left')));
	}
	
	
	function endEdit()
	{
		$("#previewTimelineContainer").children().each(function() {
            $(this).removeAttr('id');
			$(this).css('border', '');
        });	
	}
	
	
	//Create display cards
	function buildPreviewTimeline()
	{
		var container = $("#previewTimelineContainer");
		var deck = decks[selectedDeck];
			
		//Remove existing cards
		container.empty();
		
		//If there are cards, set height
		if(typeof deck !== 'undefined' && deck.cards.length > 0)
		{
			//container.css('display','block');
		}
		else
		{
			//container.css('display','none');
			return;
		}
		//Rebuild cards
		for(var i = 0; i < deck.cards.length; i++)
		{
			var card = deck.cards[i];
			//Create a div and append to container
			var timelineCard = $("<div class=\"timelineCard \"style=\"left:" + ((210 * i) + 10) + "px\" ></div>"); 
			container.append(timelineCard);
			//Append title to card
			timelineCard.append("<div id=\"timelineCardTitle\">" + card.title + "</div>");
			timelineCard.append($("<input type=\"button\" class=\"editButton\" value=\"Edit\" onClick=\"selectTimelineCard(" + i + ")\" >"));
			//Create an image container, apply same rules as display card
			var imgContainer = $("<div class=\"displayImageContainer\"></div>");
			imgContainer.css('background-color', card.backgroundColor);
			timelineCard.append(imgContainer);
			//Create image, set url
			var img =  $("<img alt=\"image\" src=\"" + card.url + "\" onmousedown=\"return false\"></img>");
			
			//Set positions of img
			var w = parseFloat(imgContainer.css('width'));
			var h = parseFloat(imgContainer.css('height'));
			var left = card.offsetX * w + 'px';
			var top = card.offsetY * h + 'px';
			
			imgContainer.append(img);
			img.css('left', left);
			img.css('top', top);
			checkDisplayImg();
			var zoom = card.zoom;
			if(img.hasClass('portrait'))
			{
				img.css('height', zoom + '%');
				img.css('width', 'auto');
			}
			else
			{
				img.css('width', zoom + '%');
				img.css('height', 'auto');
			}
			clampAndSetImg(img, parseInt(img.css('top')), parseInt(img.css('left')));
			imgContainer.append($("<input type=\"button\" class=\"deleteButton\" value=\"X\" onClick=\"deleteCard(" + i + ")\" >"));
			
			var date = card.year;
			if(card.day !== 0)
			{
				date = card.day + "-" + card.month + "-" + card.year;
			}
			imgContainer.append($("<div class=\"dateContainer\" >" + date + "</div>"));
		}
	}
	
	
	Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
	});

	
	window.addEventListener("resize", function()
	{
		console.log("Resize!");
		var mapElement = document.getElementById("cardDisplay");
		mapElement.style.height = mapElement.offsetWidth * 1.72;
	});
	
	
	function toggleYearOnly( t )
	{
		var year = document.getElementById("year");
		var date = document.getElementById("date");
		if(t)
		{
			year.style.display = 'inline';
			date.style.display = 'none';
		}
		else 
		{                                                               
			year.style.display = 'none';
			date.style.display = 'inline';
		}
		setDisplayDate();
	}
	
	
	function sortDeckByDate(a, b)
	{
		if(a.year === b.year)
		{
			if(a.month === b.month)
			{
				return a.day - b.day;	
			}
			return a.month - b.month;
		}
		return a.year - b.year;
	}
	
	
	Number.prototype.clamp = function(min, max) 
	{
  		return Math.min(Math.max(this, min), max);
	};
	
	function setZoom( value )
	{
		var zoomVal = $("#zoomValue");
		zoomVal.html(value);
		var slider = $("#zoomSlider");
		var percVal = ((slider.val() - slider.attr('min')) / (slider.attr('max') - slider.attr('min'))) * 100;
		zoomVal.css('left', percVal + '%');
		var container = $("#displayImageContainer");
		var img = $("#displayImage");
		if(img.attr('src') === '')
			return;
		
		if(container.hasClass('portrait'))
		{
			img.css('width',  value + '%');
			img.css('height', 'auto');
		}
		else
		{
			img.css('height', value + '%');
			img.css('width', 'auto');
			//img.style.left = startX + (50 - value * 0.5) + 2.5 + '%';
			//img.style.top = startY + (12.5 - value * 0.125) + 50 + '%';
		}
		checkDisplayImg();
		clampAndSetImg(img, parseInt(img.css('top')), parseInt(img.css('left')));
	}
	
	
	function setSelected()
	{
		selectedDeck = $("#deckPicker").prop('selectedIndex');	
		buildPreviewTimeline();
		newCard();
		checkEnableInput();
	}
	
	
	function setDisplayDate()
	{
		if($("#yearOnly").is(':checked'))
		{
			$("#displayDate").html($("#yearInput").val());	
		}
		else
		{
			$("#displayDate").html($("#dateInput").val());	
		}
	}
	
	
	function changeBGColor(color)
	{
		$("#displayImageContainer").css('background-color',color);	
	}
	
	
	function loadImage()
	{
		var url = $("#urlInput").val();
		var img = $("#displayImage");
		img.attr('src', url);
		img.css('top', '50%');
		img.css('left', '0');
		clampAndSetImg(img, parseInt(img.css('top')), parseInt(img.css('left')));
		checkDisplayImg();
		var container = $("#displayImageContainer");
		var zoomSet;
		if(img.hasClass('portrait'))
		{
			zoomSet = img.height() / container.height() * 100;
		}
		else
		{
			zoomSet = img.width() / container.width() * 100;
		}
		$("#zoomSlider").val(zoomSet);
	}
	
	
	function checkLoad(value)
	{
		if(value === '')
		{
			$("#loadButton").attr('disabled', 'disabled');	
		}
		else
		{
			$("#loadButton").removeAttr('disabled');	
		}
	}
	
	
	$(function() 
	{
   		$('#displayImage').slider();
	});

	$.fn.slider = function() 
	{
		return this.each(function() 
		{
			var $el = $(this);
			$el.css('top', '50%');
			$el.css('left', '0');
			var dragging = false;
			var startY = 0;
			var startT = 0;
			var startX = 0;
			var startL = 0;
			var oldY = 0;
			var oldX = 0;
			
			var top = startT;
			var left = startT;
			$el.mousedown(function(ev) 
			{
				dragging = true;
				startY = ev.clientY;
				startT = $el.css('top');
				startX = ev.clientX;
				startL = $el.css('left');
				oldY = parseInt(startT);
				oldX = parseInt(startL);
			});
			$(window).mousemove(function(ev) 
			{
				if (dragging) 
				{
					// calculate new top
					top += (parseInt(startT) + (ev.clientY - startY)) - oldY;
					left += (parseInt(startL) + (ev.clientX - startX)) - oldX;

					oldY = parseInt(top);
					oldX = parseInt(left);
					
					clampAndSetImg($el, top, left);
				}
			}).mouseup(function() 
			{
				dragging = false;
			});
		});
	};
	
	
	function clampAndSetImg($img, top, left)
	{
		//stay in parent
		var maxTop =  $img.parent().height()-$img.height(); 
		var maxLeft = $img.parent().width()-$img.width(); 
		
		if(maxTop > 0)   
			top = top.clamp(0, maxTop);
		else
			top = top.clamp(maxTop, 0);
		if(maxLeft > 0)     
			left = left.clamp(0, maxLeft);
		else
			left = left.clamp(maxLeft, 0);
		
		$img.css('top', top );
		$img.css('left', left );
	}
	
	
	function newCard()
	{
		$("#titleInput").val("");
		$("#urlInput").val("");
		$("#loadButton").attr('disabled', 'disabled');	
		$("#yearInput").val(new Date().getFullYear());
		$("#dateInput").val(new Date().toDateInputValue());
		$("#zoomSlider").val(100);
		$("#yearOnly").prop('checked', 'checked');
		toggleYearOnly(true);
		var img = $("#displayImage");
		img.attr('src', "");
		img.css('top', '50%');
		img.css('left', '0');
		img.css('width', '100%');
		$("#displayTitle").html('Title');
		$("#colorPicker :input").val("#FFFFFF");
		changeBGColor("#FFFFFF");
		endEdit();
	}
	
	
	function saveCard()
	{
		if(decks.length === 0 || decks === null || $("#titleInput").val() === "")
			return;
		
		var year, month, day;
		if($("#yearOnly").is(':checked'))
		{
			year = $("#yearInput").val();
			month = 1;
			day = 0;
		}
		else
		{
			var date = $("#dateInput").val().split('-');
			year = date[0];
			month = 1;
			day = date[2];
		}
		var w = parseFloat($("#displayImageContainer").css('width'));
		var h = parseFloat($("#displayImageContainer").css('height'));
		var imgW = parseFloat($("#displayImage").css('width'));
		var imgH = parseFloat($("#displayImage").css('height'));
		var imgL = parseFloat($("#displayImage").css('left'));
		var imgT = parseFloat($("#displayImage").css('top'));
		var unityOffsetX = (0.5 * w - (imgL + imgW * 0.5)) / w * -125;
		var unityOffsetY = (0.5 * h - (imgT + imgH * 0.5)) / h  * 230;
		var offsetX = imgL / w;
		var offsetY = imgT / h;
		var zoom = parseInt($("#zoomSlider").val());
		var unityZoom;
		var img = new Image();
		img.src = $("#displayImage").attr('src');
		if(parseFloat($("#displayImage").hasClass('portrait')))
		{
			unityZoom = parseInt($("#zoomSlider").val()) * 0.01 * 230;
		}
		else
		{
			unityZoom = parseInt($("#zoomSlider").val()) * 0.01 * 120;
		}
		var url = $("#displayImage").attr('src');
		if(url === null)
			url = "";
			
		var backColor = $("#colorPicker :input").val();
		
		var card = new Card(
			$("#titleInput").val(),
			url,
			offsetX,
			offsetY,
			unityOffsetX,
			unityOffsetY,
			zoom,
			unityZoom,
			year, month, day, backColor, -1);
	
		
		var edit = false;
		for(var i = 0; i < decks[selectedDeck].cards.length; i++)
		{
			var c = $("#previewTimelineContainer").children().get(i);
			if(c.id === 'edit')
			{
				c.removeAttribute('id');
				card.id = decks[selectedDeck].cards[i].id;
				decks[selectedDeck].cards[i] = card;
				edit = true;
			}
		}
		if(!edit)
		{
			decks[selectedDeck].cards.push(card);	
		}
		postCardData(card);
		decks[selectedDeck].cards.sort(sortDeckByDate);
		buildDeckPicker();
		checkEnableInput();
		buildPreviewTimeline();
		newCard();
	}
	
	
	function postCardData(card)
	{
		$.ajax({
			Type: "POST",
			url: "card_post.php",
			data: {
				deckID : decks[selectedDeck].id,
				name : card.title, 
				url : card.url,
				offsetX : card.unityOffsetX,
				offsetY : card.unityOffsetY,
				zoom : card.unityZoom,
				date : card.year + "-" + ("0" + card.month).slice(-2) + "-" + ("0" + card.day).slice(-2),
				id : card.id
			},
			dataType: "text",
			success: function(data){
				console.log( "Data Saved with ID: " + data );
				card.id = data;
			},
			error: function (xhr, ajaxOptions, thrownError)
			{
				alert(xhr.status);
				alert(thrownError);
			}
		});
	}
	
	
	function download() 
	{
		var deck = decks[selectedDeck];
		var filename = deck.name;
		var element = document.createElement('a');
		var text =
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" 
	+   "<Deck>\r\n" 
	+		"\t<Name>" + deck.name + "</Name>\r\n" 
	+		"\t<ID></ID>\r\n" 
	+		"\t<Cards>\r\n";
		for(var i = 0; i < deck.cards.length; i++)
		{		
			text +=	"\t\t" + "<Card>\r\n" 
					+	"\t\t\t" + "<ID></ID>\r\n" 
					+	"\t\t\t" + "<Title>" + deck.cards[i].title + "</Title>\r\n" 
					+	"\t\t\t" + "<url>" +  deck.cards[i].url + "</url>\r\n" 
					+	"\t\t\t" + "<OffsetX>" + deck.cards[i].unityOffsetX  + "</OffsetX>\r\n" 
					+	"\t\t\t" + "<OffsetY>" + deck.cards[i].unityOffsetY + "</OffsetY>\r\n" 
					+	"\t\t\t" + "<Zoom>" + deck.cards[i].unityZoom + "</Zoom>\r\n" 
					+	"\t\t\t" + "<Description>" + "</Description>\r\n" 
					+	"\t\t\t" + "<Year>" + deck.cards[i].year + "</Year>\r\n" 
					+	"\t\t\t" + "<Month>January</Month>\r\n" 
					+	"\t\t\t" + "<Day>" + deck.cards[i].day + "</Day>\r\n" 
					+	"\t\t" + "</Card>\r\n";
		}
		text += "\t</Cards>\r\n</Deck>";

		element.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(text));
		element.setAttribute('download', filename + ".xml");
		
		element.style.display = 'none';
		document.body.appendChild(element);
		
		element.click();
		
		document.body.removeChild(element);
	}