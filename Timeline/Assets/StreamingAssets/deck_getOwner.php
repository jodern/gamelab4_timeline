<?php

error_reporting( E_ALL );
ini_set( "display_errors", "1" );

// Response as plain text (utf-8):
header( "Content-Type: text/plain; charset=utf-8" ); 

// Init:
$filename = "Timeline";

$owner = "";

if ( isset($_REQUEST['owner']) ) $owner = $_REQUEST['owner'];

$owner = SQLite3::escapeString($owner);

if($owner == "")
{
	echo "ERROR: owner not defined!";
}
else
{
	$db_handle = new SQLite3( $filename );
	
	$decks = $db_handle->query( "SELECT * FROM decks WHERE owner LIKE '" . $owner . "'" );
	
	$array = array();
	while ( $row = $decks->fetchArray( SQLITE3_ASSOC ) ) 
	{
		$cardArray = array();
		$cards = $db_handle->query( "SELECT * FROM cards WHERE deckID LIKE " . $row['id']);
		while( $cardRow = $cards->fetchArray( SQLITE3_ASSOC ))
		{
			$cardArray[] = [ 'id' => $cardRow['id'], 
							 'deckID' => $cardRow['deckID'], 
							 'title' => $cardRow['title'], 
							 'url' => $cardRow['url'], 
							 'offsetX' => $cardRow['offsetX'], 
							 'offsetY' => $cardRow['offsetY'], 
							 'zoom' => $cardRow['zoom'], 
							 'date' => $cardRow['date'] ];
		}
		$array[] = [ 'id' => $row['id'],'creationDate' => $row['creationDate'],'name' => $row['name'],'owner' => $row['owner'], 'cards' => $cardArray ];
	}
	if(count($array) == 0)
		echo "ERROR: No decks found!";
	else
		echo json_encode($array);
}
?>