//Maya ASCII 2015 scene
//Name: TL_card_02_model.ma
//Last modified: Mon, Feb 08, 2016 01:10:00 PM
//Codeset: 1252
requires maya "2015";
requires -nodeType "Unfold3DUnfold" "Unfold3D" "Maya2015SP1.r1568.release.Apr  3 2014|15:09:24";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 7 Enterprise Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
fileInfo "license" "education";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 16.762759562025423 21.815317506995562 -6.2572883834486008 ;
	setAttr ".r" -type "double3" -18.338352729516121 -7442.2000000017724 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 21.422321824308632;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 8 -0.25 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 8.7459734476439355 -0.20701151796525891 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 35.76534021188094;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "TL_card_01_model";
	setAttr ".t" -type "double3" 0.5 4 -0.5 ;
createNode mesh -n "TL_card_01_modelShape" -p "TL_card_01_model";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.98360961165458027 0.9616924524307251 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bw" 3;
	setAttr ".dr" 1;
createNode mesh -n "polySurfaceShape1" -p "TL_card_01_model";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.37499998509883881 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 44 ".uvst[0].uvsp[0:43]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.54166663 0.5 0.54166663 0.75 0.54166663 0 0.54166663
		 1 0.54166663 0.25 0.45833331 0.5 0.45833331 0.75 0.45833331 0 0.45833331 1 0.45833331
		 0.25 0.54166663 0.66666663 0.45833331 0.66666663 0.125 0.083333343 0.375 0.66666663
		 0.375 0.083333343 0.45833331 0.083333343 0.54166663 0.083333343 0.625 0.083333343
		 0.625 0.66666663 0.875 0.083333343 0.54166663 0.58333331 0.45833331 0.58333331 0.125
		 0.16666667 0.375 0.58333331 0.375 0.16666667 0.45833331 0.16666667 0.54166663 0.16666667
		 0.625 0.16666667 0.625 0.58333331 0.875 0.16666667;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 17 ".pt";
	setAttr ".pt[7]" -type "float3" -0.29999995 0 0 ;
	setAttr ".pt[16]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[17]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[18]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[19]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[20]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[21]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[22]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[23]" -type "float3" 0 -5.0333338 0 ;
	setAttr ".pt[24]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[25]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[26]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[27]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[28]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[29]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[30]" -type "float3" 0 5.0333328 0 ;
	setAttr ".pt[31]" -type "float3" 0 5.0333328 0 ;
	setAttr -s 32 ".vt[0:31]"  -5 -4 0.5 4 -4 0.5 -5 12 0.5 4 12 0.5 -5 12 0
		 4 12 0 -5 -4 0 4 -4 0 3.69999981 12 0 3.69999981 -4 0 3.69999981 -4 0.5 3.69999981 12 0.5
		 -4.69999981 12 0 -4.69999981 -4 0 -4.69999981 -4 0.5 -4.69999981 12 0.5 3.69999981 1.33333397 0
		 -4.69999981 1.33333397 0 -5 1.33333397 0 -5 1.33333397 0.5 -4.69999981 1.33333397 0.5
		 3.69999981 1.33333397 0.5 4 1.33333397 0.5 4 1.33333397 0 3.69999981 6.66666698 0
		 -4.69999981 6.66666698 0 -5 6.66666698 0 -5 6.66666698 0.5 -4.69999981 6.66666698 0.5
		 3.69999981 6.66666698 0.5 4 6.66666698 0.5 4 6.66666698 0;
	setAttr -s 60 ".ed[0:59]"  0 14 0 2 15 0 4 12 0 6 13 0 0 19 0 1 22 0
		 2 4 0 3 5 0 4 26 0 5 31 0 6 0 0 7 1 0 8 5 0 9 7 0 8 24 1 10 1 0 9 10 1 11 3 0 10 21 1
		 11 8 1 12 8 0 13 9 0 12 25 1 14 10 0 13 14 1 15 11 0 14 20 1 15 12 1 16 9 1 17 13 1
		 16 17 1 18 6 0 17 18 1 19 27 0 18 19 1 20 28 1 19 20 1 21 29 1 20 21 1 22 30 0 21 22 1
		 23 7 0 22 23 1 23 16 1 24 16 1 25 17 1 24 25 1 26 18 0 25 26 1 27 2 0 26 27 1 28 15 1
		 27 28 1 29 11 1 28 29 1 30 3 0 29 30 1 31 23 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 52 51 -2 -50
		mu 0 4 38 39 23 2
		f 4 1 27 -3 -7
		mu 0 4 2 23 19 4
		f 4 2 22 48 -9
		mu 0 4 4 19 35 37
		f 4 3 24 -1 -11
		mu 0 4 6 20 22 8
		f 4 58 -10 -8 -56
		mu 0 4 41 43 11 3
		f 4 50 49 6 8
		mu 0 4 36 38 2 13
		f 4 12 9 59 -15
		mu 0 4 14 5 42 34
		f 4 -17 13 11 -16
		mu 0 4 17 15 7 9
		f 4 -54 56 55 -18
		mu 0 4 18 40 41 3
		f 4 -20 17 7 -13
		mu 0 4 14 18 3 5
		f 4 20 14 46 -23
		mu 0 4 19 14 34 35
		f 4 -25 21 16 -24
		mu 0 4 22 20 15 17
		f 4 -52 54 53 -26
		mu 0 4 23 39 40 18
		f 4 -28 25 19 -21
		mu 0 4 19 23 18 14
		f 4 -31 28 -22 -30
		mu 0 4 25 24 15 20
		f 4 -33 29 -4 -32
		mu 0 4 27 25 20 6
		f 4 10 4 -35 31
		mu 0 4 12 0 28 26
		f 4 0 26 -37 -5
		mu 0 4 0 21 29 28
		f 4 -39 -27 23 18
		mu 0 4 30 29 21 16
		f 4 -41 -19 15 5
		mu 0 4 31 30 16 1
		f 4 -12 -42 -43 -6
		mu 0 4 1 10 33 31
		f 4 -44 41 -14 -29
		mu 0 4 24 32 7 15
		f 4 -47 44 30 -46
		mu 0 4 35 34 24 25
		f 4 -49 45 32 -48
		mu 0 4 37 35 25 27
		f 4 34 33 -51 47
		mu 0 4 26 28 38 36
		f 4 36 35 -53 -34
		mu 0 4 28 29 39 38
		f 4 -55 -36 38 37
		mu 0 4 40 39 29 30
		f 4 -57 -38 40 39
		mu 0 4 41 40 30 31
		f 4 42 -58 -59 -40
		mu 0 4 31 33 43 41
		f 4 -60 57 43 -45
		mu 0 4 34 42 32 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".bw" 3;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode polyMergeVert -n "polyMergeVert1";
	setAttr ".ics" -type "componentList" 2 "vtx[7]" "vtx[9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyMergeVert -n "polyMergeVert2";
	setAttr ".ics" -type "componentList" 2 "vtx[1]" "vtx[9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak1";
	setAttr ".uopa" yes;
	setAttr ".tk[1]" -type "float3"  -0.29999995 0 0;
createNode polyMergeVert -n "polyMergeVert3";
	setAttr ".ics" -type "componentList" 2 "vtx[6]" "vtx[11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak2";
	setAttr ".uopa" yes;
	setAttr ".tk[6]" -type "float3"  0.30000019 0 0;
createNode polyMergeVert -n "polyMergeVert4";
	setAttr ".ics" -type "componentList" 2 "vtx[0]" "vtx[11]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak3";
	setAttr ".uopa" yes;
	setAttr ".tk[0]" -type "float3"  0.30000019 0 0;
createNode polyMergeVert -n "polyMergeVert5";
	setAttr ".ics" -type "componentList" 2 "vtx[4]" "vtx[10]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak4";
	setAttr ".uopa" yes;
	setAttr ".tk[4]" -type "float3"  0.30000019 0 0;
createNode polyMergeVert -n "polyMergeVert6";
	setAttr ".ics" -type "componentList" 2 "vtx[2]" "vtx[10]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak5";
	setAttr ".uopa" yes;
	setAttr ".tk[2]" -type "float3"  0.30000019 0 0;
createNode polyMergeVert -n "polyMergeVert7";
	setAttr ".ics" -type "componentList" 2 "vtx[3]" "vtx[9]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak6";
	setAttr ".uopa" yes;
	setAttr ".tk[3]" -type "float3"  -0.29999995 0 0;
createNode polyMergeVert -n "polyMergeVert8";
	setAttr ".ics" -type "componentList" 2 "vtx[5]" "vtx[8]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".d" 0.0001;
createNode polyTweak -n "polyTweak7";
	setAttr ".uopa" yes;
	setAttr ".tk[5]" -type "float3"  -0.29999995 0 0;
createNode polySplitRing -n "polySplitRing1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[21]" "e[23]" "e[25]" "e[27]" "e[32:33]" "e[35]" "e[45]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".wt" 0.2527083158493042;
	setAttr ".re" 32;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polySplitRing -n "polySplitRing2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 10 "e[10:11]" "e[13:14]" "e[18]" "e[26]" "e[34]" "e[42]" "e[50]" "e[58]" "e[66]" "e[74]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".wt" 0.43180474638938904;
	setAttr ".re" 74;
	setAttr ".sma" 29.999999999999996;
	setAttr ".stp" 2;
	setAttr ".p[0]"  0 0 1;
	setAttr ".fq" yes;
createNode polyTweak -n "polyTweak8";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[24:39]" -type "float3"  0 -2.43333387 0 0 -2.43333387
		 0 0 -2.43333387 0 0 -2.43333387 0 0 -2.43333387 0 0 -2.43333387 0 0 -2.43333387 0
		 0 -2.43333387 0 0 -5.066666603 0 0 -5.066666603 0 0 -5.066666603 0 0 -5.066666603
		 0 0 -5.066666603 0 0 -5.066666603 0 0 -5.066666603 0 0 -5.066666603 0;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	setAttr ".cch" yes;
	setAttr ".ics" -type "componentList" 12 "f[18]" "f[22]" "f[26]" "f[30]" "f[34]" "f[38]" "f[42]" "f[46:48]" "f[52:54]" "f[58]" "f[60]" "f[64]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 8 -0.25 ;
	setAttr ".rs" 41854;
	setAttr ".lt" -type "double3" 8.8817841970012523e-016 9.1886310371395276e-018 -0.075030866397862717 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -4.1999998092651367 0.30000019073486328 -0.5 ;
	setAttr ".cbx" -type "double3" 4.1999998092651367 15.699999809265137 0 ;
createNode polyTweak -n "polyTweak9";
	setAttr ".uopa" yes;
	setAttr -s 27 ".tk";
	setAttr ".tk[40]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[41]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[42]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[43]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[44]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[45]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[46]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[47]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[48]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[49]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[50]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[51]" -type "float3" 2.3000004 0 0 ;
	setAttr ".tk[52]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[53]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[54]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[55]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[56]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[57]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[58]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[59]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[60]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[61]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[62]" -type "float3" -2.2999997 0 0 ;
	setAttr ".tk[63]" -type "float3" -2.2999997 0 0 ;
createNode polyBevel2 -n "polyBevel1";
	setAttr ".cch" yes;
	setAttr ".ics" -type "componentList" 19 "e[0:1]" "e[13:14]" "e[21]" "e[23]" "e[25:27]" "e[37]" "e[42:43]" "e[52]" "e[54]" "e[56:57]" "e[66]" "e[68]" "e[70:71]" "e[76:77]" "e[89]" "e[91]" "e[95:96]" "e[106]" "e[108]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.4;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyBevel2 -n "polyBevel2";
	setAttr ".cch" yes;
	setAttr ".ics" -type "componentList" 14 "e[0:1]" "e[3:4]" "e[8:9]" "e[11]" "e[13:16]" "e[18]" "e[20:22]" "e[24]" "e[26:28]" "e[30]" "e[32:33]" "e[38:39]" "e[43:44]" "e[47:48]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.4;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n"
		+ "                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n"
		+ "                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n"
		+ "            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n"
		+ "            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n"
		+ "                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n"
		+ "                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n"
		+ "                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n"
		+ "            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n"
		+ "                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n"
		+ "                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n"
		+ "            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 0\n"
		+ "                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n"
		+ "            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n"
		+ "            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n"
		+ "                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n"
		+ "                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n"
		+ "                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n"
		+ "                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n"
		+ "                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n"
		+ "                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n"
		+ "                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n"
		+ "\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode blinn -n "TL_card_01_material";
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode polyPlanarProj -n "polyPlanarProj1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[0:153]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 8 -0.25 ;
	setAttr ".ps" -type "double2" 9 16 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyMapCut -n "polyMapCut1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 24 "e[177]" "e[182]" "e[185]" "e[187]" "e[192:193]" "e[196]" "e[199]" "e[204]" "e[207]" "e[209]" "e[214]" "e[217:218]" "e[222]" "e[226]" "e[229]" "e[231]" "e[236]" "e[238]" "e[240]" "e[245:246]" "e[249]" "e[252:253]" "e[256]" "e[259]";
createNode polyMapCut -n "polyMapCut2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 22 "e[67:68]" "e[73]" "e[77:78]" "e[81]" "e[84]" "e[89:90]" "e[95]" "e[98]" "e[100]" "e[105:106]" "e[110]" "e[115]" "e[118]" "e[120:121]" "e[124]" "e[127]" "e[131:132]" "e[136]" "e[140]" "e[142]" "e[145]" "e[147]";
createNode polyMapCut -n "polyMapCut3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[46]" "e[58]" "e[61:62]" "e[164]" "e[167]" "e[171]" "e[173]";
createNode polyMapCut -n "polyMapCut4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[38]" "e[41]" "e[49]" "e[53]";
createNode polyMapCut -n "polyMapCut5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[289]";
createNode polyMapCut -n "polyMapCut6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[295]";
createNode polyMapCut -n "polyMapCut7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[18]" "e[26]" "e[33:34]";
createNode polyMapCut -n "polyMapCut8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 4 "e[9]" "e[13]" "e[20]" "e[25]";
createNode polyMapCut -n "polyMapCut9";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[277]";
createNode polyMapCut -n "polyMapCut10";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[282]";
createNode polyMapCut -n "polyMapCut11";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[286]";
createNode polyMapCut -n "polyMapCut12";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[279]";
createNode polyMapCut -n "polyMapCut13";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[200]" "e[255]" "e[267]";
createNode polyMapCut -n "polyMapCut14";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[66]" "e[99]" "e[154]";
createNode polyMapCut -n "polyMapCut15";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[135]" "e[144]" "e[150]";
createNode polyMapCut -n "polyMapCut16";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[186]" "e[230]" "e[263]";
createNode Unfold3DUnfold -n "Unfold3DUnfold1";
	setAttr ".uvl" -type "Int32Array" 32 4 5 6 12 13 14
		 15 16 17 18 19 28 30 31 67 68 71 72
		 75 76 79 81 85 86 88 95 208 209 210 211
		 213 214 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.32468426 0.30547687 0.30547687 0.32468426
		 0.34744599 0.33374324 0.33365217 0.34743565 0.32468426 0.30547687 0.30547687 0.32468426
		 0.34756047 0.33366755 0.33461279 0.34824166 0.56355417 0.54991549 0.55059302 0.56437749
		 0.021207405 0.021207405 0.0020000001 0.0020000001 0.021207405 0.0020000001 0.021207405
		 0.0020000001 0.56404829 0.55045718 0.54980111 0.56353408 0.80751717 0.80751717 0.57323641
		 0.57323641 0.80751717 0.80751717 0.57323641 0.57323641 0.80751717 0.80751717 0.57323641
		 0.57323641 0.80751717 0.57323641 0.57323641 0.80751717 0.57323641 0.57323641 0.80751717
		 0.80751717 0.80751717 0.57323641 0.57323641 0.80751717 0.013856228 0.014277278 0.0031158119
		 0.0020000001 0.03278964 0.032740008 0.031908203 0.033021677 0.027802173 0.026664557
		 0.3319377 0.33141738 0.31295466 0.31300429 0.3428016 0.34164467 0.33833611 0.33719972
		 0.012789893 0.013319373 0.031802349 0.031744063 0.0020000001 0.0031617603 0.31232426
		 0.31226596 0.022946944 0.021809328 0.042201407 0.043280721 0.038037077 0.036899481
		 0.013360063 0.012273345 0.032343104 0.032293472 0.03318185 0.032044254 0.33262751
		 0.33144155 0.31250811 0.31255776 0.32811239 0.32697606 0.32397768 0.32284135 0.3342014
		 0.333065 0.012176645 0.013372519 0.032326691 0.032268412 0.3128486 0.3127903 0.33080724
		 0.33122015 0.30547556 0.30505449 0.31621593 0.31733176 0.30705842 0.30597171 0.33180279
		 0.33288208 0.3475101 0.33373335 0.33250767 0.33240154 0.33241427 0.33338583 0.33464798
		 0.34822065 0.56351173 0.54997218 0.56562656 0.56477535 0.54973447 0.56354243 0.56477064
		 0.56527591 0.5322023 0.53143865 0.53779465 0.53840035 0.53431213 0.53367978 0.73992234
		 0.73954087 0.18373035 0.18312375 0.19031321 0.19049855 0.63821959 0.63866729 0.6324693
		 0.63186359 0.40769595 0.40761635 0.50122553 0.50183207 0.49464262 0.4944573 0.50251895
		 0.50315851 0.52215028 0.52278984 0.51844186 0.51772267 0.46018019 0.4602598 0.51703578
		 0.51639622 0.49740449 0.49676493 0.84462023 0.84448963 0.78999698 0.79037845 0.52344376
		 0.52411628 0.51817399 0.51851851 0.46089643 0.46097603 0.64405018 0.64427435 0.63791835
		 0.63731271 0.18951733 0.18895741 0.40697971 0.40690011 0.53650624 0.53749335 0.74335444
		 0.74297297 0.54324371 0.54384935 0.79381061 0.79342914 0.49599841 0.49543849 0.84830326
		 0.84852743 0.32468426 0.30547687 0.32468426 0.32468426 0.30547687 0.32468426 0.32468426
		 0.32468426 0.021207405 0.0020000001 0.0020000001 0.0020000001 0.0020000001 0.0020000001
		 0.0020000001 0.021207405 0.59668934 0.59668934 0.58381546 0.58381546 0.72848535 0.72848535
		 0.7333743 0.7333743 0.58611041 0.58611041 0.60013324 0.60013324 0.74839634 0.74839634
		 0.57323641 0.57323641 0.82169455 0.82169455 0.82564801 0.82564801 0.86871117 0.86871117
		 0.74833733 0.74833733 0.94019884 0.94019884 0.90308028 0.90308028 0.78550589 0.78550589
		 0.97072273 0.97072273 0.98574477 0.98574477 0.80767167 0.80767167 ;
	setAttr ".mve" -type "floatArray" 252 0.0020000001 0.0020000001 0.22240488 0.22240488
		 0.42453775 0.42439273 0.14664328 0.14604598 0.27642569 0.27642569 0.33476815 0.33476815
		 0.077437967 0.077979811 0.0032371092 0.003383534 0.42500734 0.42501006 0.14532356
		 0.14456978 0.22240488 0.0020000001 0.0020000001 0.22240488 0.27642569 0.27642569
		 0.33476815 0.33476815 0.076698206 0.076987758 0.0036847736 0.0037765952 0.046521254
		 0.0020000001 0.0020000001 0.046521254 0.050501995 0.095023245 0.095023245 0.050501995
		 0.046521254 0.0020000001 0.0020000001 0.046521254 0.0020000001 0.0020000001 0.046521254
		 0.046521254 0.095023245 0.050501995 0.050501995 0.095023245 0.095023245 0.095023245
		 0.050501995 0.050501995 0.40490031 0.40557605 0.41076127 0.41040826 0.40637043 0.40700963
		 0.45262191 0.45299056 0.51029885 0.5101952 0.42959833 0.43020096 0.42876738 0.42812818
		 0.43687043 0.43704695 0.49438068 0.49429244 0.78843594 0.78783894 0.78952229 0.79016221
		 0.78100312 0.78084165 0.81507158 0.8157115 0.56360745 0.56350386 0.45291418 0.45365769
		 0.51123101 0.51112741 0.41129032 0.4105629 0.41212124 0.41276044 0.56453967 0.56443602
		 0.43543738 0.43598834 0.43451822 0.43387902 0.49358684 0.49349862 0.54683709 0.54674888
		 0.54763091 0.54754269 0.78257519 0.78203893 0.7837652 0.78440511 0.80931449 0.80995435
		 0.816755 0.81743783 0.77039993 0.76972413 0.76453894 0.76489198 0.76473731 0.76400989
		 0.81104082 0.81178427 0.42576674 0.42561758 0.42437527 0.1467423 0.078084208 0.0032213468
		 0.0020000001 0.0021457006 0.42624873 0.42623422 0.14444859 0.42501047 0.0024546094
		 0.0025545533 0.0037926747 0.076645359 0.69683999 0.6966086 0.71454859 0.71433395
		 0.68468732 0.68369949 0.61184096 0.61076427 0.78262806 0.78314382 0.79406255 0.792907
		 0.99800003 0.99733943 0.97984546 0.98006004 0.77884167 0.77770466 0.44152391 0.44100818
		 0.43008944 0.431245 0.459997 0.45995221 0.74036866 0.74032384 0.76080775 0.75971556
		 0.76374465 0.76488161 0.74072677 0.74077153 0.4603551 0.4603999 0.5747472 0.57358158
		 0.592888 0.5939647 0.75884169 0.75926787 0.7699635 0.77108198 0.773974 0.77511102
		 0.99714613 0.99585485 0.97791487 0.97812945 0.78269613 0.78387791 0.76861221 0.76747525
		 0.69360131 0.69446349 0.62152821 0.62045151 0.71261799 0.71240342 0.60365194 0.60257524
		 0.44027409 0.44145587 0.58434558 0.58305424 0.0020000001 0.0020000001 0.22240488
		 0.0020000001 0.33476815 0.33476815 0.33476815 0.27642569 0.0020000001 0.0020000001
		 0.0020000001 0.22240488 0.27642569 0.33476815 0.33476815 0.33476815 0.57982922 0.57982922
		 0.57663268 0.57663268 0.46564761 0.46564761 0.47804961 0.47804961 0.10220058 0.10220058
		 0.11541608 0.11541608 0.49026972 0.49026972 0.099003993 0.099003993 0.32422107 0.32422107
		 0.3368831 0.3368831 0.29326954 0.29326954 0.41891685 0.41891685 0.20539056 0.20539056
		 0.25102004 0.25102004 0.37947753 0.37947753 0.67112744 0.67112744 0.68334752 0.68334752
		 0.31100556 0.31100556 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[120:135]" "|TL_card_01_model|TL_card_01_modelShape.map[28:31]" "|TL_card_01_model|TL_card_01_modelShape.map[12:19]" "|TL_card_01_model|TL_card_01_modelShape.map[4:7]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold2";
	setAttr ".uvl" -type "Int32Array" 34 144 145 146 147 152 153
		 154 155 156 157 158 159 160 161 162 163 164 165
		 166 167 168 169 174 175 176 177 178 179 184 185
		 186 187 196 197 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.32686764 0.30753028 0.30753028 0.32686764
		 0.34923559 0.33544013 0.33534846 0.34922519 0.32686764 0.30753028 0.30753028 0.32686764
		 0.34935084 0.33536392 0.33631557 0.35003665 0.56680602 0.55307508 0.55375719 0.56763494
		 0.021337368 0.021337368 0.0020000001 0.0020000001 0.021337368 0.0020000001 0.021337368
		 0.0020000001 0.56730348 0.5536204 0.55295992 0.56678581 0.81202042 0.81202042 0.57615447
		 0.57615447 0.81202042 0.81202042 0.57615447 0.57615447 0.81202042 0.81202042 0.57615447
		 0.57615447 0.81202042 0.57615447 0.57615447 0.81202042 0.57615447 0.57615447 0.81202042
		 0.81202042 0.81202042 0.57615447 0.57615447 0.81202042 0.013936451 0.014360349 0.0031233616
		 0.0020000001 0.03299797 0.032948002 0.68828714 0.68940812 0.68415332 0.68300802 0.33417013
		 0.3336463 0.31505865 0.31510863 0.34510756 0.34394279 0.34061185 0.33946776 0.66903949
		 0.66957253 0.68818057 0.68812186 0.65817654 0.65934616 0.97060055 0.97054189 0.67926526
		 0.6781199 0.69865 0.6997366 0.69445747 0.69331217 0.013436927 0.012342857 0.032548413
		 0.032498445 0.68956941 0.68842411 0.33486462 0.33367065 0.31460908 0.31465906 0.33031896
		 0.32917494 0.32615626 0.32501224 0.33644915 0.33530506 0.66842204 0.66962606 0.68870842
		 0.68864977 0.97112846 0.97106975 0.98920864 0.98962432 0.30752894 0.30710503 0.318342
		 0.31946537 0.30912253 0.30802846 0.99021089 0.99129748 0.34930015 0.33543018 0.33419621
		 0.33408937 0.33410218 0.3350803 0.33635101 0.35001549 0.56676328 0.55313212 0.56889242
		 0.56803548 0.5528928 0.56679422 0.56803071 0.56853944 0.47489586 0.47412702 0.48052606
		 0.48113585 0.47701997 0.47638333 0.68402141 0.68363732 0.19852574 0.19731855 0.19605221
		 0.19781405 0.58163047 0.5820812 0.57584125 0.57523149 0.33574551 0.34279683 0.57165945
		 0.57084155 0.57780105 0.57808536 0.5637961 0.56235957 0.43384111 0.42761886 0.45518959
		 0.46674985 0.42845264 0.42097655 0.46747729 0.47550586 0.57221055 0.57419151 0.7894277
		 0.78929621 0.73443484 0.73481888 0.42666119 0.41990697 0.42199254 0.41587779 0.38097104
		 0.3735829 0.58750051 0.58772624 0.5813272 0.58071744 0.20452495 0.20331967 0.38210043
		 0.38958094 0.47922891 0.4802227 0.68747669 0.68709266 0.48601198 0.48662171 0.73827428
		 0.73789024 0.57822806 0.5790475 0.79313564 0.79336131 0.32686764 0.30753028 0.32686764
		 0.32686764 0.30753028 0.32686764 0.32686764 0.32686764 0.021337368 0.0020000001 0.0020000001
		 0.0020000001 0.0020000001 0.0020000001 0.0020000001 0.021337368 0.59976614 0.59976614
		 0.58680511 0.58680511 0.73219645 0.73219645 0.73711848 0.73711848 0.58911562 0.58911562
		 0.60323334 0.60323334 0.75224221 0.75224221 0.57615447 0.57615447 0.82629377 0.82629377
		 0.83027399 0.83027399 0.87337112 0.87337112 0.75244021 0.75244021 0.94534248 0.94534248
		 0.90797275 0.90797275 0.78986025 0.78986025 0.97607291 0.97607291 0.99119657 0.99119657
		 0.81217599 0.81217599 ;
	setAttr ".mve" -type "floatArray" 252 0.0020000001 0.0020000001 0.22389621 0.22389621
		 0.42739674 0.42725074 0.14762197 0.14702064 0.27828252 0.27828252 0.33701974 0.33701974
		 0.077948399 0.078493908 0.0032454797 0.0033928952 0.42786953 0.42787227 0.14629333
		 0.14553446 0.22389621 0.0020000001 0.0020000001 0.22389621 0.27828252 0.27828252
		 0.33701974 0.33701974 0.077203631 0.077495143 0.0036961732 0.003788616 0.046822492
		 0.0020000001 0.0020000001 0.046822492 0.050823569 0.095646054 0.095646054 0.050823569
		 0.046822492 0.0020000001 0.0020000001 0.046822492 0.0020000001 0.0020000001 0.046822492
		 0.046822492 0.095646054 0.050823569 0.050823569 0.095646054 0.095646054 0.095646054
		 0.050823569 0.050823569 0.40762696 0.40830728 0.41352758 0.41317219 0.40910703 0.40975055
		 0.58296692 0.58333808 0.64103413 0.64092982 0.43249211 0.43309882 0.43165553 0.431012
		 0.43981341 0.43999112 0.49771279 0.49762395 0.92105317 0.92045218 0.92214692 0.92279112
		 0.91357011 0.9134075 0.94786906 0.94851333 0.69470346 0.69459915 0.58326119 0.58400971
		 0.6419726 0.64186829 0.41406021 0.41332787 0.41489676 0.41554028 0.69564199 0.69553763
		 0.43837067 0.43892536 0.43744528 0.43680176 0.49691358 0.49682477 0.55052412 0.5504353
		 0.55132329 0.55123448 0.91515279 0.91461289 0.91635084 0.91699511 0.94207305 0.94271719
		 0.94956386 0.95025134 0.77559966 0.77491927 0.76969904 0.77005446 0.76989871 0.76916641
		 0.94381106 0.94455951 0.42863405 0.4284839 0.42723316 0.14772166 0.078599013 0.0032296106
		 0.0020000001 0.0021466864 0.42911932 0.42910472 0.14541245 0.42787269 0.0024576853
		 0.0025583054 0.0038048043 0.077150427 0.65478724 0.65455425 0.67261565 0.67239952
		 0.64255232 0.64155781 0.56921309 0.56812906 0.99784714 0.99800003 0.98800576 0.98808873
		 0.95798498 0.95731992 0.93970758 0.93992358 0.74704307 0.75130051 0.4437649 0.442561
		 0.44077867 0.44162863 0.45257506 0.45132509 0.60981739 0.6057809 0.64229447 0.6501984
		 0.71864313 0.71375495 0.63222778 0.63719511 0.45897633 0.45982441 0.53186834 0.53069484
		 0.55013186 0.55121583 0.62039369 0.61607951 0.62403888 0.61965472 0.68956238 0.68470347
		 0.95712531 0.95582527 0.93776393 0.93797994 0.99472147 0.99434435 0.7754494 0.78030008
		 0.65152663 0.65239465 0.57896584 0.57788187 0.670672 0.67045593 0.56096864 0.55988467
		 0.44581193 0.44685549 0.54153168 0.54023159 0.0020000001 0.0020000001 0.22389621
		 0.0020000001 0.33701974 0.33701974 0.33701974 0.27828252 0.0020000001 0.0020000001
		 0.0020000001 0.22389621 0.27828252 0.33701974 0.33701974 0.33701974 0.58372575 0.58372575
		 0.58050764 0.58050764 0.47101733 0.47101733 0.48350325 0.48350325 0.10286535 0.10286535
		 0.11617027 0.11617027 0.49580604 0.49580604 0.099647135 0.099647135 0.32638812 0.32638812
		 0.3391358 0.3391358 0.29747292 0.29747292 0.42172462 0.42172462 0.20899932 0.20899932
		 0.25493756 0.25493756 0.38201845 0.38201845 0.6778875 0.6778875 0.69019026 0.69019026
		 0.31308317 0.31308317 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[196:197]" "|TL_card_01_model|TL_card_01_modelShape.map[184:187]" "|TL_card_01_model|TL_card_01_modelShape.map[174:179]" "|TL_card_01_model|TL_card_01_modelShape.map[152:169]" "|TL_card_01_model|TL_card_01_modelShape.map[144:147]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold3";
	setAttr ".uvl" -type "Int32Array" 30 136 137 138 139 140 141
		 142 143 148 149 150 151 170 171 172 173 180 181
		 182 183 188 189 190 191 192 193 194 195 198 199 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.31793818 0.29913232 0.29913232 0.31793818
		 0.33864367 0.32522738 0.32513821 0.33863354 0.31793818 0.29913232 0.29913232 0.31793818
		 0.33875573 0.32515326 0.32607874 0.33942267 0.55023384 0.53688031 0.53754365 0.55103993
		 0.020805851 0.020805851 0.0020000001 0.0020000001 0.020805851 0.0020000001 0.020805851
		 0.0020000001 0.55071759 0.53741062 0.53676832 0.55021417 0.78777695 0.78777695 0.55839413
		 0.55839413 0.78777695 0.78777695 0.55839413 0.55839413 0.78777695 0.78777695 0.55839413
		 0.55839413 0.78777695 0.55839413 0.55839413 0.78777695 0.55839413 0.55839413 0.78777695
		 0.78777695 0.78777695 0.55839413 0.55839413 0.78777695 0.013608359 0.014020605 0.0030924841
		 0.0020000001 0.032145943 0.032097347 0.031282958 0.03237313 0.02726276 0.026148938
		 0.32503992 0.32453048 0.30645373 0.30650234 0.3356767 0.33454397 0.33130458 0.33019194
		 0.012564362 0.013082755 0.03117932 0.031122223 0.0020000001 0.0031374732 0.30583656
		 0.30577952 0.022509061 0.021395179 0.041360982 0.042417709 0.037283689 0.036169868
		 0.013122566 0.012058567 0.031708743 0.031660147 0.032529987 0.031416167 0.3257153
		 0.32455418 0.30601653 0.30606514 0.32129461 0.32018203 0.31724632 0.31613374 0.32725629
		 0.32614365 0.011963889 0.013134809 0.031692669 0.031635631 0.30634996 0.30629286
		 0.32393318 0.32433745 0.29913101 0.29871875 0.30964684 0.31073934 0.30068079 0.29961678
		 0.3249079 0.32596463 0.33870643 0.32521769 0.32401764 0.32391375 0.32392621 0.32487744
		 0.32611322 0.33940211 0.55019224 0.53693575 0.5522629 0.55142951 0.53670305 0.55022234
		 0.55142486 0.55191958 0.54199016 0.54305112 0.53489965 0.53384203 0.54666167 0.54759169
		 0.69291401 0.69401532 0.35612613 0.3549521 0.35372058 0.35543397 0.40578097 0.4054926
		 0.40873873 0.40970942 0.48957419 0.49643168 0.71900368 0.71820825 0.72497648 0.72525293
		 0.71135646 0.70995939 0.58497345 0.57892227 0.60573518 0.61697769 0.57973313 0.5724625
		 0.61768514 0.62549299 0.71953964 0.72146612 0.73463386 0.73517466 0.73450649 0.7321502
		 0.57799089 0.57142234 0.57345057 0.56750393 0.53355664 0.52637154 0.40269473 0.4014115
		 0.4018952 0.4029915 0.36196041 0.36078829 0.53465497 0.54192984 0.53975278 0.53903335
		 0.68550563 0.68660975 0.52785689 0.52679598 0.71684074 0.71907449 0.72539175 0.72618866
		 0.73212612 0.73272544 0.31793818 0.29913232 0.31793818 0.31793818 0.29913232 0.31793818
		 0.31793818 0.31793818 0.020805851 0.0020000001 0.0020000001 0.0020000001 0.0020000001
		 0.0020000001 0.0020000001 0.020805851 0.58135676 0.58135676 0.56875199 0.56875199
		 0.73326087 0.73326087 0.7380476 0.7380476 0.57099903 0.57099903 0.58472866 0.58472866
		 0.75275564 0.75275564 0.55839413 0.55839413 0.80165797 0.80165797 0.80552876 0.80552876
		 0.8705551 0.8705551 0.72983438 0.72983438 0.94054824 0.94054824 0.90420568 0.90420568
		 0.76622587 0.76622587 0.97043401 0.97043401 0.98514193 0.98514193 0.78792822 0.78792822 ;
	setAttr ".mve" -type "floatArray" 252 0.0020000001 0.0020000001 0.21779706 0.21779706
		 0.41570407 0.41556206 0.14361933 0.14303452 0.27068847 0.27068847 0.32781121 0.32781121
		 0.075860843 0.076391354 0.0032112456 0.003354609 0.41616386 0.41616651 0.1423272
		 0.14158918 0.21779706 0.0020000001 0.0020000001 0.21779706 0.27068847 0.27068847
		 0.32781121 0.32781121 0.075136542 0.075420037 0.0036495458 0.0037394473 0.045590479
		 0.0020000001 0.0020000001 0.045590479 0.049564719 0.09315519 0.09315519 0.049564719
		 0.045590479 0.0020000001 0.0020000001 0.045590479 0.0020000001 0.0020000001 0.045590479
		 0.045590479 0.09315519 0.049564719 0.049564719 0.09315519 0.09315519 0.09315519 0.049564719
		 0.049564719 0.39664134 0.39730296 0.40237978 0.40203416 0.39808074 0.39870656 0.40999153
		 0.4103525 0.46646267 0.46636122 0.42082304 0.42141306 0.42000943 0.41938362 0.42794308
		 0.42811593 0.48425102 0.48416463 0.73878497 0.73820049 0.73984867 0.74047518 0.7315076
		 0.73134947 0.76486379 0.76549035 0.51865685 0.5185554 0.41027772 0.41100568 0.46737537
		 0.46727392 0.40289778 0.40218556 0.40371132 0.40433714 0.51956958 0.51946807 0.42654002
		 0.42707944 0.42564008 0.42501423 0.48347378 0.48338741 0.53561074 0.53552437 0.53638798
		 0.53630161 0.73304677 0.73252171 0.73421192 0.73483849 0.7592271 0.75985354 0.76651204
		 0.76718062 0.75449973 0.75383806 0.7487613 0.74910694 0.74895549 0.74824333 0.76091731
		 0.7616452 0.41690737 0.41676134 0.41554499 0.14371628 0.076493576 0.0031958129 0.0020000001
		 0.002142654 0.41737929 0.41736507 0.14147054 0.41616693 0.0024450996 0.0025429537
		 0.0037551906 0.075084798 0.80283922 0.80353785 0.81320667 0.81250626 0.79907495 0.8000136
		 0.53231525 0.53294533 0.96758401 0.96773267 0.95801312 0.95809382 0.99800003 0.99750113
		 0.99510783 0.99594992 0.72367364 0.72781408 0.42873153 0.42756075 0.42582738 0.42665398
		 0.43729955 0.43608391 0.5902198 0.58629429 0.62180424 0.62949091 0.69605434 0.69130051
		 0.61201423 0.61684501 0.44352487 0.44434965 0.44634923 0.44640371 0.46977648 0.46859175
		 0.60050541 0.59630984 0.6040504 0.59978676 0.66777289 0.66304755 0.9969753 0.99663228
		 0.99006283 0.99069786 0.96454424 0.96417749 0.7512992 0.75601655 0.79687661 0.79555184
		 0.52824885 0.5288744 0.80849916 0.80780488 0.46014255 0.46150109 0.4307223 0.43173718
		 0.44365934 0.44420847 0.0020000001 0.0020000001 0.21779706 0.0020000001 0.32781121
		 0.32781121 0.32781121 0.27068847 0.0020000001 0.0020000001 0.0020000001 0.21779706
		 0.27068847 0.32781121 0.32781121 0.32781121 0.56790245 0.56790245 0.56477278 0.56477278
		 0.42927673 0.42927673 0.44141945 0.44141945 0.1002592 0.1002592 0.11319841 0.11319841
		 0.45338407 0.45338407 0.097129434 0.097129434 0.3176381 0.3176381 0.33003539 0.33003539
		 0.26050246 0.26050246 0.41035414 0.41035414 0.17446068 0.17446068 0.21913624 0.21913624
		 0.37173936 0.37173936 0.63046074 0.63046074 0.64242536 0.64242536 0.30469885 0.30469885 ;
	setAttr ".mnsl" -type "stringArray" 6 "|TL_card_01_model|TL_card_01_modelShape.map[198:199]" "|TL_card_01_model|TL_card_01_modelShape.map[188:195]" "|TL_card_01_model|TL_card_01_modelShape.map[180:183]" "|TL_card_01_model|TL_card_01_modelShape.map[170:173]" "|TL_card_01_model|TL_card_01_modelShape.map[148:151]" "|TL_card_01_model|TL_card_01_modelShape.map[136:143]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold4";
	setAttr ".uvl" -type "Int32Array" 34 56 57 58 59 60 61
		 66 67 68 69 70 71 72 73 88 89 90 91
		 94 95 96 97 98 99 100 101 102 103 112 113
		 114 115 116 117 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.31792262 0.29911768 0.29911768 0.31792262
		 0.338763 0.32534736 0.3252582 0.33875287 0.31792262 0.29911768 0.29911768 0.31792262
		 0.33887506 0.32527325 0.3261987 0.33954197 0.55034274 0.53698987 0.53765321 0.55114883
		 0.020804925 0.020804925 0.0020000001 0.0020000001 0.020804925 0.0020000001 0.020804925
		 0.0020000001 0.55082649 0.53752017 0.53687787 0.55032307 0.78797317 0.78797317 0.55860162
		 0.55860162 0.78797317 0.78797317 0.55860162 0.55860162 0.78797317 0.78797317 0.55860162
		 0.55860162 0.78797317 0.55860162 0.55860162 0.78797317 0.55860162 0.55860162 0.78797317
		 0.78797317 0.78797317 0.55860162 0.55860162 0.78797317 0.0081100399 0.0078291185
		 0.0020000001 0.0021390731 0.017223403 0.016394302 0.031281516 0.032371633 0.027261516
		 0.026147747 0.15973243 0.15882272 0.15039639 0.15127924 0.16395028 0.16317956 0.20656714
		 0.20563115 0.012563841 0.013082209 0.031177882 0.031120788 0.0020000001 0.0031374171
		 0.3058216 0.30576456 0.022508051 0.021394223 0.041359041 0.042415716 0.037281949
		 0.036168184 0.0025710638 0.0030963165 0.011430044 0.010496087 0.032528482 0.031414717
		 0.15703024 0.15641652 0.14448442 0.14537874 0.20032784 0.19939213 0.24156769 0.24058895
		 0.24812695 0.24712946 0.011963398 0.013134261 0.031691205 0.031634171 0.30633497
		 0.30627787 0.32391733 0.32432157 0.37163106 0.37146831 0.374336 0.37520283 0.37127212
		 0.3709504 0.32489198 0.32594866 0.33882576 0.32533768 0.32413769 0.3240338 0.32404625
		 0.32499745 0.32623318 0.33952141 0.55030113 0.5370453 0.55237174 0.55153835 0.5368126
		 0.55033123 0.5515337 0.55202842 0.5421322 0.5431931 0.53504205 0.53398448 0.54680347
		 0.54773343 0.6930486 0.69414985 0.35625717 0.3550832 0.35385174 0.35556504 0.40592971
		 0.40564135 0.40888733 0.40985796 0.48969868 0.49655584 0.71911687 0.7183215 0.72508937
		 0.72536582 0.71147001 0.71007305 0.58509326 0.57904238 0.60585397 0.61709589 0.57985318
		 0.5725829 0.61780334 0.62561077 0.71965283 0.72157919 0.73476642 0.73530716 0.73463905
		 0.73228288 0.57811099 0.5715428 0.57357091 0.56762457 0.53367895 0.5264942 0.40284362
		 0.40156046 0.40204412 0.40314037 0.36209118 0.36091912 0.53477722 0.54205173 0.53989494
		 0.53917551 0.68564057 0.68674463 0.52799964 0.52693874 0.71697414 0.71920782 0.72550464
		 0.72630149 0.7322588 0.73285806 0.31792262 0.29911768 0.31792262 0.31792262 0.29911768
		 0.31792262 0.31792262 0.31792262 0.020804925 0.0020000001 0.0020000001 0.0020000001
		 0.0020000001 0.0020000001 0.0020000001 0.020804925 0.58156317 0.58156317 0.568959
		 0.568959 0.73353356 0.73353356 0.73832005 0.73832005 0.57120591 0.57120591 0.58493483
		 0.58493483 0.75302738 0.75302738 0.55860162 0.55860162 0.80185348 0.80185348 0.80572408
		 0.80572408 0.87082106 0.87082106 0.73003346 0.73003346 0.94081074 0.94081074 0.90446997
		 0.90446997 0.76642317 0.76642317 0.97069508 0.97069508 0.98540223 0.98540223 0.78812444
		 0.78812444 ;
	setAttr ".mve" -type "floatArray" 252 0.0020000001 0.0020000001 0.21778643 0.21778643
		 0.41568372 0.41554171 0.14361235 0.14302757 0.27067524 0.27067524 0.32779518 0.32779518
		 0.075857207 0.076387696 0.003211186 0.0033545422 0.41614348 0.41614613 0.14232031
		 0.14158231 0.21778643 0.0020000001 0.0020000001 0.21778643 0.27067524 0.27067524
		 0.32779518 0.32779518 0.075132944 0.075416423 0.0036494646 0.0037393617 0.045588333
		 0.0020000001 0.0020000001 0.045588333 0.04956406 0.093152389 0.093152389 0.04956406
		 0.045588333 0.0020000001 0.0020000001 0.045588333 0.0020000001 0.0020000001 0.045588333
		 0.045588333 0.093152389 0.04956406 0.04956406 0.093152389 0.093152389 0.093152389
		 0.04956406 0.04956406 0.33291805 0.33392993 0.33246419 0.3317709 0.34296528 0.34379253
		 0.44922253 0.44958347 0.50569087 0.50558943 0.50552845 0.50624073 0.49774176 0.49697241
		 0.50860101 0.50953972 0.56891865 0.56962228 0.77799976 0.77741528 0.7790634 0.77968991
		 0.77072275 0.77056462 0.80407727 0.80470383 0.55788249 0.55778104 0.4495087 0.45023662
		 0.50660354 0.50650209 0.33704659 0.33572945 0.3481797 0.3488853 0.55879515 0.55869365
		 0.51190329 0.51306278 0.50284398 0.50208974 0.57360005 0.57430333 0.62858051 0.62922239
		 0.62438267 0.62499762 0.77226186 0.7717368 0.77342695 0.77405345 0.79844087 0.79906732
		 0.80572546 0.80639404 0.87318665 0.87305963 0.87072486 0.87117219 0.87272614 0.8726179
		 0.80013102 0.80085886 0.41688696 0.41674092 0.41552463 0.1437093 0.07648991 0.003195754
		 0.0020000001 0.0021426468 0.41735885 0.41734463 0.14146368 0.41614655 0.0024450778
		 0.0025429269 0.0037551043 0.075081199 0.80284882 0.80354738 0.81321573 0.81251538
		 0.79908472 0.80002332 0.53233814 0.53296822 0.96747029 0.96761894 0.95789987 0.95798057
		 0.99800003 0.99750113 0.99510795 0.99594998 0.7235719 0.72771215 0.42864433 0.4274736
		 0.42574033 0.42656687 0.43721193 0.43599635 0.59012467 0.58619934 0.62170756 0.62939382
		 0.69595397 0.69120038 0.61191803 0.61674857 0.44343692 0.44426167 0.44637635 0.44643083
		 0.46980244 0.46861777 0.60040975 0.59621441 0.60395455 0.59969115 0.66767395 0.66294879
		 0.9969753 0.99663234 0.99006319 0.99069822 0.96443069 0.96406394 0.75119615 0.75591326
		 0.7968865 0.79556179 0.52827191 0.52889746 0.80850846 0.80781424 0.46016899 0.46152747
		 0.43063501 0.43164983 0.4436866 0.44423571 0.0020000001 0.0020000001 0.21778643 0.0020000001
		 0.32779518 0.32779518 0.32779518 0.27067524 0.0020000001 0.0020000001 0.0020000001
		 0.21778643 0.27067524 0.32779518 0.32779518 0.32779518 0.56787795 0.56787795 0.56474847
		 0.56474847 0.42930096 0.42930096 0.44144309 0.44144309 0.10025772 0.10025772 0.11319629
		 0.11319629 0.45340714 0.45340714 0.097128116 0.097128116 0.31762591 0.31762591 0.3300226
		 0.3300226 0.260535 0.260535 0.41033739 0.41033739 0.17449744 0.17449744 0.21917081
		 0.21917081 0.37172452 0.37172452 0.6304751 0.6304751 0.64243913 0.64243913 0.30468732
		 0.30468732 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[112:117]" "|TL_card_01_model|TL_card_01_modelShape.map[94:103]" "|TL_card_01_model|TL_card_01_modelShape.map[88:91]" "|TL_card_01_model|TL_card_01_modelShape.map[66:73]" "|TL_card_01_model|TL_card_01_modelShape.map[56:61]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold5";
	setAttr ".uvl" -type "Int32Array" 30 62 63 64 65 74 75
		 76 77 78 79 80 81 82 83 84 85 86 87
		 92 93 104 105 106 107 108 109 110 111 118 119 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.31792638 0.2991212 0.2991212 0.31792638
		 0.33873406 0.32531828 0.32522911 0.33872393 0.31792638 0.2991212 0.2991212 0.31792638
		 0.33884612 0.32524416 0.32616961 0.33951303 0.55031633 0.53696328 0.53762662 0.55112243
		 0.020805148 0.020805148 0.0020000001 0.0020000001 0.020805148 0.0020000001 0.020805148
		 0.0020000001 0.55080009 0.53749359 0.53685129 0.55029666 0.78792554 0.78792554 0.55855131
		 0.55855131 0.78792554 0.78792554 0.55855131 0.55855131 0.78792554 0.78792554 0.55855131
		 0.55855131 0.78792554 0.55855131 0.55855131 0.78792554 0.55855131 0.55855131 0.78792554
		 0.78792554 0.78792554 0.55855131 0.55855131 0.78792554 0.0081101125 0.0078291874
		 0.0020000001 0.0021390747 0.017223584 0.016394474 0.019769082 0.020748463 0.0070312074
		 0.0020000001 0.15973431 0.15882458 0.15039815 0.15128101 0.1639522 0.16318147 0.20656957
		 0.20563357 0.1201719 0.13104199 0.14159578 0.13280463 0.11411002 0.12270442 0.32730755
		 0.32626683 0.019696079 0.012244761 0.026785554 0.02798897 0.036861498 0.032099221
		 0.0025710706 0.0030963293 0.011430155 0.010496188 0.06838277 0.0606707 0.15703207
		 0.15641835 0.1444861 0.14538044 0.2003302 0.19939446 0.24157053 0.24059178 0.24812987
		 0.24713236 0.17257874 0.19085081 0.20291139 0.19078231 0.33808991 0.33611861 0.33815974
		 0.33761755 0.37163544 0.37147269 0.37434042 0.37520728 0.3712765 0.37095478 0.34425858
		 0.34375849 0.33879682 0.32530859 0.32410857 0.32400468 0.32401714 0.32496837 0.32620409
		 0.33949247 0.55027473 0.53701872 0.55234534 0.55151194 0.53678602 0.55030483 0.55150729
		 0.55200201 0.54209775 0.54315865 0.53500754 0.53394991 0.54676908 0.54769903 0.69301593
		 0.69411719 0.3562254 0.35505143 0.35381994 0.35553327 0.40589365 0.40560529 0.4088513
		 0.40982193 0.48966849 0.49652573 0.71908939 0.71829402 0.72506195 0.7253384 0.71144247
		 0.71004546 0.58506417 0.57901323 0.60582519 0.61706722 0.57982403 0.57255369 0.61777467
		 0.62558222 0.71962535 0.72155178 0.73473424 0.73527503 0.73460686 0.73225069 0.57808185
		 0.57151359 0.5735417 0.5675953 0.53364927 0.52646446 0.40280753 0.40152434 0.402008
		 0.40310428 0.36205947 0.36088741 0.5347476 0.54202217 0.53986049 0.539141 0.68560785
		 0.68671191 0.52796501 0.52690411 0.71694177 0.71917546 0.72547722 0.72627413 0.73222661
		 0.73282588 0.31792638 0.2991212 0.31792638 0.31792638 0.2991212 0.31792638 0.31792638
		 0.31792638 0.020805148 0.0020000001 0.0020000001 0.0020000001 0.0020000001 0.0020000001
		 0.0020000001 0.020805148 0.58151311 0.58151311 0.56890881 0.56890881 0.73346746 0.73346746
		 0.73825401 0.73825401 0.57115579 0.57115579 0.58488482 0.58488482 0.75296152 0.75296152
		 0.55855131 0.55855131 0.80180603 0.80180603 0.8056767 0.8056767 0.87075657 0.87075657
		 0.72998518 0.72998518 0.94074708 0.94074708 0.90440589 0.90440589 0.7663753 0.7663753
		 0.97063178 0.97063178 0.98533911 0.98533911 0.78807688 0.78807688 ;
	setAttr ".mve" -type "floatArray" 252 0.0020000001 0.0020000001 0.21778899 0.21778899
		 0.41568866 0.41554666 0.14361404 0.14302926 0.27067843 0.27067843 0.32779905 0.32779905
		 0.075858086 0.076388583 0.0032112005 0.003354558 0.41614842 0.41615108 0.14232197
		 0.14158398 0.21778899 0.0020000001 0.0020000001 0.21778899 0.27067843 0.27067843
		 0.32779905 0.32779905 0.075133815 0.075417295 0.0036494811 0.0037393789 0.045588847
		 0.0020000001 0.0020000001 0.045588847 0.049564213 0.093153059 0.093153059 0.049564213
		 0.045588847 0.0020000001 0.0020000001 0.045588847 0.0020000001 0.0020000001 0.045588847
		 0.045588847 0.093153059 0.049564213 0.049564213 0.093153059 0.093153059 0.093153059
		 0.049564213 0.049564213 0.33292159 0.33393347 0.33246771 0.33177441 0.34296894 0.34379619
		 0.3757903 0.37604183 0.4404743 0.44478458 0.50553405 0.50624633 0.49774724 0.4969779
		 0.50860661 0.50954533 0.56892496 0.5696286 0.78105521 0.76870608 0.77846235 0.7892943
		 0.77489442 0.76518744 0.890526 0.89266199 0.50666904 0.51420194 0.3746728 0.37507701
		 0.41106912 0.4158349 0.33705017 0.33573303 0.34818342 0.34888902 0.45553514 0.46420708
		 0.51190895 0.51306844 0.50284952 0.50209528 0.57360643 0.57430971 0.62858754 0.62922943
		 0.62438965 0.62500459 0.71422547 0.69267881 0.70435899 0.71863645 0.87862879 0.88070309
		 0.895549 0.896788 0.8731966 0.87306958 0.87073475 0.87118208 0.8727361 0.87262785
		 0.88936889 0.89088219 0.4168919 0.41674587 0.41552958 0.143711 0.076490797 0.0031957682
		 0.0020000001 0.0021426482 0.41736379 0.41734958 0.14146535 0.41615149 0.0024450799
		 0.00254293 0.0037551217 0.075082071 0.80284649 0.80354506 0.81321353 0.81251317 0.79908234
		 0.80002093 0.5323326 0.53296268 0.96749783 0.96764648 0.95792729 0.95800799 0.99800003
		 0.99750108 0.99510789 0.99594992 0.72359651 0.72773683 0.42866546 0.4274947 0.4257614
		 0.42658797 0.43723315 0.43601757 0.59014773 0.58622235 0.62173098 0.6294173 0.69597828
		 0.69122463 0.61194134 0.61677194 0.44345823 0.44428298 0.44636977 0.44642425 0.46979615
		 0.46861145 0.60043293 0.59623754 0.60397774 0.59971428 0.66769791 0.66297269 0.99697524
		 0.99663228 0.99006307 0.9906981 0.96445817 0.96409142 0.75122112 0.75593829 0.79688406
		 0.79555935 0.52826631 0.52889186 0.80850619 0.80781198 0.46016258 0.46152106 0.43065614
		 0.43167099 0.44367999 0.4442291 0.0020000001 0.0020000001 0.21778899 0.0020000001
		 0.32779905 0.32779905 0.32779905 0.27067843 0.0020000001 0.0020000001 0.0020000001
		 0.21778899 0.27067843 0.32779905 0.32779905 0.32779905 0.56788385 0.56788385 0.56475437
		 0.56475437 0.42929509 0.42929509 0.44143736 0.44143736 0.10025807 0.10025807 0.11319679
		 0.11319679 0.45340154 0.45340154 0.097128421 0.097128421 0.31762886 0.31762886 0.3300257
		 0.3300257 0.2605271 0.2605271 0.41034144 0.41034144 0.17448853 0.17448853 0.21916243
		 0.21916243 0.37172809 0.37172809 0.63047165 0.63047165 0.64243579 0.64243579 0.30469012
		 0.30469012 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[118:119]" "|TL_card_01_model|TL_card_01_modelShape.map[104:111]" "|TL_card_01_model|TL_card_01_modelShape.map[92:93]" "|TL_card_01_model|TL_card_01_modelShape.map[74:87]" "|TL_card_01_model|TL_card_01_modelShape.map[62:65]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold6";
	setAttr ".uvl" -type "Int32Array" 32 0 1 2 3 8 9
		 10 11 20 21 22 23 24 25 26 27 200 201
		 202 203 204 205 206 207 208 209 210 211 212 213
		 214 215 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26453415 0.248951 0.25940284 0.28138122
		 0.30646482 0.29295591 0.29286611 0.30645463 0.27461028 0.25714245 0.23377267 0.24589372
		 0.30657765 0.29288125 0.29381314 0.30724922 0.51951611 0.50607038 0.50673831 0.52032781
		 0.02330422 0.053248297 0.041119996 0.0040637194 0.038325667 0.019444311 0.041827146
		 0.026560364 0.5200032 0.50660431 0.5059576 0.51949632 0.75867087 0.75867087 0.52770406
		 0.52770406 0.75867087 0.75867087 0.52770406 0.52770406 0.75867087 0.75867087 0.52770406
		 0.52770406 0.75867087 0.52770406 0.52770406 0.75867087 0.52770406 0.52770406 0.75867087
		 0.75867087 0.75867087 0.52770406 0.52770406 0.75867087 0.0081525343 0.007869659 0.0020000001
		 0.0021400403 0.017329279 0.016494414 0.019892452 0.020878632 0.0070661386 0.0020000001
		 0.16082944 0.15991339 0.15142846 0.15231746 0.16507663 0.16430053 0.20798987 0.20704737
		 0.12099236 0.13193792 0.14256498 0.1337128 0.11488839 0.12354247 0.32956615 0.32851818
		 0.019818941 0.01231589 0.026957639 0.02816941 0.037103537 0.032308199 0.0025750352
		 0.0031039407 0.011495628 0.010555176 0.068843663 0.061078046 0.15810844 0.15749046
		 0.14547537 0.14637591 0.20170718 0.20076495 0.24323384 0.2422483 0.24983874 0.2488343
		 0.17376305 0.19216199 0.2043063 0.19209301 0.34042338 0.33843836 0.34049368 0.33994773
		 0.37420177 0.37403789 0.37692553 0.37779841 0.37384036 0.37351641 0.34663486 0.34613129
		 0.30652803 0.29294613 0.29173779 0.29163319 0.29164574 0.29260355 0.29384786 0.30722851
		 0.51947421 0.50612617 0.52155918 0.52072001 0.50589186 0.51950455 0.52071536 0.52121347
		 0.56808025 0.56914848 0.5609408 0.55987585 0.57278401 0.5737204 0.72004622 0.72115511
		 0.37737447 0.37619236 0.37495232 0.37667754 0.43093047 0.4306401 0.43390864 0.43488604
		 0.51174408 0.51864892 0.7427578 0.74195695 0.74877185 0.7490502 0.73505783 0.7336511
		 0.60780209 0.60170913 0.62870723 0.64002728 0.60252553 0.59520471 0.64073968 0.64860141
		 0.74329752 0.74523729 0.7620542 0.76259875 0.76192594 0.75955337 0.60077125 0.5941574
		 0.59619957 0.59021193 0.55603021 0.54879552 0.42782292 0.42653084 0.42701784 0.42812175
		 0.38324904 0.38206884 0.55713618 0.56446123 0.56582743 0.56510293 0.7125867 0.71369845
		 0.55384934 0.55278111 0.74413818 0.74638736 0.74918997 0.74999243 0.75952911 0.76013255
		 0.26468942 0.2490312 0.28380123 0.26593664 0.23356281 0.24516092 0.24688984 0.27651402
		 0.053004861 0.041857235 0.040091719 0.0020000001 0.017711312 0.0251651 0.026399745
		 0.041395966 0.5508253 0.5508253 0.53813344 0.53813344 0.7300505 0.7300505 0.73487031
		 0.73487031 0.54039603 0.54039603 0.55422038 0.55422038 0.74967992 0.74967992 0.52770406
		 0.52770406 0.77264768 0.77264768 0.77654523 0.77654523 0.86829281 0.86829281 0.70032817
		 0.70032817 0.93876928 0.93876928 0.90217578 0.90217578 0.73697096 0.73697096 0.96886146
		 0.96886146 0.98367089 0.98367089 0.75882322 0.75882322 ;
	setAttr ".mve" -type "floatArray" 252 0.41310075 0.41506144 0.16589417 0.17712879
		 0.41856089 0.4184179 0.14459726 0.14400843 0.095092438 0.089897133 0.0047670179 0.0033790322
		 0.07637088 0.076905064 0.0032196096 0.0033639625 0.41902384 0.41902652 0.14329623
		 0.14255311 0.12510213 0.43198067 0.43315557 0.11519688 0.070456095 0.060190469 0.011348118
		 0.01347451 0.07564158 0.075927034 0.0036609333 0.0037514553 0.045891486 0.0020000001
		 0.0020000001 0.045891486 0.049859066 0.093750551 0.093750551 0.049859066 0.045891486
		 0.0020000001 0.0020000001 0.045891486 0.0020000001 0.0020000001 0.045891486 0.045891486
		 0.093750551 0.049859066 0.049859066 0.093750551 0.093750551 0.093750551 0.049859066
		 0.049859066 0.40132368 0.40234256 0.40086663 0.40016851 0.41144076 0.41227376 0.44416997
		 0.44442326 0.50930309 0.51364326 0.57513458 0.5758518 0.5672937 0.56651902 0.57822847
		 0.57917368 0.63896561 0.63967413 0.85224861 0.83981377 0.84963775 0.86054492 0.84604508
		 0.83627069 0.96247947 0.96463031 0.57595742 0.58354259 0.44304472 0.44345173 0.47969374
		 0.4844926 0.40548092 0.40415463 0.41669145 0.41740197 0.52446848 0.53320062 0.5815537
		 0.58272123 0.57243139 0.5716719 0.64367956 0.64438772 0.69904244 0.69968873 0.6948154
		 0.69543457 0.78495491 0.76325864 0.77501988 0.78939652 0.95049965 0.95258838 0.96753734
		 0.96878493 0.94534975 0.9452219 0.94287086 0.94332129 0.94488609 0.94477707 0.96131432
		 0.96283811 0.41977251 0.41962546 0.4184007 0.14469489 0.077007987 0.0032040703 0.0020000001
		 0.0021436384 0.42024767 0.42023334 0.14243366 0.41902694 0.00244817 0.0025466995
		 0.0037673074 0.075589478 0.80149156 0.80219495 0.81193054 0.81122535 0.79770124 0.79864633
		 0.52909946 0.52973396 0.97351354 0.97366321 0.96387655 0.96395785 0.99800003 0.99749756
		 0.9950878 0.99593568 0.72791886 0.73208791 0.43094009 0.42976123 0.42801589 0.42884821
		 0.43956727 0.43834326 0.59354353 0.58959091 0.62534606 0.63308573 0.70010889 0.69532222
		 0.61548847 0.62035257 0.44583559 0.44666606 0.44253981 0.44259468 0.46612886 0.46493593
		 0.60390013 0.5996756 0.60746956 0.60317647 0.67163211 0.66687411 0.99696809 0.99662274
		 0.99000794 0.99064738 0.97045279 0.97008348 0.75573522 0.76048517 0.7954877 0.79415381
		 0.52500498 0.52563483 0.80719054 0.80649149 0.45642838 0.45779631 0.4329446 0.43396652
		 0.43983138 0.44038427 0.41389397 0.41709769 0.17891155 0.41271523 0.002941678 0.0020000001
		 0.0032170631 0.095025018 0.43405619 0.43452069 0.43316412 0.11363723 0.058653194
		 0.013865519 0.012680936 0.0094323158 0.57174206 0.57174206 0.56859082 0.56859082
		 0.39960489 0.39960489 0.41183147 0.41183147 0.10086951 0.10086951 0.11389806 0.11389806
		 0.4238787 0.4238787 0.097718127 0.097718127 0.3197495 0.3197495 0.33223242 0.33223242
		 0.22966516 0.22966516 0.41310579 0.41310579 0.14302921 0.14302921 0.1880133 0.1880133
		 0.37422436 0.37422436 0.60217822 0.60217822 0.61422545 0.61422545 0.30672094 0.30672094 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[200:215]" "|TL_card_01_model|TL_card_01_modelShape.map[20:27]" "|TL_card_01_model|TL_card_01_modelShape.map[8:11]" "|TL_card_01_model|TL_card_01_modelShape.map[0:3]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold7";
	setAttr ".uvl" -type "Int32Array" 32 4 5 6 7 12 13
		 14 15 16 17 18 19 28 29 30 31 120 121
		 122 123 124 125 126 127 128 129 130 131 132 133
		 134 135 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26453415 0.24895099 0.25940284 0.28138122
		 0.30646485 0.29295596 0.29286608 0.30645463 0.27461028 0.25714245 0.23377267 0.24589372
		 0.30657771 0.29288131 0.2938132 0.30724931 0.51951617 0.50607038 0.50673836 0.52032793
		 0.02330422 0.05324829 0.041119993 0.0040637194 0.038325667 0.019444311 0.041827146
		 0.026560366 0.52000332 0.50660443 0.50595766 0.51949632 0.75867087 0.75867087 0.52770406
		 0.52770406 0.75867087 0.75867087 0.52770406 0.52770406 0.75867087 0.75867087 0.52770406
		 0.52770406 0.75867087 0.52770406 0.52770406 0.75867087 0.52770406 0.52770406 0.75867087
		 0.75867087 0.75867087 0.52770406 0.52770406 0.75867087 0.0081525343 0.007869659 0.0020000001
		 0.0021400403 0.017329279 0.016494414 0.01989245 0.02087863 0.0070661386 0.0020000001
		 0.16082944 0.15991339 0.15142846 0.15231746 0.16507663 0.16430053 0.20798989 0.20704739
		 0.12099237 0.13193792 0.14256498 0.1337128 0.1148884 0.12354247 0.32956615 0.32851818
		 0.019818941 0.012315891 0.026957637 0.028169408 0.037103537 0.032308199 0.0025750352
		 0.0031039407 0.011495628 0.010555176 0.068843663 0.061078046 0.15810844 0.15749046
		 0.14547537 0.14637591 0.2017072 0.20076497 0.24323386 0.24224831 0.24983875 0.24883431
		 0.17376305 0.19216199 0.2043063 0.19209301 0.34042338 0.33843836 0.34049368 0.33994773
		 0.37420177 0.37403789 0.37692553 0.37779841 0.37384036 0.37351641 0.34663486 0.34613129
		 0.30652806 0.29294619 0.29173785 0.29163319 0.29164577 0.29260361 0.29384789 0.30722857
		 0.51947433 0.50612628 0.5215593 0.52072006 0.50589192 0.51950455 0.52071542 0.52121359
		 0.56808025 0.56914848 0.5609408 0.55987585 0.57278401 0.5737204 0.72004622 0.72115511
		 0.37737447 0.37619236 0.37495232 0.37667754 0.43093047 0.4306401 0.43390864 0.43488604
		 0.51174408 0.51864892 0.7427578 0.74195695 0.74877185 0.7490502 0.73505783 0.7336511
		 0.60780209 0.60170913 0.62870723 0.64002728 0.60252553 0.59520471 0.64073968 0.64860141
		 0.74329752 0.74523729 0.7620542 0.76259875 0.76192594 0.75955337 0.60077125 0.5941574
		 0.59619957 0.59021193 0.55603021 0.54879552 0.42782292 0.42653084 0.42701784 0.42812175
		 0.38324904 0.38206884 0.55713618 0.56446123 0.56582743 0.56510293 0.7125867 0.71369845
		 0.55384934 0.55278111 0.74413818 0.74638736 0.74918997 0.74999243 0.75952911 0.76013255
		 0.26468942 0.24903119 0.28380123 0.26593664 0.23356281 0.24516092 0.24688984 0.27651402
		 0.053004853 0.041857231 0.040091716 0.0020000001 0.017711312 0.025165102 0.026399747
		 0.041395966 0.5508253 0.5508253 0.53813344 0.53813344 0.7300505 0.7300505 0.73487031
		 0.73487031 0.54039603 0.54039603 0.55422038 0.55422038 0.74967992 0.74967992 0.52770406
		 0.52770406 0.77264768 0.77264768 0.77654523 0.77654523 0.86829281 0.86829281 0.70032817
		 0.70032817 0.93876928 0.93876928 0.90217578 0.90217578 0.73697096 0.73697096 0.96886146
		 0.96886146 0.98367089 0.98367089 0.75882322 0.75882322 ;
	setAttr ".mve" -type "floatArray" 252 0.41310075 0.41506144 0.16589417 0.17712879
		 0.41856092 0.41841796 0.14459728 0.14400841 0.095092438 0.089897133 0.0047670174
		 0.0033790322 0.076370887 0.076905057 0.0032196098 0.0033639641 0.41902381 0.41902649
		 0.14329626 0.14255314 0.12510213 0.43198067 0.43315557 0.11519688 0.070456088 0.060190465
		 0.011348114 0.013474506 0.07564158 0.075927056 0.0036609487 0.0037514651 0.045891482
		 0.0020000001 0.0020000001 0.045891482 0.049859062 0.093750544 0.093750544 0.049859062
		 0.045891482 0.0020000001 0.0020000001 0.045891482 0.0020000001 0.0020000001 0.045891482
		 0.045891482 0.093750544 0.049859062 0.049859062 0.093750544 0.093750544 0.093750544
		 0.049859062 0.049859062 0.40132368 0.40234256 0.40086663 0.40016851 0.41144076 0.41227376
		 0.44417 0.44442329 0.50930309 0.51364326 0.57513458 0.5758518 0.5672937 0.56651902
		 0.57822847 0.57917368 0.63896561 0.63967413 0.85224861 0.83981377 0.84963775 0.86054492
		 0.84604508 0.83627069 0.96247947 0.96463031 0.57595742 0.58354259 0.44304475 0.44345176
		 0.47969377 0.48449263 0.40548092 0.40415463 0.41669145 0.41740197 0.52446848 0.53320062
		 0.5815537 0.58272123 0.57243139 0.5716719 0.64367956 0.64438772 0.69904244 0.69968873
		 0.6948154 0.69543457 0.78495491 0.76325864 0.77501988 0.78939652 0.95049965 0.95258838
		 0.96753734 0.96878493 0.94534981 0.94522196 0.94287091 0.94332135 0.94488615 0.94477713
		 0.96131432 0.96283811 0.41977251 0.41962546 0.41840073 0.14469488 0.077007972 0.0032040707
		 0.0020000001 0.0021436398 0.42024764 0.42023334 0.14243366 0.41902691 0.0024481844
		 0.0025467093 0.0037673169 0.075589485 0.80149156 0.80219495 0.81193054 0.81122535
		 0.79770124 0.79864633 0.52909946 0.52973396 0.97351354 0.97366321 0.96387655 0.96395785
		 0.99800003 0.99749756 0.9950878 0.99593568 0.72791886 0.73208791 0.43094006 0.4297612
		 0.42801586 0.42884818 0.43956724 0.43834323 0.59354353 0.58959091 0.62534606 0.63308573
		 0.70010889 0.69532222 0.61548847 0.62035257 0.44583556 0.44666603 0.44253978 0.44259465
		 0.46612883 0.4649359 0.60390013 0.5996756 0.60746956 0.60317647 0.67163211 0.66687411
		 0.99696809 0.99662274 0.99000794 0.99064738 0.97045279 0.97008348 0.75573522 0.76048517
		 0.7954877 0.79415381 0.52500498 0.52563483 0.80719054 0.80649149 0.45642835 0.45779628
		 0.43294457 0.43396649 0.43983135 0.44038424 0.41389397 0.41709769 0.17891155 0.41271523
		 0.0029416778 0.0020000001 0.0032170631 0.095025018 0.43405619 0.43452069 0.43316412
		 0.11363722 0.058653191 0.013865516 0.012680932 0.009432313 0.57174206 0.57174206
		 0.56859082 0.56859082 0.39960489 0.39960489 0.41183147 0.41183147 0.10086951 0.10086951
		 0.11389806 0.11389806 0.4238787 0.4238787 0.09771812 0.09771812 0.3197495 0.3197495
		 0.33223242 0.33223242 0.22966516 0.22966516 0.41310579 0.41310579 0.14302921 0.14302921
		 0.1880133 0.1880133 0.37422436 0.37422436 0.60217822 0.60217822 0.61422545 0.61422545
		 0.30672094 0.30672094 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[120:135]" "|TL_card_01_model|TL_card_01_modelShape.map[28:31]" "|TL_card_01_model|TL_card_01_modelShape.map[12:19]" "|TL_card_01_model|TL_card_01_modelShape.map[4:7]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold8";
	setAttr ".uvl" -type "Int32Array" 12 32 33 34 35 40 41
		 42 43 44 45 46 47 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26619014 0.2505087 0.26102647 0.28314349
		 0.30687913 0.29328504 0.29319459 0.30686885 0.27632985 0.25875184 0.23523463 0.24743214
		 0.30699271 0.29320991 0.29414767 0.30766854 0.52127433 0.50774372 0.50841594 0.52209121
		 0.023438603 0.053571552 0.041366752 0.004076737 0.038554803 0.019554345 0.042078368
		 0.026715288 0.52176458 0.50828117 0.50763029 0.52125436 0.73296916 0.73296916 0.53230679
		 0.53230679 0.97519636 0.97519636 0.7427727 0.7427727 0.73500371 0.73500371 0.53027219
		 0.53027219 0.73296916 0.53230679 0.53230679 0.73296916 0.7427727 0.7427727 0.97519636
		 0.97519636 0.97519636 0.7427727 0.7427727 0.97519636 0.0081913425 0.0079066837 0.0020000001
		 0.0021409234 0.017425973 0.016585842 0.020005314 0.020997712 0.007098095 0.0020000001
		 0.1618313 0.16090947 0.15237102 0.15326564 0.16610529 0.16532429 0.20928922 0.20834078
		 0.12174296 0.13275754 0.14345165 0.13454363 0.11560048 0.12430914 0.33163238 0.33057782
		 0.019931341 0.012380961 0.027115066 0.028334482 0.037324965 0.032499377 0.0025786625
		 0.0031109042 0.011555525 0.010609141 0.069265306 0.061450701 0.15909314 0.15847127
		 0.14638038 0.14728661 0.20296691 0.20201874 0.24475552 0.24376374 0.25140208 0.2503913
		 0.1748465 0.19336151 0.20558242 0.1932921 0.34255809 0.34056056 0.34262884 0.34207946
		 0.37654954 0.37638465 0.37929049 0.38016888 0.37618586 0.37585986 0.34880877 0.34830204
		 0.30694273 0.29327521 0.29205924 0.29195392 0.29196659 0.29293045 0.2941826 0.30764768
		 0.52123225 0.50779998 0.52333033 0.52248579 0.50756413 0.52126265 0.52248114 0.52298248
		 0.57341516 0.57449013 0.56623071 0.56515902 0.5781486 0.57909089 0.7263397 0.72745562
		 0.38177472 0.38058516 0.37933728 0.38107339 0.43540031 0.4351081 0.43839726 0.43938082
		 0.51699191 0.52394032 0.74946284 0.74865693 0.7555148 0.75579494 0.7417143 0.74029869
		 0.61365587 0.60752445 0.63469285 0.64608431 0.60834599 0.60097903 0.64680117 0.6547125
		 0.75000596 0.75195795 0.76861268 0.76916069 0.76848358 0.76609606 0.60658067 0.5999251
		 0.60198015 0.59595472 0.56155741 0.55427706 0.43227315 0.43097293 0.431463 0.43257385
		 0.38768634 0.38649872 0.56267035 0.5700416 0.57114816 0.57041907 0.71883315 0.71995193
		 0.55909449 0.55801952 0.75058365 0.75284702 0.75593561 0.75674313 0.76607168 0.76667893
		 0.2663464 0.2505894 0.28557876 0.26760149 0.23502345 0.24669473 0.24843456 0.2782456
		 0.053326581 0.042108644 0.04033199 0.0020000001 0.017810415 0.025311222 0.026553655
		 0.041644465 0.55353928 0.55353928 0.54076737 0.54076737 0.72887081 0.72887081 0.73372102
		 0.73372102 0.54304427 0.54304427 0.55695575 0.55695575 0.74862403 0.74862403 0.53027219
		 0.53027219 0.77676088 0.77676088 0.78068298 0.78068298 0.86798507 0.86798507 0.70398521
		 0.70398521 0.93890613 0.93890613 0.90208179 0.90208179 0.74085909 0.74085909 0.96918809
		 0.96918809 0.98409092 0.98409092 0.76284921 0.76284921 ;
	setAttr ".mve" -type "floatArray" 252 0.41569388 0.41766694 0.16692798 0.17823346
		 0.4211885 0.42104462 0.14549674 0.14490417 0.095679648 0.090451568 0.0047844709 0.0033877308
		 0.076839998 0.077377543 0.0032273028 0.0033725677 0.42165431 0.421657 0.14418752
		 0.14343971 0.12587863 0.43469289 0.43587521 0.1159109 0.070887893 0.060557518 0.01140708
		 0.013546885 0.076106094 0.076393373 0.0036714254 0.0037625129 0.0040345774 0.071825922
		 0.071825922 0.0040345774 0.0020000001 0.046168339 0.046168339 0.0020000001 0.0040345774
		 0.071825922 0.071825922 0.0040345774 0.073860496 0.073860496 0.0020000001 0.0020000001
		 0.046168339 0.0020000001 0.0020000001 0.046168339 0.046168339 0.046168339 0.0020000001
		 0.0020000001 0.4040471 0.40507242 0.40358716 0.40288463 0.41422799 0.41506627 0.44697708
		 0.44723198 0.51252103 0.51688856 0.57895434 0.57967609 0.571064 0.57028443 0.58206779
		 0.58301896 0.643188 0.64390099 0.85762978 0.8451165 0.85500246 0.86597842 0.85138714
		 0.84155107 0.96855599 0.97072035 0.5795958 0.58722883 0.44584474 0.44625431 0.48272493
		 0.48755407 0.40823057 0.40689591 0.41951182 0.42022681 0.52778208 0.5365693 0.58541393
		 0.58658886 0.5762341 0.57546985 0.64793169 0.64864433 0.7036438 0.70429415 0.69939011
		 0.70001316 0.78991163 0.76807851 0.7799139 0.79438126 0.95650059 0.95860249 0.97364575
		 0.9749012 0.95150483 0.95137614 0.94901025 0.94946355 0.95103824 0.95092851 0.96738344
		 0.96891689 0.42240772 0.42225975 0.4210273 0.14559497 0.077481106 0.0032116657 0.0020000001
		 0.0021445458 0.42288586 0.42287147 0.14331949 0.42165741 0.0024510114 0.0025501577
		 0.0037784646 0.076053672 0.80025202 0.80095983 0.8107568 0.81004721 0.7964378 0.79738885
		 0.5261417 0.52678025 0.97341043 0.97356105 0.96371263 0.96379447 0.99800003 0.99749434
		 0.99506938 0.99592263 0.72626656 0.73046196 0.42741451 0.42622823 0.42447186 0.42530942
		 0.4360961 0.43486437 0.59104365 0.58706611 0.62304676 0.63083529 0.69828117 0.69346434
		 0.61312699 0.61802179 0.44240397 0.44323969 0.43903604 0.43909127 0.46277389 0.46157345
		 0.60146558 0.5972144 0.60505754 0.60073733 0.66962481 0.66483676 0.99696153 0.99661404
		 0.98995751 0.99060094 0.97033036 0.96995872 0.75425839 0.75903833 0.79421026 0.79286796
		 0.52202141 0.52265525 0.80598694 0.80528349 0.45301223 0.45438877 0.42943165 0.43046004
		 0.43631053 0.43686691 0.4164921 0.41971603 0.18002747 0.41530594 0.0029476176 0.0020000001
		 0.00322474 0.095611796 0.4367815 0.43724895 0.43588382 0.11434141 0.059010547 0.01394036
		 0.012748305 0.0094791939 0.55435336 0.55435336 0.55118221 0.55118221 0.38810405 0.38810405
		 0.40040776 0.40040776 0.080510624 0.080510624 0.093621358 0.093621358 0.41253096
		 0.41253096 0.077339351 0.077339351 0.30077127 0.30077127 0.31333292 0.31333292 0.21709238
		 0.21709238 0.39471641 0.39471641 0.12990995 0.12990995 0.17517778 0.17517778 0.35558975
		 0.35558975 0.59195518 0.59195518 0.60407841 0.60407841 0.28766054 0.28766054 ;
	setAttr ".mnsl" -type "stringArray" 2 "|TL_card_01_model|TL_card_01_modelShape.map[40:47]" "|TL_card_01_model|TL_card_01_modelShape.map[32:35]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold9";
	setAttr ".uvl" -type "Int32Array" 20 216 217 218 219 224 225
		 226 227 230 231 232 233 234 235 238 239 244 245
		 250 251 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26727012 0.25152457 0.26208535 0.28429279
		 0.30809227 0.29444262 0.29435179 0.30808195 0.27745128 0.25980142 0.23618808 0.24843545
		 0.30820632 0.29436716 0.29530877 0.30888492 0.52336389 0.50977796 0.51045293 0.52418411
		 0.023526242 0.053782374 0.041527681 0.0040852265 0.038704235 0.019626105 0.042242207
		 0.026816322 0.52385616 0.51031762 0.50966412 0.52334386 0.73648661 0.73648661 0.53500396
		 0.53500396 0.97953928 0.97953928 0.74616545 0.74616545 0.7385295 0.7385295 0.53296101
		 0.53296101 0.73648661 0.53500396 0.53500396 0.73648661 0.74616545 0.74616545 0.97953928
		 0.97953928 0.97953928 0.74616545 0.74616545 0.97953928 0.53302759 0.53274173 0.52681088
		 0.52695239 0.54229993 0.5414564 0.60024101 0.60123742 0.58728099 0.58216208 0.68729562
		 0.68637002 0.67779666 0.6786949 0.69158703 0.69080287 0.7349475 0.7339952 0.70239455
		 0.71345413 0.72419196 0.71524751 0.69622695 0.70497119 0.91314197 0.91208309 0.60016668
		 0.59258544 0.60737979 0.60860419 0.61763144 0.61278611 0.52739191 0.52792639 0.5364055
		 0.53545523 0.64970237 0.64185578 0.68454623 0.68392181 0.67178148 0.67269146 0.72859937
		 0.7276473 0.77055877 0.76956296 0.77723253 0.77621764 0.75571513 0.77430582 0.78657669
		 0.77423614 0.92411232 0.92210662 0.92418337 0.92363173 0.90289158 0.90272599 0.90564376
		 0.90652573 0.90252644 0.90219909 0.93038857 0.92987978 0.30815613 0.29443273 0.29321182
		 0.29310605 0.29311877 0.29408658 0.29534385 0.30886397 0.52332163 0.50983447 0.5254283
		 0.5245803 0.50959766 0.52335221 0.52457565 0.52507901 0.39512971 0.39620909 0.38791591
		 0.38683984 0.3998825 0.40082866 0.54867941 0.54979986 0.20330153 0.20210712 0.20085414
		 0.20259733 0.25655067 0.25625727 0.25955987 0.26054746 0.33907148 0.3460483 0.57249272
		 0.57168353 0.57856947 0.57885075 0.56471252 0.56329113 0.43613061 0.42997414 0.45725358
		 0.46869162 0.43079901 0.42340195 0.4694114 0.47735506 0.5730381 0.57499808 0.59112519
		 0.59167546 0.59099555 0.58859825 0.42902648 0.4223437 0.42440715 0.41835707 0.38381916
		 0.37650907 0.25341073 0.25210521 0.25259727 0.25371265 0.20923732 0.20804484 0.38493666
		 0.39233804 0.39285344 0.39212137 0.54114217 0.54226553 0.38075051 0.37967113 0.57302243
		 0.57529509 0.57899195 0.57980281 0.58857381 0.58918351 0.26742703 0.25160563 0.28673801
		 0.26868725 0.23597604 0.24769503 0.24944197 0.27937487 0.0535364 0.042272605 0.040488686
		 0.0020000001 0.017875047 0.025406517 0.026654029 0.04180653 0.98602045 0.99800003
		 0.99800003 0.98602045 0.55551153 0.55551153 0.56038153 0.56038153 0.98602045 0.99800003
		 0.99800003 0.98602045 0.57534546 0.57534546 0.98602045 0.99800003 0.98602045 0.99800003
		 0.99800003 0.98602045 0.69519448 0.69519448 0.98602045 0.99800003 0.76640546 0.76640546
		 0.72943056 0.72943056 0.99800003 0.98602045 0.79681122 0.79681122 0.81177497 0.81177497
		 0.98602045 0.99800003 ;
	setAttr ".mve" -type "floatArray" 252 0.41738504 0.41936615 0.1676022 0.17895389
		 0.42290214 0.42275766 0.14608335 0.14548835 0.096062601 0.090813152 0.0047958535
		 0.0033934037 0.077145942 0.077685684 0.0032323198 0.0033781787 0.42336982 0.42337254
		 0.14476877 0.1440179 0.12638503 0.43646172 0.43764886 0.11637656 0.071169503 0.060796898
		 0.011445535 0.013594088 0.076409034 0.076697491 0.003678258 0.0037697179 0.0040428946
		 0.072111368 0.072111368 0.0040428946 0.0020000001 0.0463489 0.0463489 0.0020000001
		 0.0040428946 0.072111368 0.072111368 0.0040428946 0.074154258 0.074154258 0.0020000001
		 0.0020000001 0.0463489 0.0020000001 0.0020000001 0.0463489 0.0463489 0.0463489 0.0020000001
		 0.0020000001 0.29604205 0.29707158 0.29558024 0.29487485 0.30626458 0.30710629 0.40201914
		 0.40227509 0.46783105 0.47221643 0.47166431 0.47238901 0.46374172 0.46295896 0.47479048
		 0.47574556 0.53616053 0.53687644 0.81435055 0.80178612 0.8117125 0.82273334 0.8080824
		 0.79820615 0.92573023 0.92790347 0.53518003 0.54284424 0.40088218 0.40129343 0.43791315
		 0.44276202 0.30024263 0.29890251 0.31157002 0.31228793 0.48315448 0.4919776 0.47815031
		 0.47933003 0.46893296 0.46816558 0.54092366 0.54163921 0.59686351 0.59751648 0.59259242
		 0.59321803 0.74635559 0.72443324 0.73631698 0.75084352 0.91362554 0.91573608 0.93084079
		 0.93210137 0.84573776 0.84560853 0.84323299 0.84368813 0.84526926 0.84515911 0.92455292
		 0.92609262 0.42412633 0.42397773 0.42274025 0.14618197 0.077789672 0.0032166189 0.0020000001
		 0.0021451365 0.42460641 0.42459196 0.14389719 0.42337295 0.002452855 0.0025524066
		 0.0037857348 0.076356396 0.75125438 0.75196505 0.76180208 0.76108962 0.74742454 0.74837947
		 0.4760235 0.47666466 0.92528611 0.92543733 0.91554868 0.91563082 0.94981074 0.94930297
		 0.94686812 0.94772488 0.67713195 0.68134445 0.37705818 0.37586704 0.37410349 0.37494448
		 0.38577527 0.3845385 0.54135621 0.53736246 0.57349014 0.58131057 0.64903212 0.64419562
		 0.56352985 0.56844467 0.39210892 0.39294806 0.38856176 0.38861722 0.41239664 0.41119128
		 0.55182076 0.54755223 0.55542743 0.55108953 0.62025863 0.61545104 0.94876802 0.94841909
		 0.94173539 0.94238144 0.92219347 0.92182028 0.70523816 0.71003765 0.74518788 0.7438401
		 0.47188637 0.4725228 0.75701272 0.75630641 0.40259507 0.40397725 0.37908357 0.38011616
		 0.3858251 0.38638377 0.41818652 0.42142361 0.18075523 0.4169955 0.0029514912 0.0020000001
		 0.0032297466 0.095994473 0.43855885 0.4390282 0.43765751 0.11480065 0.059243601 0.013989172
		 0.012792243 0.0095097683 0.67311507 0.67311507 0.68466669 0.68466669 0.30969858 0.30969858
		 0.3220526 0.3220526 0.013551611 0.013551611 0.027165305 0.027165305 0.33422536 0.33422536
		 0.0020000001 0.0020000001 0.24226166 0.24226166 0.25381327 0.25381327 0.13798782
		 0.13798782 0.3953957 0.3953957 0.050448988 0.050448988 0.095901877 0.095901877 0.32732722
		 0.32732722 0.51438308 0.51438308 0.52655584 0.52655584 0.22864798 0.22864798 ;
	setAttr ".mnsl" -type "stringArray" 6 "|TL_card_01_model|TL_card_01_modelShape.map[250:251]" "|TL_card_01_model|TL_card_01_modelShape.map[244:245]" "|TL_card_01_model|TL_card_01_modelShape.map[238:239]" "|TL_card_01_model|TL_card_01_modelShape.map[230:235]" "|TL_card_01_model|TL_card_01_modelShape.map[224:227]" "|TL_card_01_model|TL_card_01_modelShape.map[216:219]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold10";
	setAttr ".uvl" -type "Int32Array" 16 220 221 222 223 228 229
		 236 237 240 241 242 243 246 247 248 249 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26384068 0.24829867 0.25872293 0.28064325
		 0.30389297 0.29041979 0.29033014 0.30388278 0.2738902 0.25646853 0.23316047 0.24524949
		 0.30400556 0.29034531 0.29127473 0.30467537 0.51638156 0.50297123 0.50363749 0.51719117
		 0.023247948 0.053112924 0.041016661 0.0040582684 0.038229719 0.019398233 0.041721951
		 0.026495492 0.51686746 0.50350392 0.50285888 0.51636177 0.72538161 0.72538161 0.5265038
		 0.5265038 0.75484395 0.75484395 0.52448726 0.52448726 0.7273981 0.7273981 0.52448726
		 0.52448726 0.72538161 0.5265038 0.5265038 0.72538161 0.52448726 0.52448726 0.75484395
		 0.75484395 0.75484395 0.52448726 0.52448726 0.75484395 0.0081363339 0.0078541655
		 0.0020000001 0.0021396722 0.017288795 0.016456176 0.019845197 0.020828726 0.0070527284
		 0.0020000001 0.16040994 0.15949631 0.15103379 0.15192042 0.16464588 0.16387185 0.20744577
		 0.20650578 0.12067808 0.13159469 0.1421937 0.13336489 0.11459023 0.12322142 0.32870093
		 0.32765573 0.019771831 0.012288608 0.026891693 0.028100263 0.0370108 0.03222812 0.0025735144
		 0.0031010795 0.011470578 0.010532586 0.068667121 0.060921967 0.1576961 0.15707976
		 0.14509638 0.1459946 0.20117971 0.20023996 0.24259666 0.24161372 0.24918413 0.24818236
		 0.17330933 0.19165967 0.2037719 0.19159091 0.33952945 0.33754969 0.33959958 0.33905509
		 0.37321863 0.37305519 0.37593523 0.37680578 0.3728582 0.37253508 0.34572458 0.34522235
		 0.303956 0.29041004 0.2892049 0.2891005 0.28911304 0.29006836 0.29130936 0.30465469
		 0.51633984 0.50302702 0.51841927 0.51758224 0.50279325 0.51637 0.51757765 0.51807451
		 0.56223518 0.56330061 0.55511463 0.55405247 0.56692654 0.56786048 0.71379977 0.71490574
		 0.38424435 0.3830654 0.38182861 0.38354927 0.42544773 0.42515811 0.42841801 0.42939284
		 0.51825905 0.52514565 0.74866259 0.74786383 0.75466073 0.75493836 0.74098295 0.73957992
		 0.61406338 0.60798651 0.63491327 0.6462034 0.60880071 0.60149926 0.64691389 0.65475488
		 0.74920088 0.75113553 0.75569683 0.75623995 0.75556886 0.75320256 0.60705107 0.60045469
		 0.6024915 0.59651959 0.56242824 0.55521262 0.42234838 0.42105973 0.42154545 0.4226464
		 0.3901034 0.38892636 0.56353128 0.57083696 0.55998832 0.55926573 0.70635998 0.70746881
		 0.54804188 0.54697645 0.73782808 0.74007136 0.75507778 0.75587815 0.75317842 0.75378025
		 0.26399556 0.24837869 0.28305689 0.26523948 0.23295116 0.24451865 0.246243 0.27578893
		 0.052870132 0.041751955 0.039991099 0.0020000001 0.017669812 0.025103914 0.026335299
		 0.041291907 0.76165724 0.77348191 0.77348191 0.76165724 0.77948952 0.79131424 0.79131424
		 0.77948952 0.76165724 0.77348191 0.77348191 0.76165724 0.79131424 0.77948952 0.76165724
		 0.77348191 0.76165724 0.77348191 0.77348191 0.76165724 0.77948952 0.79131424 0.76165724
		 0.77348191 0.77948952 0.79131424 0.79131424 0.77948952 0.77348191 0.76165724 0.77948952
		 0.79131424 0.79131424 0.77948952 0.76165724 0.77348191 ;
	setAttr ".mve" -type "floatArray" 252 0.41201487 0.41397038 0.16546127 0.1766662
		 0.41746065 0.41731805 0.14422062 0.14363332 0.094846547 0.089664966 0.0047597084
		 0.0033753896 0.076174445 0.076707207 0.0032163882 0.0033603611 0.41792229 0.41792497
		 0.14292304 0.14218187 0.12477697 0.43084493 0.43201673 0.11489788 0.070275269 0.060036764
		 0.011323422 0.013444198 0.07544706 0.075731792 0.003656558 0.0037468353 0.0040164837
		 0.071204953 0.071204953 0.0040164837 0.076787896 0.12056345 0.12056345 0.076787896
		 0.0040164837 0.071204953 0.071204953 0.0040164837 0.07322143 0.07322143 0.0020000001
		 0.0020000001 0.12056345 0.076787896 0.076787896 0.12056345 0.12056345 0.12056345
		 0.076787896 0.076787896 0.40004438 0.40106058 0.39958853 0.39889225 0.41013473 0.41096556
		 0.44129393 0.44154656 0.50625497 0.5105837 0.57339615 0.57411146 0.56557596 0.56480336
		 0.57648188 0.57742465 0.63705856 0.63776517 0.84829462 0.83589268 0.84569073 0.85656905
		 0.84210753 0.83235896 0.95823437 0.96037954 0.57273328 0.58029842 0.44017166 0.4405776
		 0.47672388 0.48151007 0.40419063 0.40286785 0.4153716 0.41608021 0.52138031 0.53008938
		 0.57979828 0.58096278 0.57070011 0.56994265 0.64176011 0.64246637 0.69697672 0.69762129
		 0.69276088 0.69337839 0.78117871 0.75953978 0.77126992 0.78560865 0.9462862 0.94836944
		 0.96327889 0.9645232 0.94263351 0.94250596 0.94016111 0.94061035 0.94217104 0.94206232
		 0.95707232 0.95859212 0.41866902 0.41852236 0.41730088 0.14431797 0.076809853 0.0032008903
		 0.0020000001 0.0021432599 0.4191429 0.41912863 0.14206272 0.41792539 0.0024469972
		 0.0025452615 0.0037626452 0.075395107 0.8020106 0.80271208 0.81242192 0.8117187 0.79823029
		 0.79917288 0.53033793 0.53097081 0.95378834 0.95393765 0.94417679 0.94425792 0.99800003
		 0.99749881 0.99509543 0.9959411 0.7088424 0.71300042 0.41264805 0.41147229 0.40973157
		 0.41056168 0.42125243 0.42003167 0.57482201 0.57087988 0.6065405 0.61425984 0.68110585
		 0.67633188 0.59670895 0.60156024 0.42750421 0.42833251 0.44400692 0.44406167 0.46753368
		 0.46634391 0.58515126 0.58093792 0.58871132 0.5844295 0.65270436 0.64795893 0.99697077
		 0.99662632 0.99002904 0.99066675 0.95073569 0.95036733 0.73658526 0.7413227 0.79602253
		 0.79469216 0.5262543 0.52688253 0.80769449 0.8069973 0.45785883 0.45922312 0.41464725
		 0.41566649 0.44130567 0.4418571 0.412806 0.41600126 0.17844425 0.41163036 0.00293919
		 0.0020000001 0.0032138482 0.094779298 0.43291494 0.43337825 0.43202525 0.11334235
		 0.058503546 0.013834174 0.012652719 0.0094126808 0.66443872 0.66443872 0.67584103
		 0.67584103 0.41588098 0.41588098 0.42728326 0.42728326 0.013402268 0.013402268 0.026839962
		 0.026839962 0.44072095 0.44072095 0.0020000001 0.0020000001 0.2391555 0.2391555 0.25055775
		 0.25055775 0.14175202 0.14175202 0.39030978 0.39030978 0.0020000001 0.0020000001
		 0.074563548 0.074563548 0.32312131 0.32312131 0.63959885 0.63959885 0.65303653 0.65303653
		 0.22571781 0.22571781 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[246:249]" "|TL_card_01_model|TL_card_01_modelShape.map[240:243]" "|TL_card_01_model|TL_card_01_modelShape.map[236:237]" "|TL_card_01_model|TL_card_01_modelShape.map[228:229]" "|TL_card_01_model|TL_card_01_modelShape.map[220:223]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold11";
	setAttr ".uvl" -type "Int32Array" 12 36 37 38 39 48 49
		 50 51 52 53 54 55 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26266065 0.2471887 0.25756598 0.2793875
		 0.30228153 0.28886905 0.28877982 0.30227137 0.27266487 0.25532174 0.23211871 0.24415326
		 0.30239362 0.28879493 0.28972015 0.30306041 0.51381248 0.50046259 0.50112587 0.51461846
		 0.023152191 0.052882578 0.040840827 0.0040489924 0.038066443 0.019319825 0.04154294
		 0.026385101 0.51429623 0.50099289 0.50035077 0.51379281 0.72184688 0.72184688 0.52386528
		 0.52386528 0.72184688 0.72184688 0.52386522 0.52386522 0.72385424 0.72385424 0.52185786
		 0.52185786 0.72184688 0.52386528 0.52386528 0.72184688 0.52185786 0.52185786 0.72385424
		 0.72385424 0.72184688 0.52386522 0.52386522 0.72184688 0.0081086792 0.007827783 0.0020000001
		 0.0021390426 0.017219894 0.016391026 0.019764775 0.020743871 0.0070299576 0.0020000001
		 0.15969604 0.15878654 0.15036215 0.15124479 0.16391289 0.16314235 0.2065199 0.20558414
		 0.12014324 0.13101065 0.1415619 0.13277288 0.11408282 0.12267512 0.32722861 0.32618812
		 0.019691739 0.012242241 0.026779516 0.027982639 0.036853019 0.032091893 0.0025709297
		 0.0030961172 0.011427897 0.010494133 0.068366677 0.060656428 0.15699443 0.15638086
		 0.1444515 0.14534567 0.20028208 0.19934656 0.24151237 0.24053386 0.24807017 0.24707291
		 0.1725373 0.19080494 0.20286259 0.19073649 0.33800831 0.33603749 0.33807814 0.3375361
		 0.37154567 0.37138298 0.37425002 0.37511668 0.37118688 0.3708652 0.34417555 0.34367558
		 0.30234426 0.28885937 0.28765965 0.28755572 0.28756821 0.2885192 0.28975463 0.30303982
		 0.513771 0.50051814 0.51584101 0.51500779 0.50028545 0.51380098 0.5150032 0.5154978
		 0.71365064 0.71471131 0.70656222 0.70550483 0.71832091 0.71925062 0.86453223 0.86563319
		 0.35402757 0.35285392 0.3516227 0.35333562 0.57747966 0.57719135 0.58043659 0.58140701
		 0.48743832 0.49429387 0.71680349 0.71600837 0.72277462 0.72305101 0.70915848 0.70776176
		 0.58281088 0.57676142 0.60356683 0.61480606 0.57757193 0.57030338 0.61551338 0.62331903
		 0.7173394 0.71926534 0.90624046 0.90678114 0.90611303 0.90375739 0.57583016 0.56926352
		 0.57129115 0.56534618 0.53140843 0.52422535 0.57439429 0.57311141 0.57359499 0.57469094
		 0.35986021 0.35868847 0.53250653 0.53977931 0.71141392 0.71069461 0.85712594 0.85822976
		 0.6995213 0.6984607 0.88845223 0.89068538 0.72318977 0.72398657 0.90373337 0.90433252
		 0.26281485 0.24726835 0.28179026 0.26405317 0.23191035 0.24342571 0.24514228 0.27455506
		 0.052640878 0.041572805 0.039819889 0.0020000001 0.017599193 0.024999792 0.026225628
		 0.041114833 0.73053318 0.74230456 0.74230456 0.73053318 0.74883801 0.76060945 0.76060945
		 0.74883801 0.73053318 0.74230456 0.74230456 0.73053318 0.76060945 0.74883801 0.73053318
		 0.74230456 0.73053318 0.74230456 0.74230456 0.73053318 0.74883801 0.76060945 0.73053318
		 0.74230456 0.74883801 0.76060945 0.76060945 0.74883801 0.74230456 0.73053318 0.74883801
		 0.76060945 0.76060945 0.74883801 0.73053318 0.74230456 ;
	setAttr ".mve" -type "floatArray" 252 0.4101671 0.41211379 0.16472462 0.17587905
		 0.41558832 0.41544637 0.14357968 0.14299503 0.094428122 0.089269891 0.0047472715
		 0.0033691912 0.075840168 0.07637053 0.0032109062 0.0033542304 0.41604787 0.41605055
		 0.14228795 0.14155012 0.12422366 0.42891228 0.4300788 0.11438908 0.069967575 0.059775215
		 0.011281405 0.013392624 0.075116061 0.075399511 0.0036490925 0.0037389628 0.0040073963
		 0.070893072 0.070893072 0.0040073963 0.078568831 0.14545451 0.14545451 0.078568831
		 0.0040073963 0.070893072 0.070893072 0.0040073963 0.072900467 0.072900467 0.0020000001
		 0.0020000001 0.14545451 0.078568831 0.078568831 0.14545451 0.14746191 0.14746191
		 0.076561436 0.076561436 0.39913017 0.40014178 0.39867637 0.39798322 0.40917504 0.41000211
		 0.4412573 0.4415088 0.5059256 0.51023483 0.57170069 0.57241279 0.56391573 0.56314665
		 0.57477254 0.57571101 0.63507617 0.63577962 0.8464238 0.83407772 0.84383166 0.85466093
		 0.84026456 0.83055997 0.95586807 0.95800358 0.57210433 0.57963538 0.4401401 0.44054419
		 0.47652757 0.48129222 0.40325773 0.40194091 0.4143883 0.41509372 0.52098274 0.5296526
		 0.57807398 0.57923323 0.56901681 0.56826276 0.63975656 0.64045966 0.69472432 0.69536597
		 0.6905275 0.6911422 0.77961034 0.75806892 0.76974618 0.7840203 0.94397378 0.9460476
		 0.96088988 0.96212858 0.93927401 0.93914706 0.93681276 0.93725997 0.93881363 0.93870538
		 0.95471126 0.9562242 0.41679123 0.41664523 0.41542926 0.14367659 0.076472715 0.0031954781
		 0.0020000001 0.0021426142 0.417263 0.41724879 0.14143151 0.41605097 0.0024449828
		 0.0025428042 0.0037547015 0.075064339 0.76072299 0.76142132 0.77108741 0.77038735
		 0.75695974 0.75789809 0.49027467 0.49090469 0.99785137 0.99800003 0.98828316 0.98836386
		 0.9558292 0.95533025 0.95293766 0.95377952 0.75400931 0.75814855 0.45914978 0.45797932
		 0.45624644 0.45707279 0.46771538 0.4665001 0.62059289 0.61666852 0.65216845 0.65985298
		 0.72639775 0.7216453 0.64238119 0.6472106 0.47393897 0.47476354 0.40433273 0.40438721
		 0.42775345 0.42656904 0.63087559 0.62668121 0.63441962 0.63015705 0.69812423 0.6934002
		 0.9548046 0.95446169 0.9478941 0.94852895 0.99481249 0.9944458 0.78162712 0.78634322
		 0.75476193 0.75343758 0.48620942 0.48683482 0.76638126 0.76568723 0.4181222 0.41948035
		 0.46113998 0.4621546 0.40164363 0.40219259 0.41095465 0.41413552 0.17764908 0.40978432
		 0.0029349574 0.0020000001 0.0032083779 0.094361179 0.43097296 0.43143418 0.4300873
		 0.11284057 0.058248904 0.013780842 0.012604712 0.0093792742 0.66145337 0.66145337
		 0.6728043 0.6728043 0.41401571 0.41401571 0.42536661 0.42536661 0.013350883 0.013350883
		 0.026728019 0.026728019 0.43874374 0.43874374 0.0020000001 0.0020000001 0.23808673
		 0.23808673 0.2494376 0.2494376 0.14112219 0.14112219 0.38855982 0.38855982 0.0020000001
		 0.0020000001 0.07423652 0.07423652 0.32167414 0.32167414 0.63672537 0.63672537 0.6501025
		 0.6501025 0.2247096 0.2247096 ;
	setAttr ".mnsl" -type "stringArray" 2 "|TL_card_01_model|TL_card_01_modelShape.map[48:55]" "|TL_card_01_model|TL_card_01_modelShape.map[36:39]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold12";
	setAttr ".uvl" -type "Int32Array" 34 144 145 146 147 152 153
		 154 155 156 157 158 159 160 161 162 163 164 165
		 166 167 168 169 174 175 176 177 178 179 184 185
		 186 187 196 197 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.25064003 0.23588158 0.24578029 0.26659551
		 0.28893927 0.27614534 0.27606022 0.28892958 0.26018289 0.24363956 0.22150655 0.23298612
		 0.2890462 0.27607462 0.27695718 0.28968224 0.49071527 0.47798103 0.4786137 0.49148408
		 0.022176737 0.050536077 0.039049644 0.0039545009 0.036403202 0.018521104 0.039719377
		 0.025260556 0.49117669 0.47848687 0.47787437 0.49069649 0.69074547 0.69074547 0.501894
		 0.501894 0.8899371 0.8899371 0.70108557 0.70108557 0.69266027 0.69266027 0.49997914
		 0.49997914 0.69074547 0.501894 0.501894 0.69074547 0.69917077 0.69917077 0.8918519
		 0.8918519 0.8899371 0.70108557 0.70108557 0.8899371 0.50580609 0.50553817 0.49997914
		 0.50011176 0.51449716 0.51370651 0.67826992 0.67920387 0.66612238 0.66132438 0.65040284
		 0.6495353 0.6414994 0.64234138 0.65442526 0.65369028 0.69506741 0.69417477 0.7740193
		 0.78438556 0.79445022 0.78606653 0.76823837 0.77643442 0.97155476 0.97056222 0.67820024
		 0.6710943 0.68496114 0.68610877 0.69457012 0.69002855 0.50052375 0.50102472 0.50897223
		 0.50808156 0.72463048 0.7172758 0.64782584 0.64724058 0.63586134 0.63671428 0.68911725
		 0.68822485 0.72844619 0.72751278 0.73470151 0.73375028 0.8239972 0.84142238 0.85292399
		 0.84135711 0.98183733 0.9799574 0.98190391 0.9813869 0.85248286 0.85232764 0.85506248
		 0.8558892 0.85214061 0.85183376 0.98772013 0.98724324 0.28899911 0.2761361 0.27499169
		 0.27489257 0.27490446 0.27581161 0.27699006 0.2896626 0.49067569 0.47803402 0.49265024
		 0.49185547 0.47781205 0.4907043 0.49185106 0.49232286 0.13605812 0.13706987 0.12929659
		 0.12828796 0.140513 0.14139985 0.27998164 0.28103185 0.44683281 0.44604772 0.43741238
		 0.43996933 0.0061667995 0.0058917874 0.0089873644 0.0099130357 0.42827013 0.44737074
		 0.73475498 0.73382205 0.74406594 0.74399716 0.72318703 0.72121722 0.52442753 0.50847703
		 0.58652943 0.61963511 0.59992027 0.57988727 0.60905337 0.63049388 0.73504227 0.73835224
		 0.31976646 0.32028219 0.3196449 0.31739789 0.51133645 0.49373764 0.5002802 0.48394307
		 0.47137496 0.45162204 0.00322371 0.0020000001 0.0024612721 0.0035066819 0.44968393
		 0.44851738 0.55325019 0.57337981 0.13392454 0.1332384 0.27291691 0.27396983 0.12258036
		 0.12156867 0.30279857 0.30492872 0.74210781 0.74228662 0.317375 0.31794649 0.25078711
		 0.23595755 0.26888746 0.25196832 0.2213078 0.23229213 0.23392953 0.2619859 0.050305523
		 0.039747868 0.038075786 0.0020000001 0.016879821 0.023939133 0.02510844 0.039311014
		 0.89836246 0.90959096 0.90959096 0.89836246 0.91628969 0.92751825 0.92751825 0.91628969
		 0.89836246 0.90959096 0.90959096 0.89836246 0.92751825 0.91628969 0.89836246 0.90959096
		 0.89836246 0.90959096 0.90959096 0.89836246 0.91628969 0.92751825 0.89836246 0.90959096
		 0.91628969 0.92751825 0.92751825 0.91628969 0.90959096 0.89836246 0.91628969 0.92751825
		 0.92751825 0.91628969 0.89836246 0.90959096 ;
	setAttr ".mve" -type "floatArray" 252 0.39134407 0.39320099 0.15722041 0.16786045
		 0.39651528 0.39637989 0.13705058 0.13649291 0.090165704 0.085245356 0.0046205781
		 0.0033060494 0.072434954 0.072940856 0.0031550638 0.0032917785 0.39695364 0.39695621
		 0.13581844 0.13511463 0.11858719 0.40922481 0.41033754 0.10920615 0.066833183 0.057110853
		 0.010853384 0.012867241 0.071744241 0.072014615 0.0035730398 0.0036587655 0.0039148233
		 0.067716002 0.067716002 0.0039148233 0.0039148224 0.06771601 0.06771601 0.0039148224
		 0.0039148233 0.067716002 0.067716002 0.0039148233 0.069630824 0.069630824 0.0020000001
		 0.0020000001 0.06771601 0.0039148224 0.0039148224 0.06771601 0.069630831 0.069630831
		 0.0020000001 0.0020000001 0.074708708 0.07567367 0.074275836 0.073614657 0.084290355
		 0.08507929 0.50114924 0.50138915 0.56283528 0.56694579 0.23932098 0.24000023 0.23189503
		 0.23116142 0.24225117 0.24314636 0.29977384 0.30044484 0.88763106 0.87585431 0.88515848
		 0.89548832 0.88175589 0.87249881 0.99202818 0.99406523 0.62596214 0.63314587 0.50008351
		 0.50046897 0.53479296 0.53933787 0.07864593 0.077389836 0.089263201 0.089936092 0.57719803
		 0.58546805 0.24540035 0.24650615 0.23676087 0.23604159 0.30423838 0.30490905 0.35667127
		 0.35728332 0.35266799 0.35325432 0.82389879 0.80335075 0.81448948 0.82810539 0.98068243
		 0.98266059 0.99681842 0.99800003 0.58994329 0.58982223 0.58759558 0.58802217 0.58950412
		 0.58940089 0.99092472 0.99236792 0.39766273 0.39752346 0.39636356 0.13714303 0.073038332
		 0.0031403473 0.0020000001 0.0021360372 0.39811274 0.39809918 0.13500148 0.39695659
		 0.002424459 0.0025177691 0.0036737784 0.071694903 0.74423522 0.74490136 0.75412166
		 0.75345391 0.74064553 0.74154061 0.48625889 0.48685986 0.84801495 0.84817505 0.83822227
		 0.83896571 0.93034393 0.92986798 0.92758572 0.92838871 0.53520209 0.55698627 0.39864349
		 0.39645535 0.40380549 0.40486944 0.397017 0.39360228 0.4187654 0.39831799 0.52596933
		 0.56644076 0.64215165 0.61753935 0.53096235 0.55736732 0.41490307 0.41852057 0.40428025
		 0.40433222 0.4266209 0.42549112 0.42440218 0.40209657 0.42978066 0.40926161 0.49384925
		 0.46947312 0.92936653 0.92903948 0.92277473 0.92338032 0.84653968 0.84652263 0.67956638
		 0.70401764 0.73854905 0.73728579 0.48238111 0.48297769 0.74963254 0.74897051 0.4174338
		 0.41872934 0.40853417 0.40996298 0.40171516 0.40223882 0.3920953 0.39512947 0.16954885
		 0.39097893 0.0028918409 0.0020000001 0.0031526522 0.090101853 0.41119045 0.41163039
		 0.41034561 0.10772905 0.055654928 0.013237556 0.012115665 0.0090389717 0.63104206
		 0.63104206 0.64186949 0.64186949 0.39501518 0.39501518 0.40584263 0.40584263 0.012827425
		 0.012827425 0.025587663 0.025587663 0.41860285 0.41860285 0.0020000001 0.0020000001
		 0.22719936 0.22719936 0.23802678 0.23802678 0.13470644 0.13470644 0.37073323 0.37073323
		 0.0020000001 0.0020000001 0.070905261 0.070905261 0.30693206 0.30693206 0.60745436
		 0.60745436 0.62021458 0.62021458 0.21443914 0.21443914 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[196:197]" "|TL_card_01_model|TL_card_01_modelShape.map[184:187]" "|TL_card_01_model|TL_card_01_modelShape.map[174:179]" "|TL_card_01_model|TL_card_01_modelShape.map[152:169]" "|TL_card_01_model|TL_card_01_modelShape.map[144:147]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold13";
	setAttr ".uvl" -type "Int32Array" 34 144 145 146 147 152 153
		 154 155 156 157 158 159 160 161 162 163 164 165
		 166 167 168 169 174 175 176 177 178 179 184 185
		 186 187 196 197 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.26000229 0.24468812 0.25495955 0.27655855
		 0.30058753 0.28731185 0.28722355 0.30057749 0.26990446 0.25273821 0.22977181 0.24168363
		 0.30069849 0.28723848 0.28815427 0.30135849 0.50996113 0.49674743 0.49740392 0.51075894
		 0.022936469 0.052363645 0.040444706 0.0040280954 0.037698615 0.019143187 0.041139655
		 0.026136406 0.51043993 0.49727231 0.49663675 0.5099417 0.7163347 0.7163347 0.52037227
		 0.52037227 0.7163347 0.7163347 0.52037221 0.52037221 0.71832162 0.71832162 0.51838529
		 0.51838529 0.7163347 0.52037227 0.52037227 0.7163347 0.51838529 0.51838529 0.71832162
		 0.71832162 0.7163347 0.52037221 0.52037221 0.7163347 0.008046357 0.0077683455 0.0020000001
		 0.0021376139 0.017064681 0.016244253 0.019583605 0.020552715 0.0069786576 0.0020000001
		 0.15808772 0.15718751 0.14884903 0.14972271 0.16226161 0.16149893 0.20443407 0.20350783
		 0.1189383 0.12969489 0.14013854 0.13143916 0.1129397 0.12144437 0.32391173 0.32288182
		 0.019511303 0.012137791 0.026526771 0.027717611 0.036497571 0.031784985 0.0025651141
		 0.0030849548 0.011331713 0.010407504 0.067689806 0.060058199 0.15541369 0.15480639
		 0.14299867 0.14388373 0.19825988 0.19733387 0.23906969 0.23810112 0.24556056 0.2445735
		 0.17079806 0.18887937 0.20081405 0.18881164 0.33458146 0.33263075 0.33465055 0.33411407
		 0.36777681 0.36761576 0.37045357 0.3713114 0.36742166 0.36710328 0.34068578 0.34019092
		 0.30064964 0.28730229 0.28611478 0.28601193 0.28602427 0.28696558 0.2881884 0.30133811
		 0.50992006 0.49680242 0.51196897 0.51114428 0.49657208 0.50994974 0.51113969 0.51162928
		 0.7220394 0.72308922 0.71502328 0.71397662 0.72666204 0.72758228 0.87138218 0.87247193
		 0.35240188 0.35129037 0.34871584 0.35057348 0.58725715 0.58697182 0.59018391 0.59114444
		 0.46402174 0.47304454 0.70992613 0.70897293 0.71729386 0.71758235 0.70186543 0.70029396
		 0.56475526 0.5568372 0.59371245 0.60885829 0.57159209 0.56202251 0.60738808 0.61772269
		 0.71109176 0.71333444 0.91266507 0.9132002 0.91253889 0.91020727 0.55700308 0.54838485
		 0.55184633 0.54403859 0.5108192 0.50136787 0.58420324 0.58293349 0.58341211 0.58449692
		 0.3578378 0.3565993 0.52327228 0.53285551 0.71982551 0.71911353 0.86405146 0.86514401
		 0.70805413 0.70700437 0.89505827 0.89726859 0.71720338 0.71798092 0.91018355 0.91077656
		 0.2601549 0.24476695 0.2789368 0.26138058 0.22956558 0.24096352 0.24266256 0.27177536
		 0.052124411 0.041169219 0.039434176 0.0020000001 0.017440103 0.024765225 0.025978561
		 0.040715918 0.72493947 0.73659074 0.73659074 0.72493947 0.7433818 0.75503314 0.75503314
		 0.7433818 0.72493947 0.73659074 0.73659074 0.72493947 0.75503314 0.7433818 0.72493947
		 0.73659074 0.72493947 0.73659074 0.73659074 0.72493947 0.7433818 0.75503314 0.72493947
		 0.73659074 0.7433818 0.75503314 0.75503314 0.7433818 0.73659074 0.72493947 0.7433818
		 0.75503314 0.75503314 0.7433818 0.72493947 0.73659074 ;
	setAttr ".mve" -type "floatArray" 252 0.40600437 0.40793121 0.16306505 0.17410573
		 0.41137025 0.41122976 0.14213574 0.14155707 0.093485482 0.08837986 0.0047192527 0.0033552269
		 0.075087093 0.075612046 0.0031985561 0.0033404187 0.41182512 0.4118278 0.1408572
		 0.14012688 0.12297715 0.42455837 0.425713 0.11324287 0.069274403 0.059185989 0.011186748
		 0.013276435 0.074370377 0.074650928 0.0036322705 0.0037212241 0.0039869235 0.070190459
		 0.070190459 0.0039869235 0.077852771 0.14405632 0.14405632 0.077852771 0.0039869235
		 0.070190459 0.070190459 0.0039869235 0.07217738 0.07217738 0.0020000001 0.0020000001
		 0.14405632 0.077852771 0.077852771 0.14405632 0.14604324 0.14604324 0.07586585 0.07586585
		 0.3944 0.3954013 0.39395085 0.39326477 0.40434244 0.40516108 0.435002 0.43525094
		 0.49901074 0.50327605 0.56521052 0.56591541 0.55750501 0.55674374 0.56825107 0.56917995
		 0.6279397 0.62863594 0.83603632 0.82381612 0.83347064 0.84418941 0.8299399 0.82033426
		 0.94436437 0.94647813 0.56451458 0.57196879 0.43389612 0.4342961 0.46991253 0.47462857
		 0.39848548 0.39718208 0.40950254 0.41020077 0.51391429 0.52249575 0.57151884 0.57266629
		 0.56255406 0.56180769 0.63257235 0.63326824 0.68697953 0.68761462 0.68282551 0.68343389
		 0.76990426 0.74858254 0.76014066 0.77426928 0.93259144 0.93464404 0.94933498 0.95056111
		 0.92903513 0.92890948 0.92659897 0.92704165 0.92857939 0.92847228 0.94321936 0.94471693
		 0.41256091 0.4124164 0.41121283 0.14223167 0.075713195 0.0031832855 0.0020000001
		 0.0021411595 0.41302788 0.41301382 0.14000949 0.41182819 0.0024404414 0.002537265
		 0.0037368024 0.074319176 0.72897089 0.72966212 0.73922962 0.73853672 0.72524607 0.72617483
		 0.46128079 0.46190441 0.99770766 0.99800003 0.98852623 0.98860234 0.92208731 0.92159343
		 0.91922528 0.92005849 0.74762279 0.7538116 0.46903092 0.46773866 0.46587241 0.46687901
		 0.47672561 0.47524774 0.61967051 0.61382848 0.66043758 0.6718964 0.73754001 0.73045075
		 0.65201181 0.65932512 0.48429704 0.48542342 0.37621534 0.37626928 0.39939722 0.39822489
		 0.62980533 0.62351334 0.63347191 0.62730056 0.69528764 0.68824881 0.92107314 0.92073375
		 0.91423309 0.9148615 0.99491388 0.99462873 0.78883433 0.79586673 0.72307062 0.7217598
		 0.457257 0.45787606 0.73457146 0.73388451 0.38986418 0.3912085 0.47180855 0.47289282
		 0.37355369 0.37409705 0.40678388 0.40993232 0.17585771 0.40562549 0.002925422 0.0020000001
		 0.0031960539 0.093419224 0.42659804 0.42705455 0.42572138 0.11171015 0.057675242
		 0.013660694 0.012496558 0.0093040159 0.65472794 0.65472794 0.66596305 0.66596305
		 0.4098137 0.4098137 0.42104885 0.42104885 0.013235119 0.013235119 0.02647583 0.02647583
		 0.43428954 0.43428954 0.0020000001 0.0020000001 0.23567899 0.23567899 0.2469141 0.2469141
		 0.13970335 0.13970335 0.38461745 0.38461745 0.0020000001 0.0020000001 0.073499806
		 0.073499806 0.31841394 0.31841394 0.63025206 0.63025206 0.64349276 0.64349276 0.22243829
		 0.22243829 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[196:197]" "|TL_card_01_model|TL_card_01_modelShape.map[184:187]" "|TL_card_01_model|TL_card_01_modelShape.map[174:179]" "|TL_card_01_model|TL_card_01_modelShape.map[152:169]" "|TL_card_01_model|TL_card_01_modelShape.map[144:147]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold14";
	setAttr ".uvl" -type "Int32Array" 32 0 1 2 3 8 9
		 10 11 20 21 22 23 24 25 26 27 200 201
		 202 203 204 205 206 207 208 209 210 211 212 213
		 214 215 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.2365654 0.22272354 0.22471707 0.23860198
		 0.26535061 0.25166681 0.25157577 0.26534027 0.24095117 0.22615071 0.21861386 0.23019601
		 0.26546499 0.25159118 0.25253513 0.26614526 0.48116097 0.46754104 0.4682177 0.4819833
		 0.017085455 0.019482704 0.0056487261 0.003245651 0.01958449 0.0048641996 0.025713351
		 0.013780624 0.4816545 0.46808207 0.46742696 0.48114094 0.69434166 0.69434166 0.49235478
		 0.49235478 0.9071933 0.9071933 0.70520639 0.70520639 0.69638962 0.69638962 0.49030671
		 0.49030671 0.69434166 0.49235478 0.49235478 0.69434166 0.70315838 0.70315838 0.90924126
		 0.90924126 0.9071933 0.70520639 0.70520639 0.9071933 0.49653897 0.49625239 0.49030671
		 0.49044856 0.50583452 0.50498891 0.66702622 0.66802514 0.65403378 0.64890206 0.65119302
		 0.6502651 0.64167029 0.64257085 0.65549523 0.6547091 0.69896418 0.69800949 0.76943541
		 0.78052264 0.79128736 0.78232056 0.76325238 0.77201849 0.98071027 0.97964871 0.66695172
		 0.65935153 0.67418283 0.67541033 0.68446016 0.67960274 0.49088919 0.49142504 0.49992532
		 0.49897268 0.71661139 0.70874512 0.64843678 0.64781082 0.63564008 0.63655233 0.69260019
		 0.69164568 0.73466462 0.73366624 0.741355 0.74033761 0.82288945 0.84152663 0.85382819
		 0.84145683 0.99170804 0.98969734 0.99177921 0.99122626 0.86732852 0.86716253 0.87008756
		 0.8709718 0.86696249 0.86663431 0.99800003 0.99748993 0.26541463 0.25165695 0.25043294
		 0.25032693 0.25033963 0.25130987 0.2525703 0.26612425 0.48111865 0.46759772 0.48323053
		 0.48238051 0.46736029 0.48114923 0.48237577 0.48288041 0.4646599 0.46574199 0.45742807
		 0.45634925 0.46942464 0.47037318 0.61859387 0.61971712 0.27290732 0.27176163 0.26910794
		 0.27102271 0.32573405 0.32543996 0.32875079 0.32974085 0.38795868 0.39725885 0.64142287
		 0.64044034 0.6490171 0.6493144 0.63311434 0.63149458 0.49178901 0.48362753 0.52163643
		 0.5372479 0.49883604 0.48897225 0.53573251 0.54638481 0.64262432 0.64493597 0.66114593
		 0.66169751 0.66101587 0.65861255 0.4837985 0.47491533 0.47848323 0.47043547 0.43619481
		 0.42645293 0.32258627 0.32127747 0.32177082 0.32288897 0.27851033 0.27723378 0.44903073
		 0.45890859 0.46237794 0.46164408 0.61103779 0.61216396 0.45024467 0.44916263 0.64299786
		 0.64527613 0.64892381 0.64972526 0.65858811 0.65919936 0.23653542 0.22285455 0.23984841
		 0.23781121 0.2184929 0.22991559 0.23142913 0.24228731 0.019584902 0.00572574 0.0044028261
		 0.0020000001 0.003543346 0.012546976 0.014026712 0.025563592 0.91601002 0.92801946
		 0.92801946 0.91601002 0.93479103 0.94680059 0.94680059 0.93479103 0.91601002 0.92801946
		 0.92801946 0.91601002 0.94680059 0.93479103 0.91601002 0.92801946 0.91601002 0.92801946
		 0.92801946 0.91601002 0.93479103 0.94680059 0.91601002 0.92801946 0.93479103 0.94680059
		 0.94680059 0.93479103 0.92801946 0.91601002 0.93479103 0.94680059 0.94680059 0.93479103
		 0.91601002 0.92801946 ;
	setAttr ".mve" -type "floatArray" 252 0.42927533 0.42926934 0.14972003 0.14971444
		 0.42395547 0.42381066 0.14644392 0.14584745 0.079128578 0.080407068 0.0046924413
		 0.0032278141 0.077334002 0.077875093 0.0032354032 0.0033816267 0.42442432 0.42442709
		 0.14512607 0.1443733 0.14668243 0.43155596 0.4317084 0.14674184 0.077994354 0.075664595
		 0.0068670074 0.0072381846 0.076595247 0.076884426 0.0036824478 0.0037741361 0.0040480071
		 0.072286822 0.072286822 0.0040480071 0.0040480043 0.072286829 0.072286829 0.0040480043
		 0.0040480071 0.072286822 0.072286822 0.0040480071 0.07433483 0.07433483 0.0020000001
		 0.0020000001 0.072286829 0.0040480043 0.0040480043 0.072286829 0.074334837 0.074334837
		 0.0020000001 0.0020000001 0.079678521 0.080710597 0.079215556 0.078508392 0.089926623
		 0.090770431 0.3089515 0.3092081 0.37492806 0.37932447 0.25574023 0.25646681 0.24779783
		 0.24701317 0.25887427 0.2598317 0.32039788 0.32111552 0.72231472 0.70971882 0.71967018
		 0.73071843 0.7160309 0.70612991 0.83397305 0.83615184 0.44244564 0.45012903 0.30781162
		 0.3082239 0.34493527 0.3497963 0.083889596 0.082546122 0.095245346 0.09596505 0.39028975
		 0.39913505 0.2622425 0.2634252 0.25300211 0.25223279 0.32517296 0.32589024 0.38125277
		 0.38190737 0.37697104 0.37759814 0.65414959 0.63217235 0.64408582 0.65864879 0.8218382
		 0.82395393 0.83909649 0.84036034 0.63074982 0.6306203 0.6282388 0.62869507 0.63028008
		 0.63016969 0.83279288 0.83433646 0.42518276 0.42503381 0.42379323 0.1465428 0.077979356
		 0.0032196629 0.0020000001 0.0021454988 0.42566407 0.42564958 0.1442523 0.42442751
		 0.0024539786 0.0025537785 0.0037901932 0.076542474 0.65946156 0.66017407 0.67003566
		 0.66932148 0.65562224 0.65657955 0.38354191 0.38418472 0.82871807 0.82901943 0.81925434
		 0.81933284 0.8585149 0.85800588 0.85556489 0.85642374 0.57094491 0.57732397 0.28378832
		 0.28245634 0.28053272 0.28157026 0.29171959 0.29019627 0.43905899 0.43303737 0.48107937
		 0.49289045 0.56055212 0.55324495 0.47239456 0.4799327 0.29952377 0.30068478 0.2958613
		 0.29591691 0.31975588 0.31854749 0.44950539 0.44301996 0.45328468 0.44692361 0.51700079
		 0.5097456 0.85746956 0.85711974 0.85041922 0.85106701 0.82583839 0.82554448 0.61342341
		 0.62067199 0.65337992 0.6520288 0.37939441 0.38003251 0.66523433 0.66452622 0.30992976
		 0.31131542 0.28665134 0.28776896 0.29311785 0.2936779 0.43052348 0.43050742 0.14972073
		 0.42926472 0.0034520137 0.0020000001 0.0030515161 0.078945324 0.43279782 0.43295902
		 0.43169883 0.14674753 0.075423963 0.0070767254 0.0060043572 0.0056320699 0.67479461
		 0.67479461 0.68637514 0.68637514 0.42235106 0.42235106 0.43393159 0.43393159 0.013580519
		 0.013580519 0.027228286 0.027228286 0.44757935 0.44757935 0.0020000001 0.0020000001
		 0.24286294 0.24286294 0.25444347 0.25444347 0.14393674 0.14393674 0.39638019 0.39638019
		 0.0020000001 0.0020000001 0.075697914 0.075697914 0.32814139 0.32814139 0.64956629
		 0.64956629 0.66321409 0.66321409 0.22921519 0.22921519 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[200:215]" "|TL_card_01_model|TL_card_01_modelShape.map[20:27]" "|TL_card_01_model|TL_card_01_modelShape.map[8:11]" "|TL_card_01_model|TL_card_01_modelShape.map[0:3]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold15";
	setAttr ".uvl" -type "Int32Array" 34 144 145 146 147 152 153
		 154 155 156 157 158 159 160 161 162 163 164 165
		 166 167 168 169 174 175 176 177 178 179 184 185
		 186 187 196 197 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 67 66 70 71 71 70
		 72 73 61 60 69 68 74 75 76 77 75 74
		 78 79 77 76 80 81 65 64 82 83 84 85
		 86 87 88 89 90 91 87 86 92 93 94 95
		 96 97 95 94 98 99 99 98 100 101 91 90
		 97 96 73 72 102 103 79 78 83 82 104 105
		 106 107 105 104 93 92 107 106 108 109 110 111
		 81 80 112 113 114 115 115 114 103 102 101 100
		 116 117 118 119 109 108 79 104 75 113 116 114
		 75 104 107 76 57 89 58 90 89 57 61 71
		 94 67 82 93 104 79 114 116 100 103 63 84
		 87 64 73 98 94 71 64 87 93 82 103 100
		 98 73 80 109 119 110 94 97 68 67 76 107
		 109 80 97 90 61 68 120 121 5 4 122 123
		 6 5 124 125 14 13 126 127 15 14 123 124
		 13 6 128 129 17 16 130 131 16 19 132 133
		 31 30 134 135 28 31 135 130 19 28 129 120
		 4 17 127 132 30 15 136 137 138 139 137 136
		 140 141 141 140 142 143 144 145 146 147 148 149
		 150 151 147 146 152 153 139 138 151 150 154 155
		 156 157 155 154 158 159 159 158 160 161 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 161 160 175 174 176 177 177 176
		 178 179 180 181 182 183 184 185 186 187 187 186
		 165 164 188 189 190 191 189 188 192 193 191 190
		 194 195 183 182 193 192 143 142 173 172 153 152
		 179 178 196 197 169 168 198 199 195 194 136 188
		 140 144 147 185 136 139 192 188 196 168 158 154
		 157 196 154 162 176 174 140 188 191 142 185 147
		 153 186 173 195 199 170 165 178 176 162 142 191
		 195 173 186 153 178 165 183 150 149 180 160 166
		 162 174 192 139 150 183 158 168 166 160 200 201
		 1 0 202 203 0 3 204 205 11 10 206 207
		 8 11 207 202 3 8 208 209 22 21 210 211
		 23 22 211 212 25 23 212 213 27 25 214 215
		 26 27 201 208 21 1 215 204 10 26 216 217
		 218 219 220 221 222 223 224 225 226 227 223 222
		 228 229 225 224 230 231 232 233 234 235 236 237
		 221 220 238 239 217 216 240 241 242 243 235 234
		 244 245 243 242 237 236 245 244 239 238 246 247
		 248 249 250 251 233 232 229 228 247 246 227 226
		 251 250 ;
	setAttr ".mue" -type "floatArray" 252 0.24365571 0.22939545 0.23144923 0.24575385
		 0.27248031 0.25838289 0.2582891 0.27246967 0.24817404 0.2329262 0.22516154 0.23709379
		 0.27259815 0.25830498 0.25927746 0.27329898 0.49481404 0.48078242 0.48147956 0.49566123
		 0.01754145 0.020011161 0.0057590175 0.0032833037 0.020116024 0.0049507767 0.026430145
		 0.014136721 0.4953225 0.48133981 0.48066491 0.49479342 0.71430039 0.71430039 0.50620794
		 0.50620794 0.93372113 0.93372113 0.72562867 0.72562867 0.71641022 0.71641022 0.50409794
		 0.50409794 0.71430039 0.50620794 0.50620794 0.71430039 0.72351873 0.72351873 0.93583101
		 0.93583101 0.93372113 0.72562867 0.72562867 0.93372113 0.51051861 0.51022339 0.50409794
		 0.50424409 0.52009517 0.51922399 0.050323464 0.051352572 0.036938295 0.031651456
		 0.66984743 0.66889149 0.66003686 0.66096467 0.67427969 0.67346984 0.71906263 0.71807909
		 0.15582821 0.16725059 0.17834069 0.16910285 0.14945829 0.15848938 0.37348935 0.37239569
		 0.050246704 0.042416785 0.057696398 0.058961 0.068284385 0.063280143 0.50469804 0.5052501
		 0.51400733 0.51302588 0.10140745 0.09330342 0.66700792 0.666363 0.65382439 0.65476424
		 0.71250623 0.71152288 0.75584221 0.75481361 0.76273477 0.76168668 0.21089803 0.23009856
		 0.24277197 0.23002665 0.38481957 0.38274807 0.38489288 0.38432321 0.8925162 0.89234513
		 0.89535862 0.89626956 0.89213908 0.891801 0.39130172 0.39077622 0.27254626 0.25837272
		 0.25711173 0.2570025 0.25701559 0.25801516 0.2593137 0.27327734 0.49477044 0.48084083
		 0.49694616 0.49607044 0.48059621 0.49480194 0.49606559 0.49658546 0.50158048 0.50269526
		 0.49413002 0.4930186 0.50648922 0.50746644 0.66016746 0.66132468 0.31512836 0.3141523
		 0.30839837 0.31031871 0.35845521 0.35815224 0.36156315 0.36258313 0.39155 0.40137383
		 0.68701559 0.68626785 0.69673586 0.69667399 0.67565423 0.67414081 0.4965466 0.48810941
		 0.52491087 0.54140872 0.50480336 0.49445161 0.54178029 0.5528779 0.68460572 0.68688345
		 0.70400578 0.70457399 0.70387179 0.70139581 0.48600736 0.47674838 0.47943959 0.47098517
		 0.43870002 0.42848092 0.3552123 0.35386392 0.35437217 0.35552415 0.31877679 0.31727099
		 0.45597231 0.4663519 0.49922952 0.49847347 0.65238297 0.65354317 0.48672947 0.48561475
		 0.68530911 0.68765628 0.69421136 0.69459015 0.7013706 0.70200032 0.24362482 0.22953042
		 0.24703795 0.24493918 0.22503692 0.23680489 0.23836419 0.24955058 0.020116448 0.0058383592
		 0.0044754571 0.0020000001 0.0035899973 0.012865784 0.014390248 0.026275858 0.94293952
		 0.95531195 0.95531195 0.94293952 0.9627071 0.97507972 0.97507972 0.9627071 0.94293952
		 0.95531195 0.95531195 0.94293952 0.97507972 0.9627071 0.94293952 0.95531195 0.94293952
		 0.95531195 0.95531195 0.94293952 0.9627071 0.97507972 0.94293952 0.95531195 0.9627071
		 0.97507972 0.97507972 0.9627071 0.95531195 0.94293952 0.9627071 0.97507972 0.97507972
		 0.9627071 0.94293952 0.95531195 ;
	setAttr ".mve" -type "floatArray" 252 0.44219077 0.4421846 0.15418522 0.15417947
		 0.43671006 0.43656087 0.15081008 0.15019558 0.081459977 0.082777113 0.0047738268
		 0.0032649278 0.079611152 0.080168597 0.003272746 0.0034233895 0.4371931 0.43719596
		 0.1494524 0.14867687 0.1510558 0.44454032 0.44469738 0.15111701 0.080291465 0.077891283
		 0.0070141242 0.0073965215 0.078850061 0.079147987 0.0037333036 0.0038277635 0.004109913
		 0.074411415 0.074411415 0.004109913 0.0041099102 0.074411429 0.074411429 0.0041099102
		 0.004109913 0.074411415 0.074411415 0.004109913 0.07652133 0.07652133 0.0020000001
		 0.0020000001 0.074411429 0.0041099102 0.0041099102 0.074411429 0.076521344 0.076521344
		 0.0020000001 0.0020000001 0.081680357 0.08274363 0.081203394 0.080474854 0.092238232
		 0.093107544 0.45052806 0.4507924 0.5184989 0.52302819 0.26306394 0.26381248 0.25488147
		 0.25407308 0.26629272 0.26727909 0.32967603 0.33041537 0.87638617 0.86340952 0.8736617
		 0.88504392 0.86991239 0.85971212 0.99141961 0.99366426 0.5880574 0.59597301 0.44935372
		 0.44977847 0.48759952 0.4926075 0.086018719 0.084634639 0.097717725 0.098459184 0.53432494
		 0.5434376 0.26976275 0.27098122 0.26024306 0.2594505 0.33459544 0.33533442 0.3923704
		 0.3930448 0.38795924 0.3886053 0.80616057 0.78351903 0.79579264 0.81079578 0.97891796
		 0.98109764 0.99669796 0.99800003 0.64940912 0.64927566 0.64682221 0.64729226 0.64892519
		 0.64881146 0.9902038 0.99179405 0.43797445 0.437821 0.43654293 0.15091196 0.080276012
		 0.00325653 0.0020000001 0.0021498967 0.4384703 0.4384554 0.14855221 0.43719637 0.0024677012
		 0.0025705176 0.0038443059 0.078795694 0.71387893 0.71461296 0.72477263 0.72403687
		 0.70992357 0.71090978 0.42961892 0.43028116 0.87867051 0.87929446 0.87036145 0.87037468
		 0.91894913 0.91842473 0.91590995 0.91679478 0.58902043 0.59747291 0.32803082 0.32642689
		 0.32785124 0.32896158 0.33399421 0.332057 0.45554301 0.44743609 0.50655949 0.52226293
		 0.59281069 0.58320838 0.50026482 0.5104993 0.34417367 0.3459219 0.33928794 0.33934525
		 0.3639048 0.36265987 0.46495613 0.45622659 0.46965489 0.46140093 0.53518873 0.52566534
		 0.91787219 0.91751182 0.91060877 0.9112761 0.87526613 0.87544978 0.6451624 0.65469611
		 0.70761347 0.70622152 0.42534605 0.42600343 0.71982616 0.71909666 0.35378167 0.3552092
		 0.33341998 0.33494157 0.33646157 0.33703855 0.44347665 0.44346011 0.15418595 0.44217983
		 0.0034959042 0.0020000001 0.0030833008 0.081271186 0.44581974 0.44598579 0.44468752
		 0.15112287 0.07764338 0.0072301817 0.0061253984 0.0057418579 0.69513154 0.69513154
		 0.70706207 0.70706207 0.43505716 0.43505716 0.44698775 0.44698775 0.013930568 0.013930568
		 0.027990874 0.027990874 0.46104804 0.46104804 0.0020000001 0.0020000001 0.25014362
		 0.25014362 0.2620742 0.2620742 0.14822711 0.14822711 0.40830132 0.40830132 0.0020000001
		 0.0020000001 0.077925608 0.077925608 0.33799982 0.33799982 0.66914052 0.66914052
		 0.68320084 0.68320084 0.23608334 0.23608334 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[196:197]" "|TL_card_01_model|TL_card_01_modelShape.map[184:187]" "|TL_card_01_model|TL_card_01_modelShape.map[174:179]" "|TL_card_01_model|TL_card_01_modelShape.map[152:169]" "|TL_card_01_model|TL_card_01_modelShape.map[144:147]"  ;
createNode polyMapCut -n "polyMapCut17";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "e[74]" "e[109]" "e[157]" "e[208]" "e[221]" "e[269]" "e[296]";
createNode polyMapCut -n "polyMapCut18";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[126]" "e[148]" "e[181]" "e[239]" "e[261]" "e[290]";
createNode Unfold3DUnfold -n "Unfold3DUnfold16";
	setAttr ".uvl" -type "Int32Array" 18 154 155 156 157 158 159
		 160 161 163 166 167 168 169 174 196 197 254 257 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 81 80 85 84 108 109
		 110 111 112 113 95 94 111 110 114 115 116 117
		 83 82 118 119 120 121 121 120 107 106 105 104
		 122 123 124 125 115 114 81 108 77 119 122 120
		 77 108 111 78 57 91 58 92 91 57 61 73
		 101 70 84 95 113 81 120 122 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 115 125 116 96 99 68 67 78 111
		 115 82 99 92 61 68 126 127 5 4 128 129
		 6 5 130 131 14 13 132 133 15 14 129 130
		 13 6 134 135 17 16 136 137 16 19 138 139
		 31 30 140 141 28 31 141 136 19 28 135 126
		 4 17 133 138 30 15 142 143 144 145 143 142
		 146 147 148 149 150 151 152 153 154 155 156 157
		 158 159 155 154 160 161 145 144 159 158 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 176 177 177 176 178 179 180 181
		 182 183 184 185 169 168 186 187 188 189 189 188
		 190 191 192 193 194 195 196 197 198 199 199 198
		 173 172 200 201 202 203 204 205 206 207 203 202
		 208 209 195 194 207 206 151 150 183 182 161 160
		 191 190 210 211 179 178 212 213 209 208 142 205
		 146 152 155 197 142 145 206 205 210 178 166 162
		 165 210 162 170 188 187 149 200 203 150 197 155
		 161 198 183 209 213 180 173 190 188 170 150 203
		 209 183 198 161 190 173 195 158 157 192 168 176
		 175 184 206 145 158 195 166 178 176 168 214 215
		 1 0 216 217 0 3 218 219 11 10 220 221
		 8 11 221 216 3 8 222 223 22 21 224 225
		 23 22 225 226 25 23 226 227 27 25 228 229
		 26 27 215 222 21 1 229 218 10 26 230 231
		 232 233 234 235 236 237 238 239 240 241 237 236
		 242 243 239 238 244 245 246 247 248 249 250 251
		 252 253 254 255 231 230 256 257 258 259 249 248
		 260 261 259 258 251 250 261 260 255 254 262 263
		 264 265 266 267 268 269 243 242 263 262 241 240
		 267 266 ;
	setAttr ".mue" -type "floatArray" 270 0.28265771 0.26609591 0.26848117 0.28509447
		 0.3137179 0.29734522 0.29723629 0.31370553 0.28790528 0.2701965 0.26117867 0.27503672
		 0.31385475 0.29725474 0.29838416 0.31466869 0.57193512 0.55563891 0.55644852 0.57291907
		 0.02004976 0.02291807 0.0063657034 0.0034904222 0.023039857 0.0054270169 0.030373044
		 0.016095527 0.57252568 0.55628622 0.55550241 0.57191116 0.82567316 0.82567316 0.58399564
		 0.58399564 0.82567304 0.82567304 0.58399558 0.58399558 0.82812351 0.82812351 0.58154511
		 0.58154511 0.82567316 0.58399564 0.58399564 0.82567316 0.58154511 0.58154511 0.82812351
		 0.82812351 0.82567304 0.58399558 0.58399558 0.82567304 0.67860675 0.67931068 0.6733678
		 0.67284334 0.69499344 0.69509578 0.59535515 0.59640181 0.58102697 0.57559091 0.95622569
		 0.95615929 0.94173133 0.94172454 0.58235276 0.58375913 0.58635259 0.58495462 0.59452397
		 0.593095 0.69867575 0.71045256 0.72165823 0.71212488 0.6922453 0.70155603 0.91946685
		 0.91832924 0.59390146 0.58583844 0.60288483 0.60417002 0.61329854 0.60814577 0.67801106
		 0.67722076 0.69519049 0.6950987 0.64660567 0.63825113 0.95992476 0.96050113 0.94173217
		 0.9417482 0.57559091 0.57695794 0.58500373 0.58357519 0.5949983 0.59356183 0.60456085
		 0.60312122 0.75553083 0.77533543 0.78813529 0.77498931 0.77533543 0.75553083 0.93114489
		 0.92901087 0.93104184 0.93044794 0.58265352 0.58255863 0.5870471 0.58769363 0.58255553
		 0.58227998 0.93764168 0.93708992 0.31379449 0.29733342 0.2958689 0.29574203 0.29575723
		 0.29691812 0.29842627 0.31464356 0.57188451 0.55570674 0.57441139 0.5733943 0.5554226
		 0.57192111 0.5733887 0.57399243 0.90889502 0.90894485 0.89432561 0.89427948 0.91559088
		 0.9153257 0.62812358 0.62653655 0.62813073 0.62972212 0.32588744 0.32521969 0.3155112
		 0.31739283 0.63279778 0.63307703 0.63751918 0.63737261 0.26795563 0.28141928 0.94965065
		 0.95030779 0.95932287 0.95840377 0.93578273 0.93603343 0.6870302 0.6861769 0.36046687
		 0.38376778 0.38030502 0.36579949 0.67484212 0.67482805 0.68652362 0.68634462 0.93558568
		 0.93612564 0.62267846 0.62330729 0.63655877 0.63325179 0.6774947 0.6768778 0.29038918
		 0.30342793 0.29916382 0.28712854 0.2893683 0.27502844 0.63169926 0.63116372 0.63770813
		 0.63784176 0.32789266 0.32650602 0.3565731 0.37107441 0.6176036 0.61601752 0.61752927
		 0.61912102 0.9138127 0.91304076 0.89406121 0.89400738 0.61131626 0.61459208 0.95261079
		 0.95199037 0.61830372 0.61929309 0.28262183 0.26625267 0.28658581 0.28414831 0.26103392
		 0.27470118 0.27651215 0.28950396 0.023040351 0.0064578508 0.0048749829 0.0020000001
		 0.0038466146 0.014619465 0.01638997 0.030193858 0.94475317 0.95912248 0.95912248
		 0.94475317 0.96658534 0.98095483 0.98095483 0.96658534 0.40537903 0.39100972 0.39100972
		 0.40537903 0.98095483 0.96658534 0.40537903 0.39100972 0.94475317 0.95912248 0.95912248
		 0.94475317 0.96658534 0.98095483 0.98095483 0.96658534 0.94475317 0.95912248 0.96658534
		 0.98095483 0.98095483 0.96658534 0.95912248 0.94475317 0.96658534 0.98095483 0.98095483
		 0.96658534 0.40537903 0.39100972 0.39100972 0.40537903 ;
	setAttr ".mve" -type "floatArray" 270 0.51323527 0.51322812 0.17874713 0.17874046
		 0.50687003 0.50669676 0.17482726 0.17411359 0.094284408 0.095814124 0.005221508 0.0034690804
		 0.092137195 0.092784606 0.0034781606 0.0036531172 0.50743103 0.50743437 0.17325047
		 0.17234977 0.17511263 0.51596403 0.51614648 0.17518373 0.092927307 0.090139747 0.0078233778
		 0.0082674921 0.091253266 0.091599278 0.0040130499 0.004122755 0.0044504418 0.086098239
		 0.086098239 0.0044504418 0.09496861 0.17661643 0.17661643 0.09496861 0.0044504418
		 0.086098239 0.086098239 0.0044504418 0.08854869 0.08854869 0.0020000001 0.0020000001
		 0.17661643 0.09496861 0.09496861 0.17661643 0.17906687 0.17906687 0.092518173 0.092518173
		 0.78524137 0.7862972 0.79047316 0.78979856 0.7848354 0.78624696 0.18416005 0.18443988
		 0.25333369 0.25789919 0.78279042 0.78418505 0.78518468 0.78376961 0.33655292 0.33635566
		 0.34222212 0.34274575 0.43263105 0.43280283 0.61944795 0.60633034 0.61689502 0.62840867
		 0.61278313 0.6024726 0.73892033 0.74119818 0.32439318 0.33238617 0.18303636 0.1834821
		 0.22214016 0.22719657 0.79356831 0.7920478 0.79425228 0.79566383 0.2701157 0.27932653
		 0.79029524 0.79177213 0.79462153 0.79320782 0.34232673 0.34147149 0.43376684 0.43393835
		 0.51765084 0.51773047 0.51722658 0.51726604 0.54839206 0.52549726 0.53813899 0.55330974
		 0.52549726 0.54839206 0.72628611 0.72848797 0.74441653 0.74573851 0.85794467 0.85770929
		 0.8569926 0.85800236 0.85723376 0.85692048 0.73785871 0.73947501 0.50833851 0.50816029
		 0.5066759 0.17494558 0.092909358 0.0034593274 0.0020000001 0.0021740892 0.50891435
		 0.50889707 0.17220499 0.50743484 0.0025431856 0.0026625961 0.0041419673 0.091190122
		 0.75102186 0.75249469 0.75125855 0.74978769 0.75292146 0.75443125 0.9579016 0.95742339
		 0.57578677 0.57582253 0.99694383 0.99800003 0.99190873 0.99104267 0.75288922 0.75228167
		 0.75375068 0.7552343 0.67906433 0.68282348 0.76433909 0.76234478 0.76985019 0.77095705
		 0.76167125 0.7588293 0.76092535 0.75847071 0.53748542 0.54527193 0.63093024 0.62628967
		 0.77259511 0.77115917 0.77389872 0.77675736 0.77653491 0.77982134 0.4560391 0.45577559
		 0.48194444 0.48204842 0.76494765 0.76372987 0.51036924 0.51465756 0.52225292 0.51806086
		 0.60490632 0.60028213 0.74928004 0.7478351 0.74388897 0.74535275 0.9919489 0.99281746
		 0.7044121 0.70897299 0.9591344 0.95810455 0.57573724 0.57576621 0.7433368 0.74490613
		 0.74143428 0.73996443 0.48189545 0.48205641 0.77439892 0.77593422 0.45457447 0.4548212
		 0.51472872 0.51470947 0.17874798 0.51322258 0.0037373353 0.0020000001 0.0032581398
		 0.094065152 0.51744998 0.5176428 0.51613498 0.17519054 0.089851834 0.008074306 0.0067912163
		 0.0063457745 0.51880676 0.51880676 0.53266281 0.53266281 0.50891989 0.50891989 0.52277601
		 0.52277601 0.78686881 0.78686881 0.77053922 0.77053922 0.53910553 0.53910553 0.80072492
		 0.80072492 0.0020000001 0.0020000001 0.015856121 0.015856121 0.17182742 0.17182742
		 0.5049504 0.5049504 0.18568356 0.18568356 0.0020000001 0.0020000001 0.090179615 0.090179615
		 0.10403575 0.10403575 0.78078294 0.78078294 0.79711252 0.79711252 0.52886164 0.52886164
		 0.51253211 0.51253211 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[210:211]" "|TL_card_01_model|TL_card_01_modelShape.map[184:185]" "|TL_card_01_model|TL_card_01_modelShape.map[174:179]" "|TL_card_01_model|TL_card_01_modelShape.map[162:169]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold17";
	setAttr ".uvl" -type "Int32Array" 16 148 149 150 151 180 181
		 182 183 200 201 202 203 208 209 212 213 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 81 80 85 84 108 109
		 110 111 112 113 95 94 111 110 114 115 116 117
		 83 82 118 119 120 121 121 120 107 106 105 104
		 122 123 124 125 115 114 81 108 77 119 122 120
		 77 108 111 78 57 91 58 92 91 57 61 73
		 101 70 84 95 113 81 120 122 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 115 125 116 96 99 68 67 78 111
		 115 82 99 92 61 68 126 127 5 4 128 129
		 6 5 130 131 14 13 132 133 15 14 129 130
		 13 6 134 135 17 16 136 137 16 19 138 139
		 31 30 140 141 28 31 141 136 19 28 135 126
		 4 17 133 138 30 15 142 143 144 145 143 142
		 146 147 148 149 150 151 152 153 154 155 156 157
		 158 159 155 154 160 161 145 144 159 158 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 176 177 177 176 178 179 180 181
		 182 183 184 185 169 168 186 187 188 189 189 188
		 190 191 192 193 194 195 196 197 198 199 199 198
		 173 172 200 201 202 203 204 205 206 207 203 202
		 208 209 195 194 207 206 151 150 183 182 161 160
		 191 190 210 211 179 178 212 213 209 208 142 205
		 146 152 155 197 142 145 206 205 210 178 166 162
		 165 210 162 170 188 187 149 200 203 150 197 155
		 161 198 183 209 213 180 173 190 188 170 150 203
		 209 183 198 161 190 173 195 158 157 192 168 176
		 175 184 206 145 158 195 166 178 176 168 214 215
		 1 0 216 217 0 3 218 219 11 10 220 221
		 8 11 221 216 3 8 222 223 22 21 224 225
		 23 22 225 226 25 23 226 227 27 25 228 229
		 26 27 215 222 21 1 229 218 10 26 230 231
		 232 233 234 235 236 237 238 239 240 241 237 236
		 242 243 239 238 244 245 246 247 248 249 250 251
		 252 253 254 255 231 230 256 257 258 259 249 248
		 260 261 259 258 251 250 261 260 255 254 262 263
		 264 265 266 267 268 269 243 242 263 262 241 240
		 267 266 ;
	setAttr ".mue" -type "floatArray" 270 0.282839 0.26626649 0.2686533 0.28527734
		 0.31540459 0.29902133 0.29891235 0.31539223 0.28808996 0.27036974 0.26134607 0.27521309
		 0.31554154 0.29893079 0.30006096 0.316356 0.57378858 0.55748183 0.55829197 0.57477319
		 0.020061418 0.022931581 0.0063685235 0.0034913847 0.023053447 0.0054292306 0.030391371
		 0.016104631 0.5743795 0.55812955 0.55734527 0.57376462 0.82720661 0.82720661 0.58537298
		 0.58537298 0.82720649 0.82720649 0.58537292 0.58537292 0.82965857 0.82965857 0.58292091
		 0.58292091 0.82720661 0.58537298 0.58537298 0.82720661 0.58292091 0.58292091 0.82965857
		 0.82965857 0.82720649 0.58537292 0.58537292 0.82720649 0.69971728 0.70042169 0.69447494
		 0.69395018 0.71611458 0.71621698 0.60269791 0.60374522 0.58836049 0.58292091 0.97751552
		 0.97744912 0.9630118 0.96300501 0.61149645 0.61290371 0.61549884 0.61409998 0.62367553
		 0.62224561 0.70608526 0.71786964 0.72908258 0.71954304 0.69965065 0.70896739 0.92701894
		 0.92588061 0.60124326 0.59317505 0.61023247 0.6115185 0.62065291 0.61549681 0.69912124
		 0.6983304 0.71631175 0.7162199 0.65398151 0.6456216 0.98121697 0.98179376 0.96301264
		 0.96302873 0.60473025 0.60609812 0.61414915 0.61271966 0.62415016 0.62271273 0.63371885
		 0.63227832 0.76297706 0.78279442 0.79560256 0.78244811 0.78279442 0.76297706 0.93870455
		 0.93656915 0.93860143 0.93800712 0.61179739 0.61170244 0.61619383 0.61684078 0.61169934
		 0.61142361 0.94520551 0.94465339 0.31548125 0.29900953 0.29754406 0.2974171 0.29743233
		 0.29859397 0.3001031 0.31633085 0.57373792 0.55754972 0.57626647 0.57524872 0.5572654
		 0.57377458 0.57524312 0.57584721 0.94013816 0.94018799 0.92555934 0.92551321 0.94683832
		 0.94657302 0.46432936 0.46278423 0.51220465 0.53493106 0.32564637 0.32497817 0.31526342
		 0.31714627 0.66386265 0.66414207 0.66858709 0.6684404 0.26767713 0.28114948 0.97291559
		 0.97357315 0.98259401 0.98167431 0.95903867 0.95928955 0.71012551 0.70927167 0.36024812
		 0.38356408 0.38009909 0.36558419 0.69792956 0.69791549 0.70961863 0.70943952 0.9588415
		 0.95938182 0.46707356 0.46875933 0.50209308 0.48891303 0.70058388 0.69996655 0.29012516
		 0.30317235 0.29890549 0.28686243 0.28910363 0.27475452 0.66276342 0.66222751 0.66877615
		 0.66890985 0.32765287 0.32626534 0.35635185 0.37086251 0.45407006 0.45229089 0.37474105
		 0.39636886 0.945059 0.94428658 0.92529476 0.92524093 0.40022963 0.4152005 0.97587764
		 0.9752568 0.45644403 0.45820722 0.28280309 0.26642334 0.28676963 0.28433055 0.26120123
		 0.27487734 0.27668947 0.28968966 0.02305394 0.00646073 0.0048768399 0.0020000001
		 0.0038478074 0.014627616 0.016399264 0.030212069 0.95169663 0.96607524 0.96607524
		 0.95169663 0.54128492 0.5556637 0.5556637 0.54128492 0.65479678 0.64041817 0.64041817
		 0.65479678 0.5556637 0.54128492 0.65479678 0.64041817 0.95169663 0.96607524 0.96607524
		 0.95169663 0.58292091 0.5972997 0.5972997 0.58292091 0.95169663 0.96607524 0.58292091
		 0.5972997 0.5972997 0.58292091 0.96607524 0.95169663 0.54128492 0.5556637 0.5556637
		 0.54128492 0.65479678 0.64041817 0.64041817 0.65479678 ;
	setAttr ".mve" -type "floatArray" 270 0.51356548 0.51355833 0.17886131 0.17885461
		 0.50719613 0.50702274 0.17493889 0.17422475 0.09434402 0.095874719 0.0052235886 0.0034700292
		 0.092195414 0.092843242 0.0034791152 0.0036541848 0.50775748 0.50776082 0.17336106
		 0.17245978 0.17522445 0.51629603 0.5164786 0.17529559 0.09298604 0.090196677 0.0078271395
		 0.0082715405 0.091310911 0.091657147 0.00401435 0.0041241259 0.0044520241 0.086152554
		 0.086152554 0.0044520241 0.095021933 0.17672248 0.17672248 0.095021933 0.0044520241
		 0.086152554 0.086152554 0.0044520241 0.088604584 0.088604584 0.0020000001 0.0020000001
		 0.17672248 0.095021933 0.095021933 0.17672248 0.17917451 0.17917451 0.092569917 0.092569917
		 0.79345268 0.79450917 0.79868782 0.79801279 0.79304641 0.79445893 0.18426426 0.18454427
		 0.25348258 0.25805104 0.79100013 0.79239565 0.79339594 0.79197997 0.39332131 0.39312392
		 0.39899418 0.39951813 0.48946148 0.48963338 0.61983329 0.60670722 0.6172787 0.6287998
		 0.61316419 0.60284698 0.7393828 0.74166214 0.32458797 0.33258611 0.18313983 0.18358587
		 0.22226889 0.22732857 0.80178499 0.80026346 0.80246937 0.80388188 0.27027541 0.2794922
		 0.79850978 0.79998761 0.80283886 0.80142426 0.39909884 0.39824307 0.49059802 0.49076965
		 0.5745362 0.5746159 0.57411164 0.57415116 0.54873151 0.52582192 0.53847182 0.55365235
		 0.52582192 0.54873151 0.72674042 0.72894371 0.74488258 0.74620539 0.91504985 0.91481429
		 0.91409713 0.91510755 0.91433847 0.91402501 0.73832053 0.73993784 0.5086655 0.50848722
		 0.50700182 0.17505728 0.092968069 0.0034602699 0.0020000001 0.0021742017 0.50924176
		 0.50922447 0.17231491 0.5077613 0.0025435362 0.002663024 0.0041433508 0.091247723
		 0.75909376 0.76056755 0.75933057 0.75785875 0.76099455 0.76250535 0.98943454 0.98911184
		 0.67320967 0.67234409 0.99694312 0.99800003 0.9919048 0.99103814 0.76096231 0.76035434
		 0.76182431 0.76330888 0.67885834 0.68261987 0.77253693 0.77054137 0.77805161 0.77915919
		 0.76986736 0.76702362 0.76912099 0.7666648 0.53718799 0.54497951 0.63069314 0.62604958
		 0.78079832 0.77936143 0.78210276 0.78496325 0.78474063 0.78802919 0.5165379 0.5157901
		 0.58415931 0.58552712 0.77314591 0.77192736 0.51005429 0.51434535 0.52194566 0.51775086
		 0.6046524 0.60002524 0.7573508 0.75590491 0.75195622 0.75342095 0.99194497 0.99281406
		 0.70422244 0.70878631 0.99031675 0.99023384 0.68385834 0.68311292 0.75140369 0.75297403
		 0.74949998 0.74802917 0.59153563 0.59137446 0.78260326 0.78413957 0.51295996 0.51297128
		 0.51505989 0.51504064 0.17886215 0.51355278 0.0037384573 0.0020000001 0.0032589524
		 0.094124623 0.51778293 0.51797587 0.51646709 0.1753024 0.089908578 0.0080782296 0.0067943111
		 0.0063485815 0.51914048 0.51914048 0.53300554 0.53300554 0.51319361 0.51319361 0.52705866
		 0.52705866 0.78780359 0.78780359 0.77146345 0.77146345 0.54339874 0.54339874 0.80166864
		 0.80166864 0.0020000001 0.0020000001 0.015865069 0.015865069 0.53171343 0.53171343
		 0.86505157 0.86505157 0.18580219 0.18580219 0.36177632 0.36177632 0.45001289 0.45001289
		 0.10410164 0.10410164 0.78523225 0.78523225 0.80157238 0.80157238 0.52962977 0.52962977
		 0.51328969 0.51328969 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[212:213]" "|TL_card_01_model|TL_card_01_modelShape.map[208:209]" "|TL_card_01_model|TL_card_01_modelShape.map[200:203]" "|TL_card_01_model|TL_card_01_modelShape.map[180:183]" "|TL_card_01_model|TL_card_01_modelShape.map[148:151]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold18";
	setAttr ".uvl" -type "Int32Array" 16 148 149 150 151 180 181
		 182 183 200 201 202 203 208 209 212 213 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 81 80 85 84 108 109
		 110 111 112 113 95 94 111 110 114 115 116 117
		 83 82 118 119 120 121 121 120 107 106 105 104
		 122 123 124 125 115 114 81 108 77 119 122 120
		 77 108 111 78 57 91 58 92 91 57 61 73
		 101 70 84 95 113 81 120 122 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 115 125 116 96 99 68 67 78 111
		 115 82 99 92 61 68 126 127 5 4 128 129
		 6 5 130 131 14 13 132 133 15 14 129 130
		 13 6 134 135 17 16 136 137 16 19 138 139
		 31 30 140 141 28 31 141 136 19 28 135 126
		 4 17 133 138 30 15 142 143 144 145 143 142
		 146 147 148 149 150 151 152 153 154 155 156 157
		 158 159 155 154 160 161 145 144 159 158 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 176 177 177 176 178 179 180 181
		 182 183 184 185 169 168 186 187 188 189 189 188
		 190 191 192 193 194 195 196 197 198 199 199 198
		 173 172 200 201 202 203 204 205 206 207 203 202
		 208 209 195 194 207 206 151 150 183 182 161 160
		 191 190 210 211 179 178 212 213 209 208 142 205
		 146 152 155 197 142 145 206 205 210 178 166 162
		 165 210 162 170 188 187 149 200 203 150 197 155
		 161 198 183 209 213 180 173 190 188 170 150 203
		 209 183 198 161 190 173 195 158 157 192 168 176
		 175 184 206 145 158 195 166 178 176 168 214 215
		 1 0 216 217 0 3 218 219 11 10 220 221
		 8 11 221 216 3 8 222 223 22 21 224 225
		 23 22 225 226 25 23 226 227 27 25 228 229
		 26 27 215 222 21 1 229 218 10 26 230 231
		 232 233 234 235 236 237 238 239 240 241 237 236
		 242 243 239 238 244 245 246 247 248 249 250 251
		 252 253 254 255 231 230 256 257 258 259 249 248
		 260 261 259 258 251 250 261 260 255 254 262 263
		 264 265 266 267 268 269 243 242 263 262 241 240
		 267 266 ;
	setAttr ".mue" -type "floatArray" 270 0.28283942 0.26626688 0.26865369 0.28527775
		 0.31548059 0.2990973 0.29898831 0.31546822 0.28809038 0.27037013 0.26134646 0.27521351
		 0.31561753 0.29900676 0.30013692 0.316432 0.57386494 0.55755818 0.55836833 0.57484955
		 0.020061444 0.022931607 0.0063685249 0.0034913868 0.023053478 0.0054292367 0.030391416
		 0.016104653 0.57445586 0.5582059 0.55742162 0.57384098 0.82735634 0.82735634 0.58552235
		 0.58552235 0.82735616 0.82735616 0.58552229 0.58552229 0.82980829 0.82980829 0.58307028
		 0.58307028 0.82735634 0.58552235 0.58552235 0.82735634 0.58307028 0.58307028 0.82980824
		 0.82980824 0.82735616 0.58552229 0.58552229 0.82735616 0.69989514 0.70059955 0.6946528
		 0.69412804 0.7162925 0.7163949 0.60284728 0.60389459 0.58850986 0.58307028 0.97769386
		 0.97762746 0.96319008 0.96318328 0.61165142 0.61305869 0.61565381 0.61425495 0.6238305
		 0.62240058 0.70623481 0.71801919 0.72923213 0.71969259 0.69980019 0.70911694 0.92716879
		 0.92603046 0.60139263 0.59332442 0.61038184 0.61166787 0.62080234 0.61564618 0.6992991
		 0.69850826 0.71648967 0.71639782 0.65413094 0.64577103 0.9813953 0.9819721 0.96319091
		 0.96320701 0.60488516 0.60625303 0.61430413 0.61287463 0.62430513 0.6228677 0.63387382
		 0.6324333 0.76312667 0.78294408 0.79575223 0.78259778 0.78294408 0.76312667 0.93885446
		 0.93671906 0.93875134 0.93815702 0.61195236 0.61185741 0.6163488 0.61699575 0.61185431
		 0.61157858 0.94535542 0.9448033 0.31555724 0.2990855 0.29762003 0.29749307 0.2975083
		 0.29866993 0.30017906 0.31640685 0.57381427 0.55762607 0.57634282 0.57532507 0.55734175
		 0.57385093 0.57531947 0.57592356 0.90461117 0.904661 0.89003229 0.88998616 0.91131133
		 0.91104603 0.45928305 0.45774445 0.50660807 0.52835977 0.32571477 0.32504657 0.31533179
		 0.31721467 0.62833524 0.62861466 0.63305968 0.63291299 0.26774544 0.28121781 0.96912855
		 0.96978611 0.97880703 0.97788733 0.95525163 0.95550251 0.70633811 0.70548427 0.36031657
		 0.38363257 0.38016757 0.36565265 0.6941421 0.69412804 0.70583123 0.70565212 0.95505446
		 0.95559478 0.46081865 0.46248424 0.49704543 0.48433718 0.69679642 0.69617909 0.2901935
		 0.30324072 0.29897386 0.28693077 0.28917196 0.27482286 0.62723601 0.6267001 0.63324875
		 0.63338244 0.32772127 0.32633373 0.35642028 0.37093097 0.44908875 0.4472447 0.37483689
		 0.39554211 0.90953201 0.90875959 0.88976771 0.88971388 0.39891529 0.41329399 0.9720906
		 0.97146976 0.45017385 0.45196018 0.28280351 0.26642373 0.28677005 0.28433096 0.26120162
		 0.27487776 0.27668989 0.28969008 0.023053966 0.0064607314 0.004876839 0.0020000001
		 0.0038478114 0.014627636 0.016399289 0.030212114 0.95194077 0.96631944 0.96631944
		 0.95194077 0.53547406 0.54985285 0.54985285 0.53547406 0.57166761 0.557289 0.557289
		 0.57166761 0.54985285 0.53547406 0.57166761 0.557289 0.95194077 0.96631944 0.96631944
		 0.95194077 0.58307028 0.59744906 0.59744906 0.58307028 0.95194077 0.96631944 0.58307028
		 0.59744906 0.59744906 0.58307028 0.96631944 0.95194077 0.53547406 0.54985285 0.54985285
		 0.53547406 0.57166761 0.557289 0.557289 0.57166761 ;
	setAttr ".mve" -type "floatArray" 270 0.51356626 0.5135591 0.17886156 0.17885487
		 0.50719684 0.50702345 0.17493914 0.174225 0.094344154 0.095874853 0.0052235932 0.0034700313
		 0.092195548 0.092843376 0.0034791173 0.0036541868 0.5077582 0.50776154 0.17336132
		 0.17246002 0.17522471 0.5162968 0.51647937 0.17529584 0.092986166 0.090196803 0.0078271441
		 0.0082715461 0.091311038 0.091657273 0.0040143491 0.004124125 0.0044520274 0.086152673
		 0.086152673 0.0044520274 0.095023073 0.17672373 0.17672373 0.095023073 0.0044520274
		 0.086152673 0.086152673 0.0044520274 0.088604711 0.088604711 0.0020000001 0.0020000001
		 0.17672373 0.095023073 0.095023073 0.17672373 0.17917578 0.17917578 0.092571057 0.092571057
		 0.77857029 0.77962679 0.78380549 0.78313047 0.77816403 0.77957654 0.18426655 0.18454656
		 0.25348496 0.25805345 0.77611774 0.77751327 0.77851355 0.77709758 0.39334631 0.39314893
		 0.39901918 0.39954314 0.48948663 0.48965853 0.61983621 0.60671014 0.61728162 0.62880278
		 0.61316711 0.6028499 0.7393859 0.74166524 0.32459047 0.33258861 0.18314213 0.18358816
		 0.22227125 0.22733092 0.78690267 0.78538114 0.78758705 0.78899956 0.27027783 0.27949464
		 0.78362739 0.78510529 0.78795654 0.78654194 0.39912385 0.39826807 0.49062318 0.49079481
		 0.57456148 0.57464117 0.57413691 0.57417643 0.54873437 0.52582473 0.53847462 0.55365521
		 0.52582473 0.54873437 0.72674352 0.72894681 0.74488574 0.74620855 0.9150756 0.91484004
		 0.91412288 0.9151333 0.91436422 0.91405076 0.73832363 0.73994094 0.50866622 0.50848794
		 0.50700253 0.17505753 0.092968203 0.003460272 0.0020000001 0.0021742017 0.50924248
		 0.50922519 0.17231515 0.50776201 0.0025435332 0.0026630207 0.0041433498 0.091247849
		 0.74471426 0.74618804 0.74495107 0.74347925 0.74661505 0.74812585 0.99171013 0.99137843
		 0.67912221 0.67871219 0.99694312 0.99800003 0.99190474 0.99103808 0.74658281 0.74597484
		 0.74744481 0.74892938 0.67885786 0.68261939 0.7576704 0.75567484 0.76318508 0.76429266
		 0.75500083 0.75215709 0.75425446 0.75179827 0.53718734 0.54497886 0.6306926 0.62604904
		 0.76593179 0.7644949 0.76723623 0.77009672 0.7698741 0.77316266 0.5165168 0.51575089
		 0.58424366 0.58534718 0.75827938 0.75706083 0.51005358 0.51434463 0.52194494 0.51775014
		 0.60465181 0.60002464 0.7429713 0.74152541 0.73757672 0.73904145 0.99194491 0.992814
		 0.70422202 0.70878589 0.99270999 0.99267495 0.68680966 0.68648636 0.73702419 0.73859453
		 0.73512048 0.73364967 0.58963501 0.5897513 0.76773673 0.76927304 0.51294351 0.51302183
		 0.51506066 0.51504141 0.17886241 0.51355356 0.0037384597 0.0020000001 0.0032589543
		 0.094124757 0.5177837 0.51797664 0.51646787 0.17530265 0.089908704 0.0080782343 0.0067943144
		 0.0063485843 0.5191412 0.5191412 0.53300625 0.53300625 0.51318622 0.51318622 0.52705127
		 0.52705127 0.78772312 0.78772312 0.77138299 0.77138299 0.54339141 0.54339141 0.80158818
		 0.80158818 0.0020000001 0.0020000001 0.015865088 0.015865088 0.53173059 0.53173059
		 0.86506921 0.86506921 0.18580244 0.18580244 0.36179325 0.36179325 0.45002997 0.45002997
		 0.10410178 0.10410178 0.78522521 0.78522521 0.80156535 0.80156535 0.52954894 0.52954894
		 0.51320887 0.51320887 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[212:213]" "|TL_card_01_model|TL_card_01_modelShape.map[208:209]" "|TL_card_01_model|TL_card_01_modelShape.map[200:203]" "|TL_card_01_model|TL_card_01_modelShape.map[180:183]" "|TL_card_01_model|TL_card_01_modelShape.map[148:151]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold19";
	setAttr ".uvl" -type "Int32Array" 20 152 153 154 155 160 161
		 170 171 172 173 186 187 188 189 190 191 196 197
		 198 199 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 81 80 85 84 108 109
		 110 111 112 113 95 94 111 110 114 115 116 117
		 83 82 118 119 120 121 121 120 107 106 105 104
		 122 123 124 125 115 114 81 108 77 119 122 120
		 77 108 111 78 57 91 58 92 91 57 61 73
		 101 70 84 95 113 81 120 122 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 115 125 116 96 99 68 67 78 111
		 115 82 99 92 61 68 126 127 5 4 128 129
		 6 5 130 131 14 13 132 133 15 14 129 130
		 13 6 134 135 17 16 136 137 16 19 138 139
		 31 30 140 141 28 31 141 136 19 28 135 126
		 4 17 133 138 30 15 142 143 144 145 143 142
		 146 147 148 149 150 151 152 153 154 155 156 157
		 158 159 155 154 160 161 145 144 159 158 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 176 177 177 176 178 179 180 181
		 182 183 184 185 169 168 186 187 188 189 189 188
		 190 191 192 193 194 195 196 197 198 199 199 198
		 173 172 200 201 202 203 204 205 206 207 203 202
		 208 209 195 194 207 206 151 150 183 182 161 160
		 191 190 210 211 179 178 212 213 209 208 142 205
		 146 152 155 197 142 145 206 205 210 178 166 162
		 165 210 162 170 188 187 149 200 203 150 197 155
		 161 198 183 209 213 180 173 190 188 170 150 203
		 209 183 198 161 190 173 195 158 157 192 168 176
		 175 184 206 145 158 195 166 178 176 168 214 215
		 1 0 216 217 0 3 218 219 11 10 220 221
		 8 11 221 216 3 8 222 223 22 21 224 225
		 23 22 225 226 25 23 226 227 27 25 228 229
		 26 27 215 222 21 1 229 218 10 26 230 231
		 232 233 234 235 236 237 238 239 240 241 237 236
		 242 243 239 238 244 245 246 247 248 249 250 251
		 252 253 254 255 231 230 256 257 258 259 249 248
		 260 261 259 258 251 250 261 260 255 254 262 263
		 264 265 266 267 268 269 243 242 263 262 241 240
		 267 266 ;
	setAttr ".mue" -type "floatArray" 270 0.2788927 0.26255307 0.26490632 0.28129676
		 0.31107023 0.29491717 0.29480973 0.31105804 0.28406987 0.26659864 0.25770178 0.27137396
		 0.31120524 0.29482791 0.29594219 0.31200826 0.56582344 0.54974586 0.55054462 0.56679422
		 0.019807622 0.022637449 0.0063071325 0.0034704278 0.022757607 0.0053810445 0.029992422
		 0.015906436 0.56640607 0.55038446 0.54961121 0.56579983 0.81574768 0.81574768 0.57731223
		 0.57731223 0.8157475 0.8157475 0.57731217 0.57731217 0.81816518 0.81816518 0.57489461
		 0.57489461 0.81574768 0.57731223 0.57731223 0.81574768 0.57489461 0.57489461 0.81816512
		 0.81816512 0.8157475 0.57731217 0.57731217 0.8157475 0.69007587 0.69077039 0.6849072
		 0.68438983 0.7062428 0.70634377 0.59439367 0.59542626 0.58025777 0.57489461 0.96397054
		 0.9639051 0.94967061 0.94966394 0.60307378 0.60446125 0.6070199 0.60564071 0.61508173
		 0.6136719 0.69632828 0.70794702 0.71900243 0.70959693 0.68998408 0.69916987 0.91415739
		 0.91303509 0.59295946 0.58500463 0.60182238 0.60309035 0.61209643 0.60701275 0.68948823
		 0.68870848 0.70643723 0.70634663 0.64495665 0.63671422 0.96762002 0.9681887 0.94967145
		 0.9496873 0.59640259 0.59775126 0.60568923 0.60427982 0.61554968 0.6141324 0.62498391
		 0.62356359 0.7524206 0.77195954 0.78458768 0.77161807 0.77195954 0.7524206 0.92567885
		 0.92357343 0.92557716 0.92499119 0.60337049 0.60327685 0.60770512 0.60834301 0.60327381
		 0.60300195 0.93208843 0.93154407 0.31114581 0.29490554 0.29346067 0.2933355 0.29335052
		 0.29449582 0.29598373 0.31198347 0.56577349 0.54981279 0.56826651 0.56726307 0.54953247
		 0.56580961 0.56725752 0.56785315 0.9623059 0.96235502 0.94793189 0.94788641 0.96891189
		 0.96865034 0.29642877 0.29491177 0.34308872 0.36453474 0.64409357 0.64308459 0.63499981
		 0.6368745 0.68991256 0.69018805 0.6945706 0.69442594 0.62182117 0.62859213 0.93792826
		 0.93857658 0.94747072 0.9465639 0.92424631 0.92449367 0.67883086 0.67798901 0.64288205
		 0.64420718 0.66640019 0.66140485 0.66680628 0.66679239 0.67833114 0.67815453 0.92405194
		 0.92458469 0.29794279 0.29958495 0.33366045 0.32113078 0.66942328 0.6688146 0.63822043
		 0.63932949 0.63392138 0.63223505 0.63494158 0.63003612 0.68882877 0.68830037 0.69475698
		 0.69488883 0.64794356 0.6464771 0.66599768 0.67331576 0.28637773 0.28455961 0.21316935
		 0.2335836 0.96715754 0.96639603 0.947671 0.94761795 0.23690936 0.251086 0.94084865
		 0.94023657 0.28744757 0.2892088 0.27885729 0.26270771 0.2827681 0.28036329 0.25755897
		 0.27104291 0.27282959 0.28564709 0.022758089 0.0063980431 0.0048364098 0.0020000001
		 0.0038218435 0.014450177 0.016196931 0.02981564 0.93857515 0.95275176 0.95275176
		 0.93857515 0.37154636 0.38572308 0.38572308 0.37154636 0.40723088 0.39305434 0.39305434
		 0.40723088 0.38572308 0.37154636 0.40723088 0.39305434 0.93857515 0.95275176 0.95275176
		 0.93857515 0.57489461 0.58907133 0.58907133 0.57489461 0.93857515 0.95275176 0.57489461
		 0.58907133 0.58907133 0.57489461 0.95275176 0.93857515 0.37154636 0.38572308 0.38572308
		 0.37154636 0.40723088 0.39305434 0.39305434 0.40723088 ;
	setAttr ".mve" -type "floatArray" 270 0.50637704 0.50637001 0.17637607 0.17636947
		 0.50009722 0.49992624 0.17250879 0.17180468 0.093046412 0.094555601 0.0051782909
		 0.0034493725 0.090928011 0.091566734 0.0034583309 0.0036309401 0.5006507 0.50065398
		 0.17095314 0.17006451 0.17279033 0.50906926 0.50924921 0.17286047 0.091707513 0.088957347
		 0.0077452534 0.0081834104 0.090055928 0.090397298 0.0039860411 0.0040942742 0.0044175684
		 0.08497005 0.08497005 0.0044175684 0.093715727 0.17426825 0.17426825 0.093715727
		 0.0044175684 0.08497005 0.08497005 0.0044175684 0.087387629 0.087387629 0.0020000001
		 0.0020000001 0.17426825 0.093715727 0.093715727 0.17426825 0.17668582 0.17668582
		 0.09129817 0.09129817 0.78461432 0.78565598 0.78977597 0.78911042 0.78421378 0.78560644
		 0.181705 0.18198107 0.24995066 0.25445494 0.78219628 0.7835722 0.78455842 0.78316236
		 0.38784507 0.38765046 0.39343822 0.39395481 0.48263431 0.48280379 0.61115348 0.59821188
		 0.60863477 0.61999404 0.60457808 0.59440589 0.7290231 0.73127043 0.32005692 0.32794264
		 0.18059637 0.18103614 0.21917561 0.22416417 0.79282963 0.79132944 0.79350436 0.79489702
		 0.26650754 0.27559483 0.78960037 0.79105753 0.79386866 0.79247397 0.3935414 0.39269766
		 0.48375487 0.48392409 0.56651354 0.56659216 0.56609499 0.56613392 0.54105085 0.51846319
		 0.53093529 0.54590255 0.51846319 0.54105085 0.7165584 0.71873075 0.73444569 0.7357499
		 0.90224236 0.90201014 0.90130305 0.90229923 0.90154099 0.90123194 0.72797579 0.72957033
		 0.50154591 0.50137013 0.49990562 0.17262551 0.09168981 0.0034397503 0.0020000001
		 0.0021717534 0.50211412 0.50209707 0.16992168 0.50065446 0.0025358947 0.0026537031
		 0.004113229 0.089993633 0.75025141 0.75170451 0.75048488 0.74903375 0.7521255 0.75361508
		 0.97561371 0.97528672 0.66741872 0.66701442 0.99701411 0.99800003 0.9897759 0.9886775
		 0.75209373 0.75149429 0.75294358 0.75440729 0.65475363 0.65499765 0.76408428 0.76211673
		 0.76952147 0.77061343 0.7614522 0.75864846 0.76071632 0.75829464 0.49061877 0.49069872
		 0.57381952 0.57371229 0.77222955 0.77081281 0.77351564 0.77633595 0.77611643 0.7793588
		 0.50709844 0.50634331 0.57387352 0.57496154 0.76468468 0.76348323 0.48461756 0.48535073
		 0.49384102 0.49292699 0.57325822 0.5726673 0.74853295 0.74710739 0.74321419 0.74465835
		 0.99026757 0.99086952 0.65631139 0.65658432 0.97659957 0.976565 0.6749981 0.67467934
		 0.7426694 0.74421769 0.74079245 0.73934233 0.57918912 0.57930374 0.77400911 0.77552384
		 0.50357538 0.50365257 0.50785047 0.50783151 0.17637691 0.50636452 0.0037140285 0.0020000001
		 0.0032412617 0.092830099 0.51053524 0.51072544 0.50923789 0.17286718 0.088673294
		 0.0079928152 0.0067269383 0.0062874723 0.51187366 0.51187366 0.52554387 0.52554387
		 0.5056411 0.5056411 0.51931131 0.51931131 0.77634728 0.77634728 0.76023674 0.76023674
		 0.53542179 0.53542179 0.79001749 0.79001749 0.0020000001 0.0020000001 0.015670238
		 0.015670238 0.52428514 0.52428514 0.85293925 0.85293925 0.18321942 0.18321942 0.35673597
		 0.35673597 0.44373268 0.44373268 0.10266691 0.10266691 0.77385712 0.77385712 0.7899676
		 0.7899676 0.52180123 0.52180123 0.50569075 0.50569075 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[196:199]" "|TL_card_01_model|TL_card_01_modelShape.map[186:191]" "|TL_card_01_model|TL_card_01_modelShape.map[170:173]" "|TL_card_01_model|TL_card_01_modelShape.map[160:161]" "|TL_card_01_model|TL_card_01_modelShape.map[152:155]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold20";
	setAttr ".uvl" -type "Int32Array" 32 62 63 64 65 76 77
		 78 79 80 81 82 83 84 85 86 87 88 89
		 94 95 108 109 110 111 112 113 114 115 116 117
		 124 125 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 81 80 85 84 108 109
		 110 111 112 113 95 94 111 110 114 115 116 117
		 83 82 118 119 120 121 121 120 107 106 105 104
		 122 123 124 125 115 114 81 108 77 119 122 120
		 77 108 111 78 57 91 58 92 91 57 61 73
		 101 70 84 95 113 81 120 122 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 115 125 116 96 99 68 67 78 111
		 115 82 99 92 61 68 126 127 5 4 128 129
		 6 5 130 131 14 13 132 133 15 14 129 130
		 13 6 134 135 17 16 136 137 16 19 138 139
		 31 30 140 141 28 31 141 136 19 28 135 126
		 4 17 133 138 30 15 142 143 144 145 143 142
		 146 147 148 149 150 151 152 153 154 155 156 157
		 158 159 155 154 160 161 145 144 159 158 162 163
		 164 165 163 162 166 167 167 166 168 169 170 171
		 172 173 174 175 176 177 177 176 178 179 180 181
		 182 183 184 185 169 168 186 187 188 189 189 188
		 190 191 192 193 194 195 196 197 198 199 199 198
		 173 172 200 201 202 203 204 205 206 207 203 202
		 208 209 195 194 207 206 151 150 183 182 161 160
		 191 190 210 211 179 178 212 213 209 208 142 205
		 146 152 155 197 142 145 206 205 210 178 166 162
		 165 210 162 170 188 187 149 200 203 150 197 155
		 161 198 183 209 213 180 173 190 188 170 150 203
		 209 183 198 161 190 173 195 158 157 192 168 176
		 175 184 206 145 158 195 166 178 176 168 214 215
		 1 0 216 217 0 3 218 219 11 10 220 221
		 8 11 221 216 3 8 222 223 22 21 224 225
		 23 22 225 226 25 23 226 227 27 25 228 229
		 26 27 215 222 21 1 229 218 10 26 230 231
		 232 233 234 235 236 237 238 239 240 241 237 236
		 242 243 239 238 244 245 246 247 248 249 250 251
		 252 253 254 255 231 230 256 257 258 259 249 248
		 260 261 259 258 251 250 261 260 255 254 262 263
		 264 265 266 267 268 269 243 242 263 262 241 240
		 267 266 ;
	setAttr ".mue" -type "floatArray" 270 0.2744922 0.25841224 0.26072812 0.27685806
		 0.30552849 0.28963211 0.28952637 0.30551648 0.27958709 0.26239353 0.25363806 0.26709297
		 0.30566132 0.28954428 0.29064083 0.30645159 0.55623305 0.540411 0.54119706 0.55718839
		 0.019524615 0.022309471 0.0062386817 0.0034470591 0.022427719 0.0053273113 0.029547555
		 0.01568543 0.55680645 0.54103941 0.54027849 0.5562098 0.80322993 0.80322993 0.56858385
		 0.56858385 0.80322975 0.80322975 0.56858379 0.56858379 0.80560899 0.80560899 0.56620461
		 0.56620461 0.80322993 0.56858385 0.56858385 0.80322993 0.56620461 0.56620461 0.80560899
		 0.80560899 0.80322975 0.56858379 0.56858379 0.80322975 0.71086484 0.71154827 0.7057783
		 0.70526916 0.72677481 0.72687417 0.58263093 0.58366209 0.56631553 0.56024474 0.98040664
		 0.98034221 0.96633393 0.96632737 0.61051589 0.61188132 0.61439925 0.613042 0.62233299
		 0.62094557 0.71185559 0.73413837 0.73851281 0.72644478 0.71023142 0.7996155 0.94726312
		 0.94587255 0.58964884 0.58035225 0.5901624 0.59159291 0.60200876 0.59632093 0.7102865
		 0.70951915 0.72696614 0.72687697 0.64787847 0.63898623 0.98399812 0.98455775 0.96633476
		 0.96635038 0.60395068 0.60527796 0.61308974 0.61170274 0.6227935 0.62139875 0.63207781
		 0.63068002 0.80264241 0.80462605 0.81610906 0.80241817 0.80094773 0.80056393 0.96142864
		 0.95887148 0.95913178 0.95834321 0.6108079 0.61071569 0.61507362 0.61570138 0.61071271
		 0.6104452 0.96742868 0.96656597 0.30560285 0.2896207 0.28819877 0.28807557 0.28809035
		 0.28921747 0.29068172 0.30642718 0.55618387 0.54047686 0.55863732 0.55764979 0.54020101
		 0.55621946 0.55764431 0.55823052 0.89346802 0.8935163 0.87932241 0.87927765 0.89996898
		 0.89971161 0.40577126 0.40427837 0.45168966 0.47279486 0.29012766 0.28913471 0.28117841
		 0.28302333 0.62540364 0.62567472 0.62998766 0.62984526 0.26820922 0.27487257 0.92441672
		 0.92505473 0.93380749 0.93291509 0.91095221 0.91119564 0.66943699 0.66860855 0.28893539
		 0.29023948 0.31207979 0.30716383 0.65760356 0.65758985 0.66894525 0.66877145 0.91076088
		 0.91128516 0.40726122 0.40887728 0.44241124 0.43008071 0.66017896 0.65957993 0.28434786
		 0.28543931 0.28011715 0.27845761 0.28112113 0.27629361 0.62433708 0.62381709 0.63017106
		 0.63030082 0.29391646 0.29247332 0.31168365 0.31888545 0.39587995 0.39409074 0.32383502
		 0.34392485 0.89824253 0.89749312 0.87906569 0.87901348 0.34719774 0.3611491 0.92729068
		 0.92668831 0.39693281 0.39866602 0.27445737 0.25856444 0.27830604 0.27593943 0.25349754
		 0.26676717 0.26852545 0.28113925 0.022428194 0.0063281474 0.0047913324 0.0020000001
		 0.0037928899 0.014252313 0.015971307 0.029373582 0.56024474 0.57419604 0.57419604
		 0.56024474 0.47879264 0.49274406 0.49274406 0.47879264 0.51459688 0.50064564 0.50064564
		 0.51459688 0.49274406 0.47879264 0.51459688 0.50064564 0.56024474 0.57419604 0.57419604
		 0.56024474 0.58209771 0.59604913 0.59604913 0.58209771 0.56024474 0.57419604 0.58209771
		 0.59604913 0.59604913 0.58209771 0.57419604 0.56024474 0.47879264 0.49274406 0.49274406
		 0.47879264 0.51459688 0.50064564 0.50064564 0.51459688 ;
	setAttr ".mve" -type "floatArray" 270 0.49836129 0.49835438 0.17360482 0.17359832
		 0.49218127 0.49201301 0.169799 0.16910608 0.091599464 0.093084671 0.0051277801 0.0034263383
		 0.089514732 0.090143308 0.0034351544 0.0036050202 0.49272597 0.49272919 0.16826807
		 0.16739357 0.17007607 0.50101072 0.5011878 0.17014509 0.090281844 0.087575383 0.0076539475
		 0.008085141 0.088656507 0.088992454 0.0039544716 0.0040609841 0.0043791472 0.083651446
		 0.083651446 0.0043791472 0.092383012 0.17165536 0.17165536 0.092383012 0.0043791472
		 0.083651446 0.083651446 0.0043791472 0.086030602 0.086030602 0.0020000001 0.0020000001
		 0.17165536 0.092383012 0.092383012 0.17165536 0.17403452 0.17403452 0.090003878 0.090003878
		 0.82516408 0.82618916 0.83024371 0.82958871 0.82476991 0.8261404 0.17954268 0.17983808
		 0.24923426 0.25474796 0.82278442 0.82413846 0.82510906 0.82373518 0.4180232 0.41783169
		 0.42352745 0.42403585 0.51130599 0.51147282 0.65549964 0.63113886 0.65384358 0.6678769
		 0.64495254 0.54368031 0.80756009 0.81011462 0.32477129 0.33471233 0.17800778 0.178317
		 0.21172613 0.21779765 0.83324885 0.83177251 0.83391285 0.83528334 0.26022997 0.2707594
		 0.83007085 0.83150488 0.83427131 0.8328988 0.42362899 0.42279866 0.51240879 0.51257527
		 0.59385222 0.59392959 0.59344029 0.59347862 0.54933304 0.55008566 0.56523049 0.58043802
		 0.542382 0.54300725 0.79398739 0.79636562 0.81489748 0.81636113 0.92424548 0.92401695
		 0.92332113 0.92430145 0.92355525 0.92325115 0.80860263 0.81033546 0.49360695 0.49343395
		 0.49199271 0.16991387 0.090264425 0.0034168693 0.0020000001 0.0021690233 0.49416611
		 0.49414936 0.167253 0.49272966 0.0025273715 0.0026433072 0.004079638 0.088595204
		 0.79178756 0.79321754 0.79201728 0.79058921 0.79363185 0.79509777 0.96232861 0.96200681
		 0.65903157 0.65863371 0.99702972 0.99800003 0.98990655 0.98882562 0.79360062 0.79301065
		 0.79443693 0.7958774 0.66020858 0.66044873 0.80496532 0.803029 0.81031609 0.8113907
		 0.80237508 0.79961586 0.80165088 0.79926765 0.49868226 0.49876094 0.58056074 0.58045524
		 0.81298113 0.81158692 0.81424677 0.81702226 0.81680626 0.81999707 0.50125915 0.500516
		 0.56697303 0.56804371 0.80555618 0.8043738 0.49277642 0.49349794 0.50185329 0.50095379
		 0.58000839 0.57942683 0.7900964 0.78869349 0.78486216 0.78628337 0.99039042 0.99098283
		 0.66174161 0.66201019 0.9632988 0.96326476 0.6664905 0.6661768 0.78432602 0.78584969
		 0.78247893 0.78105181 0.57220411 0.57231694 0.81473243 0.81622308 0.49779207 0.49786803
		 0.49981129 0.49979264 0.17360564 0.49834895 0.0036867885 0.0020000001 0.0032215349
		 0.091386594 0.50245339 0.50264055 0.50117666 0.1701517 0.087295845 0.0078975754 0.006651816
		 0.006219334 0.84045613 0.84045613 0.85390908 0.85390908 0.49802604 0.49802604 0.51147902
		 0.51147902 0.7644316 0.7644316 0.74857712 0.74857712 0.52733344 0.52733344 0.7778846
		 0.7778846 0.3386856 0.3386856 0.35213858 0.35213858 0.55519575 0.55519575 0.87862676
		 0.87862676 0.51702499 0.51702499 0.39030933 0.39030933 0.47592348 0.47592348 0.43775266
		 0.43775266 0.76197946 0.76197946 0.77783394 0.77783394 0.51393098 0.51393098 0.4980765
		 0.4980765 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[124:125]" "|TL_card_01_model|TL_card_01_modelShape.map[108:117]" "|TL_card_01_model|TL_card_01_modelShape.map[94:95]" "|TL_card_01_model|TL_card_01_modelShape.map[76:89]" "|TL_card_01_model|TL_card_01_modelShape.map[62:65]"  ;
createNode polyMapCut -n "polyMapCut19";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[91]";
createNode Unfold3DUnfold -n "Unfold3DUnfold21";
	setAttr ".uvl" -type "Int32Array" 16 148 149 150 151 180 181
		 182 183 200 201 202 203 208 209 212 213 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 108 109 85 84 110 111
		 112 113 114 115 95 94 113 112 116 117 118 119
		 83 82 120 121 122 123 123 122 107 106 105 104
		 124 125 126 127 117 116 81 110 77 121 124 122
		 77 110 113 78 57 91 58 92 91 57 61 73
		 101 70 84 95 115 108 122 124 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 117 127 118 96 99 68 67 78 113
		 117 82 99 92 61 68 128 129 5 4 130 131
		 6 5 132 133 14 13 134 135 15 14 131 132
		 13 6 136 137 17 16 138 139 16 19 140 141
		 31 30 142 143 28 31 143 138 19 28 137 128
		 4 17 135 140 30 15 144 145 146 147 145 144
		 148 149 150 151 152 153 154 155 156 157 158 159
		 160 161 157 156 162 163 147 146 161 160 164 165
		 166 167 165 164 168 169 169 168 170 171 172 173
		 174 175 176 177 178 179 179 178 180 181 182 183
		 184 185 186 187 171 170 188 189 190 191 191 190
		 192 193 194 195 196 197 198 199 200 201 201 200
		 175 174 202 203 204 205 206 207 208 209 205 204
		 210 211 197 196 209 208 153 152 185 184 163 162
		 193 192 212 213 181 180 214 215 211 210 144 207
		 148 154 157 199 144 147 208 207 212 180 168 164
		 167 212 164 172 190 189 151 202 205 152 199 157
		 163 200 185 211 215 182 175 192 190 172 152 205
		 211 185 200 163 192 175 197 160 159 194 170 178
		 177 186 208 147 160 197 168 180 178 170 216 217
		 1 0 218 219 0 3 220 221 11 10 222 223
		 8 11 223 218 3 8 224 225 22 21 226 227
		 23 22 227 228 25 23 228 229 27 25 230 231
		 26 27 217 224 21 1 231 220 10 26 232 233
		 234 235 236 237 238 239 240 241 242 243 239 238
		 244 245 241 240 246 247 248 249 250 251 252 253
		 254 255 256 257 233 232 258 259 260 261 251 250
		 262 263 261 260 253 252 263 262 257 256 264 265
		 266 267 268 269 270 271 245 244 265 264 243 242
		 269 268 ;
	setAttr ".mue" -type "floatArray" 272 0.29224914 0.27512133 0.27758813 0.29476917
		 0.32433832 0.30740604 0.30729342 0.32432553 0.29767603 0.27936205 0.27003604 0.28436774
		 0.32447979 0.30731249 0.3084805 0.32532156 0.59138 0.57452691 0.57536423 0.59239763
		 0.020666605 0.023632936 0.0065148952 0.0035413564 0.02375889 0.0055441353 0.031342689
		 0.01657724 0.59199077 0.57519627 0.57438576 0.59135526 0.85412854 0.85412854 0.60419178
		 0.60419178 0.8541283 0.8541283 0.60419172 0.60419172 0.85666263 0.85666263 0.60165745
		 0.60165745 0.85412854 0.60419178 0.60419178 0.85412854 0.60165745 0.60165745 0.85666257
		 0.85666257 0.8541283 0.60419172 0.60419172 0.8541283 0.30582941 0.30655736 0.3004114
		 0.29986906 0.32277614 0.32288197 0.73464829 0.73542088 0.68787992 0.67996216 0.59293586
		 0.5928672 0.57794607 0.57793909 0.91435826 0.91581261 0.91849464 0.91704893 0.92694539
		 0.92546755 0.017454002 0.015818205 0.031099886 0.032462556 0.010578344 0.0020000001
		 0.23257679 0.23326598 0.67338097 0.66056031 0.74200672 0.74312532 0.73679715 0.72895813
		 0.30521336 0.304396 0.32297993 0.32288495 0.7547406 0.74200338 0.59676135 0.59735745
		 0.57794696 0.57796359 0.9073652 0.90877897 0.91709983 0.91562241 0.92743593 0.92595029
		 0.93732524 0.93583637 0.75696337 0.63105243 0.0068649384 0.0083775427 0.023257485
		 0.024398714 0.75874197 0.75811291 0.23262326 0.23257406 0.24323593 0.24363528 0.91466928
		 0.91457105 0.91921294 0.91988164 0.91456789 0.91428292 0.24420249 0.24471407 0.32441753
		 0.30739388 0.30587929 0.30574808 0.30576381 0.30696437 0.30852404 0.32529557 0.59132767
		 0.57459706 0.59394097 0.59288907 0.57430327 0.59136552 0.59288323 0.59350765 0.57925314
		 0.57930458 0.56418574 0.56413805 0.58617771 0.58590358 0.83588707 0.834297 0.88479728
		 0.90727443 0.61912525 0.61806756 0.6095928 0.61155796 0.29372036 0.29400909 0.29860309
		 0.29845142 0.59577847 0.60287607 0.28621462 0.2868942 0.29621732 0.29526678 0.2718727
		 0.27213198 0.014619156 0.013736725 0.61785525 0.61924434 0.64250785 0.63727158 0.0020146025
		 0.0020000001 0.014095373 0.01391024 0.27166888 0.27222735 0.8374747 0.83919615 0.87490547
		 0.86177343 0.0047578253 0.0041197632 0.6129688 0.61413139 0.60846239 0.6066947 0.60953182
		 0.60438967 0.2925843 0.29203042 0.29879844 0.29893667 0.62316096 0.62162375 0.64208591
		 0.64975703 0.82535124 0.8234455 0.74863237 0.77002817 0.58433878 0.5835405 0.56391227
		 0.56385666 0.77350241 0.78836083 0.28927585 0.28863424 0.82647347 0.82831973 0.29221204
		 0.27528346 0.2963115 0.29379067 0.26988637 0.28402072 0.28589356 0.29932934 0.023759397
		 0.0066101905 0.0049732295 0.0020000001 0.0039097234 0.015050734 0.016881745 0.03115738
		 0.86425257 0.87911302 0.87911302 0.86425257 0.94459885 0.95945942 0.95945942 0.94459885
		 0.94770122 0.93284082 0.93284082 0.94770122 0.95945942 0.94459885 0.94770122 0.93284082
		 0.86425257 0.87911302 0.87911302 0.86425257 0.88580889 0.90066946 0.90066946 0.88580889
		 0.86425257 0.87911302 0.88580889 0.90066946 0.90066946 0.88580889 0.87911302 0.86425257
		 0.94459885 0.95945942 0.95945942 0.94459885 0.94770122 0.93284082 0.93284082 0.94770122 ;
	setAttr ".mve" -type "floatArray" 272 0.53070664 0.53069925 0.18478744 0.18478051
		 0.52412391 0.52394468 0.18073362 0.17999554 0.097438209 0.099020198 0.0053316015
		 0.0035192855 0.095217623 0.095887162 0.0035286762 0.003709611 0.5247041 0.52470756
		 0.17910291 0.17817143 0.18102874 0.53352875 0.53371733 0.18110226 0.096034728 0.093151897
		 0.0080223866 0.0084816786 0.094303474 0.09466131 0.0040818304 0.0041952836 0.004534184
		 0.088972256 0.088972256 0.004534184 0.097959951 0.18239807 0.18239807 0.097959951
		 0.004534184 0.088972256 0.088972256 0.004534184 0.091506451 0.091506451 0.0020000001
		 0.0020000001 0.18239807 0.097959951 0.097959951 0.18239807 0.18493226 0.18493226
		 0.095425785 0.095425785 0.55164808 0.55273998 0.55705875 0.55636108 0.55122823 0.55268806
		 0.18885159 0.18959123 0.24276637 0.24481153 0.54911339 0.55055565 0.55158949 0.55012608
		 0.0022039893 0.0020000001 0.0080669243 0.0086084511 0.10156554 0.10174324 0.67165452
		 0.64567649 0.65561664 0.67014551 0.66680545 0.56065464 0.62122983 0.6234169 0.32025602
		 0.32470933 0.19099568 0.19193299 0.22625026 0.22896552 0.5602597 0.55868715 0.56096697
		 0.56242681 0.29035512 0.29551539 0.55687463 0.55840212 0.5613488 0.55988687 0.0081750769
		 0.0072906464 0.10274021 0.10291754 0.18949088 0.1895733 0.18905212 0.18909295 0.61094445
		 0.65878397 0.56202942 0.56131524 0.56308371 0.57917631 0.61041772 0.61079115 0.60576236
		 0.6085152 0.61850744 0.61975592 0.5414142 0.54117078 0.54042959 0.54147381 0.54067898
		 0.54035509 0.61035347 0.61179131 0.52564251 0.52545822 0.52392304 0.18085597 0.096016169
		 0.0035091995 0.0020000001 0.0021800373 0.52623808 0.52622026 0.1780217 0.52470803
		 0.0025617336 0.0026852239 0.0042151534 0.094238169 0.54155141 0.54307461 0.54179609
		 0.54027498 0.54351592 0.54507732 0.99696726 0.99662435 0.67388743 0.67345893 0.72596449
		 0.72699803 0.71837711 0.71722579 0.5434826 0.54285419 0.54437345 0.54590774 0.36719444
		 0.36745024 0.54519272 0.54313022 0.55089217 0.55203682 0.54243368 0.53949469 0.54166234
		 0.53912377 0.19514228 0.19522609 0.28235638 0.282244 0.55373091 0.5522458 0.55507898
		 0.55803537 0.5578053 0.56120402 0.50584346 0.50505197 0.57583278 0.57697582 0.54582208
		 0.54456264 0.18885159 0.18962012 0.19851996 0.19756185 0.28176802 0.28114855 0.53975004
		 0.53825569 0.53417474 0.53568852 0.71889251 0.71952355 0.36882737 0.36911348 0.99800003
		 0.99796361 0.68186003 0.68152142 0.53360367 0.53522658 0.53163618 0.53011608 0.58142477
		 0.58154184 0.55559629 0.5571841 0.50215054 0.5022313 0.53225112 0.53223127 0.1847883
		 0.53069347 0.0037967078 0.0020000001 0.003301136 0.097211465 0.53506541 0.53526473
		 0.53370547 0.18110929 0.092854142 0.0082818903 0.0069549512 0.0064942865 0.53646839
		 0.53646839 0.550798 0.550798 0.0020000001 0.0020000001 0.016329639 0.016329639 0.58767295
		 0.58767295 0.57078528 0.57078528 0.033217207 0.033217207 0.60200262 0.60200262 0.0020000001
		 0.0020000001 0.016329639 0.016329639 0.17763121 0.17763121 0.5221386 0.5221386 0.19196087
		 0.19196087 0.0020000001 0.0020000001 0.093193181 0.093193181 0.10752276 0.10752276
		 0.28315392 0.28315392 0.30004153 0.30004153 0.32084849 0.32084849 0.30396086 0.30396086 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[214:215]" "|TL_card_01_model|TL_card_01_modelShape.map[210:211]" "|TL_card_01_model|TL_card_01_modelShape.map[202:205]" "|TL_card_01_model|TL_card_01_modelShape.map[182:185]" "|TL_card_01_model|TL_card_01_modelShape.map[150:153]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold22";
	setAttr ".uvl" -type "Int32Array" 18 76 77 78 79 80 81
		 82 83 110 111 112 113 116 117 118 119 126 127 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 57 56 60 61 62 63
		 64 65 66 67 68 69 70 71 72 73 73 72
		 74 75 61 60 69 68 76 77 78 79 77 76
		 80 81 79 78 82 83 65 64 84 85 86 87
		 88 89 90 91 92 93 89 88 94 95 96 97
		 98 99 100 101 102 103 103 102 104 105 93 92
		 99 98 75 74 106 107 108 109 85 84 110 111
		 112 113 114 115 95 94 113 112 116 117 118 119
		 83 82 120 121 122 123 123 122 107 106 105 104
		 124 125 126 127 117 116 81 110 77 121 124 122
		 77 110 113 78 57 91 58 92 91 57 61 73
		 101 70 84 95 115 108 122 124 104 107 63 86
		 89 64 75 102 101 73 64 89 95 84 107 104
		 102 75 82 117 127 118 96 99 68 67 78 113
		 117 82 99 92 61 68 128 129 5 4 130 131
		 6 5 132 133 14 13 134 135 15 14 131 132
		 13 6 136 137 17 16 138 139 16 19 140 141
		 31 30 142 143 28 31 143 138 19 28 137 128
		 4 17 135 140 30 15 144 145 146 147 145 144
		 148 149 150 151 152 153 154 155 156 157 158 159
		 160 161 157 156 162 163 147 146 161 160 164 165
		 166 167 165 164 168 169 169 168 170 171 172 173
		 174 175 176 177 178 179 179 178 180 181 182 183
		 184 185 186 187 171 170 188 189 190 191 191 190
		 192 193 194 195 196 197 198 199 200 201 201 200
		 175 174 202 203 204 205 206 207 208 209 205 204
		 210 211 197 196 209 208 153 152 185 184 163 162
		 193 192 212 213 181 180 214 215 211 210 144 207
		 148 154 157 199 144 147 208 207 212 180 168 164
		 167 212 164 172 190 189 151 202 205 152 199 157
		 163 200 185 211 215 182 175 192 190 172 152 205
		 211 185 200 163 192 175 197 160 159 194 170 178
		 177 186 208 147 160 197 168 180 178 170 216 217
		 1 0 218 219 0 3 220 221 11 10 222 223
		 8 11 223 218 3 8 224 225 22 21 226 227
		 23 22 227 228 25 23 228 229 27 25 230 231
		 26 27 217 224 21 1 231 220 10 26 232 233
		 234 235 236 237 238 239 240 241 242 243 239 238
		 244 245 241 240 246 247 248 249 250 251 252 253
		 254 255 256 257 233 232 258 259 260 261 251 250
		 262 263 261 260 253 252 263 262 257 256 264 265
		 266 267 268 269 270 271 245 244 265 264 243 242
		 269 268 ;
	setAttr ".mue" -type "floatArray" 272 0.29225066 0.27512276 0.27758959 0.29477072
		 0.32434699 0.30741462 0.307302 0.3243342 0.29767758 0.27936351 0.27003744 0.28436923
		 0.32448846 0.30732107 0.30848908 0.32533023 0.59139007 0.57453686 0.57537419 0.5924077
		 0.020666704 0.02363305 0.006514919 0.0035413643 0.023759004 0.005544154 0.031342842
		 0.016577316 0.59200084 0.57520622 0.57439572 0.59136534 0.85414678 0.85414678 0.60420877
		 0.60420877 0.85414654 0.85414654 0.60420871 0.60420871 0.85668087 0.85668087 0.60167438
		 0.60167438 0.85414678 0.60420877 0.60420877 0.85414678 0.60167438 0.60167438 0.85668087
		 0.85668087 0.85414654 0.60420871 0.60420871 0.85414654 0.30583784 0.30656582 0.30041984
		 0.29987746 0.32278466 0.32289049 0.73466659 0.73543918 0.68789798 0.67998016 0.59294575
		 0.59287709 0.5779559 0.57794893 0.91438383 0.91583818 0.91852027 0.91707456 0.92697102
		 0.92549318 0.010059508 0.011283498 0.023818929 0.022252603 0.0020000001 0.0032662102
		 0.27654529 0.27588812 0.67339897 0.66057825 0.74202508 0.74314368 0.73681545 0.72897643
		 0.3052218 0.30440444 0.32298845 0.32289347 0.75475901 0.74202174 0.5967713 0.59736741
		 0.5779568 0.57797343 0.90739077 0.90880454 0.91712546 0.91564798 0.92746156 0.92597592
		 0.93735093 0.93586206 0.75698179 0.6310702 0.012349523 0.013385487 0.032272417 0.030691335
		 0.75876039 0.75813133 0.28316298 0.28162834 0.2943573 0.29465899 0.91469485 0.91459662
		 0.91923857 0.91990727 0.91459346 0.91430849 0.29522315 0.29601496 0.3244262 0.30740246
		 0.30588788 0.30575663 0.30577236 0.30697292 0.30853263 0.32530424 0.59133774 0.57460701
		 0.59395105 0.59289914 0.57431322 0.59137559 0.5928933 0.59351772 0.57926285 0.57931429
		 0.56419533 0.56414765 0.58618742 0.5859133 0.83590859 0.83431846 0.88481903 0.9072963
		 0.61914212 0.61808443 0.60960966 0.61157483 0.29372856 0.29401729 0.29861128 0.29845962
		 0.59579521 0.60289288 0.28621608 0.28689566 0.29621884 0.2952683 0.2718741 0.27213338
		 0.014619221 0.013736785 0.61787212 0.61926121 0.64252484 0.63728857 0.0020146025
		 0.0020000001 0.014095436 0.013910302 0.27167028 0.27222875 0.83749622 0.83921766
		 0.87492716 0.86179507 0.0047578393 0.0041197739 0.61298567 0.61414826 0.6084792 0.60671151
		 0.60954869 0.60440648 0.29259247 0.29203859 0.29880664 0.29894486 0.62317789 0.62164068
		 0.6421029 0.64977407 0.8253727 0.8234669 0.74865341 0.77004933 0.5843485 0.58355021
		 0.56392187 0.56386626 0.77352357 0.78838211 0.28927734 0.28863573 0.82649493 0.82834119
		 0.29221356 0.27528489 0.29631305 0.29379219 0.26988778 0.28402221 0.28589505 0.29933089
		 0.02375951 0.0066102147 0.0049732449 0.0020000001 0.0039097331 0.015050802 0.016881824
		 0.031157533 0.86427689 0.87913746 0.87913746 0.86427689 0.94462544 0.95948607 0.95948607
		 0.94462544 0.94772756 0.93286711 0.93286711 0.94772756 0.95948607 0.94462544 0.94772756
		 0.93286711 0.86427689 0.87913746 0.87913746 0.86427689 0.88583386 0.90069449 0.90069449
		 0.88583386 0.86427689 0.87913746 0.88583386 0.90069449 0.90069449 0.88583386 0.87913746
		 0.86427689 0.94462544 0.95948607 0.95948607 0.94462544 0.94772756 0.93286711 0.93286711
		 0.94772756 ;
	setAttr ".mve" -type "floatArray" 272 0.53070945 0.53070205 0.18478839 0.18478146
		 0.52412665 0.52394742 0.18073456 0.17999646 0.097438708 0.099020705 0.0053316187
		 0.0035192934 0.095218107 0.095887654 0.0035286841 0.0037096196 0.52470684 0.5247103
		 0.17910384 0.17817234 0.18102968 0.53353155 0.53372014 0.1811032 0.09603522 0.093152374
		 0.0080224182 0.0084817121 0.094303951 0.094661787 0.0040818374 0.0041952906 0.004534197
		 0.088972703 0.088972703 0.004534197 0.097960532 0.18239909 0.18239909 0.097960532
		 0.004534197 0.088972703 0.088972703 0.004534197 0.091506913 0.091506913 0.0020000001
		 0.0020000001 0.18239909 0.097960532 0.097960532 0.18239909 0.18493329 0.18493329
		 0.095426351 0.095426351 0.55165112 0.55274308 0.55706185 0.55636418 0.55123127 0.55269116
		 0.18885274 0.18959238 0.2427678 0.24481297 0.54911643 0.55055869 0.55159253 0.55012912
		 0.0022039902 0.0020000001 0.008066956 0.0086084856 0.10156605 0.10174376 0.58036244
		 0.57848644 0.57920909 0.58166039 0.57149053 0.57033074 0.58089435 0.58370322 0.32025784
		 0.32471117 0.19099683 0.19193415 0.2262516 0.22896688 0.5602628 0.55869025 0.56097007
		 0.5624299 0.29035679 0.29551709 0.55687773 0.55840522 0.5613519 0.55988997 0.0081751086
		 0.0072906734 0.10274073 0.10291806 0.18949185 0.18957427 0.18905309 0.18909392 0.61094779
		 0.65878755 0.56832373 0.5666517 0.56219631 0.56554824 0.61042106 0.61079448 0.56653035
		 0.56905127 0.5775016 0.57894558 0.541417 0.54117358 0.54043239 0.54147661 0.54068178
		 0.54035789 0.56727797 0.5687083 0.52564526 0.52546096 0.52392578 0.18085691 0.09601666
		 0.0035092072 0.0020000001 0.002180038 0.52624083 0.526223 0.17802261 0.52471077 0.0025617324
		 0.0026852232 0.0042151604 0.094238646 0.54155433 0.54307753 0.54179901 0.5402779
		 0.54351884 0.54508024 0.9969672 0.99662429 0.6738857 0.67345721 0.72596842 0.72700197
		 0.71838099 0.71722966 0.54348558 0.54285711 0.54437643 0.54591072 0.3671965 0.36745229
		 0.54519564 0.54313314 0.55089515 0.5520398 0.5424366 0.53949761 0.54166526 0.53912669
		 0.19514346 0.19522727 0.28235802 0.28224564 0.55373389 0.55224878 0.55508196 0.55803835
		 0.55780828 0.561207 0.50584084 0.50504935 0.57583058 0.57697362 0.545825 0.54456556
		 0.18885274 0.18962127 0.19852115 0.19756304 0.28176963 0.28115016 0.53975296 0.53825861
		 0.53417766 0.53569144 0.71889639 0.71952742 0.36882946 0.36911556 0.99800003 0.99796361
		 0.68185836 0.68151975 0.53360653 0.5352295 0.53163904 0.53011894 0.58142257 0.58153963
		 0.55559927 0.55718708 0.50214791 0.50222868 0.53225392 0.53223407 0.18478926 0.53069627
		 0.0037967172 0.0020000001 0.0033011427 0.097211964 0.53506821 0.53526753 0.53370827
		 0.18111023 0.092854619 0.0082819229 0.0069549773 0.0064943098 0.53647113 0.53647113
		 0.5508008 0.5508008 0.0020000001 0.0020000001 0.016329713 0.016329713 0.58767605
		 0.58767605 0.57078832 0.57078832 0.033217371 0.033217371 0.60200578 0.60200578 0.0020000001
		 0.0020000001 0.016329711 0.016329711 0.17763212 0.17763212 0.52214128 0.52214128
		 0.19196184 0.19196184 0.0020000001 0.0020000001 0.09319365 0.09319365 0.10752331
		 0.10752331 0.28315538 0.28315538 0.30004308 0.30004308 0.32085025 0.32085025 0.30396253
		 0.30396253 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[126:127]" "|TL_card_01_model|TL_card_01_modelShape.map[116:119]" "|TL_card_01_model|TL_card_01_modelShape.map[110:113]" "|TL_card_01_model|TL_card_01_modelShape.map[76:83]"  ;
createNode polyTweakUV -n "polyTweakUV1";
	setAttr ".uopa" yes;
	setAttr -s 19 ".uvtk";
	setAttr ".uvtk[76]" -type "float2" -0.8071633 0.10645181 ;
	setAttr ".uvtk[77]" -type "float2" -0.800053 0.079511344 ;
	setAttr ".uvtk[78]" -type "float2" -0.72723347 0.08988899 ;
	setAttr ".uvtk[79]" -type "float2" -0.73633248 0.12509155 ;
	setAttr ".uvtk[80]" -type "float2" -0.85398197 -0.020955861 ;
	setAttr ".uvtk[81]" -type "float2" -0.8466264 -0.037611365 ;
	setAttr ".uvtk[82]" -type "float2" 0.74088061 0.11409056 ;
	setAttr ".uvtk[83]" -type "float2" 0.73706305 0.1544283 ;
	setAttr ".uvtk[110]" -type "float2" -0.7938605 -0.066433728 ;
	setAttr ".uvtk[111]" -type "float2" -0.78784233 -0.090445489 ;
	setAttr ".uvtk[112]" -type "float2" -0.67812628 -0.15442833 ;
	setAttr ".uvtk[113]" -type "float2" -0.68731099 -0.10629186 ;
	setAttr ".uvtk[116]" -type "float2" 0.77932346 -0.09218806 ;
	setAttr ".uvtk[117]" -type "float2" 0.77040851 -0.05598551 ;
	setAttr ".uvtk[118]" -type "float2" 0.84435248 0.065367877 ;
	setAttr ".uvtk[119]" -type "float2" 0.84610486 0.086104631 ;
	setAttr ".uvtk[126]" -type "float2" 0.84938228 -0.081451535 ;
	setAttr ".uvtk[127]" -type "float2" 0.85398203 -0.060910702 ;
createNode polyMapCut -n "polyMapCut20";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 6 "e[86]" "e[126]" "e[149]" "e[176]" "e[260]" "e[291]";
createNode polyMapCut -n "polyMapCut21";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[139]" "e[151]" "e[188]" "e[262]" "e[288]";
createNode polyMapCut -n "polyMapCut22";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 5 "e[79]" "e[156]" "e[225]" "e[268]" "e[297]";
createNode polyMapCut -n "polyMapCut23";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[198]" "e[266]";
createNode polyMapCut -n "polyMapCut24";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[64]" "e[153]";
createNode Unfold3DUnfold -n "Unfold3DUnfold23";
	setAttr ".uvl" -type "Int32Array" 16 76 78 79 82 83 110
		 111 112 113 116 117 118 119 126 127 279 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.29867923 0.28117198 0.28369346 0.30125511
		 0.33095628 0.31364888 0.31353375 0.3309432 0.30422637 0.28550667 0.27597404 0.29062325
		 0.33110088 0.31355327 0.31474715 0.33196127 0.6039139 0.58668745 0.58754331 0.60495406
		 0.02108014 0.024112187 0.006614917 0.0035755029 0.02424093 0.005622651 0.031992737
		 0.01690018 0.6045382 0.58737165 0.58654314 0.60388863 0.87207568 0.87207568 0.61660194
		 0.61660194 0.87207538 0.87207538 0.61660188 0.61660188 0.87466586 0.87466586 0.61401141
		 0.61401141 0.87207568 0.61660194 0.61660194 0.87207568 0.61401141 0.61401141 0.87466586
		 0.87466586 0.87207538 0.61660188 0.61660188 0.87207538 0.61004502 0.61168098 0.60333806
		 0.60216606 0.31811401 0.31738794 0.33444703 0.33454469 0.74754149 0.74833119 0.69973707
		 0.69164383 0.60637021 0.60629237 0.5912683 0.59127015 0.071793191 0.07353539 0.079802088
		 0.078230791 0.93562973 0.93707651 0.9455092 0.9440304 0.30590302 0.30614403 0.32023984
		 0.32015386 0.94453913 0.9443199 0.93778521 0.93819928 0.58446097 0.58469677 0.68491691
		 0.67181224 0.755063 0.75620633 0.74973798 0.74172533 0.31671575 0.31590262 0.33459318
		 0.33448872 0.76807892 0.75505954 0.61017352 0.61076421 0.59121007 0.5912357 0.9259398
		 0.92735469 0.93565702 0.93417853 0.94597805 0.94449139 0.95587385 0.95438403 0.77035099
		 0.64165062 0.30405724 0.30497792 0.3202765 0.32043007 0.77216893 0.77152598 0.58444238
		 0.58442974 0.59579569 0.59601903 0.08838249 0.087736212 0.097441919 0.1007986 0.93833053
		 0.93766171 0.9330135 0.93272847 0.59661907 0.59710288 0.94268423 0.086891904 0.61053586
		 0.067149602 0.33103725 0.31363645 0.31208831 0.31195417 0.31197026 0.3131974 0.31479165
		 0.33193472 0.60386044 0.58675915 0.60653162 0.60545641 0.58645886 0.60389912 0.60545045
		 0.6060887 0.57670718 0.57674068 0.56146705 0.56143731 0.11508045 0.11328649 0.11982258
		 0.1214603 0.85718912 0.85556376 0.90718281 0.93015796 0.13866413 0.13761236 0.12835075
		 0.13034286 0.28793469 0.28823429 0.29286155 0.29268953 0.62444121 0.62246001 0.60808873
		 0.61524969 0.14809304 0.14926103 0.15674388 0.15552241 0.30170193 0.30102631 0.28651789
		 0.286764 0.02654068 0.025635188 0.63014877 0.63155031 0.65512812 0.64984524 0.013868174
		 0.013845382 0.026083944 0.025912849 0.28639507 0.28697783 0.85881191 0.8605715 0.8970719
		 0.88364893 0.016597608 0.015945988 0.17273444 0.17384006 0.1656151 0.16388689 0.61889213
		 0.6206767 0.6218586 0.61667001 0.28683114 0.28628919 0.293183 0.29330438 0.63614929
		 0.63459921 0.65480936 0.66254896 0.84641987 0.84447187 0.76800138 0.78987116 0.58194661
		 0.5811196 0.56131393 0.56127614 0.79342234 0.80861002 0.30417621 0.30353642 0.84756696
		 0.8494541 0.10860844 0.14095126 0.14868949 0.17669569 0.29864132 0.28133771 0.30283159
		 0.30025491 0.27582106 0.29026854 0.29218286 0.30591628 0.024241447 0.0067123235 0.0050390973
		 0.0020000001 0.0039520306 0.015339856 0.017211432 0.031803325 0.0020000001 0.017189715
		 0.017189715 0.0020000001 0.023716534 0.038906258 0.038906258 0.023716534 0.97863978
		 0.96345019 0.96345019 0.97863978 0.96679455 0.95160478 0.95160478 0.96679455 0.97863978
		 0.96345019 0.045433067 0.060622763 0.060622763 0.045433067 0.88250673 0.8976965 0.8976965
		 0.88250673 0.90422326 0.91941297 0.91941297 0.90422326 0.88250673 0.8976965 0.8976965
		 0.88250673 0.90422326 0.91941297 0.91941297 0.90422326 0.96679455 0.95160478 0.95160478
		 0.96679455 0.97863978 0.96345019 0.96345019 0.97863978 ;
	setAttr ".mve" -type "floatArray" 302 0.54241949 0.54241192 0.18883686 0.18882978
		 0.5356909 0.53550768 0.18469323 0.18393879 0.09955252 0.10116956 0.0054054083 0.0035529432
		 0.097282737 0.097967111 0.0035625419 0.0037474849 0.53628391 0.53628749 0.18302639
		 0.18207426 0.18499489 0.54530412 0.54549688 0.18507004 0.098117948 0.09517125 0.0081558051
		 0.0086252717 0.09634833 0.096714094 0.0041279467 0.0042439126 0.0045903251 0.090899006
		 0.090899006 0.0045903251 0.10002811 0.18633685 0.18633685 0.10002811 0.0045903251
		 0.090899006 0.090899006 0.0045903251 0.093489349 0.093489349 0.0020000001 0.0020000001
		 0.18633685 0.10002811 0.10002811 0.18633685 0.18892717 0.18892717 0.097437806 0.097437806
		 0.57151324 0.5730114 0.58258259 0.58165658 0.55971563 0.55861217 0.55829221 0.55976218
		 0.19287564 0.19363165 0.24798483 0.25007528 0.55779934 0.55925053 0.56020075 0.5587278
		 0.57517523 0.57422799 0.58062786 0.58199781 0.0033207349 0.0027792195 0.096340597
		 0.09651804 0.55122989 0.55000162 0.55285686 0.55438489 0.55088508 0.55332911 0.54324311
		 0.54167795 0.55206764 0.55357927 0.32719114 0.33174312 0.1950672 0.1960253 0.23110282
		 0.23387823 0.56727588 0.56568819 0.56809533 0.56956398 0.29662782 0.30190241 0.56563407
		 0.56717509 0.5700233 0.56855202 0.0028846494 0.0020000001 0.097513556 0.097690627
		 0.18432309 0.18440518 0.18388651 0.183927 0.62431943 0.67321873 0.54200399 0.54064423
		 0.5427255 0.54423422 0.62378103 0.62416273 0.54193377 0.54345125 0.55016494 0.55143905
		 0.58444953 0.5840416 0.57439178 0.57572091 0.5365324 0.53548735 0.53573573 0.53541154
		 0.54180038 0.5432862 0.53823662 0.58288503 0.5836457 0.58425117 0.53724313 0.53705478
		 0.53548557 0.1848183 0.098098971 0.0035426335 0.0020000001 0.0021840255 0.53785187
		 0.53783369 0.18192121 0.53628796 0.0025741737 0.0027003996 0.0042642225 0.096281581
		 0.5856719 0.58721286 0.58573627 0.58419746 0.58419257 0.58331436 0.57651228 0.57779074
		 0.99694431 0.99659377 0.6667071 0.66626912 0.58336151 0.58449137 0.57592118 0.57462376
		 0.58415657 0.58352453 0.58511674 0.58666646 0.71960264 0.7207666 0.36648276 0.36673206
		 0.57664776 0.57474661 0.58471543 0.58563471 0.5534004 0.55548835 0.55277777 0.5498063
		 0.55338997 0.55082935 0.19287564 0.19295846 0.28083366 0.28072673 0.56565434 0.56415361
		 0.56695127 0.56993991 0.56831294 0.57174456 0.49494031 0.4941313 0.56648022 0.56764859
		 0.55764705 0.55637771 0.57491034 0.57601553 0.58452725 0.58307815 0.19533049 0.19629492
		 0.28028083 0.27966222 0.5803681 0.57885021 0.5748055 0.57633799 0.72126979 0.72190833
		 0.36807296 0.36835212 0.99800003 0.99796277 0.67485631 0.67451024 0.57769638 0.57932794
		 0.57545859 0.57392073 0.57219607 0.57231569 0.56598532 0.5675934 0.49116561 0.49124816
		 0.57446003 0.57652259 0.58777446 0.58291936 0.54399818 0.54397792 0.18883774 0.54240602
		 0.0038365114 0.0020000001 0.0033299609 0.099320754 0.54687482 0.54707855 0.54548472
		 0.18507724 0.094866902 0.0084210569 0.007064722 0.0065938514 0.57240444 0.57240444
		 0.58705151 0.58705151 0.57400966 0.57400966 0.58865672 0.58865672 0.29199734 0.29199734
		 0.27473557 0.27473557 0.60059011 0.60059011 0.58332843 0.58332843 0.30664447 0.30664447
		 0.5741598 0.5741598 0.58880687 0.58880687 0.1815221 0.1815221 0.5336616 0.5336616
		 0.18152209 0.18152209 0.53366166 0.53366166 0.0020000001 0.0020000001 0.09521345
		 0.09521345 0.0020000001 0.0020000001 0.095213376 0.095213376 0.32785466 0.32785466
		 0.31059292 0.31059292 0.019261762 0.019261762 0.0020000001 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[134:135]" "|TL_card_01_model|TL_card_01_modelShape.map[122:125]" "|TL_card_01_model|TL_card_01_modelShape.map[116:119]" "|TL_card_01_model|TL_card_01_modelShape.map[88:89]" "|TL_card_01_model|TL_card_01_modelShape.map[80:83]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold24";
	setAttr ".uvl" -type "Int32Array" 16 64 65 66 67 90 91
		 92 93 94 95 100 101 114 115 120 121 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.28500563 0.26830527 0.27071053 0.2874628
		 0.3169696 0.30045986 0.30035007 0.31695712 0.29029712 0.27244017 0.26334688 0.27732095
		 0.31710753 0.30036867 0.30150753 0.31792828 0.57734692 0.5609144 0.5617308 0.5783391
		 0.020200759 0.023093062 0.0064022206 0.0035028898 0.023215871 0.0054556872 0.030610407
		 0.016213449 0.57794243 0.56156707 0.56077677 0.57732278 0.83269513 0.83269513 0.58899587
		 0.58899587 0.83269483 0.83269483 0.58899581 0.58899581 0.83516592 0.83516592 0.58652472
		 0.58652472 0.83269513 0.58899587 0.58899587 0.83269513 0.58652472 0.58652472 0.83516592
		 0.83516592 0.83269483 0.58899581 0.58899581 0.83269483 0.14877567 0.15033624 0.14237779
		 0.1412598 0.36508554 0.36439294 0.38066581 0.38075897 0.69191128 0.69318229 0.65086943
		 0.64050943 0.64005637 0.6399821 0.62565047 0.62565225 0.12736571 0.12902761 0.13500547
		 0.1335066 0.76617807 0.76755816 0.77560222 0.77419156 0.47834349 0.47857338 0.49201953
		 0.49193752 0.11655225 0.11634313 0.11010959 0.11050458 0.74406302 0.74428791 0.65418673
		 0.63966334 0.70221698 0.7036494 0.71161669 0.70176429 0.36375174 0.36297607 0.38080522
		 0.38070557 0.74988723 0.73402441 0.64368439 0.64424783 0.62559491 0.62561935 0.75693476
		 0.75828445 0.76620412 0.76479375 0.77604944 0.77463132 0.78548914 0.78406799 0.68506086
		 0.68329877 0.47658277 0.47746101 0.49205449 0.492201 0.69584686 0.69395071 0.74404526
		 0.74403322 0.7548753 0.75508839 0.091375448 0.090758957 0.10001735 0.10321932 0.76875442
		 0.76811641 0.76368243 0.76341051 0.75566077 0.75612223 0.11478282 0.089953564 0.14924388
		 0.12293615 0.31704685 0.30044803 0.29897124 0.29884326 0.29885861 0.30002922 0.30154997
		 0.31790295 0.5772959 0.56098282 0.579844 0.57881832 0.56069636 0.57733279 0.57881266
		 0.57942146 0.90386975 0.9039017 0.889332 0.88930362 0.0081737032 0.0064624269 0.012697295
		 0.01425954 0.30146399 0.29991356 0.34915352 0.37106979 0.081467941 0.080464646 0.071629904
		 0.07353019 0.62840647 0.62869221 0.63310623 0.63294214 0.59662646 0.59473658 0.58102763
		 0.58785856 0.062468074 0.063582227 0.070720211 0.069555029 0.90509105 0.9044466 0.89060682
		 0.8908416 0.64261168 0.64174795 0.60207093 0.60340786 0.62589902 0.62085962 0.63052326
		 0.63050151 0.64217597 0.64201277 0.8904897 0.89104557 0.30301198 0.30469048 0.33950862
		 0.32670429 0.63312685 0.6325053 0.05258419 0.053638853 0.045792971 0.044144414 0.59133309
		 0.5930354 0.59416282 0.58921337 0.62735373 0.62683678 0.63341284 0.63352865 0.60779494
		 0.60631627 0.62559497 0.63297784 0.29119107 0.28933287 0.21638681 0.23724863 0.90886772
		 0.90807879 0.88918597 0.8891499 0.24063616 0.25512385 0.90745127 0.90684098 0.29228529
		 0.29408547 0.0020000001 0.083649658 0.063037023 0.056362864 0.28496948 0.26846337
		 0.28896663 0.28650868 0.26320097 0.27698258 0.27880868 0.29190913 0.023216365 0.0064951377
		 0.0048990287 0.0020000001 0.0038620636 0.014725038 0.016510354 0.030429725 0.19623078
		 0.21072042 0.21072042 0.19623078 0.17607476 0.19056439 0.19056439 0.17607476 0.89602512
		 0.88153565 0.88153565 0.89602512 0.79174972 0.80623937 0.80623937 0.79174972 0.89602512
		 0.88153565 0.15591873 0.17040837 0.17040837 0.15591873 0.8412236 0.85571325 0.85571325
		 0.8412236 0.86137962 0.87586921 0.87586921 0.86137962 0.8412236 0.85571325 0.85571325
		 0.8412236 0.86137962 0.87586921 0.87586921 0.86137962 0.79174972 0.80623937 0.80623937
		 0.79174972 0.89602512 0.88153565 0.88153565 0.89602512 ;
	setAttr ".mve" -type "floatArray" 302 0.5175122 0.51750499 0.18022576 0.18021901
		 0.51109368 0.51091892 0.17627311 0.17555343 0.095056437 0.096598946 0.0052484567
		 0.0034813697 0.092891261 0.093544096 0.003490526 0.003666945 0.51165938 0.51166278
		 0.17468308 0.17377484 0.17656086 0.52026385 0.52044773 0.17663255 0.093687981 0.090877093
		 0.0078720907 0.0083199209 0.091999918 0.092348829 0.0040298719 0.004140493 0.0044709402
		 0.08680176 0.08680176 0.0044709402 0.095408365 0.17773923 0.17773923 0.095408365
		 0.0044709402 0.08680176 0.08680176 0.0044709402 0.089272715 0.089272715 0.0020000001
		 0.0020000001 0.17773923 0.095408365 0.095408365 0.17773923 0.18021017 0.18021017
		 0.092937447 0.092937447 0.52401423 0.52544338 0.53457344 0.53369009 0.73068231 0.7296297
		 0.72932452 0.73072672 0.18387491 0.18456303 0.27223739 0.27805012 0.72885436 0.73023868
		 0.73114508 0.72974002 0.52514493 0.52424133 0.53034621 0.53165305 0.18513477 0.1846182
		 0.27386746 0.27403671 0.7006458 0.69947416 0.70219779 0.70365536 0.53647202 0.53880346
		 0.52918226 0.52768928 0.70144492 0.70288688 0.36287984 0.37326261 0.18441206 0.18519989
		 0.22956985 0.23647927 0.73789412 0.73637962 0.73867583 0.74007678 0.29438213 0.30582947
		 0.73632801 0.73779798 0.74051493 0.73911142 0.18471879 0.18387491 0.27498636 0.27515528
		 0.35779494 0.35787323 0.35737848 0.35741711 0.67359555 0.67375159 0.69184512 0.690548
		 0.69253337 0.69397253 0.67569429 0.67615414 0.69177812 0.69322568 0.6996299 0.7008453
		 0.53416586 0.53377676 0.52457172 0.52583957 0.6937713 0.69277442 0.69301134 0.69270211
		 0.69165087 0.69306821 0.52440649 0.53267348 0.53558755 0.53380257 0.51257437 0.51239467
		 0.51089782 0.17639241 0.093669876 0.0034715352 0.0020000001 0.0021755439 0.51315504
		 0.5131377 0.17362884 0.5116632 0.0025477107 0.0026681188 0.0041598668 0.091936246
		 0.75356364 0.75503361 0.75362509 0.75215715 0.53423458 0.53339684 0.52690828 0.52812779
		 0.99699295 0.99665856 0.68197596 0.68155819 0.53319573 0.53427356 0.52609837 0.52486074
		 0.75211817 0.75151527 0.75303411 0.75451237 0.68632567 0.68743598 0.34948069 0.34971848
		 0.52679813 0.5249846 0.53449404 0.53537095 0.7094124 0.71140409 0.70881844 0.70598394
		 0.70940244 0.70695984 0.18387491 0.18395391 0.26777905 0.26767704 0.72110158 0.71967
		 0.72233874 0.72518963 0.72363764 0.72691107 0.51812571 0.51735401 0.58636844 0.58748299
		 0.71346331 0.7122525 0.52510852 0.52616274 0.53428215 0.53289986 0.18621661 0.18713659
		 0.2672517 0.26666158 0.74850428 0.74705637 0.7431981 0.74465996 0.68791598 0.68852508
		 0.35099757 0.35126388 0.99800003 0.99796444 0.6897496 0.68941951 0.74595571 0.7475121
		 0.74382108 0.74235409 0.59182084 0.59193498 0.72141731 0.72295123 0.514525 0.51460373
		 0.52495056 0.52667207 0.53741205 0.5327484 0.51901811 0.5189988 0.18022661 0.51749933
		 0.0037518686 0.0020000001 0.0032686645 0.094835356 0.52176219 0.5219565 0.52043617
		 0.17663942 0.090586774 0.008125118 0.0068312949 0.0063821259 0.52347744 0.52347744
		 0.53744942 0.53744942 0.52370453 0.52370453 0.53767651 0.53767651 0.27863169 0.27863169
		 0.26216549 0.26216549 0.18387491 0.18387491 0.20034102 0.20034102 0.29260373 0.29260373
		 0.52393162 0.52393162 0.53790367 0.53790367 0.17324813 0.17324813 0.50915796 0.50915796
		 0.17324811 0.17324811 0.50915796 0.50915796 0.0020000001 0.0020000001 0.090917349
		 0.090917349 0.0020000001 0.0020000001 0.090917274 0.090917274 0.4440403 0.4440403
		 0.46050647 0.46050647 0.018466188 0.018466188 0.0020000001 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[120:121]" "|TL_card_01_model|TL_card_01_modelShape.map[114:115]" "|TL_card_01_model|TL_card_01_modelShape.map[100:101]" "|TL_card_01_model|TL_card_01_modelShape.map[90:95]" "|TL_card_01_model|TL_card_01_modelShape.map[64:67]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold25";
	setAttr ".uvl" -type "Int32Array" 16 164 165 166 167 200 201
		 202 203 222 223 224 225 230 231 234 235 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.28500551 0.26830515 0.27071041 0.28746268
		 0.31696928 0.30045953 0.30034974 0.31695679 0.290297 0.27244005 0.26334676 0.27732083
		 0.3171072 0.30036834 0.3015072 0.31792796 0.5773465 0.56091398 0.56173038 0.57833868
		 0.020200752 0.023093052 0.0064022187 0.0035028891 0.023215862 0.0054556858 0.030610396
		 0.016213443 0.57794201 0.56156665 0.56077635 0.57732236 0.83269435 0.83269435 0.58899522
		 0.58899522 0.83269405 0.83269405 0.58899516 0.58899516 0.83516514 0.83516514 0.58652407
		 0.58652407 0.83269435 0.58899522 0.58899522 0.83269435 0.58652407 0.58652407 0.83516514
		 0.83516514 0.83269405 0.58899516 0.58899516 0.83269405 0.1487755 0.15033607 0.14237764
		 0.14125966 0.36508515 0.36439255 0.38066539 0.38075855 0.69191056 0.69318157 0.65086871
		 0.64050871 0.64005584 0.63998157 0.62564993 0.62565172 0.12736557 0.12902747 0.13500533
		 0.13350645 0.76617718 0.76755726 0.77560133 0.77419066 0.47834295 0.47857285 0.492019
		 0.49193698 0.11655211 0.11634298 0.11010946 0.11050445 0.74406236 0.74428725 0.65418601
		 0.63966262 0.70221627 0.70364869 0.71161592 0.70176351 0.36375135 0.36297569 0.38080481
		 0.38070515 0.74988645 0.73402363 0.64368385 0.64424729 0.62559438 0.62561882 0.75693393
		 0.75828362 0.76620322 0.76479286 0.77604854 0.77463043 0.78548825 0.78406709 0.68506008
		 0.68329799 0.47658223 0.47746047 0.49205396 0.49220046 0.69584608 0.69394994 0.7440446
		 0.74403256 0.75487465 0.75508773 0.091375351 0.09075886 0.10001725 0.10321923 0.76875353
		 0.76811552 0.76368159 0.76340967 0.75566012 0.75612158 0.11478268 0.089953467 0.14924373
		 0.12293601 0.31704652 0.3004477 0.29897091 0.29884294 0.29885828 0.30002889 0.30154964
		 0.31790262 0.57729548 0.56098241 0.57984358 0.5788179 0.56069595 0.57733238 0.57881224
		 0.57942104 0.90386891 0.90390086 0.88933122 0.88930285 0.0081736976 0.0064624227
		 0.012697287 0.01425953 0.30146343 0.29991302 0.34915301 0.37106916 0.081467852 0.080464557
		 0.071629822 0.073530108 0.62840575 0.62869149 0.63310552 0.63294142 0.5966258 0.59473592
		 0.58102697 0.5878579 0.062468007 0.063582152 0.070720144 0.06955497 0.90509027 0.90444583
		 0.89060605 0.89084083 0.64261097 0.64174724 0.60207027 0.6034072 0.62589836 0.62085897
		 0.63052255 0.63050079 0.64217526 0.64201206 0.89048892 0.8910448 0.30301145 0.30468994
		 0.33950788 0.32670364 0.63312614 0.63250458 0.052584141 0.053638808 0.045792926 0.04414437
		 0.59133244 0.59303474 0.59416217 0.58921272 0.62735301 0.62683606 0.63341212 0.63352793
		 0.60779428 0.60631561 0.62559432 0.63297719 0.29119051 0.28933233 0.21638657 0.23724839
		 0.90886688 0.90807796 0.88918519 0.88914913 0.24063577 0.25512335 0.9074505 0.90684021
		 0.29228476 0.29408494 0.0020000001 0.083649576 0.063036978 0.056362823 0.28496936
		 0.26846325 0.28896651 0.28650856 0.26320085 0.27698246 0.27880856 0.29190901 0.023216356
		 0.0064951358 0.0048990273 0.0020000001 0.0038620627 0.014725032 0.016510349 0.030429713
		 0.19623056 0.2107202 0.2107202 0.19623056 0.17607456 0.1905642 0.1905642 0.17607456
		 0.89602411 0.88153464 0.88153464 0.89602411 0.80623847 0.79174882 0.79174882 0.80623847
		 0.89602411 0.88153464 0.15591857 0.17040817 0.17040817 0.15591857 0.84122264 0.85571229
		 0.85571229 0.84122264 0.86137861 0.8758682 0.8758682 0.86137861 0.84122264 0.85571229
		 0.85571229 0.84122264 0.86137861 0.8758682 0.8758682 0.86137861 0.80623847 0.79174882
		 0.79174882 0.80623847 0.89602411 0.88153464 0.88153464 0.89602411 ;
	setAttr ".mve" -type "floatArray" 302 0.51751202 0.51750481 0.18022569 0.18021894
		 0.5110935 0.51091874 0.17627305 0.17555337 0.0950564 0.096598908 0.0052484553 0.003481369
		 0.092891231 0.093544066 0.0034905253 0.0036669443 0.5116592 0.5116626 0.17468302
		 0.17377478 0.17656079 0.52026367 0.52044755 0.17663248 0.093687944 0.090877056 0.0078720879
		 0.0083199181 0.091999888 0.092348799 0.004029871 0.0041404921 0.0044709393 0.08680173
		 0.08680173 0.0044709393 0.095408328 0.17773916 0.17773916 0.095408328 0.0044709393
		 0.08680173 0.08680173 0.0044709393 0.089272678 0.089272678 0.0020000001 0.0020000001
		 0.17773916 0.095408328 0.095408328 0.17773916 0.1802101 0.1802101 0.09293741 0.09293741
		 0.52401406 0.5254432 0.53457326 0.53368992 0.73068202 0.7296294 0.72932422 0.73072642
		 0.18387483 0.18456295 0.27223727 0.27805001 0.728854 0.73023832 0.73114473 0.72973967
		 0.52514476 0.52424115 0.53034604 0.53165287 0.18513469 0.18461813 0.27386734 0.27403659
		 0.70064551 0.69947386 0.70219749 0.70365506 0.53647184 0.53880328 0.52918208 0.5276891
		 0.70144463 0.70288658 0.36287969 0.37326247 0.18441199 0.18519981 0.22956975 0.23647916
		 0.73789382 0.73637933 0.73867548 0.74007642 0.29438201 0.30582935 0.73632765 0.73779762
		 0.74051458 0.73911107 0.18471871 0.18387483 0.27498624 0.27515516 0.35779479 0.35787308
		 0.35737833 0.35741696 0.67359525 0.67375129 0.69184482 0.6905477 0.69253308 0.69397223
		 0.67569399 0.67615384 0.69177783 0.69322538 0.6996296 0.700845 0.53416568 0.53377658
		 0.52457148 0.52583933 0.69377106 0.69277418 0.69301111 0.69270188 0.69165057 0.69306791
		 0.52440631 0.5326733 0.53558737 0.53380239 0.5125742 0.51239449 0.51089764 0.17639235
		 0.093669847 0.0034715345 0.0020000001 0.0021755437 0.51315486 0.51313752 0.17362878
		 0.51166302 0.0025477104 0.0026681186 0.0041598659 0.091936216 0.75356328 0.75503325
		 0.75362474 0.75215679 0.53423434 0.5333966 0.52690804 0.52812755 0.99699306 0.99665874
		 0.6819759 0.68155801 0.53319556 0.53427339 0.52609819 0.52486056 0.75211781 0.75151491
		 0.75303376 0.75451201 0.68632537 0.68743569 0.34948054 0.34971833 0.52679795 0.52498442
		 0.53449386 0.53537077 0.7094121 0.71140379 0.70881814 0.70598364 0.70940214 0.70695955
		 0.18387483 0.18395384 0.26777893 0.26767692 0.72110128 0.7196697 0.72233844 0.72518933
		 0.72363734 0.72691077 0.51812583 0.51735419 0.58636838 0.58748293 0.71346301 0.7122522
		 0.52510834 0.52616256 0.53428197 0.53289968 0.18621653 0.18713652 0.26725158 0.26666147
		 0.74850392 0.74705601 0.74319774 0.7446596 0.68791568 0.68852478 0.35099742 0.35126373
		 0.99800003 0.9979645 0.68974996 0.68941969 0.74595535 0.74751174 0.74382073 0.74235374
		 0.59182113 0.59193522 0.72141701 0.72295094 0.51452512 0.51460385 0.52495033 0.52667189
		 0.53741187 0.53274822 0.51901793 0.51899862 0.18022653 0.51749915 0.0037518679 0.0020000001
		 0.0032686638 0.094835319 0.52176195 0.52195626 0.52043599 0.17663935 0.090586737
		 0.0081251152 0.006831293 0.006382124 0.5234772 0.5234772 0.53744918 0.53744918 0.52370435
		 0.52370435 0.53767633 0.53767633 0.27863163 0.27863163 0.26216543 0.26216543 0.46050632
		 0.46050632 0.44404021 0.44404021 0.29260367 0.29260367 0.52393144 0.52393144 0.53790349
		 0.53790349 0.17324807 0.17324807 0.50915778 0.50915778 0.17324807 0.17324807 0.50915784
		 0.50915784 0.0020000001 0.0020000001 0.090917312 0.090917312 0.0020000001 0.0020000001
		 0.090917252 0.090917252 0.20034099 0.20034099 0.18387483 0.18387483 0.018466184 0.018466184
		 0.0020000001 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[234:235]" "|TL_card_01_model|TL_card_01_modelShape.map[230:231]" "|TL_card_01_model|TL_card_01_modelShape.map[222:225]" "|TL_card_01_model|TL_card_01_modelShape.map[200:203]" "|TL_card_01_model|TL_card_01_modelShape.map[164:167]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold26";
	setAttr ".uvl" -type "Int32Array" 16 164 165 166 167 200 201
		 202 203 222 223 224 225 230 231 234 235 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.28500551 0.26830515 0.27071041 0.28746268
		 0.31696928 0.30045953 0.30034974 0.31695679 0.290297 0.27244005 0.26334676 0.27732083
		 0.3171072 0.30036834 0.3015072 0.31792796 0.5773465 0.56091398 0.56173038 0.57833868
		 0.020200752 0.023093056 0.0064022234 0.0035028888 0.02321586 0.0054556844 0.030610392
		 0.016213439 0.57794201 0.56156665 0.56077635 0.57732236 0.83269435 0.83269435 0.58899516
		 0.58899516 0.83269399 0.83269399 0.5889951 0.5889951 0.83516514 0.83516514 0.58652401
		 0.58652401 0.83269435 0.58899516 0.58899516 0.83269435 0.58652401 0.58652401 0.83516508
		 0.83516508 0.83269399 0.5889951 0.5889951 0.83269399 0.14877549 0.15033606 0.14237763
		 0.14125964 0.36508512 0.36439252 0.38066536 0.38075852 0.69191051 0.69318151 0.65086865
		 0.64050865 0.64005584 0.63998157 0.62564993 0.62565172 0.12736556 0.12902746 0.13500531
		 0.13350643 0.76617712 0.7675572 0.77560127 0.7741906 0.47834292 0.47857282 0.49201897
		 0.49193695 0.11655209 0.11634298 0.11010946 0.11050444 0.7440623 0.74428719 0.65418595
		 0.63966256 0.70221621 0.70364863 0.71161586 0.70176345 0.36375132 0.36297566 0.38080478
		 0.38070512 0.74988639 0.73402357 0.64368385 0.64424729 0.62559438 0.62561882 0.75693387
		 0.75828356 0.76620317 0.7647928 0.77604848 0.77463037 0.78548819 0.78406703 0.68506008
		 0.68329799 0.4765822 0.47746044 0.49205393 0.49220043 0.69584608 0.69394994 0.74404454
		 0.7440325 0.75487459 0.75508767 0.091375336 0.090758845 0.10001724 0.10321921 0.76875347
		 0.76811546 0.76368153 0.76340961 0.75566006 0.75612152 0.11478267 0.08995346 0.14924371
		 0.122936 0.31704652 0.3004477 0.29897091 0.29884294 0.29885828 0.30002889 0.30154964
		 0.31790262 0.57729548 0.56098241 0.57984358 0.5788179 0.56069595 0.57733238 0.57881224
		 0.57942104 0.90386891 0.90390086 0.88933122 0.88930285 0.008173693 0.0064624189 0.012697282
		 0.014259526 0.3014634 0.29991299 0.34915298 0.37106913 0.081467845 0.080464549 0.071629822
		 0.073530108 0.62840575 0.62869149 0.63310552 0.63294142 0.59662575 0.59473586 0.58102691
		 0.58785784 0.062468003 0.063582145 0.070720144 0.06955497 0.90509021 0.90444577 0.89060599
		 0.89084077 0.64261091 0.64174718 0.60207027 0.6034072 0.62589836 0.62085897 0.63052249
		 0.63050073 0.6421752 0.642012 0.89048886 0.89104474 0.30301142 0.30468991 0.33950788
		 0.32670361 0.63312608 0.63250452 0.052584138 0.053638805 0.045792922 0.044144366
		 0.59133244 0.59303474 0.59416217 0.58921272 0.62735301 0.62683606 0.63341212 0.63352793
		 0.60779423 0.60631555 0.62559426 0.63297713 0.29119048 0.28933233 0.21638656 0.23724838
		 0.90886688 0.90807796 0.88918519 0.88914913 0.24063575 0.25512332 0.90745044 0.90684015
		 0.29228473 0.29408491 0.0020000001 0.083649568 0.063036993 0.056362819 0.28496936
		 0.26846325 0.28896651 0.28650856 0.26320085 0.27698246 0.27880856 0.29190901 0.023216359
		 0.006495141 0.0048990324 0.0020000001 0.0038620613 0.014725029 0.016510345 0.03042971
		 0.19623056 0.2107202 0.2107202 0.19623056 0.17607455 0.19056419 0.19056419 0.17607455
		 0.89602405 0.88153458 0.88153458 0.89602405 0.79174876 0.80623841 0.80623841 0.79174876
		 0.89602405 0.88153458 0.15591855 0.17040814 0.17040814 0.15591855 0.84122258 0.85571223
		 0.85571223 0.84122258 0.86137861 0.8758682 0.8758682 0.86137861 0.84122258 0.85571223
		 0.85571223 0.84122258 0.86137861 0.8758682 0.8758682 0.86137861 0.79174876 0.80623841
		 0.80623841 0.79174876 0.89602405 0.88153458 0.88153458 0.89602405 ;
	setAttr ".mve" -type "floatArray" 302 0.51751202 0.51750481 0.18022569 0.18021894
		 0.5110935 0.51091874 0.17627305 0.17555337 0.0950564 0.096598908 0.0052484553 0.0034813688
		 0.092891231 0.093544066 0.0034905251 0.0036669441 0.5116592 0.5116626 0.17468302
		 0.17377478 0.17656079 0.52026367 0.52044755 0.17663248 0.093687944 0.090877056 0.0078720916
		 0.0083199218 0.091999888 0.092348799 0.004029871 0.0041404921 0.0044709393 0.08680173
		 0.08680173 0.0044709393 0.095408328 0.17773916 0.17773916 0.095408328 0.0044709393
		 0.08680173 0.08680173 0.0044709393 0.089272678 0.089272678 0.0020000001 0.0020000001
		 0.17773916 0.095408328 0.095408328 0.17773916 0.1802101 0.1802101 0.09293741 0.09293741
		 0.524014 0.52544314 0.5345732 0.53368986 0.73068196 0.72962934 0.72932416 0.73072636
		 0.18387483 0.18456295 0.27223727 0.27805001 0.728854 0.73023832 0.73114473 0.72973967
		 0.52514476 0.52424115 0.53034604 0.53165287 0.18513469 0.18461813 0.27386734 0.27403659
		 0.70064545 0.6994738 0.70219743 0.703655 0.53647178 0.53880322 0.52918208 0.5276891
		 0.70144463 0.70288658 0.36287969 0.37326247 0.18441199 0.18519981 0.22956973 0.23647916
		 0.73789376 0.73637927 0.73867542 0.74007636 0.29438201 0.30582935 0.73632765 0.73779762
		 0.74051458 0.73911107 0.18471871 0.18387483 0.27498624 0.27515516 0.35779479 0.35787308
		 0.35737833 0.35741696 0.67359525 0.67375129 0.69184476 0.69054765 0.69253302 0.69397217
		 0.67569399 0.67615384 0.69177783 0.69322538 0.6996296 0.700845 0.53416562 0.53377652
		 0.52457148 0.52583933 0.69377106 0.69277418 0.69301111 0.69270188 0.69165057 0.69306791
		 0.52440631 0.53267324 0.53558731 0.53380239 0.5125742 0.51239449 0.51089764 0.17639235
		 0.093669847 0.0034715342 0.0020000001 0.0021755437 0.51315486 0.51313752 0.17362878
		 0.51166302 0.0025477104 0.0026681183 0.0041598659 0.091936216 0.75356328 0.75503325
		 0.75362474 0.75215679 0.53423429 0.53339654 0.52690804 0.52812755 0.99699306 0.99665874
		 0.6819759 0.68155801 0.5331955 0.53427333 0.52609813 0.5248605 0.75211781 0.75151491
		 0.75303376 0.75451201 0.68632537 0.68743569 0.34948054 0.34971833 0.52679795 0.52498442
		 0.5344938 0.53537071 0.70941204 0.71140373 0.70881808 0.70598358 0.70940208 0.70695949
		 0.18387483 0.18395384 0.26777893 0.26767692 0.72110122 0.71966964 0.72233838 0.72518927
		 0.72363728 0.72691071 0.51812589 0.51735419 0.58636844 0.58748293 0.71346295 0.71225214
		 0.52510828 0.52616251 0.53428191 0.53289962 0.18621653 0.18713652 0.26725158 0.26666147
		 0.74850392 0.74705601 0.74319774 0.7446596 0.68791568 0.68852478 0.35099742 0.35126373
		 0.99800003 0.9979645 0.68974996 0.68941969 0.74595535 0.74751174 0.74382073 0.74235374
		 0.59182119 0.59193528 0.72141695 0.72295088 0.51452512 0.51460391 0.52495027 0.52667183
		 0.53741181 0.53274816 0.51901793 0.51899862 0.18022653 0.51749915 0.0037518679 0.0020000001
		 0.0032686635 0.094835319 0.52176195 0.52195626 0.52043599 0.17663935 0.090586737
		 0.0081251189 0.0068312967 0.0063821273 0.5234772 0.5234772 0.53744918 0.53744918
		 0.52370435 0.52370435 0.53767633 0.53767633 0.2786316 0.2786316 0.26216543 0.26216543
		 0.18387483 0.18387483 0.20034094 0.20034094 0.29260364 0.29260364 0.52393144 0.52393144
		 0.53790349 0.53790349 0.17324805 0.17324805 0.50915772 0.50915772 0.17324805 0.17324805
		 0.50915778 0.50915778 0.0020000001 0.0020000001 0.090917304 0.090917304 0.0020000001
		 0.0020000001 0.090917245 0.090917245 0.44404015 0.44404015 0.46050629 0.46050629
		 0.018466182 0.018466182 0.0020000001 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[234:235]" "|TL_card_01_model|TL_card_01_modelShape.map[230:231]" "|TL_card_01_model|TL_card_01_modelShape.map[222:225]" "|TL_card_01_model|TL_card_01_modelShape.map[200:203]" "|TL_card_01_model|TL_card_01_modelShape.map[164:167]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold27";
	setAttr ".uvl" -type "Int32Array" 16 164 165 166 167 200 201
		 202 203 222 223 224 225 230 231 234 235 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.28500551 0.26830515 0.27071041 0.28746268
		 0.31696928 0.30045953 0.30034974 0.31695679 0.290297 0.27244005 0.26334676 0.27732083
		 0.3171072 0.30036834 0.3015072 0.31792796 0.5773465 0.56091398 0.56173038 0.57833868
		 0.020200752 0.02309306 0.0064022285 0.0035028886 0.023215858 0.005455683 0.030610388
		 0.016213436 0.57794201 0.56156665 0.56077635 0.57732236 0.83269435 0.83269435 0.58899516
		 0.58899516 0.83269399 0.83269399 0.5889951 0.5889951 0.83516514 0.83516514 0.58652401
		 0.58652401 0.83269435 0.58899516 0.58899516 0.83269435 0.58652401 0.58652401 0.83516508
		 0.83516508 0.83269399 0.5889951 0.5889951 0.83269399 0.14877549 0.15033606 0.14237763
		 0.14125964 0.36508512 0.36439252 0.38066536 0.38075852 0.69191051 0.69318151 0.65086865
		 0.64050865 0.64005584 0.63998157 0.62564993 0.62565172 0.12736556 0.12902746 0.13500531
		 0.13350643 0.76617712 0.7675572 0.77560127 0.7741906 0.47834292 0.47857282 0.49201897
		 0.49193695 0.11655208 0.11634297 0.11010946 0.11050444 0.7440623 0.74428719 0.65418595
		 0.63966256 0.70221621 0.70364863 0.71161586 0.70176345 0.36375132 0.36297566 0.38080478
		 0.38070512 0.74988639 0.73402357 0.64368385 0.64424729 0.62559438 0.62561882 0.75693387
		 0.75828356 0.76620317 0.7647928 0.77604848 0.77463037 0.78548819 0.78406703 0.68506008
		 0.68329799 0.4765822 0.47746044 0.49205393 0.49220043 0.69584608 0.69394994 0.74404454
		 0.7440325 0.75487459 0.75508767 0.091375336 0.090758845 0.10001724 0.10321922 0.76875347
		 0.76811546 0.76368153 0.76340961 0.75566006 0.75612152 0.11478268 0.08995346 0.14924371
		 0.122936 0.31704652 0.3004477 0.29897091 0.29884294 0.29885828 0.30002889 0.30154964
		 0.31790262 0.57729548 0.56098241 0.57984358 0.5788179 0.56069595 0.57733238 0.57881224
		 0.57942104 0.90386891 0.90390086 0.88933122 0.88930285 0.008173733 0.0064624543 0.012697298
		 0.014259548 0.3014634 0.29991299 0.34915298 0.37106913 0.081467845 0.080464549 0.071629822
		 0.073530108 0.62840575 0.62869149 0.63310552 0.63294142 0.5966258 0.59473592 0.58102691
		 0.58785784 0.062468003 0.063582152 0.070720151 0.06955497 0.90509021 0.90444577 0.89060599
		 0.89084077 0.64261091 0.64174718 0.60207027 0.6034072 0.62589836 0.62085897 0.63052249
		 0.63050073 0.6421752 0.642012 0.89048886 0.89104474 0.30301142 0.30468991 0.33950788
		 0.32670361 0.63312608 0.63250452 0.052584138 0.053638805 0.045792922 0.044144366
		 0.59133244 0.59303474 0.59416217 0.58921272 0.62735301 0.62683606 0.63341212 0.63352793
		 0.60779428 0.60631561 0.62559426 0.63297713 0.29119048 0.28933233 0.21638656 0.23724838
		 0.90886688 0.90807796 0.88918519 0.88914913 0.24063575 0.25512332 0.90745044 0.90684015
		 0.29228473 0.29408491 0.0020000001 0.083649568 0.063036978 0.056362819 0.28496936
		 0.26846325 0.28896651 0.28650856 0.26320085 0.27698246 0.27880856 0.29190901 0.023216363
		 0.0064951461 0.0048990375 0.0020000001 0.0038620599 0.014725027 0.016510341 0.030429706
		 0.19623056 0.2107202 0.2107202 0.19623056 0.17607455 0.19056419 0.19056419 0.17607455
		 0.89602405 0.88153458 0.88153458 0.89602405 0.80623841 0.79174876 0.79174876 0.80623841
		 0.89602405 0.88153458 0.15591855 0.17040813 0.17040813 0.15591855 0.84122258 0.85571223
		 0.85571223 0.84122258 0.86137861 0.8758682 0.8758682 0.86137861 0.84122258 0.85571223
		 0.85571223 0.84122258 0.86137861 0.8758682 0.8758682 0.86137861 0.80623841 0.79174876
		 0.79174876 0.80623841 0.89602405 0.88153458 0.88153458 0.89602405 ;
	setAttr ".mve" -type "floatArray" 302 0.51751202 0.51750481 0.18022569 0.18021894
		 0.5110935 0.51091874 0.17627305 0.17555337 0.0950564 0.096598908 0.0052484553 0.0034813685
		 0.092891231 0.093544066 0.0034905248 0.0036669439 0.5116592 0.5116626 0.17468302
		 0.17377478 0.17656079 0.52026367 0.52044755 0.17663248 0.093687944 0.090877056 0.0078720953
		 0.0083199255 0.091999888 0.092348799 0.004029871 0.0041404921 0.0044709388 0.086801715
		 0.086801715 0.0044709388 0.095408313 0.17773914 0.17773914 0.095408313 0.0044709388
		 0.086801715 0.086801715 0.0044709388 0.089272663 0.089272663 0.0020000001 0.0020000001
		 0.17773914 0.095408313 0.095408313 0.17773914 0.18021008 0.18021008 0.092937395 0.092937395
		 0.524014 0.52544314 0.5345732 0.53368986 0.73068196 0.72962934 0.72932416 0.73072636
		 0.18387482 0.18456294 0.27223724 0.27804998 0.728854 0.73023832 0.73114473 0.72973967
		 0.52514476 0.52424115 0.53034604 0.53165287 0.18513468 0.18461812 0.27386731 0.27403656
		 0.70064545 0.6994738 0.70219743 0.703655 0.53647178 0.53880322 0.52918208 0.5276891
		 0.70144457 0.70288652 0.36287966 0.37326244 0.18441197 0.1851998 0.22956972 0.23647915
		 0.73789376 0.73637927 0.73867542 0.74007636 0.29438198 0.30582932 0.73632765 0.73779762
		 0.74051458 0.73911107 0.1847187 0.18387482 0.27498621 0.27515513 0.35779476 0.35787305
		 0.3573783 0.35741693 0.67359519 0.67375124 0.69184476 0.69054765 0.69253302 0.69397217
		 0.67569393 0.67615378 0.69177777 0.69322532 0.69962955 0.70084494 0.53416562 0.53377652
		 0.52457148 0.52583933 0.69377106 0.69277418 0.69301111 0.69270188 0.69165051 0.69306785
		 0.52440631 0.53267324 0.53558737 0.53380239 0.5125742 0.51239449 0.51089764 0.17639235
		 0.093669847 0.003471534 0.0020000001 0.0021755437 0.51315486 0.51313752 0.17362878
		 0.51166302 0.0025477102 0.0026681181 0.0041598659 0.091936216 0.75356328 0.75503325
		 0.75362474 0.75215679 0.53423429 0.53339654 0.52690804 0.52812755 0.99699306 0.99665874
		 0.6819759 0.68155801 0.5331955 0.53427333 0.52609813 0.5248605 0.75211781 0.75151491
		 0.75303376 0.75451201 0.68632531 0.68743563 0.34948051 0.3497183 0.52679795 0.52498442
		 0.5344938 0.53537071 0.70941204 0.71140373 0.70881808 0.70598358 0.70940208 0.70695949
		 0.18387482 0.18395382 0.2677789 0.26767689 0.72110122 0.71966964 0.72233838 0.72518927
		 0.72363728 0.72691071 0.51812589 0.51735419 0.58636844 0.58748293 0.71346295 0.71225214
		 0.52510828 0.52616251 0.53428191 0.53289962 0.18621652 0.1871365 0.26725155 0.26666144
		 0.74850392 0.74705601 0.74319774 0.7446596 0.68791562 0.68852472 0.35099739 0.3512637
		 0.99800003 0.9979645 0.68974996 0.68941969 0.74595535 0.74751174 0.74382073 0.74235374
		 0.59182119 0.59193528 0.72141695 0.72295088 0.51452512 0.51460391 0.52495027 0.52667183
		 0.53741181 0.53274816 0.51901793 0.51899862 0.18022653 0.51749915 0.0037518679 0.0020000001
		 0.0032686633 0.094835319 0.52176195 0.52195626 0.52043599 0.17663935 0.090586737
		 0.0081251226 0.0068313004 0.0063821306 0.5234772 0.5234772 0.53744918 0.53744918
		 0.52370435 0.52370435 0.53767633 0.53767633 0.2786316 0.2786316 0.26216543 0.26216543
		 0.46050626 0.46050626 0.44404015 0.44404015 0.29260364 0.29260364 0.52393144 0.52393144
		 0.53790349 0.53790349 0.17324805 0.17324805 0.50915772 0.50915772 0.17324805 0.17324805
		 0.50915778 0.50915778 0.0020000001 0.0020000001 0.090917304 0.090917304 0.0020000001
		 0.0020000001 0.090917245 0.090917245 0.20034096 0.20034096 0.18387482 0.18387482
		 0.018466182 0.018466182 0.0020000001 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 5 "|TL_card_01_model|TL_card_01_modelShape.map[234:235]" "|TL_card_01_model|TL_card_01_modelShape.map[230:231]" "|TL_card_01_model|TL_card_01_modelShape.map[222:225]" "|TL_card_01_model|TL_card_01_modelShape.map[200:203]" "|TL_card_01_model|TL_card_01_modelShape.map[164:167]"  ;
createNode polyTweakUV -n "polyTweakUV2";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[164]" -type "float2" 0.18212445 2.4739354 ;
	setAttr ".uvtk[165]" -type "float2" 0.11309221 2.4706826 ;
	setAttr ".uvtk[166]" -type "float2" 0.069364816 -0.59124875 ;
	setAttr ".uvtk[167]" -type "float2" 0.096146941 -0.59531593 ;
	setAttr ".uvtk[200]" -type "float2" 0.10686201 -2.1855454 ;
	setAttr ".uvtk[201]" -type "float2" 0.15092322 -2.1930537 ;
	setAttr ".uvtk[202]" -type "float2" 0.11688292 -1.5215309 ;
	setAttr ".uvtk[203]" -type "float2" 0.085875392 -1.5106864 ;
	setAttr ".uvtk[222]" -type "float2" -0.098286405 2.4837332 ;
	setAttr ".uvtk[223]" -type "float2" -0.14331605 2.4833872 ;
	setAttr ".uvtk[224]" -type "float2" -0.092879802 -0.5156064 ;
	setAttr ".uvtk[225]" -type "float2" -0.067385986 -0.51881945 ;
	setAttr ".uvtk[230]" -type "float2" -0.12255132 -1.4684744 ;
	setAttr ".uvtk[231]" -type "float2" -0.087467432 -1.4673641 ;
	setAttr ".uvtk[234]" -type "float2" -0.17471874 -2.2205811 ;
	setAttr ".uvtk[235]" -type "float2" -0.12746315 -2.2198143 ;
createNode polyPlanarProj -n "polyPlanarProj2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[84]" "f[95]" "f[102]" "f[104]" "f[106]" "f[109]" "f[116]" "f[118]" "f[120]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -4.3499999046325684 8 -0.48499533534049988 ;
	setAttr ".ps" -type "double2" 0.30000019073486328 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyPlanarProj -n "polyPlanarProj3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[84]" "f[95]" "f[102]" "f[104]" "f[106]" "f[109]" "f[116]" "f[118]" "f[120]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -4.3499999046325684 8 -0.48499533534049988 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 0.030009329319000244 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV3";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[164]" -type "float2" 0.47139385 0.38493359 ;
	setAttr ".uvtk[165]" -type "float2" -0.47139478 0.38431215 ;
	setAttr ".uvtk[166]" -type "float2" -0.47139478 -0.12497842 ;
	setAttr ".uvtk[167]" -type "float2" 0.47139385 -0.12497842 ;
	setAttr ".uvtk[200]" -type "float2" -0.47139478 -0.38431215 ;
	setAttr ".uvtk[201]" -type "float2" 0.47139385 -0.38493353 ;
	setAttr ".uvtk[202]" -type "float2" 0.47139385 -0.24995685 ;
	setAttr ".uvtk[203]" -type "float2" -0.47139478 -0.24995685 ;
	setAttr ".uvtk[222]" -type "float2" -0.47139382 0.38643372 ;
	setAttr ".uvtk[223]" -type "float2" 0.47139475 0.38493359 ;
	setAttr ".uvtk[224]" -type "float2" 0.47139475 -0.12497842 ;
	setAttr ".uvtk[225]" -type "float2" -0.47139382 -0.12497842 ;
	setAttr ".uvtk[230]" -type "float2" 0.47139475 -0.24995685 ;
	setAttr ".uvtk[231]" -type "float2" -0.47139382 -0.24995685 ;
	setAttr ".uvtk[234]" -type "float2" 0.47139475 -0.38493353 ;
	setAttr ".uvtk[235]" -type "float2" -0.47139382 -0.38643369 ;
createNode polyPlanarProj -n "polyPlanarProj4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[84]" "f[95]" "f[102]" "f[104]" "f[106]" "f[109]" "f[116]" "f[118]" "f[120]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -4.3499999046325684 8 -0.48499533534049988 ;
	setAttr ".ps" -type "double2" 0.30000019073486328 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV4";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[164]" -type "float2" 0.97893679 -0.00084365904 ;
	setAttr ".uvtk[165]" -type "float2" 0.8803035 -0.00055141747 ;
	setAttr ".uvtk[166]" -type "float2" 0.8803035 0.23901936 ;
	setAttr ".uvtk[167]" -type "float2" 0.97893679 0.23901936 ;
	setAttr ".uvtk[200]" -type "float2" 0.8803035 0.36101022 ;
	setAttr ".uvtk[201]" -type "float2" 0.97893679 0.36130252 ;
	setAttr ".uvtk[202]" -type "float2" 0.97893679 0.29780936 ;
	setAttr ".uvtk[203]" -type "float2" 0.8803035 0.29780936 ;
	setAttr ".uvtk[222]" -type "float2" 0.091541648 -0.001549378 ;
	setAttr ".uvtk[223]" -type "float2" -0.0070916414 -0.00084365904 ;
	setAttr ".uvtk[224]" -type "float2" -0.0070916414 0.23901936 ;
	setAttr ".uvtk[225]" -type "float2" 0.091541648 0.23901936 ;
	setAttr ".uvtk[230]" -type "float2" -0.0070916414 0.29780936 ;
	setAttr ".uvtk[231]" -type "float2" 0.091541648 0.29780936 ;
	setAttr ".uvtk[234]" -type "float2" -0.0070916414 0.36130252 ;
	setAttr ".uvtk[235]" -type "float2" 0.091541648 0.36200818 ;
createNode polyPlanarProj -n "polyPlanarProj5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "f[28]" "f[36:37]" "f[39]" "f[45]" "f[47]" "f[60]" "f[62]" "f[64]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" -4.3499999046325684 8 -0.015004664659500122 ;
	setAttr ".ps" -type "double2" 0.30000019073486328 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV5";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[64]" -type "float2" 0.95773113 0.36050895 ;
	setAttr ".uvtk[65]" -type "float2" 0.85913396 0.36021766 ;
	setAttr ".uvtk[66]" -type "float2" 0.85913396 0.29719976 ;
	setAttr ".uvtk[67]" -type "float2" 0.95773113 0.29719976 ;
	setAttr ".uvtk[90]" -type "float2" 0.85913396 0.23857994 ;
	setAttr ".uvtk[91]" -type "float2" 0.95773113 0.23857994 ;
	setAttr ".uvtk[92]" -type "float2" 0.070661269 0.36121276 ;
	setAttr ".uvtk[93]" -type "float2" -0.027935978 0.36050895 ;
	setAttr ".uvtk[94]" -type "float2" -0.027935978 0.29719976 ;
	setAttr ".uvtk[95]" -type "float2" 0.070661269 0.29719976 ;
	setAttr ".uvtk[100]" -type "float2" -0.027935978 0.23857994 ;
	setAttr ".uvtk[101]" -type "float2" 0.070661269 0.23857994 ;
	setAttr ".uvtk[114]" -type "float2" 0.85913396 -0.00029674324 ;
	setAttr ".uvtk[115]" -type "float2" 0.95773113 -0.00058815046 ;
	setAttr ".uvtk[120]" -type "float2" -0.027935978 -0.00058815046 ;
	setAttr ".uvtk[121]" -type "float2" 0.070661269 -0.0012919621 ;
createNode polyPlanarProj -n "polyPlanarProj6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "f[87]" "f[92]" "f[98]" "f[100:101]" "f[107]" "f[117]" "f[119]" "f[121]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 4.3499999046325684 8 -0.48499533534049988 ;
	setAttr ".ps" -type "double2" 0.30000019073486328 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV6";
	setAttr ".uopa" yes;
	setAttr -s 17 ".uvtk";
	setAttr ".uvtk[176]" -type "float2" 0.0471281 0.0011590674 ;
	setAttr ".uvtk[177]" -type "float2" -0.051353507 0.00086998485 ;
	setAttr ".uvtk[178]" -type "float2" -0.051353507 0.23810661 ;
	setAttr ".uvtk[179]" -type "float2" 0.0471281 0.23810661 ;
	setAttr ".uvtk[190]" -type "float2" 0.83467752 0.35974866 ;
	setAttr ".uvtk[191]" -type "float2" 0.93315911 0.35905078 ;
	setAttr ".uvtk[192]" -type "float2" 0.93315911 0.29625282 ;
	setAttr ".uvtk[193]" -type "float2" 0.83467752 0.29625282 ;
	setAttr ".uvtk[210]" -type "float2" -0.051353507 0.35905078 ;
	setAttr ".uvtk[211]" -type "float2" 0.0471281 0.35876158 ;
	setAttr ".uvtk[212]" -type "float2" 0.0471281 0.29625282 ;
	setAttr ".uvtk[213]" -type "float2" -0.051353507 0.29625282 ;
	setAttr ".uvtk[218]" -type "float2" 0.93315911 0.00086998485 ;
	setAttr ".uvtk[219]" -type "float2" 0.83467752 0.00017201353 ;
	setAttr ".uvtk[220]" -type "float2" 0.83467752 0.23810661 ;
	setAttr ".uvtk[221]" -type "float2" 0.93315911 0.23810661 ;
createNode polyPlanarProj -n "polyPlanarProj7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 7 "f[31]" "f[41:42]" "f[44]" "f[51:52]" "f[61]" "f[63]" "f[65]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 4.3499999046325684 8 -0.015004664659500122 ;
	setAttr ".ps" -type "double2" 0.30000019073486328 15.460018157958984 ;
	setAttr ".cam" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode polyTweakUV -n "polyTweakUV7";
	setAttr ".uopa" yes;
	setAttr -s 139 ".uvtk";
	setAttr ".uvtk[32]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[33]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[34]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[35]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[36]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[37]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[38]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[39]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[40]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[41]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[42]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[43]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[44]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[45]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[46]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[47]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[48]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[49]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[50]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[51]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[52]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[53]" -type "float2" 0.49039203 0.11547458 ;
	setAttr ".uvtk[54]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[55]" -type "float2" 0.49039203 0.11547457 ;
	setAttr ".uvtk[60]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[61]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[62]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[63]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[68]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[69]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[70]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[71]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[76]" -type "float2" 0.021978647 0.35618037 ;
	setAttr ".uvtk[77]" -type "float2" -0.07641688 0.35646772 ;
	setAttr ".uvtk[78]" -type "float2" -0.07641688 0.29404968 ;
	setAttr ".uvtk[79]" -type "float2" 0.021978647 0.29404968 ;
	setAttr ".uvtk[80]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[81]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[82]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[83]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[88]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[89]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[96]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[97]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[98]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[99]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[102]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[103]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[104]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[105]" -type "float2" -0.3569214 0.25344419 ;
	setAttr ".uvtk[106]" -type "float2" 0.90723413 0.35646772 ;
	setAttr ".uvtk[107]" -type "float2" 0.80883861 0.35716149 ;
	setAttr ".uvtk[108]" -type "float2" 0.80883861 0.29404968 ;
	setAttr ".uvtk[109]" -type "float2" 0.90723413 0.29404968 ;
	setAttr ".uvtk[110]" -type "float2" 0.80883861 0.23625517 ;
	setAttr ".uvtk[111]" -type "float2" 0.90723413 0.23625517 ;
	setAttr ".uvtk[112]" -type "float2" -0.07641688 0.23625517 ;
	setAttr ".uvtk[113]" -type "float2" 0.021978647 0.23625517 ;
	setAttr ".uvtk[116]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[117]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[118]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[119]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[122]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[123]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[124]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[125]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[130]" -type "float2" -0.07641688 0.00045371801 ;
	setAttr ".uvtk[131]" -type "float2" 0.021978647 0.000741072 ;
	setAttr ".uvtk[132]" -type "float2" 0.80883861 -0.00023996085 ;
	setAttr ".uvtk[133]" -type "float2" 0.90723413 0.00045371801 ;
	setAttr ".uvtk[134]" -type "float2" -0.4708963 0.22495049 ;
	setAttr ".uvtk[135]" -type "float2" -0.4708963 0.22495043 ;
	setAttr ".uvtk[156]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[157]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[158]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[159]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[172]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[173]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[174]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[175]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[184]" -type "float2" -0.62236291 0.23110758 ;
	setAttr ".uvtk[185]" -type "float2" -0.62236291 0.23063819 ;
	setAttr ".uvtk[186]" -type "float2" -0.62236291 0.23124765 ;
	setAttr ".uvtk[187]" -type "float2" -0.62236291 0.2319157 ;
	setAttr ".uvtk[188]" -type "float2" -0.62236291 0.23110996 ;
	setAttr ".uvtk[189]" -type "float2" -0.62236291 0.23168568 ;
	setAttr ".uvtk[194]" -type "float2" -0.62236291 0.22835247 ;
	setAttr ".uvtk[195]" -type "float2" -0.62236291 0.22868989 ;
	setAttr ".uvtk[196]" -type "float2" -0.62236291 0.22806089 ;
	setAttr ".uvtk[197]" -type "float2" -0.62236291 0.22738896 ;
	setAttr ".uvtk[198]" -type "float2" -0.62236291 0.22775476 ;
	setAttr ".uvtk[199]" -type "float2" -0.62236291 0.22698323 ;
	setAttr ".uvtk[204]" -type "float2" -0.62236291 0.23015283 ;
	setAttr ".uvtk[205]" -type "float2" -0.62236291 0.23043822 ;
	setAttr ".uvtk[214]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[215]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[216]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[217]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[226]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[227]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[228]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[229]" -type "float2" -0.62086326 0.21895176 ;
	setAttr ".uvtk[232]" -type "float2" -0.62236291 0.22827815 ;
	setAttr ".uvtk[233]" -type "float2" -0.62236291 0.22791652 ;
	setAttr ".uvtk[264]" -type "float2" -0.61372215 0.61483216 ;
	setAttr ".uvtk[265]" -type "float2" -0.59929579 0.62932146 ;
	setAttr ".uvtk[266]" -type "float2" -0.61576176 0.64571583 ;
	setAttr ".uvtk[267]" -type "float2" -0.63018811 0.63122654 ;
	setAttr ".uvtk[268]" -type "float2" -0.51832801 0.4115563 ;
	setAttr ".uvtk[269]" -type "float2" -0.50375873 0.42604584 ;
	setAttr ".uvtk[270]" -type "float2" -0.52022451 0.4426024 ;
	setAttr ".uvtk[271]" -type "float2" -0.53479385 0.42811298 ;
	setAttr ".uvtk[272]" -type "float2" -0.59975022 0.60092092 ;
	setAttr ".uvtk[273]" -type "float2" -0.58532387 0.61541021 ;
	setAttr ".uvtk[278]" -type "float2" 0.022495022 0.47839466 ;
	setAttr ".uvtk[279]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[280]" -type "float2" 0.022495052 0.47839463 ;
	setAttr ".uvtk[281]" -type "float2" 0.022495022 0.47839463 ;
	setAttr ".uvtk[282]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[283]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[284]" -type "float2" 0.022495052 0.47839469 ;
	setAttr ".uvtk[285]" -type "float2" 0.022495052 0.47839469 ;
	setAttr ".uvtk[286]" -type "float2" 0.022495022 0.47839466 ;
	setAttr ".uvtk[287]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[288]" -type "float2" 0.022495052 0.47839469 ;
	setAttr ".uvtk[289]" -type "float2" 0.022495022 0.47839469 ;
	setAttr ".uvtk[290]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[291]" -type "float2" 0.022495052 0.47839466 ;
	setAttr ".uvtk[292]" -type "float2" 0.022495052 0.47839469 ;
	setAttr ".uvtk[293]" -type "float2" 0.022495052 0.47839469 ;
	setAttr ".uvtk[294]" -type "float2" -0.77848935 0.67315191 ;
	setAttr ".uvtk[295]" -type "float2" -0.76392001 0.68764126 ;
	setAttr ".uvtk[296]" -type "float2" -0.78038597 0.70419788 ;
	setAttr ".uvtk[297]" -type "float2" -0.79495531 0.68970847 ;
	setAttr ".uvtk[298]" -type "float2" -0.87388504 0.8738637 ;
	setAttr ".uvtk[299]" -type "float2" -0.85945874 0.88835299 ;
	setAttr ".uvtk[300]" -type "float2" -0.87592477 0.90474749 ;
	setAttr ".uvtk[301]" -type "float2" -0.89035112 0.89025819 ;
createNode Unfold3DUnfold -n "Unfold3DUnfold28";
	setAttr ".uvl" -type "Int32Array" 32 0 1 2 3 8 9
		 10 11 20 21 22 23 24 25 26 27 240 241
		 242 243 244 245 246 247 248 249 250 251 252 253
		 254 255 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 256 257
		 258 259 260 261 262 263 264 265 266 267 268 269
		 270 271 265 264 272 273 274 275 276 277 278 279
		 280 281 282 283 284 285 286 287 288 289 290 291
		 292 293 289 288 279 278 293 292 283 282 294 295
		 296 297 298 299 300 301 271 270 295 294 267 266
		 299 298 ;
	setAttr ".mue" -type "floatArray" 302 0.27582192 0.25966352 0.26199067 0.27819934
		 0.30625439 0.29028037 0.29017416 0.30624229 0.28094169 0.26366425 0.25486603 0.26838657
		 0.30638781 0.29019213 0.29129404 0.30718195 0.55818236 0.54228306 0.54307294 0.55914229
		 0.019610135 0.022408584 0.006259372 0.0034541213 0.022527404 0.0053435476 0.029681982
		 0.015752211 0.5587585 0.54291457 0.5421499 0.55815899 0.51723367 0.51723367 0.28144267
		 0.28144267 0.76368052 0.76368052 0.52788979 0.52788979 0.51962435 0.51962435 0.27905163
		 0.27905163 0.51723367 0.28144267 0.28144267 0.51723367 0.52549875 0.52549875 0.76607138
		 0.76607138 0.76368052 0.52788979 0.52788979 0.76368052 0.12524705 0.12675697 0.11905681
		 0.11797511 0.27948189 0.27881175 0.29455656 0.29464668 0.24361481 0.244721 0.244721
		 0.24361481 0.54552978 0.5454579 0.53159136 0.53159308 0.1045425 0.10615047 0.11193434
		 0.11048411 0.23712699 0.23837529 0.23837529 0.23712699 0.49820855 0.498431 0.51144081
		 0.51136148 0.7975086 0.79730624 0.79127502 0.79165721 0.75530535 0.75552291 0.244721
		 0.24361481 0.25356713 0.25467327 0.25467327 0.25356713 0.27819136 0.27744088 0.29469144
		 0.294595 0.25467327 0.25356713 0.54904008 0.54958522 0.53153759 0.53156126 0.22589639
		 0.22714463 0.22714463 0.22589639 0.22714463 0.22589639 0.23837529 0.23712699 0.244721
		 0.24361481 0.49650496 0.49735472 0.51147461 0.51161641 0.25467327 0.25356713 0.75528812
		 0.7552765 0.76576674 0.76597291 0.083914012 0.083317518 0.092275478 0.095373549 0.23837529
		 0.23712699 0.22714463 0.22589639 0.76652676 0.7669732 0.79579663 0.082538269 0.12570009
		 0.10025669 0.3063291 0.29026893 0.28884006 0.28871623 0.2887311 0.28986371 0.29133511
		 0.30715743 0.55813295 0.54234928 0.56059837 0.55960602 0.54207212 0.55816865 0.55960053
		 0.56018955 0.76132655 0.76135749 0.74726063 0.74723315 0.0079734139 0.0063176649
		 0.012350174 0.013861731 0.20978872 0.21086937 0.21086937 0.20978872 0.07433863 0.073367894
		 0.06481985 0.066658467 0.49480218 0.49507865 0.49934945 0.49919069 0.27040997 0.27159795
		 0.27159795 0.27040997 0.79127502 0.79235303 0.79925942 0.798132 0.76343066 0.7628004
		 0.74926567 0.74949533 0.50673741 0.50589275 0.26091045 0.25972247 0.25972247 0.26091045
		 0.49491549 0.49489421 0.5063113 0.50615174 0.74915117 0.74969476 0.21086937 0.20978872
		 0.20978872 0.21086937 0.49746168 0.49685383 0.060099624 0.061120071 0.053528789 0.051933728
		 0.27159795 0.27040997 0.27040997 0.27159795 0.49378362 0.49328345 0.4996461 0.49975815
		 0.25972247 0.26091045 0.26091045 0.25972247 0.219511 0.22059165 0.22059165 0.219511
		 0.76616234 0.76539904 0.74711931 0.74708444 0.22059165 0.219511 0.76573884 0.76514196
		 0.22059165 0.219511 0.0020000001 0.076449551 0.79182553 0.063755691 0.27578691 0.25981644
		 0.27965438 0.27727622 0.25472483 0.26805919 0.26982605 0.28250143 0.022527887 0.0063492749
		 0.0048049595 0.0020000001 0.003801638 0.014312103 0.016039485 0.02950716 0.17113036
		 0.1851498 0.1851498 0.17113036 0.15180118 0.16582063 0.16582063 0.15180118 0.2044788
		 0.19045955 0.19045955 0.2044788 0.27751791 0.27744088 0.29337233 0.29344946 0.20447889
		 0.19045964 0.13247199 0.14649138 0.14649138 0.13247199 0.77194583 0.78596532 0.78596532
		 0.77194583 0.77194583 0.78596526 0.78596526 0.77194583 0.77194583 0.78596532 0.78596532
		 0.77194583 0.77194583 0.78596526 0.78596526 0.77194583 0.52923697 0.5291599 0.54509151
		 0.54516864 0.20447886 0.19045961 0.19045956 0.20447882 ;
	setAttr ".mve" -type "floatArray" 302 0.5007835 0.50077653 0.17444225 0.17443572
		 0.49457335 0.49440429 0.17061786 0.16992155 0.092036702 0.093529157 0.0051430441
		 0.0034332995 0.0899418 0.090573452 0.0034421571 0.0036128513 0.4951207 0.49512401
		 0.16907944 0.16820067 0.17089626 0.5034458 0.50362378 0.17096563 0.090712659 0.087992996
		 0.0076815425 0.0081148399 0.08907938 0.089416973 0.0039640013 0.0040710326 0.50210583
		 0.58176494 0.58176494 0.50210583 0.5021801 0.58183932 0.58183932 0.5021801 0.50210583
		 0.58176494 0.58176494 0.50210583 0.58415568 0.58415568 0.49971509 0.49971509 0.58183932
		 0.5021801 0.5021801 0.58183932 0.58423007 0.58423007 0.49978936 0.49978936 0.50696898
		 0.50835174 0.51718551 0.51633084 0.63971251 0.63869405 0.63839877 0.63975549 0.50647163
		 0.50686723 0.59237134 0.59237134 0.63794386 0.63928324 0.64016026 0.6388008 0.50806105
		 0.50718677 0.51309353 0.51435798 0.50717199 0.50677764 0.59243762 0.59243762 0.61473
		 0.61359632 0.61623162 0.61764193 0.013673962 0.015929746 0.0066207903 0.0051762587
		 0.61550319 0.61689836 0.67190802 0.67190802 0.50551701 0.50647163 0.59237134 0.59237134
		 0.64669031 0.64522499 0.64744657 0.6488021 0.67190802 0.67190802 0.64517498 0.64659727
		 0.64922607 0.6478681 0.50677764 0.50582552 0.59243762 0.59243762 0.67175239 0.67175239
		 0.67175239 0.67175239 0.9960227 0.99641824 0.60621482 0.60495985 0.60688084 0.60827321
		 0.99641824 0.99737287 0.60615009 0.60755062 0.61374712 0.614923 0.51661479 0.51623833
		 0.50733197 0.50855869 0.99535698 0.99496257 0.99630904 0.99535698 0.60602701 0.60739827
		 0.0020000001 0.51517081 0.51816678 0.51643777 0.49600601 0.49583215 0.49438387 0.1707333
		 0.09069515 0.0034237825 0.0020000001 0.0021698473 0.49656785 0.49655107 0.16805941
		 0.4951244 0.0025299368 0.0026464376 0.0040897778 0.089017779 0.66258335 0.66400564
		 0.66264284 0.66122252 0.51669836 0.51588786 0.50960988 0.51078981 0.9970448 0.99664909
		 0.67237252 0.67237252 0.51568687 0.51672971 0.50881982 0.50762236 0.66118479 0.66060144
		 0.66207099 0.66350132 0.99516493 0.99555981 0.67156976 0.67156976 0.015904397 0.014149714
		 0.023350539 0.024198987 0.62271678 0.62420553 0.62227285 0.6201542 0.62270927 0.62088358
		 0.50544512 0.50639844 0.5921604 0.5921604 0.63145381 0.63038379 0.63237852 0.6345095
		 0.63334948 0.63579625 0.50724906 0.5068534 0.59279603 0.59279603 0.62574458 0.62483954
		 0.50769496 0.50871497 0.51657093 0.51523346 0.50639844 0.50679314 0.5921604 0.5921604
		 0.6576882 0.65628725 0.65255415 0.65396863 0.99555981 0.99651301 0.67156976 0.67156976
		 0.99800003 0.9970448 0.67237252 0.67237252 0.6552223 0.65672821 0.65315694 0.65173757
		 0.59279603 0.59279603 0.63168997 0.63283646 0.5068534 0.50589812 0.50771564 0.50937492
		 0.026173851 0.51508695 0.50224054 0.50222182 0.17444305 0.50077116 0.0036950205 0.0020000001
		 0.0032274965 0.091822781 0.50489551 0.50508368 0.50361258 0.17097226 0.087712087
		 0.0079263588 0.0066745207 0.0062399278 0.50644261 0.50644261 0.51996124 0.51996124
		 0.5066604 0.5066604 0.52017903 0.52017903 0.77387971 0.77387977 0.75794798 0.75794792
		 0.60294223 0.58892286 0.5888353 0.60285455 0.7873984 0.7873984 0.5068782 0.5068782
		 0.52039689 0.52039689 0.16769102 0.16769102 0.49270043 0.49270043 0.66161299 0.66161299
		 0.98662245 0.98662245 0.0020000001 0.0020000001 0.088031948 0.088031948 0.49592197
		 0.49592197 0.58195382 0.58195382 0.60155833 0.58753914 0.58745158 0.60147083 0.52215672
		 0.52215666 0.50622481 0.50622481 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[240:255]" "|TL_card_01_model|TL_card_01_modelShape.map[20:27]" "|TL_card_01_model|TL_card_01_modelShape.map[8:11]" "|TL_card_01_model|TL_card_01_modelShape.map[0:3]"  ;
createNode polyMapSewMove -n "polyMapSewMove1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[78]";
createNode polyMapSewMove -n "polyMapSewMove2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[226]";
createNode polyMapSewMove -n "polyMapSewMove3";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[67]";
createNode polyMapSewMove -n "polyMapSewMove4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[199]";
createNode polyMapSewMove -n "polyMapSewMove5";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[182]";
createNode polyMapSewMove -n "polyMapSewMove6";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[90]";
createNode polyMapSewMove -n "polyMapSewMove7";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[187]";
createNode polyMapSewMove -n "polyMapSewMove8";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[140]";
createNode Unfold3DUnfold -n "Unfold3DUnfold29";
	setAttr ".uvl" -type "Int32Array" 10 126 127 128 129 137 168
		 169 170 171 237 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 129 170
		 169 126 163 86 85 160 56 181 256 257 258 259
		 260 261 181 56 59 182 73 206 209 74 262 263
		 264 265 266 267 268 269 270 271 272 273 274 275
		 276 277 273 272 263 262 277 276 267 266 278 279
		 280 281 282 283 284 285 261 260 279 278 257 256
		 283 282 ;
	setAttr ".mue" -type "floatArray" 286 0.36356294 0.34222689 0.34529972 0.36670217
		 0.40173486 0.38064227 0.38050202 0.40171888 0.37032324 0.34750956 0.33589214 0.35374507
		 0.40191102 0.38052574 0.38198075 0.40295964 0.73438835 0.7133944 0.71443743 0.73565584
		 0.025252968 0.028948126 0.0076242066 0.0039200666 0.029105021 0.0064149238 0.038552146
		 0.020158846 0.73514909 0.71422827 0.71321857 0.73435748 0.683011 0.683011 0.37166524
		 0.37166524 0.35879195 0.35879195 0.04744659 0.04744659 0.68616772 0.68616772 0.36850801
		 0.36850801 0.683011 0.37166524 0.37166524 0.683011 0.044289388 0.044289388 0.36194894
		 0.36194894 0.35879195 0.04744659 0.04744659 0.35879195 0.9164266 0.91909879 0.91823173
		 0.91642678 0.27655536 0.2756705 0.29646042 0.29657942 0.8195948 0.82105547 0.82105547
		 0.8195948 0.62785316 0.62775826 0.60944843 0.6094507 0.67700535 0.67907053 0.67890042
		 0.67668033 0.85657567 0.85822392 0.85822392 0.85657567 0.034456179 0.034749903 0.051928464
		 0.051823713 0.74079645 0.73885304 0.74050879 0.7418468 0.37393475 0.37422201 0.82105547
		 0.8195948 0.83273619 0.83419675 0.83419675 0.83273619 0.27485132 0.27386034 0.29663852
		 0.29651117 0.83419675 0.83273619 0.63248825 0.6332081 0.60937744 0.60940868 0.84174639
		 0.84339464 0.84339464 0.84174639 0.84339464 0.84174639 0.85822392 0.85657567 0.82105547
		 0.8195948 0.032206707 0.033328746 0.05197309 0.052160326 0.83419675 0.83273619 0.37391201
		 0.37389666 0.3877483 0.38802052 0.6339345 0.63214892 0.63214892 0.6339345 0.85822392
		 0.85657567 0.84339464 0.84174639 0.38875183 0.38934132 0.74794286 0.6262719 0.92588967
		 0.66654754 0.4018335 0.38062716 0.37874043 0.37857693 0.37859657 0.38009208 0.38203499
		 0.40292725 0.73432308 0.71348184 0.73757851 0.73626816 0.71311587 0.73437023 0.73626095
		 0.73703867 0.35593238 0.35597321 0.33735931 0.33732301 0.72266686 0.72073758 0.72171402
		 0.7241326 0.86591178 0.86733872 0.86733872 0.86591178 0.65423179 0.65244621 0.65244621
		 0.65423179 0.004005373 0.0043704407 0.010009727 0.0098000988 0.81155533 0.813124
		 0.813124 0.81155533 0.89556479 0.89807153 0.89806807 0.89621848 0.9949522 0.99412
		 0.97624832 0.97655153 0.65600652 0.65489119 0.79901189 0.79744327 0.79744327 0.79901189
		 0.64039648 0.6403684 0.65544385 0.65523314 0.97609711 0.97681493 0.86733872 0.86591178
		 0.86591178 0.86733872 0.64375854 0.6429559 0.69543737 0.69711155 0.69766814 0.69524646
		 0.813124 0.81155533 0.81155533 0.813124 0.0026604431 0.0020000001 0.010401435 0.010549398
		 0.79744327 0.79901189 0.79901189 0.79744327 0.87874937 0.88017631 0.88017631 0.87874937
		 0.36231768 0.36130983 0.33717269 0.33712664 0.88017631 0.87874937 0.99800003 0.99721187
		 0.88017631 0.87874937 0.71085072 0.6601088 0.88806337 0.70462251 0.36351672 0.3424288
		 0.36862344 0.36548325 0.3357057 0.35331279 0.35564581 0.37238276 0.029105658 0.0077429172
		 0.0057037552 0.0020000001 0.0043789386 0.018257283 0.02053817 0.038321305 0.89806902
		 0.91642588 0.99781018 0.99791193 0.97687554 0.97677368 0.74508494 0.76359671 0.76359671
		 0.74508494 0.77126408 0.78977579 0.78977579 0.77126408 0.74508494 0.76359671 0.76359671
		 0.74508494 0.77126408 0.78977579 0.78977579 0.77126408 0.66543257 0.66553438 0.64449775
		 0.64439595 0.91641867 0.89806175 0.89806122 0.91641808 ;
	setAttr ".mve" -type "floatArray" 286 0.66060919 0.66059995 0.22969808 0.22968946
		 0.65240908 0.65218586 0.22464824 0.22372881 0.12088725 0.12285794 0.0061501726 0.0038925728
		 0.11812107 0.11895513 0.0039042686 0.0041296585 0.65313184 0.65313619 0.22261685
		 0.22145651 0.22501585 0.66412455 0.66435957 0.22510745 0.11913894 0.11554781 0.0095020849
		 0.010074223 0.1169823 0.11742807 0.0045933281 0.0047346554 0.66213834 0.76732272
		 0.76732272 0.66213834 0.67302942 0.77821392 0.77821392 0.67302942 0.66213834 0.76732272
		 0.76732272 0.66213834 0.7704795 0.7704795 0.65898156 0.65898156 0.77821392 0.67302942
		 0.67302942 0.77821392 0.7813707 0.7813707 0.66987258 0.66987258 0.35301372 0.35377562
		 0.36909556 0.36962548 0.80555075 0.80420589 0.80381602 0.8056075 0.0032605203 0.0037828777
		 0.11668516 0.11668516 0.80321532 0.80498391 0.80614197 0.80434686 0.82098597 0.82006598
		 0.83277464 0.8310824 0.0037779191 0.0032572148 0.1163653 0.1163653 0.79668713 0.79519016
		 0.79866987 0.8005321 0.83239216 0.83497965 0.82068908 0.82037574 0.79770803 0.79955029
		 0.22170785 0.22170785 0.0020000001 0.0032605203 0.11668516 0.11668516 0.81476444
		 0.81282955 0.815763 0.81755292 0.22170785 0.22170785 0.81276351 0.81464159 0.81811273
		 0.81631958 0.0032572148 0.0020000001 0.1163653 0.1163653 0.22109497 0.22109497 0.22109497
		 0.22109497 0.64967889 0.6502012 0.78544343 0.7837863 0.78632283 0.78816134 0.6502012
		 0.65146172 0.78535795 0.78720725 0.79538929 0.79694194 0.83664989 0.83612686 0.81932247
		 0.8187995 0.64839244 0.64787167 0.64964962 0.64839244 0.78519541 0.78700608 0.82059366
		 0.8277247 0.36477417 0.82432818 0.65430081 0.65407121 0.65215892 0.22480068 0.11911582
		 0.0038800063 0.0020000001 0.0022242714 0.65504265 0.65502053 0.22126998 0.65313673
		 0.0026997449 0.0028535761 0.0047594071 0.11690097 0.83597636 0.83785439 0.83605486
		 0.83417946 0.83229476 0.83125001 0.82111806 0.81960058 0.65052521 0.65000272 0.22181794
		 0.22181794 0.83612686 0.83664989 0.8187995 0.81932247 0.83412963 0.83335936 0.83529979
		 0.83718848 0.64864123 0.64916259 0.22135612 0.22135612 0.35477689 0.35312873 0.36951143
		 0.36885345 0.7996332 0.80159903 0.79904705 0.79624951 0.79962331 0.7972126 0.0020000001
		 0.003258789 0.11650154 0.11650154 0.81116986 0.80975699 0.81239086 0.81520468 0.81367296
		 0.81690377 0.0037838218 0.003261386 0.11674268 0.11674268 0.80363125 0.80243617 0.81945163
		 0.82010812 0.83284265 0.83382082 0.003258789 0.0037799657 0.11650154 0.11650154 0.82951266
		 0.82766277 0.82273346 0.82460117 0.64916259 0.65042126 0.22135612 0.22135612 0.65178657
		 0.65052521 0.22181794 0.22181794 0.82625657 0.82824504 0.82352942 0.82165521 0.11674268
		 0.11674268 0.81148171 0.81299555 0.003261386 0.0020000001 0.82688975 0.8277247 0.36596021
		 0.82299244 0.6625331 0.66250837 0.22969915 0.66059291 0.0042381575 0.0020000001 0.0036208243
		 0.12060478 0.66603881 0.66628724 0.66434479 0.22511619 0.11517689 0.0098253479 0.0081723817
		 0.0075985319 0.33160821 0.3316077 0.77278548 0.79129714 0.79141271 0.7729013 0.22078353
		 0.22078353 0.64993596 0.64993596 0.22078353 0.22078353 0.64993608 0.64993608 0.0020000001
		 0.0020000001 0.11559924 0.11559924 0.0020000001 0.0020000001 0.11559912 0.11559912
		 0.77461284 0.7931242 0.79323983 0.77472836 0.022861328 0.022861686 0.0020004367 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[237]" "|TL_card_01_model|TL_card_01_modelShape.map[168:171]" "|TL_card_01_model|TL_card_01_modelShape.map[137]" "|TL_card_01_model|TL_card_01_modelShape.map[126:129]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold30";
	setAttr ".uvl" -type "Int32Array" 10 72 73 74 75 139 206
		 207 208 209 239 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 129 170
		 169 126 163 86 85 160 56 181 256 257 258 259
		 260 261 181 56 59 182 73 206 209 74 262 263
		 264 265 266 267 268 269 270 271 272 273 274 275
		 276 277 273 272 263 262 277 276 267 266 278 279
		 280 281 282 283 284 285 261 260 279 278 257 256
		 283 282 ;
	setAttr ".mue" -type "floatArray" 286 0.36356294 0.34222689 0.34529972 0.36670217
		 0.40173486 0.38064227 0.38050202 0.40171888 0.37032324 0.34750956 0.33589214 0.35374507
		 0.40191102 0.38052574 0.38198075 0.40295964 0.73438835 0.7133944 0.71443743 0.73565584
		 0.025252968 0.028948126 0.0076242066 0.0039200666 0.029105021 0.0064149238 0.038552146
		 0.020158846 0.73514909 0.71422827 0.71321857 0.73435748 0.68301105 0.68301105 0.37166527
		 0.37166527 0.35879195 0.35879195 0.04744659 0.04744659 0.68616778 0.68616778 0.36850804
		 0.36850804 0.68301105 0.37166527 0.37166527 0.68301105 0.044289388 0.044289388 0.36194894
		 0.36194894 0.35879195 0.04744659 0.04744659 0.35879195 0.9164266 0.91909879 0.91823173
		 0.91642678 0.27655539 0.27567053 0.29646045 0.29657945 0.81959486 0.82105553 0.82105553
		 0.81959486 0.62785316 0.62775826 0.60944843 0.6094507 0.67242455 0.67421013 0.67421013
		 0.67242455 0.85657573 0.85822397 0.85822397 0.85657573 0.034456179 0.034749903 0.051928464
		 0.051823713 0.7367689 0.73482549 0.73648125 0.73781925 0.37393478 0.37422204 0.82105553
		 0.81959486 0.83273625 0.83419681 0.83419681 0.83273625 0.27485135 0.27386037 0.29663855
		 0.2965112 0.83419681 0.83273625 0.63248825 0.6332081 0.60937744 0.60940868 0.84174645
		 0.8433947 0.8433947 0.84174645 0.8433947 0.84174645 0.85822397 0.85657573 0.82105553
		 0.81959486 0.032206707 0.033328746 0.05197309 0.052160326 0.83419681 0.83273625 0.37391204
		 0.37389669 0.38774833 0.38802055 0.6339345 0.63214892 0.63214892 0.6339345 0.85822397
		 0.85657573 0.8433947 0.84174645 0.38875186 0.38934135 0.74391532 0.6262719 0.92588967
		 0.66654754 0.4018335 0.38062716 0.37874043 0.37857693 0.37859657 0.38009208 0.38203499
		 0.40292725 0.73432308 0.71348184 0.73757851 0.73626816 0.71311587 0.73437023 0.73626095
		 0.73703867 0.35593238 0.35597321 0.33735931 0.33732301 0.71863931 0.71671003 0.71768647
		 0.72010505 0.86591178 0.86733872 0.86733872 0.86591178 0.65423179 0.65244621 0.65244621
		 0.65423179 0.004005373 0.0043704407 0.010009727 0.0098000988 0.81155533 0.813124
		 0.813124 0.81155533 0.89556479 0.89807153 0.89806807 0.89621848 0.9949522 0.99412
		 0.97624832 0.97655153 0.65600652 0.65489119 0.79901189 0.79744327 0.79744327 0.79901189
		 0.64039648 0.6403684 0.65544385 0.65523314 0.97609711 0.97681493 0.86733872 0.86591178
		 0.86591178 0.86733872 0.64375854 0.6429559 0.69272178 0.69450742 0.69450742 0.69272178
		 0.813124 0.81155533 0.81155533 0.813124 0.0026604431 0.0020000001 0.010401435 0.010549398
		 0.79744327 0.79901189 0.79901189 0.79744327 0.87874937 0.88017631 0.88017631 0.87874937
		 0.36231768 0.36130983 0.33717269 0.33712664 0.88017631 0.87874937 0.99800003 0.99721187
		 0.88017631 0.87874937 0.70682317 0.6601088 0.88806337 0.70038444 0.36351672 0.3424288
		 0.36862344 0.36548325 0.3357057 0.35331279 0.35564581 0.37238276 0.029105658 0.0077429172
		 0.0057037552 0.0020000001 0.0043789386 0.018257283 0.02053817 0.038321305 0.89806902
		 0.91642588 0.64449769 0.64439595 0.66543233 0.6655342 0.74508494 0.76359671 0.76359671
		 0.74508494 0.77126414 0.78977585 0.78977585 0.77126414 0.74508494 0.76359671 0.76359671
		 0.74508494 0.77126414 0.78977585 0.78977585 0.77126414 0.97687525 0.97677344 0.99781007
		 0.99791187 0.91641867 0.89806175 0.89806122 0.91641808 ;
	setAttr ".mve" -type "floatArray" 286 0.66060919 0.66059995 0.22969808 0.22968946
		 0.65240914 0.65218592 0.22464825 0.22372882 0.12088725 0.12285794 0.0061501726 0.0038925728
		 0.11812108 0.11895514 0.0039042686 0.0041296589 0.6531319 0.65313625 0.22261687 0.22145653
		 0.22501585 0.66412455 0.66435957 0.22510745 0.11913894 0.11554781 0.0095020849 0.010074223
		 0.11698232 0.11742809 0.0045933351 0.0047346628 0.6621384 0.76732278 0.76732278 0.6621384
		 0.67302942 0.77821392 0.77821392 0.67302942 0.6621384 0.76732278 0.76732278 0.6621384
		 0.77047956 0.77047956 0.65898162 0.65898162 0.77821392 0.67302942 0.67302942 0.77821392
		 0.7813707 0.7813707 0.66987258 0.66987258 0.35301378 0.35377568 0.36909562 0.36962554
		 0.80555075 0.80420589 0.80381602 0.8056075 0.0032605201 0.0037828775 0.11668515 0.11668515
		 0.80321532 0.80498391 0.80614197 0.80434686 0.8199535 0.81943053 0.83728093 0.8367579
		 0.0037779191 0.0032572148 0.11636531 0.11636531 0.79668713 0.79519016 0.79866987
		 0.8005321 0.83237106 0.83495855 0.82066798 0.82035464 0.79770803 0.79955029 0.22170784
		 0.22170784 0.0020000001 0.0032605201 0.11668515 0.11668515 0.81476444 0.81282955
		 0.815763 0.81755292 0.22170784 0.22170784 0.81276351 0.81464159 0.81811273 0.81631958
		 0.0032572148 0.0020000001 0.11636531 0.11636531 0.22109498 0.22109498 0.22109498
		 0.22109498 0.64967889 0.6502012 0.78544343 0.7837863 0.78632283 0.78816134 0.6502012
		 0.65146172 0.78535795 0.78720725 0.79538929 0.79694194 0.83664989 0.83612686 0.81932247
		 0.8187995 0.6483925 0.64787173 0.64964968 0.6483925 0.78519541 0.78700608 0.82057256
		 0.8277247 0.36477423 0.82835573 0.65430087 0.65407127 0.65215898 0.22480069 0.11911583
		 0.0038800063 0.0020000001 0.0022242719 0.65504271 0.65502059 0.22126999 0.65313679
		 0.0026997519 0.0028535835 0.0047594146 0.11690098 0.83597642 0.83785444 0.83605492
		 0.83417952 0.83227366 0.83122891 0.82109696 0.81957948 0.65052521 0.65000272 0.22181794
		 0.22181794 0.83612686 0.83664989 0.8187995 0.81932247 0.83412969 0.83335942 0.83529985
		 0.83718854 0.64864129 0.64916265 0.22135614 0.22135614 0.35477695 0.35312879 0.36951149
		 0.36885351 0.79963338 0.8015992 0.79904723 0.79624969 0.79962349 0.79721278 0.0020000001
		 0.003258789 0.11650155 0.11650155 0.81117004 0.80975717 0.81239104 0.81520486 0.81367314
		 0.81690395 0.0037838218 0.003261386 0.11674268 0.11674268 0.80363142 0.80243635 0.81943053
		 0.8199535 0.8367579 0.83728093 0.003258789 0.0037799657 0.11650155 0.11650155 0.82951272
		 0.82766283 0.82273352 0.82460123 0.64916265 0.65042132 0.22135614 0.22135614 0.65178657
		 0.65052521 0.22181794 0.22181794 0.82625663 0.8282451 0.82352948 0.82165527 0.11674268
		 0.11674268 0.81148189 0.81299573 0.003261386 0.0020000001 0.82686865 0.8277247 0.36596027
		 0.82835573 0.6625331 0.66250837 0.22969915 0.66059291 0.0042381575 0.0020000001 0.0036208243
		 0.12060478 0.66603881 0.66628724 0.66434479 0.22511619 0.11517689 0.0098253479 0.0081723817
		 0.0075985319 0.33160827 0.33160776 0.79324007 0.77472842 0.77461284 0.79312426 0.22078355
		 0.22078355 0.64993596 0.64993596 0.22078352 0.22078352 0.64993602 0.64993602 0.0020000001
		 0.0020000001 0.11559924 0.11559924 0.0020000001 0.0020000001 0.11559911 0.11559911
		 0.79141271 0.77290136 0.77278572 0.7912972 0.022861332 0.022861689 0.0020004371 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[239]" "|TL_card_01_model|TL_card_01_modelShape.map[206:209]" "|TL_card_01_model|TL_card_01_modelShape.map[139]" "|TL_card_01_model|TL_card_01_modelShape.map[72:75]"  ;
createNode Unfold3DUnfold -n "Unfold3DUnfold31";
	setAttr ".uvl" -type "Int32Array" 10 84 85 86 87 136 160
		 161 162 163 236 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 129 170
		 169 126 163 86 85 160 56 181 256 257 258 259
		 260 261 181 56 59 182 73 206 209 74 262 263
		 264 265 266 267 268 269 270 271 272 273 274 275
		 276 277 273 272 263 262 277 276 267 266 278 279
		 280 281 282 283 284 285 261 260 279 278 257 256
		 283 282 ;
	setAttr ".mue" -type "floatArray" 286 0.36356294 0.34222689 0.34529972 0.36670217
		 0.40173486 0.38064227 0.38050202 0.40171888 0.37032324 0.34750956 0.33589214 0.35374507
		 0.40191102 0.38052574 0.38198075 0.40295964 0.73438835 0.7133944 0.71443743 0.73565584
		 0.025252968 0.028948126 0.0076242066 0.0039200666 0.029105021 0.0064149238 0.038552146
		 0.020158846 0.73514909 0.71422827 0.71321857 0.73435748 0.68301105 0.68301105 0.37166527
		 0.37166527 0.35879195 0.35879195 0.04744659 0.04744659 0.68616778 0.68616778 0.36850804
		 0.36850804 0.68301105 0.37166527 0.37166527 0.68301105 0.044289388 0.044289388 0.36194894
		 0.36194894 0.35879195 0.04744659 0.04744659 0.35879195 0.9164266 0.91909879 0.91823173
		 0.91642678 0.27655539 0.27567053 0.29646045 0.29657945 0.81959486 0.82105553 0.82105553
		 0.81959486 0.62785316 0.62775826 0.60944843 0.6094507 0.67242455 0.67421013 0.67421013
		 0.67242455 0.85657573 0.85822397 0.85822397 0.85657573 0.034456179 0.034749903 0.051928464
		 0.051823713 0.73478299 0.73299742 0.73299742 0.73478299 0.37393478 0.37422204 0.82105553
		 0.81959486 0.83273625 0.83419681 0.83419681 0.83273625 0.27485135 0.27386037 0.29663855
		 0.2965112 0.83419681 0.83273625 0.63248825 0.6332081 0.60937744 0.60940868 0.84174645
		 0.8433947 0.8433947 0.84174645 0.8433947 0.84174645 0.85822397 0.85657573 0.82105553
		 0.81959486 0.032206707 0.033328746 0.05197309 0.052160326 0.83419681 0.83273625 0.37391204
		 0.37389669 0.38774833 0.38802055 0.6339345 0.63214892 0.63214892 0.6339345 0.85822397
		 0.85657573 0.8433947 0.84174645 0.38875186 0.38934135 0.74066001 0.6262719 0.92588967
		 0.66654754 0.4018335 0.38062716 0.37874043 0.37857693 0.37859657 0.38009208 0.38203499
		 0.40292725 0.73432308 0.71348184 0.73757851 0.73626816 0.71311587 0.73437023 0.73626095
		 0.73703867 0.35593238 0.35597321 0.33735931 0.33732301 0.71448576 0.71270013 0.71270013
		 0.71448576 0.86591178 0.86733872 0.86733872 0.86591178 0.65423179 0.65244621 0.65244621
		 0.65423179 0.004005373 0.0043704407 0.010009727 0.0098000988 0.81155533 0.813124
		 0.813124 0.81155533 0.89556479 0.89807153 0.89806807 0.89621848 0.9949522 0.99412
		 0.97624832 0.97655153 0.65600652 0.65489119 0.79901189 0.79744327 0.79744327 0.79901189
		 0.64039648 0.6403684 0.65544385 0.65523314 0.97609711 0.97681493 0.86733872 0.86591178
		 0.86591178 0.86733872 0.64375854 0.6429559 0.69272178 0.69450742 0.69450742 0.69272178
		 0.813124 0.81155533 0.81155533 0.813124 0.0026604431 0.0020000001 0.010401435 0.010549398
		 0.79744327 0.79901189 0.79901189 0.79744327 0.87874937 0.88017631 0.88017631 0.87874937
		 0.36231768 0.36130983 0.33717269 0.33712664 0.88017631 0.87874937 0.99800003 0.99721187
		 0.88017631 0.87874937 0.70682317 0.6601088 0.88806337 0.70038444 0.36351672 0.3424288
		 0.36862344 0.36548325 0.3357057 0.35331279 0.35564581 0.37238276 0.029105658 0.0077429172
		 0.0057037552 0.0020000001 0.0043789386 0.018257283 0.02053817 0.038321305 0.89806902
		 0.91642588 0.99781013 0.99791187 0.97687548 0.97677362 0.74508494 0.76359677 0.76359677
		 0.74508494 0.77126414 0.78977585 0.78977585 0.77126414 0.74508494 0.76359677 0.76359677
		 0.74508494 0.77126414 0.78977585 0.78977585 0.77126414 0.66543257 0.66553438 0.64449775
		 0.64439595 0.91641867 0.89806175 0.89806122 0.91641808 ;
	setAttr ".mve" -type "floatArray" 286 0.66060919 0.66059995 0.22969808 0.22968946
		 0.65240914 0.65218592 0.22464825 0.22372882 0.12088725 0.12285794 0.0061501726 0.0038925728
		 0.11812108 0.11895514 0.0039042686 0.0041296589 0.6531319 0.65313625 0.22261687 0.22145653
		 0.22501585 0.66412455 0.66435957 0.22510745 0.11913894 0.11554781 0.0095020849 0.010074223
		 0.11698232 0.11742809 0.0045933351 0.0047346628 0.6621384 0.76732278 0.76732278 0.6621384
		 0.67302942 0.77821392 0.77821392 0.67302942 0.6621384 0.76732278 0.76732278 0.6621384
		 0.77047956 0.77047956 0.65898162 0.65898162 0.77821392 0.67302942 0.67302942 0.77821392
		 0.7813707 0.7813707 0.66987258 0.66987258 0.35301378 0.35377568 0.36909562 0.36962554
		 0.80555075 0.80420589 0.80381602 0.8056075 0.0032605198 0.0037828772 0.11668514 0.11668514
		 0.80321532 0.80498391 0.80614197 0.80434686 0.81995332 0.81943035 0.83728075 0.83675772
		 0.0037779189 0.0032572146 0.1163653 0.1163653 0.79668713 0.79519016 0.79866987 0.8005321
		 0.83697057 0.8374936 0.8196432 0.82016617 0.79770803 0.79955029 0.22170782 0.22170782
		 0.0020000001 0.0032605198 0.11668514 0.11668514 0.81476444 0.81282955 0.815763 0.81755292
		 0.22170782 0.22170782 0.81276351 0.81464159 0.81811273 0.81631958 0.0032572146 0.0020000001
		 0.1163653 0.1163653 0.22109497 0.22109497 0.22109497 0.22109497 0.64967883 0.65020114
		 0.78544343 0.7837863 0.78632283 0.78816134 0.65020114 0.65146166 0.78535795 0.78720725
		 0.79538929 0.79694194 0.83664989 0.83612686 0.81932247 0.8187995 0.6483925 0.64787173
		 0.64964962 0.6483925 0.78519541 0.78700608 0.8285684 0.8277247 0.36477423 0.82835555
		 0.65430087 0.65407127 0.65215898 0.22480069 0.11911583 0.0038800063 0.0020000001
		 0.0022242719 0.65504271 0.65502059 0.22126999 0.65313679 0.0026997519 0.0028535835
		 0.0047594146 0.11690098 0.83597642 0.83785444 0.83605492 0.83417952 0.8374936 0.83697057
		 0.82016617 0.8196432 0.65052521 0.65000272 0.22181794 0.22181794 0.83612686 0.83664989
		 0.8187995 0.81932247 0.83412969 0.83335942 0.83529985 0.83718854 0.64864129 0.64916265
		 0.22135614 0.22135614 0.35477695 0.35312879 0.36951149 0.36885351 0.7996332 0.80159903
		 0.79904705 0.79624951 0.79962337 0.79721266 0.0020000001 0.003258789 0.11650155 0.11650155
		 0.81116992 0.80975705 0.81239092 0.81520474 0.81367296 0.81690377 0.0037838218 0.003261386
		 0.11674268 0.11674268 0.80363131 0.80243623 0.81943035 0.81995332 0.83675772 0.83728075
		 0.003258789 0.0037799657 0.11650155 0.11650155 0.82951272 0.82766283 0.82273352 0.82460123
		 0.64916265 0.65042132 0.22135614 0.22135614 0.65178657 0.65052521 0.22181794 0.22181794
		 0.82625663 0.8282451 0.82352948 0.82165527 0.11674268 0.11674268 0.81148171 0.81299555
		 0.003261386 0.0020000001 0.8285684 0.8277247 0.36596027 0.82835555 0.6625331 0.66250837
		 0.22969915 0.66059291 0.0042381575 0.0020000001 0.0036208243 0.12060478 0.66603881
		 0.66628724 0.66434479 0.22511619 0.11517689 0.0098253479 0.0081723817 0.0075985319
		 0.33160827 0.33160776 0.77278554 0.7912972 0.79141277 0.77290136 0.22078356 0.22078356
		 0.64993596 0.64993596 0.22078353 0.22078353 0.64993602 0.64993602 0.0020000001 0.0020000001
		 0.11559925 0.11559925 0.0020000001 0.0020000001 0.11559912 0.11559912 0.7746129 0.79312426
		 0.79323989 0.77472842 0.022861332 0.022861689 0.0020004376 0.0020000001 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[236]" "|TL_card_01_model|TL_card_01_modelShape.map[160:163]" "|TL_card_01_model|TL_card_01_modelShape.map[136]" "|TL_card_01_model|TL_card_01_modelShape.map[84:87]"  ;
createNode polyMapCut -n "polyMapCut25";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "e[292]";
createNode polyTweakUV -n "polyTweakUV8";
	setAttr ".uopa" yes;
	setAttr -s 11 ".uvtk";
	setAttr ".uvtk[56]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[57]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[58]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[59]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[138]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[180]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[182]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[183]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[238]" -type "float2" -0.14775018 0.46921885 ;
	setAttr ".uvtk[287]" -type "float2" -0.14775018 0.46921885 ;
createNode Unfold3DUnfold -n "Unfold3DUnfold32";
	setAttr ".uvl" -type "Int32Array" 10 56 57 58 59 138 180
		 182 183 238 287 ;
	setAttr ".usn" -type "string" "map1";
	setAttr ".mdp" -type "string" "|TL_card_01_model|TL_card_01_modelShape";
	setAttr ".miee" yes;
	setAttr ".uvce" -type "Int32Array" 22 54 4 2 3 1 4
		 1 3 1 4 1 3 50 4 2 3 2 4
		 2 3 38 4 ;
	setAttr ".fpve" -type "Int32Array" 608 0 1 2 3 4 5
		 6 7 8 9 10 11 12 13 14 15 3 2
		 9 8 12 7 6 13 16 17 18 19 20 21
		 22 23 24 20 23 25 26 24 25 27 28 29
		 30 31 19 18 29 28 17 4 7 18 2 1
		 21 20 32 33 34 35 10 9 24 26 29 12
		 15 30 36 37 38 39 33 32 40 41 35 34
		 42 43 34 33 44 45 32 35 46 47 39 38
		 48 49 37 36 50 51 38 37 52 53 36 39
		 54 55 56 57 58 59 60 61 62 63 64 65
		 66 67 68 69 70 71 72 73 74 75 76 77
		 78 79 63 62 71 70 80 81 82 83 84 85
		 86 87 83 82 88 89 67 66 90 91 92 93
		 94 95 96 97 98 99 95 94 100 101 102 103
		 104 105 106 107 108 109 109 108 110 111 99 98
		 105 104 79 78 112 113 114 115 91 90 116 117
		 118 119 120 121 101 100 119 118 122 123 124 125
		 89 88 126 127 128 129 130 131 113 112 111 110
		 132 133 134 135 123 122 87 136 84 127 137 128
		 81 116 119 82 57 138 58 98 97 60 63 75
		 139 72 90 101 121 114 131 132 110 113 65 92
		 95 66 79 108 107 76 66 95 101 90 113 110
		 108 79 88 123 135 124 102 105 70 69 82 119
		 123 88 105 98 63 70 140 141 5 4 142 143
		 6 5 144 145 14 13 146 147 15 14 143 144
		 13 6 148 149 17 16 150 151 16 19 152 153
		 31 30 154 155 28 31 155 150 19 28 149 140
		 4 17 147 152 30 15 156 157 158 159 160 161
		 162 163 164 165 166 167 168 169 170 171 172 173
		 174 175 176 177 178 179 159 158 175 174 180 181
		 182 183 184 185 186 187 187 186 188 189 190 191
		 192 193 194 195 196 197 197 196 198 199 200 201
		 202 203 204 205 189 188 206 207 208 209 210 211
		 212 213 214 215 216 217 218 219 220 221 221 220
		 193 192 222 223 224 225 226 227 228 229 225 224
		 230 231 217 216 229 228 167 166 203 202 179 178
		 213 212 232 233 199 198 234 235 231 230 161 236
		 162 168 171 237 156 159 228 227 232 198 186 185
		 183 238 180 239 208 207 165 222 225 166 219 176
		 179 220 203 231 235 200 193 212 211 190 166 225
		 231 203 220 179 212 193 217 174 173 214 188 196
		 195 204 228 159 174 217 186 198 196 188 240 241
		 1 0 242 243 0 3 244 245 11 10 246 247
		 8 11 247 242 3 8 248 249 22 21 250 251
		 23 22 251 252 25 23 252 253 27 25 254 255
		 26 27 241 248 21 1 255 244 10 26 129 170
		 169 126 163 86 85 160 256 257 258 259 260 261
		 262 263 181 56 59 182 73 206 209 74 264 265
		 266 267 268 269 270 271 272 273 274 275 276 277
		 278 279 275 274 265 264 279 278 269 268 280 281
		 282 283 284 285 286 287 263 262 281 280 259 258
		 285 284 ;
	setAttr ".mue" -type "floatArray" 288 0.36356321 0.34222713 0.34529996 0.36670244
		 0.40173471 0.38064212 0.38050187 0.40171874 0.37032351 0.34750983 0.33589238 0.35374534
		 0.40191087 0.38052559 0.3819806 0.4029595 0.73438847 0.71339452 0.71443754 0.73565596
		 0.025252985 0.028948147 0.0076242108 0.003920068 0.029105041 0.0064149271 0.038552172
		 0.020158859 0.7351492 0.71422839 0.71321869 0.7343576 0.68301111 0.68301111 0.37166512
		 0.37166512 0.35879216 0.35879216 0.047446575 0.047446575 0.68616784 0.68616784 0.36850789
		 0.36850789 0.68301111 0.37166512 0.37166512 0.68301111 0.044289373 0.044289373 0.36194915
		 0.36194915 0.35879216 0.047446575 0.047446575 0.35879216 0.65244597 0.65423155 0.65423155
		 0.65244597 0.27655527 0.27567041 0.29646036 0.29657936 0.8195945 0.82105517 0.82105517
		 0.8195945 0.62785333 0.62775844 0.60944855 0.60945082 0.71269989 0.71448547 0.71448547
		 0.71269989 0.85657537 0.85822362 0.85822362 0.85657537 0.034456167 0.034749892 0.051928464
		 0.051823713 0.77505833 0.77327275 0.77327275 0.77505833 0.37393501 0.37422228 0.82105517
		 0.8195945 0.8327359 0.83419645 0.83419645 0.8327359 0.27485123 0.27386025 0.29663846
		 0.29651108 0.83419645 0.8327359 0.63248843 0.63320827 0.60937756 0.6094088 0.84174609
		 0.84339434 0.84339434 0.84174609 0.84339434 0.84174609 0.85822362 0.85657537 0.82105517
		 0.8195945 0.032206696 0.033328734 0.05197309 0.052160326 0.83419645 0.8327359 0.37391227
		 0.37389693 0.38774857 0.38802078 0.67420983 0.67242426 0.67242426 0.67420983 0.85822362
		 0.85657537 0.84339434 0.84174609 0.3887521 0.38934159 0.78093535 0.66654724 0.66010857
		 0.70682287 0.40183336 0.38062701 0.37874028 0.37857679 0.37859643 0.38009194 0.38203484
		 0.4029271 0.7343232 0.71348196 0.73757863 0.73626828 0.71311599 0.73437035 0.73626107
		 0.73703879 0.35593265 0.35597348 0.33735955 0.33732325 0.75476104 0.7529754 0.7529754
		 0.75476104 0.86591142 0.86733836 0.86733836 0.86591142 0.69450718 0.69272161 0.69272161
		 0.69450718 0.0040053744 0.0043704426 0.010009732 0.0098001044 0.81155503 0.8131237
		 0.8131237 0.81155503 0.63214868 0.63393426 0.63393426 0.63214868 0.9949522 0.99412
		 0.97624826 0.97655147 0.65600628 0.65489095 0.79901153 0.79744291 0.79744291 0.79901153
		 0.64039618 0.6403681 0.65544361 0.65523291 0.97609705 0.97681487 0.86733836 0.86591142
		 0.86591142 0.86733836 0.64375824 0.6429556 0.73299712 0.73478276 0.73478276 0.73299712
		 0.8131237 0.81155503 0.81155503 0.8131237 0.0026604435 0.0020000001 0.010401441 0.010549405
		 0.79744291 0.79901153 0.79901153 0.79744291 0.87874901 0.88017595 0.88017595 0.87874901
		 0.36231795 0.36131009 0.33717293 0.33712688 0.88017595 0.87874901 0.99800003 0.99721187
		 0.88017595 0.87874901 0.74709845 0.7003842 0.62627167 0.74065977 0.36351699 0.34242904
		 0.3686237 0.36548352 0.33570594 0.35331306 0.35564607 0.37238303 0.029105678 0.0077429214
		 0.005703758 0.0020000001 0.0043789404 0.018257294 0.020538183 0.038321331 0.88806301
		 0.90654254 0.90654504 0.88806373 0.64449745 0.64439571 0.6654321 0.66553396 0.74508464
		 0.76359653 0.76359653 0.74508464 0.77126378 0.78977555 0.78977555 0.77126378 0.74508464
		 0.76359653 0.76359653 0.74508464 0.77126378 0.78977555 0.78977555 0.77126378 0.97687525
		 0.97677344 0.99781007 0.99791187 0.888071 0.90655237 0.90655291 0.8880716 ;
	setAttr ".mve" -type "floatArray" 288 0.66060966 0.66060042 0.22969824 0.22968963
		 0.65240961 0.65218639 0.22464842 0.22372898 0.12088734 0.12285803 0.0061501754 0.0038925742
		 0.11812116 0.11895522 0.00390427 0.0041296603 0.65313238 0.65313673 0.22261703 0.22145669
		 0.22501601 0.66412503 0.66436005 0.22510761 0.11913903 0.1155479 0.0095020905 0.010074229
		 0.1169824 0.11742817 0.004593337 0.0047346647 0.66213888 0.76732332 0.76732332 0.66213888
		 0.6730299 0.77821451 0.77821451 0.6730299 0.66213888 0.76732332 0.76732332 0.66213888
		 0.7704801 0.7704801 0.6589821 0.6589821 0.77821451 0.6730299 0.6730299 0.77821451
		 0.7813713 0.7813713 0.66987306 0.66987306 0.81880015 0.81932312 0.83612758 0.83665055
		 0.80555135 0.80420649 0.80381662 0.80560809 0.0032605208 0.0037828786 0.11668523
		 0.11668523 0.80321592 0.80498451 0.80614257 0.80434746 0.82016689 0.81964391 0.83749431
		 0.83697128 0.00377792 0.0032572153 0.11636538 0.11636538 0.79668772 0.79519075 0.79867047
		 0.8005327 0.83718407 0.8377071 0.8198567 0.82037967 0.79770857 0.79955083 0.22170798
		 0.22170798 0.0020000001 0.0032605208 0.11668523 0.11668523 0.81476504 0.81283015
		 0.81576359 0.81755352 0.22170798 0.22170798 0.81276411 0.81464219 0.81811333 0.81632018
		 0.0032572153 0.0020000001 0.11636538 0.11636538 0.22109513 0.22109513 0.22109513
		 0.22109513 0.6496793 0.65020162 0.78544396 0.78378683 0.78632337 0.78816187 0.65020162
		 0.65146214 0.78535849 0.78720778 0.79538983 0.79694247 0.83728153 0.83675849 0.8199541
		 0.81943113 0.64839298 0.64787221 0.6496501 0.64839298 0.78519595 0.78700662 0.8287819
		 0.82835633 0.82772535 0.82856911 0.65430135 0.65407175 0.65215945 0.22480085 0.11911591
		 0.0038800074 0.0020000001 0.0022242719 0.65504318 0.65502107 0.22127016 0.65313727
		 0.0026997523 0.002853584 0.0047594164 0.11690106 0.83597702 0.83785504 0.83605552
		 0.83418012 0.8377071 0.83718407 0.82037967 0.8198567 0.65052569 0.65000319 0.2218181
		 0.2218181 0.83675849 0.83728153 0.81943113 0.8199541 0.83413029 0.83336002 0.83530045
		 0.83718914 0.64864177 0.64916313 0.2213563 0.2213563 0.81932312 0.81880015 0.83665055
		 0.83612758 0.79963392 0.8015998 0.79904777 0.79625022 0.79962409 0.79721338 0.0020000001
		 0.0032587899 0.11650163 0.11650163 0.81117064 0.80975777 0.81239164 0.81520545 0.81367373
		 0.81690454 0.003783823 0.0032613869 0.11674277 0.11674277 0.80363202 0.80243695 0.81964391
		 0.82016689 0.83697128 0.83749431 0.0032587899 0.0037799668 0.11650163 0.11650163
		 0.82951331 0.82766342 0.82273412 0.82460183 0.64916313 0.6504218 0.2213563 0.2213563
		 0.65178704 0.65052569 0.2218181 0.2218181 0.82625723 0.8282457 0.82353008 0.82165587
		 0.11674277 0.11674277 0.81148249 0.81299633 0.0032613869 0.0020000001 0.8287819 0.82835633
		 0.82772535 0.82856911 0.66253358 0.66250885 0.22969931 0.66059339 0.0042381589 0.0020000001
		 0.0036208255 0.12060487 0.66603929 0.66628772 0.66434526 0.22511636 0.11517698 0.0098253535
		 0.0081723863 0.0075985361 0.0021157868 0.0020000001 0.023666417 0.023666926 0.79324067
		 0.77472901 0.77461344 0.79312485 0.22078371 0.22078371 0.64993638 0.64993638 0.2207837
		 0.2207837 0.6499365 0.6499365 0.0020000001 0.0020000001 0.11559933 0.11559933 0.0020000001
		 0.0020000001 0.11559921 0.11559921 0.79141331 0.77290195 0.77278632 0.79129779 0.33450639
		 0.33450603 0.35550871 0.35550916 ;
	setAttr ".mnsl" -type "stringArray" 4 "|TL_card_01_model|TL_card_01_modelShape.map[238]" "|TL_card_01_model|TL_card_01_modelShape.map[180:183]" "|TL_card_01_model|TL_card_01_modelShape.map[138]" "|TL_card_01_model|TL_card_01_modelShape.map[56:59]"  ;
createNode polyTweakUV -n "polyTweakUV9";
	setAttr ".uopa" yes;
	setAttr -s 238 ".uvtk";
	setAttr ".uvtk[8]" -type "float2" -0.0038980162 0 ;
	setAttr ".uvtk[10]" -type "float2" 0.010985329 0 ;
	setAttr ".uvtk[11]" -type "float2" 0.012402783 0.0021261908 ;
	setAttr ".uvtk[24]" -type "float2" -0.003543651 0 ;
	setAttr ".uvtk[26]" -type "float2" -0.0095678587 0 ;
	setAttr ".uvtk[27]" -type "float2" -0.01452897 0 ;
	setAttr ".uvtk[32]" -type "float2" 0.056763113 0.0097495597 ;
	setAttr ".uvtk[33]" -type "float2" 0.056763113 0.016668169 ;
	setAttr ".uvtk[34]" -type "float2" 0.036284059 0.016668169 ;
	setAttr ".uvtk[35]" -type "float2" 0.036284059 0.0097495597 ;
	setAttr ".uvtk[36]" -type "float2" 0.0036295024 0.0019581155 ;
	setAttr ".uvtk[37]" -type "float2" 0.0036295024 0.0088767242 ;
	setAttr ".uvtk[38]" -type "float2" -0.016849544 0.0088767242 ;
	setAttr ".uvtk[39]" -type "float2" -0.016849544 0.0019581155 ;
	setAttr ".uvtk[40]" -type "float2" 0.056970775 0.0097495597 ;
	setAttr ".uvtk[41]" -type "float2" 0.056970775 0.016668169 ;
	setAttr ".uvtk[42]" -type "float2" 0.036076397 0.016668169 ;
	setAttr ".uvtk[43]" -type "float2" 0.036076397 0.0097495597 ;
	setAttr ".uvtk[44]" -type "float2" 0.056763113 0.016875772 ;
	setAttr ".uvtk[45]" -type "float2" 0.036284059 0.016875772 ;
	setAttr ".uvtk[46]" -type "float2" 0.036284059 0.0095418971 ;
	setAttr ".uvtk[47]" -type "float2" 0.056763113 0.0095418971 ;
	setAttr ".uvtk[48]" -type "float2" -0.017057203 0.0088767242 ;
	setAttr ".uvtk[49]" -type "float2" -0.017057203 0.0019581155 ;
	setAttr ".uvtk[50]" -type "float2" 0.003837165 0.0019581155 ;
	setAttr ".uvtk[51]" -type "float2" 0.003837165 0.0088767242 ;
	setAttr ".uvtk[52]" -type "float2" 0.0036295024 0.0090843868 ;
	setAttr ".uvtk[53]" -type "float2" -0.016849544 0.0090843868 ;
	setAttr ".uvtk[54]" -type "float2" -0.016849544 0.0017504529 ;
	setAttr ".uvtk[55]" -type "float2" 0.0036295024 0.0017504529 ;
	setAttr ".uvtk[56]" -type "float2" 0.12033232 0.14008091 ;
	setAttr ".uvtk[57]" -type "float2" 0.12193741 0.140551 ;
	setAttr ".uvtk[58]" -type "float2" 0.12193741 0.15565714 ;
	setAttr ".uvtk[59]" -type "float2" 0.12033232 0.1561273 ;
	setAttr ".uvtk[60]" -type "float2" -0.25859204 0.083898455 ;
	setAttr ".uvtk[61]" -type "float2" -0.25752765 0.083459049 ;
	setAttr ".uvtk[62]" -type "float2" -0.2429809 0.078874215 ;
	setAttr ".uvtk[63]" -type "float2" -0.24290133 0.080072209 ;
	setAttr ".uvtk[64]" -type "float2" 0.042617183 0.0045002596 ;
	setAttr ".uvtk[65]" -type "float2" 0.043284576 0.0047388738 ;
	setAttr ".uvtk[66]" -type "float2" 0.043284576 0.056317739 ;
	setAttr ".uvtk[67]" -type "float2" 0.042617183 0.056317739 ;
	setAttr ".uvtk[68]" -type "float2" -0.021371141 0.07847248 ;
	setAttr ".uvtk[69]" -type "float2" -0.02143456 0.079655215 ;
	setAttr ".uvtk[70]" -type "float2" -0.033678785 0.080429539 ;
	setAttr ".uvtk[71]" -type "float2" -0.033677295 0.079229161 ;
	setAttr ".uvtk[72]" -type "float2" 0.16340879 0.14130947 ;
	setAttr ".uvtk[73]" -type "float2" 0.16501388 0.14083937 ;
	setAttr ".uvtk[74]" -type "float2" 0.16501388 0.15688577 ;
	setAttr ".uvtk[75]" -type "float2" 0.16340879 0.15641561 ;
	setAttr ".uvtk[76]" -type "float2" 0.059511706 0.0047366498 ;
	setAttr ".uvtk[77]" -type "float2" 0.060264751 0.0044987667 ;
	setAttr ".uvtk[78]" -type "float2" 0.060264751 0.056171604 ;
	setAttr ".uvtk[79]" -type "float2" 0.059511706 0.056171604 ;
	setAttr ".uvtk[80]" -type "float2" -0.018529285 0.064855553 ;
	setAttr ".uvtk[81]" -type "float2" -0.01833285 0.063854493 ;
	setAttr ".uvtk[82]" -type "float2" -0.006845165 0.066181466 ;
	setAttr ".uvtk[83]" -type "float2" -0.0069152415 0.067426786 ;
	setAttr ".uvtk[84]" -type "float2" 0.21170361 0.15660688 ;
	setAttr ".uvtk[85]" -type "float2" 0.21009846 0.15707704 ;
	setAttr ".uvtk[86]" -type "float2" 0.21009846 0.14103064 ;
	setAttr ".uvtk[87]" -type "float2" 0.21170361 0.1415008 ;
	setAttr ".uvtk[88]" -type "float2" 0.20848781 0.065538205 ;
	setAttr ".uvtk[89]" -type "float2" 0.20867983 0.066770181 ;
	setAttr ".uvtk[90]" -type "float2" 0.043284576 0.10429677 ;
	setAttr ".uvtk[91]" -type "float2" 0.042617183 0.10429677 ;
	setAttr ".uvtk[92]" -type "float2" 0.048620801 0.0039243903 ;
	setAttr ".uvtk[93]" -type "float2" 0.049288016 0.0045002596 ;
	setAttr ".uvtk[94]" -type "float2" 0.049288016 0.056317739 ;
	setAttr ".uvtk[95]" -type "float2" 0.048620801 0.056317739 ;
	setAttr ".uvtk[96]" -type "float2" -0.25789148 0.084999591 ;
	setAttr ".uvtk[97]" -type "float2" -0.25643805 0.084901735 ;
	setAttr ".uvtk[98]" -type "float2" -0.24286181 0.086863324 ;
	setAttr ".uvtk[99]" -type "float2" -0.24294698 0.088060305 ;
	setAttr ".uvtk[100]" -type "float2" 0.049288016 0.10429677 ;
	setAttr ".uvtk[101]" -type "float2" 0.048620801 0.10429677 ;
	setAttr ".uvtk[102]" -type "float2" -0.018271521 0.084857568 ;
	setAttr ".uvtk[103]" -type "float2" -0.017790154 0.086113378 ;
	setAttr ".uvtk[104]" -type "float2" -0.03372629 0.088434622 ;
	setAttr ".uvtk[105]" -type "float2" -0.033705369 0.087235495 ;
	setAttr ".uvtk[106]" -type "float2" 0.052737046 0.0044987667 ;
	setAttr ".uvtk[107]" -type "float2" 0.053490024 0.0039243903 ;
	setAttr ".uvtk[108]" -type "float2" 0.053490024 0.056171604 ;
	setAttr ".uvtk[109]" -type "float2" 0.052737046 0.056171604 ;
	setAttr ".uvtk[110]" -type "float2" 0.053490024 0.10401681 ;
	setAttr ".uvtk[111]" -type "float2" 0.052737046 0.10401681 ;
	setAttr ".uvtk[112]" -type "float2" 0.060264751 0.10401681 ;
	setAttr ".uvtk[113]" -type "float2" 0.059511706 0.10401681 ;
	setAttr ".uvtk[114]" -type "float2" 0.043284576 0.29981312 ;
	setAttr ".uvtk[115]" -type "float2" 0.042617183 0.30005172 ;
	setAttr ".uvtk[116]" -type "float2" -0.019297527 0.059360713 ;
	setAttr ".uvtk[117]" -type "float2" -0.019559249 0.05806854 ;
	setAttr ".uvtk[118]" -type "float2" -0.0068153143 0.057924666 ;
	setAttr ".uvtk[119]" -type "float2" -0.0066901334 0.059154131 ;
	setAttr ".uvtk[120]" -type "float2" 0.049288016 0.30005172 ;
	setAttr ".uvtk[121]" -type "float2" 0.048620801 0.30062756 ;
	setAttr ".uvtk[122]" -type "float2" 0.20847252 0.057279445 ;
	setAttr ".uvtk[123]" -type "float2" 0.20846236 0.058516063 ;
	setAttr ".uvtk[124]" -type "float2" 0.21772522 0.06398759 ;
	setAttr ".uvtk[125]" -type "float2" 0.21790725 0.065025903 ;
	setAttr ".uvtk[126]" -type "float2" 0.13546142 0.1566945 ;
	setAttr ".uvtk[127]" -type "float2" 0.13385633 0.15622434 ;
	setAttr ".uvtk[128]" -type "float2" 0.13385633 0.1411182 ;
	setAttr ".uvtk[129]" -type "float2" 0.13546142 0.1406481 ;
	setAttr ".uvtk[130]" -type "float2" 0.060264751 0.29922548 ;
	setAttr ".uvtk[131]" -type "float2" 0.059511706 0.2989876 ;
	setAttr ".uvtk[132]" -type "float2" 0.053490024 0.29979977 ;
	setAttr ".uvtk[133]" -type "float2" 0.052737046 0.29922548 ;
	setAttr ".uvtk[134]" -type "float2" 0.21839628 0.057170726 ;
	setAttr ".uvtk[135]" -type "float2" 0.21879044 0.058381595 ;
	setAttr ".uvtk[136]" -type "float2" 0.21698667 0.14905384 ;
	setAttr ".uvtk[137]" -type "float2" 0.12857321 0.1486713 ;
	setAttr ".uvtk[138]" -type "float2" 0.12722047 0.1481041 ;
	setAttr ".uvtk[139]" -type "float2" 0.15812576 0.14886257 ;
	setAttr ".uvtk[156]" -type "float2" 0.25010717 -0.014336533 ;
	setAttr ".uvtk[157]" -type "float2" 0.25013447 -0.013080663 ;
	setAttr ".uvtk[158]" -type "float2" 0.23768692 -0.014284021 ;
	setAttr ".uvtk[159]" -type "float2" 0.2376626 -0.015538162 ;
	setAttr ".uvtk[160]" -type "float2" 0.19345756 0.15707704 ;
	setAttr ".uvtk[161]" -type "float2" 0.19185244 0.15660688 ;
	setAttr ".uvtk[162]" -type "float2" 0.19185244 0.1415008 ;
	setAttr ".uvtk[163]" -type "float2" 0.19345756 0.14103064 ;
	setAttr ".uvtk[164]" -type "float2" 0.063776836 0.30019984 ;
	setAttr ".uvtk[165]" -type "float2" 0.064428732 0.29996106 ;
	setAttr ".uvtk[166]" -type "float2" 0.064428732 0.10434711 ;
	setAttr ".uvtk[167]" -type "float2" 0.063776836 0.10434711 ;
	setAttr ".uvtk[168]" -type "float2" 0.1537075 0.15622434 ;
	setAttr ".uvtk[169]" -type "float2" 0.15210241 0.1566945 ;
	setAttr ".uvtk[170]" -type "float2" 0.15210241 0.1406481 ;
	setAttr ".uvtk[171]" -type "float2" 0.1537075 0.1411182 ;
	setAttr ".uvtk[172]" -type "float2" 0.013937562 -0.013179368 ;
	setAttr ".uvtk[173]" -type "float2" 0.015009724 -0.016086584 ;
	setAttr ".uvtk[174]" -type "float2" 0.018780846 -0.014788991 ;
	setAttr ".uvtk[175]" -type "float2" 0.018640654 -0.013525969 ;
	setAttr ".uvtk[176]" -type "float2" 0.038944405 0.29933915 ;
	setAttr ".uvtk[177]" -type "float2" 0.039661031 0.29957733 ;
	setAttr ".uvtk[178]" -type "float2" 0.039661031 0.10413615 ;
	setAttr ".uvtk[179]" -type "float2" 0.038944405 0.10413615 ;
	setAttr ".uvtk[180]" -type "float2" 0.10208626 0.140551 ;
	setAttr ".uvtk[181]" -type "float2" 0.10369141 0.14008091 ;
	setAttr ".uvtk[182]" -type "float2" 0.10369141 0.1561273 ;
	setAttr ".uvtk[183]" -type "float2" 0.10208626 0.15565714 ;
	setAttr ".uvtk[184]" -type "float2" -0.39757663 0.12233403 ;
	setAttr ".uvtk[185]" -type "float2" -0.39813316 0.12364867 ;
	setAttr ".uvtk[186]" -type "float2" -0.41008437 0.12194207 ;
	setAttr ".uvtk[187]" -type "float2" -0.40988165 0.12007132 ;
	setAttr ".uvtk[188]" -type "float2" -0.6242373 0.12232748 ;
	setAttr ".uvtk[189]" -type "float2" -0.62498313 0.12071541 ;
	setAttr ".uvtk[190]" -type "float2" 0.033213954 0.0039243903 ;
	setAttr ".uvtk[191]" -type "float2" 0.032497387 0.0044995164 ;
	setAttr ".uvtk[192]" -type "float2" 0.032497387 0.056233838 ;
	setAttr ".uvtk[193]" -type "float2" 0.033213954 0.056233838 ;
	setAttr ".uvtk[194]" -type "float2" -0.63474566 0.12869343 ;
	setAttr ".uvtk[195]" -type "float2" -0.63469487 0.12910411 ;
	setAttr ".uvtk[196]" -type "float2" -0.62461358 0.13086544 ;
	setAttr ".uvtk[197]" -type "float2" -0.62475449 0.1327471 ;
	setAttr ".uvtk[198]" -type "float2" -0.41018552 0.13172279 ;
	setAttr ".uvtk[199]" -type "float2" -0.40970552 0.13388334 ;
	setAttr ".uvtk[200]" -type "float2" 0.064428732 0.0047393302 ;
	setAttr ".uvtk[201]" -type "float2" 0.063776836 0.004500675 ;
	setAttr ".uvtk[202]" -type "float2" 0.063776836 0.05634401 ;
	setAttr ".uvtk[203]" -type "float2" 0.064428732 0.05634401 ;
	setAttr ".uvtk[204]" -type "float2" -0.63491201 0.12528364 ;
	setAttr ".uvtk[205]" -type "float2" -0.63370067 0.12476058 ;
	setAttr ".uvtk[206]" -type "float2" 0.18165472 0.14083937 ;
	setAttr ".uvtk[207]" -type "float2" 0.18325987 0.14130947 ;
	setAttr ".uvtk[208]" -type "float2" 0.18325987 0.15641561 ;
	setAttr ".uvtk[209]" -type "float2" 0.18165472 0.15688577 ;
	setAttr ".uvtk[210]" -type "float2" 0.039661031 0.0044995164 ;
	setAttr ".uvtk[211]" -type "float2" 0.038944405 0.0047376128 ;
	setAttr ".uvtk[212]" -type "float2" 0.038944405 0.056233838 ;
	setAttr ".uvtk[213]" -type "float2" 0.039661031 0.056233838 ;
	setAttr ".uvtk[214]" -type "float2" 0.015338285 -0.032919601 ;
	setAttr ".uvtk[215]" -type "float2" 0.017012747 -0.03535286 ;
	setAttr ".uvtk[216]" -type "float2" 0.019042792 -0.023192359 ;
	setAttr ".uvtk[217]" -type "float2" 0.019141741 -0.021943396 ;
	setAttr ".uvtk[218]" -type "float2" 0.032497387 0.29957733 ;
	setAttr ".uvtk[219]" -type "float2" 0.033213954 0.30015233 ;
	setAttr ".uvtk[220]" -type "float2" 0.033213954 0.10413615 ;
	setAttr ".uvtk[221]" -type "float2" 0.032497387 0.10413615 ;
	setAttr ".uvtk[222]" -type "float2" 0.069641635 0.30077615 ;
	setAttr ".uvtk[223]" -type "float2" 0.070293531 0.30019984 ;
	setAttr ".uvtk[224]" -type "float2" 0.070293531 0.10434711 ;
	setAttr ".uvtk[225]" -type "float2" 0.069641635 0.10434711 ;
	setAttr ".uvtk[226]" -type "float2" 0.25437719 -0.020836359 ;
	setAttr ".uvtk[227]" -type "float2" 0.25370324 -0.019506639 ;
	setAttr ".uvtk[228]" -type "float2" 0.23756211 -0.02266009 ;
	setAttr ".uvtk[229]" -type "float2" 0.23753135 -0.023913397 ;
	setAttr ".uvtk[230]" -type "float2" 0.070293531 0.05634401 ;
	setAttr ".uvtk[231]" -type "float2" 0.069641635 0.05634401 ;
	setAttr ".uvtk[232]" -type "float2" -0.39553851 0.13025746 ;
	setAttr ".uvtk[233]" -type "float2" -0.39606559 0.1312698 ;
	setAttr ".uvtk[234]" -type "float2" 0.070293531 0.004500675 ;
	setAttr ".uvtk[235]" -type "float2" 0.069641635 0.0039243903 ;
	setAttr ".uvtk[236]" -type "float2" 0.18656944 0.14905384 ;
	setAttr ".uvtk[237]" -type "float2" 0.15899056 0.1486713 ;
	setAttr ".uvtk[238]" -type "float2" 0.096803203 0.1481041 ;
	setAttr ".uvtk[239]" -type "float2" 0.18854299 0.14886257 ;
	setAttr ".uvtk[244]" -type "float2" 0.010985329 0 ;
	setAttr ".uvtk[245]" -type "float2" 0.012402783 0.0021261913 ;
	setAttr ".uvtk[246]" -type "float2" 0.012402783 0.0021261908 ;
	setAttr ".uvtk[247]" -type "float2" -0.0038980162 0 ;
	setAttr ".uvtk[253]" -type "float2" -0.01452897 0 ;
	setAttr ".uvtk[254]" -type "float2" -0.01452897 0 ;
	setAttr ".uvtk[255]" -type "float2" -0.0095678587 0 ;
	setAttr ".uvtk[256]" -type "float2" 0.073896691 0.0039772848 ;
	setAttr ".uvtk[257]" -type "float2" 0.082338974 0.0039243903 ;
	setAttr ".uvtk[258]" -type "float2" 0.082340106 0.013822597 ;
	setAttr ".uvtk[259]" -type "float2" 0.073897049 0.013822835 ;
	setAttr ".uvtk[260]" -type "float2" -0.63378382 0.2013212 ;
	setAttr ".uvtk[261]" -type "float2" -0.63385189 0.18894203 ;
	setAttr ".uvtk[262]" -type "float2" -0.61978436 0.18886466 ;
	setAttr ".uvtk[263]" -type "float2" -0.61971623 0.20124365 ;
	setAttr ".uvtk[264]" -type "float2" 0.0085777501 0.10387458 ;
	setAttr ".uvtk[265]" -type "float2" 0.017034877 0.10387458 ;
	setAttr ".uvtk[266]" -type "float2" 0.017034877 0.2999306 ;
	setAttr ".uvtk[267]" -type "float2" 0.0085777501 0.2999306 ;
	setAttr ".uvtk[268]" -type "float2" 0.020537604 0.10387453 ;
	setAttr ".uvtk[269]" -type "float2" 0.028994601 0.10387453 ;
	setAttr ".uvtk[270]" -type "float2" 0.028994601 0.2999306 ;
	setAttr ".uvtk[271]" -type "float2" 0.020537604 0.2999306 ;
	setAttr ".uvtk[272]" -type "float2" 0.0085777501 0.0039243903 ;
	setAttr ".uvtk[273]" -type "float2" 0.017034877 0.0039243903 ;
	setAttr ".uvtk[274]" -type "float2" 0.017034877 0.05582165 ;
	setAttr ".uvtk[275]" -type "float2" 0.0085777501 0.05582165 ;
	setAttr ".uvtk[276]" -type "float2" 0.020537604 0.0039243903 ;
	setAttr ".uvtk[277]" -type "float2" 0.028994601 0.0039243903 ;
	setAttr ".uvtk[278]" -type "float2" 0.028994601 0.05582156 ;
	setAttr ".uvtk[279]" -type "float2" 0.020537604 0.05582156 ;
	setAttr ".uvtk[280]" -type "float2" -0.41151541 0.20009919 ;
	setAttr ".uvtk[281]" -type "float2" -0.41158354 0.18772013 ;
	setAttr ".uvtk[282]" -type "float2" -0.39751589 0.18764283 ;
	setAttr ".uvtk[283]" -type "float2" -0.39744776 0.20002194 ;
	setAttr ".uvtk[284]" -type "float2" 0.073900387 0.15582809 ;
	setAttr ".uvtk[285]" -type "float2" 0.082343385 0.15582809 ;
	setAttr ".uvtk[286]" -type "float2" 0.082343742 0.16542298 ;
	setAttr ".uvtk[287]" -type "float2" 0.073900625 0.16542313 ;
createNode polyFlipUV -n "polyFlipUV1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 8 "f[87]" "f[92]" "f[98]" "f[100:101]" "f[107]" "f[117]" "f[119]" "f[121]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
createNode polyFlipUV -n "polyFlipUV2";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 9 "f[84]" "f[95]" "f[102]" "f[104]" "f[106]" "f[109]" "f[116]" "f[118]" "f[120]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
createNode polyBevel2 -n "polyBevel3";
	setAttr ".cch" yes;
	setAttr ".ics" -type "componentList" 18 "e[64]" "e[66]" "e[74]" "e[79]" "e[86]" "e[91]" "e[135]" "e[139]" "e[176]" "e[181]" "e[186]" "e[188]" "e[198]" "e[200]" "e[221]" "e[225]" "e[288:292]" "e[295:297]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0.5 4 -0.5 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".f" 0.4;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyTweakUV -n "polyTweakUV10";
	setAttr ".uopa" yes;
	setAttr -s 18 ".uvtk";
	setAttr ".uvtk[155]" -type "float2" -0.00079289614 0.0012346506 ;
	setAttr ".uvtk[156]" -type "float2" -0.00079289614 0 ;
	setAttr ".uvtk[229]" -type "float2" 0 -0.0027898098 ;
	setAttr ".uvtk[230]" -type "float2" 0 -0.0027898098 ;
	setAttr ".uvtk[234]" -type "float2" -0.00014413845 0.003027404 ;
	setAttr ".uvtk[236]" -type "float2" -0.00014413845 0.003027404 ;
	setAttr ".uvtk[244]" -type "float2" 0.0024321557 0 ;
	setAttr ".uvtk[254]" -type "float2" -0.0025106336 0 ;
	setAttr ".uvtk[263]" -type "float2" 0.002824422 0 ;
	setAttr ".uvtk[269]" -type "float2" 0.0027852335 0 ;
	setAttr ".uvtk[271]" -type "float2" -0.0025890726 0 ;
	setAttr ".uvtk[273]" -type "float2" -0.0022752637 0 ;
	setAttr ".uvtk[274]" -type "float2" 0 -0.00044313437 ;
	setAttr ".uvtk[275]" -type "float2" 0.0028421471 -0.00044313437 ;
	setAttr ".uvtk[277]" -type "float2" 0.0025157381 0 ;
	setAttr ".uvtk[281]" -type "float2" -0.0028244462 0 ;
	setAttr ".uvtk[357]" -type "float2" -0.00079289614 -0.0012346506 ;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderQuality;
	setAttr ".eaa" 0;
	setAttr ".ert" yes;
select -ne :defaultResolution;
	setAttr ".w" 1920;
	setAttr ".h" 1080;
	setAttr ".pa" 1;
	setAttr ".dar" 1.7777777910232544;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "polyTweakUV10.out" "TL_card_01_modelShape.i";
connectAttr "polyTweakUV10.uvtk[0]" "TL_card_01_modelShape.uvst[0].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polySurfaceShape1.o" "polyMergeVert1.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert1.mp";
connectAttr "polyTweak1.out" "polyMergeVert2.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert2.mp";
connectAttr "polyMergeVert1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyMergeVert3.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert3.mp";
connectAttr "polyMergeVert2.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polyMergeVert4.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert4.mp";
connectAttr "polyMergeVert3.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyMergeVert5.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert5.mp";
connectAttr "polyMergeVert4.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyMergeVert6.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert6.mp";
connectAttr "polyMergeVert5.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyMergeVert7.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert7.mp";
connectAttr "polyMergeVert6.out" "polyTweak6.ip";
connectAttr "polyTweak7.out" "polyMergeVert8.ip";
connectAttr "TL_card_01_modelShape.wm" "polyMergeVert8.mp";
connectAttr "polyMergeVert7.out" "polyTweak7.ip";
connectAttr "polyMergeVert8.out" "polySplitRing1.ip";
connectAttr "TL_card_01_modelShape.wm" "polySplitRing1.mp";
connectAttr "polyTweak8.out" "polySplitRing2.ip";
connectAttr "TL_card_01_modelShape.wm" "polySplitRing2.mp";
connectAttr "polySplitRing1.out" "polyTweak8.ip";
connectAttr "polyTweak9.out" "polyExtrudeFace1.ip";
connectAttr "TL_card_01_modelShape.wm" "polyExtrudeFace1.mp";
connectAttr "polySplitRing2.out" "polyTweak9.ip";
connectAttr "polyExtrudeFace1.out" "polyBevel1.ip";
connectAttr "TL_card_01_modelShape.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyBevel2.ip";
connectAttr "TL_card_01_modelShape.wm" "polyBevel2.mp";
connectAttr "TL_card_01_material.oc" "blinn1SG.ss";
connectAttr "TL_card_01_modelShape.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo1.sg";
connectAttr "TL_card_01_material.msg" "materialInfo1.m";
connectAttr "polyBevel2.out" "polyPlanarProj1.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj1.mp";
connectAttr "polyPlanarProj1.out" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyMapCut2.ip";
connectAttr "polyMapCut2.out" "polyMapCut3.ip";
connectAttr "polyMapCut3.out" "polyMapCut4.ip";
connectAttr "polyMapCut4.out" "polyMapCut5.ip";
connectAttr "polyMapCut5.out" "polyMapCut6.ip";
connectAttr "polyMapCut6.out" "polyMapCut7.ip";
connectAttr "polyMapCut7.out" "polyMapCut8.ip";
connectAttr "polyMapCut8.out" "polyMapCut9.ip";
connectAttr "polyMapCut9.out" "polyMapCut10.ip";
connectAttr "polyMapCut10.out" "polyMapCut11.ip";
connectAttr "polyMapCut11.out" "polyMapCut12.ip";
connectAttr "polyMapCut12.out" "polyMapCut13.ip";
connectAttr "polyMapCut13.out" "polyMapCut14.ip";
connectAttr "polyMapCut14.out" "polyMapCut15.ip";
connectAttr "polyMapCut15.out" "polyMapCut16.ip";
connectAttr "polyMapCut16.out" "Unfold3DUnfold1.im";
connectAttr "Unfold3DUnfold1.om" "Unfold3DUnfold2.im";
connectAttr "Unfold3DUnfold2.om" "Unfold3DUnfold3.im";
connectAttr "Unfold3DUnfold3.om" "Unfold3DUnfold4.im";
connectAttr "Unfold3DUnfold4.om" "Unfold3DUnfold5.im";
connectAttr "Unfold3DUnfold5.om" "Unfold3DUnfold6.im";
connectAttr "Unfold3DUnfold6.om" "Unfold3DUnfold7.im";
connectAttr "Unfold3DUnfold7.om" "Unfold3DUnfold8.im";
connectAttr "Unfold3DUnfold8.om" "Unfold3DUnfold9.im";
connectAttr "Unfold3DUnfold9.om" "Unfold3DUnfold10.im";
connectAttr "Unfold3DUnfold10.om" "Unfold3DUnfold11.im";
connectAttr "Unfold3DUnfold11.om" "Unfold3DUnfold12.im";
connectAttr "Unfold3DUnfold12.om" "Unfold3DUnfold13.im";
connectAttr "Unfold3DUnfold13.om" "Unfold3DUnfold14.im";
connectAttr "Unfold3DUnfold14.om" "Unfold3DUnfold15.im";
connectAttr "Unfold3DUnfold15.om" "polyMapCut17.ip";
connectAttr "polyMapCut17.out" "polyMapCut18.ip";
connectAttr "polyMapCut18.out" "Unfold3DUnfold16.im";
connectAttr "Unfold3DUnfold16.om" "Unfold3DUnfold17.im";
connectAttr "Unfold3DUnfold17.om" "Unfold3DUnfold18.im";
connectAttr "Unfold3DUnfold18.om" "Unfold3DUnfold19.im";
connectAttr "Unfold3DUnfold19.om" "Unfold3DUnfold20.im";
connectAttr "Unfold3DUnfold20.om" "polyMapCut19.ip";
connectAttr "polyMapCut19.out" "Unfold3DUnfold21.im";
connectAttr "Unfold3DUnfold21.om" "Unfold3DUnfold22.im";
connectAttr "Unfold3DUnfold22.om" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapCut20.ip";
connectAttr "polyMapCut20.out" "polyMapCut21.ip";
connectAttr "polyMapCut21.out" "polyMapCut22.ip";
connectAttr "polyMapCut22.out" "polyMapCut23.ip";
connectAttr "polyMapCut23.out" "polyMapCut24.ip";
connectAttr "polyMapCut24.out" "Unfold3DUnfold23.im";
connectAttr "Unfold3DUnfold23.om" "Unfold3DUnfold24.im";
connectAttr "Unfold3DUnfold24.om" "Unfold3DUnfold25.im";
connectAttr "Unfold3DUnfold25.om" "Unfold3DUnfold26.im";
connectAttr "Unfold3DUnfold26.om" "Unfold3DUnfold27.im";
connectAttr "Unfold3DUnfold27.om" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyPlanarProj2.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj2.mp";
connectAttr "polyPlanarProj2.out" "polyPlanarProj3.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj3.mp";
connectAttr "polyPlanarProj3.out" "polyTweakUV3.ip";
connectAttr "polyTweakUV3.out" "polyPlanarProj4.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj4.mp";
connectAttr "polyPlanarProj4.out" "polyTweakUV4.ip";
connectAttr "polyTweakUV4.out" "polyPlanarProj5.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj5.mp";
connectAttr "polyPlanarProj5.out" "polyTweakUV5.ip";
connectAttr "polyTweakUV5.out" "polyPlanarProj6.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj6.mp";
connectAttr "polyPlanarProj6.out" "polyTweakUV6.ip";
connectAttr "polyTweakUV6.out" "polyPlanarProj7.ip";
connectAttr "TL_card_01_modelShape.wm" "polyPlanarProj7.mp";
connectAttr "polyPlanarProj7.out" "polyTweakUV7.ip";
connectAttr "polyTweakUV7.out" "Unfold3DUnfold28.im";
connectAttr "Unfold3DUnfold28.om" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyMapSewMove2.ip";
connectAttr "polyMapSewMove2.out" "polyMapSewMove3.ip";
connectAttr "polyMapSewMove3.out" "polyMapSewMove4.ip";
connectAttr "polyMapSewMove4.out" "polyMapSewMove5.ip";
connectAttr "polyMapSewMove5.out" "polyMapSewMove6.ip";
connectAttr "polyMapSewMove6.out" "polyMapSewMove7.ip";
connectAttr "polyMapSewMove7.out" "polyMapSewMove8.ip";
connectAttr "polyMapSewMove8.out" "Unfold3DUnfold29.im";
connectAttr "Unfold3DUnfold29.om" "Unfold3DUnfold30.im";
connectAttr "Unfold3DUnfold30.om" "Unfold3DUnfold31.im";
connectAttr "Unfold3DUnfold31.om" "polyMapCut25.ip";
connectAttr "polyMapCut25.out" "polyTweakUV8.ip";
connectAttr "polyTweakUV8.out" "Unfold3DUnfold32.im";
connectAttr "Unfold3DUnfold32.om" "polyTweakUV9.ip";
connectAttr "polyTweakUV9.out" "polyFlipUV1.ip";
connectAttr "TL_card_01_modelShape.wm" "polyFlipUV1.mp";
connectAttr "polyFlipUV1.out" "polyFlipUV2.ip";
connectAttr "TL_card_01_modelShape.wm" "polyFlipUV2.mp";
connectAttr "polyFlipUV2.out" "polyBevel3.ip";
connectAttr "TL_card_01_modelShape.wm" "polyBevel3.mp";
connectAttr "polyBevel3.out" "polyTweakUV10.ip";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "TL_card_01_material.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of TL_card_02_model.ma
